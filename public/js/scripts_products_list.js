function productListSortChange() {
    var sortOption = $("#sort_option").val();
    if (sortOption === 'date_desc') {
        window.location.href = "/products/list/category/all/sortby/date/sorttype/desc";
    } else if (sortOption === 'date_asc') {
        window.location.href = "/products/list/category/all/sortby/date/sorttype/asc";
    } else if (sortOption === 'price_desc') {
        window.location.href = "/products/list/category/all/sortby/price/sorttype/desc";
    } else if (sortOption === 'price_asc') {
        window.location.href = "/products/list/category/all/sortby/price/sorttype/asc";
    }
}

function myProductListSortChange() {
    var sortOption = $("#sort_option").val();
    if (sortOption === 'date_desc') {
        window.location.href = "/profile/products/sortby/date/sorttype/desc";
    } else if (sortOption === 'date_asc') {
        window.location.href = "/profile/products/sortby/date/sorttype/asc";
    } else if (sortOption === 'price_desc') {
        window.location.href = "/profile/products/sortby/price/sorttype/desc";
    } else if (sortOption === 'price_asc') {
        window.location.href = "/profile/products/sortby/price/sorttype/asc";
    }
}

function addProductClick() {
    showAddProductModal('signedin');
}

