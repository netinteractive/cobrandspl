var selectedBrandBranches = new Array();

function showAddBrandModal(add_brand_type) {
    tinymce.init({
        selector: "#brand_description",
        toolbar: "link | image",
        language: "pl",
        menubar: false,
        statusbar: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality template textcolor paste textcolor"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
                relative_urls: false,
    });
    $('#brand_image_upload').fileupload({
        dataType: 'json',
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                document.getElementById("brand_image_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                goUpload = false;
            }
            if (uploadFile.size > 2000000) { // 2mb
                document.getElementById("brand_image_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }
        },
        done: function(e, data) {
            if (data.result.error) {
                document.getElementById("brand_image_upload_error").innerHTML = data.result.error;
            } else if (data.result.success) {
                document.getElementById("brand_image_placeholder").innerHTML = '<img src=\'' + data.result.brand_image_url + '\' />';
                document.getElementById("brand_image_id").value = data.result.brand_image_id;
            } else {
                document.getElementById("brand_image_upload_error").innerHTML = 'Wystąpił nieznany błąd';
            }
        }
    });
    $("#addBrandModal").modal();
    if (add_brand_type === 'register') {
        $("#brand_add_type").val('register');
    } else if (add_brand_type === 'signedin') {
        $("#brand_add_type").val('signedin');
    }
    if ($("#avatar_id").val() != '') {
        $("#copyFromLogoButton").prop('disabled', false);
    } else {
        $("#copyFromLogoButton").prop('disabled', true);
    }

    selectedBrandBranches = new Array();
    $("#selected_branches_box").html('');
    $("#register_branches").val('');
    $("#registration_branch_selector").val($("#registration_branch_selector option:first").val());
    clearAddBrandErrors();
}

function showEditBrandModal(brand_id) {
    tinymce.init({
        selector: "#brand_description",
        toolbar: "link | image",
        language: "pl",
        menubar: false,
        statusbar: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality template textcolor paste textcolor"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
                relative_urls: false,
    });
    $('#brand_image_upload').fileupload({
        dataType: 'json',
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                document.getElementById("brand_image_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                goUpload = false;
            }
            if (uploadFile.size > 2000000) { // 2mb
                document.getElementById("brand_image_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }
        },
        done: function(e, data) {
            if (data.result.error) {
                document.getElementById("brand_image_upload_error").innerHTML = data.result.error;
            } else if (data.result.success) {
                document.getElementById("brand_image_placeholder").innerHTML = '<img src=\'' + data.result.brand_image_url + '\' />';
                document.getElementById("brand_image_id").value = data.result.brand_image_id;
            } else {
                document.getElementById("brand_image_upload_error").innerHTML = 'Wystąpił nieznany błąd';
            }
        }
    });
    $.get("/brand/" + brand_id, function(data) {
        $("#brand_id").val(data.brand_id);
        $("#brand_image_id").val(data.logo_id);
        $("#brand_image_placeholder").html("<img src='" + data.logo_url + "' />");
        $("#brand_name").val(data.name);
        selectedBrandBranches = new Array();
        $("#selected_branches_box").html('');
        $("#register_branches").val('');
        $("#registration_branch_selector").val($("#registration_branch_selector option:first").val());
        clearAddBrandErrors();
        tinyMCE.get('brand_description').setContent(data.description);
        for (var branch_id = 0; branch_id < data.branch_ids.length; branch_id++) {
            var value = data.branch_ids[branch_id].toString();
            var name = data.branch_names[branch_id];
            selectedBrandBranches.push(value);
            document.getElementById("brand_branches").value = JSON.stringify(selectedBrandBranches)
            var divBranches = document.getElementById("selected_branches_box");
            divBranches.innerHTML = divBranches.innerHTML + "<span style='display:block' id='branch" + value + "'>" + name + "<input type='button' onclick=\"removeBrandBranch('" + value + "')\" value='usuń'></span>";
        }
    });
    $("#editBrandModal").modal();
}

function addBrand() {
    clearAddBrandErrors();
    var addBrandType = $("#brand_add_type").val();
    var imageId = $("#brand_image_id").val();
    var brandName = $("#brand_name").val();
    var brandBranches = $("#brand_branches").val();
    var description = tinyMCE.get('brand_description').getContent();

    var validationSuccess = true;

    if (imageId === '') {
        $("#brand_image_error").html('Należy dodać logo brandu');
        validationSuccess = false;
    }

    if (brandName === '') {
        $("#brand_name_error").html('Należy podać nazwę brandu');
        validationSuccess = false;
    }

    if (description === '') {
        $("#brand_description_error").html('Należy uzupełnić opis brandu');
        validationSuccess = false;
    }

    if (selectedBrandBranches.length === 0) {
        $("#brand_branches_error").html('Należy wybrać przynajmniej jedną branżę.');
        validationSuccess = false;
    }

    if (!validationSuccess) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/brand/dodaj',
        data: {
            image_id: imageId,
            add_type: addBrandType,
            brand_name: brandName,
            brand_branches: brandBranches,
            description: description

        },
        success: function(msg) {
            addBrandSuccess(msg);
        },
        error: function() {
        },
        dataType: 'html'
    });
}

function saveBrand(brand_id) {
    clearAddBrandErrors();
    var addBrandType = $("#brand_add_type").val();
    var imageId = $("#brand_image_id").val();
    var brandName = $("#brand_name").val();
    var brandBranches = $("#brand_branches").val();
    var description = tinyMCE.get('brand_description').getContent();

    var validationSuccess = true;

    if (imageId === '') {
        $("#brand_image_error").html('Należy dodać logo brandu');
        validationSuccess = false;
    }

    if (brandName === '') {
        $("#brand_name_error").html('Należy podać nazwę brandu');
        validationSuccess = false;
    }

    if (description === '') {
        $("#brand_description_error").html('Należy uzupełnić opis brandu');
        validationSuccess = false;
    }

    if (selectedBrandBranches.length === 0) {
        $("#brand_branches_error").html('Należy wybrać przynajmniej jedną branżę.');
        validationSuccess = false;
    }

    if (!validationSuccess) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/brand/zapisz/' + brand_id,
        data: {
            image_id: imageId,
            brand_name: brandName,
            brand_branches: brandBranches,
            description: description

        },
        success: function(msg) {
            saveBrandSuccess(msg);
        },
        error: function() {
        },
        dataType: 'html'
    });
}

function saveBrandSuccess(msg) {
    data = $.parseJSON(msg);
    if (data.error) {
        var errors = data.error;
        if (errors.error) {
            alert(errors.error);
        }
        if (errors.image_id) {
            $("#brand_image_error").html(errors.image_id);
        }
        if (errors.product_name) {
            $("#brand_name_error").html(errors.brand_name);
        }
        if (errors.brand_branches) {
            $("#brand_branches_error").html(errors.brand_branches);
        }
        if (errors.description) {
            $("#brand_description_error").html(errors.description);
        }
    } else if (data.success) {
        alert("Brand został prawidłowo zapisany");
        location.reload();
    } else {
        alert('Wystąpił błąd, spróbuj ponownie później.');
    }
}

function clearAddBrandErrors() {
    $("#brand_image_error").html('');
    $("#brand_name_error").html('');
    $("#brand_branches_error").html('');
    $("#brand_description_error").html('');
}

function copyFromLogo() {
    var avatar_id = $("#avatar_id").val();
    if (avatar_id !== '') {
        $.ajax({
            type: 'POST',
            url: '/image/brand/copy/' + avatar_id,
            data: {
            },
            success: function(msg) {
                data = $.parseJSON(msg);
                if (data.error) {
                    alert(data.error);
                } else {
                    $("#brand_image_id").val(data.brand_image_id);
                    $("#brand_image_placeholder").html('<img src=\'' + data.brand_image_url + '\' />');
                }
            },
            error: function() {
            },
            dataType: 'html'
        });
    } else {
        alert("Błędny identyfikator avatara!");
    }

}

function addBrandSuccess(msg) {
    data = $.parseJSON(msg);
    if (data.error) {
        var errors = data.error;
        if (errors.error) {
            alert(errors.error);
        }
        if (errors.image_id) {
            $("#brand_image_error").html(errors.image_id);
        }
        if (errors.product_name) {
            $("#brand_name_error").html(errors.brand_name);
        }
        if (errors.brand_branches) {
            $("#brand_branches_error").html(errors.brand_branches);
        }
        if (errors.description) {
            $("#brand_description_error").html(errors.description);
        }
    } else if (data.success) {
        if (data.type === 'register') {
            $("#brands_holder").html(data.brands_list_html);
            resetAddBrandModal();
            $("#addBrandModal").modal('hide');
        } else if (data.type === 'signedin') {
            if ($("#profile_brands_list") != 0) {
                $("#profile_brands_list").html(data.brands_list_html);
            }
            if ($("#action_brands_box") != 0) {
                $("#action_brands_box").html(data.action_brands_list_html);
            }
            resetAddBrandModal();
            $("#addBrandModal").modal('hide');
        }

    } else {
        alert('Wystąpił błąd, spróbuj ponownie później.');
    }

}

/**
 * Czyści okienko dodawania brandu ze wszystkich inputów
 * @returns {undefined}
 */
function resetAddBrandModal() {
    clearAddBrandErrors();
    $("#brand_image_placeholder").html('');
    $("#brand_image_id").val('0');
    $("#brand_name").val('');
    tinyMCE.get('brand_description').setContent('');
}

function removeBrandConfirm(brand_id) {
    var c = confirm("Czy na pewno chcesz usunąć ten brand? Spowoduje to także usunięcie wszystkich powiązanych z brandem akcji.");
    if (c) {
        removeBrand(brand_id);
    }
}

function removeBrand(brand_id) {
    $.ajax({
        type: 'POST',
        url: '/brand/usun/' + brand_id,
        data: {
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                alert(data.error);
            } else if (data.success) {
                $("#profile_brands_list").html(data.brands_list_html);
            } else {
                alert('Wystąpił błąd, spróbuj ponownie później.');
            }
        },
        error: function() {
        },
        dataType: 'html'
    });
}


function onBrandBranchSelect() {
    var registration_branch_selector = document.getElementById("registration_branch_selector");
    var value = registration_branch_selector.options[registration_branch_selector.selectedIndex].value;
    var name = registration_branch_selector.options[registration_branch_selector.selectedIndex].innerHTML;
    var branchNotSelected = $.inArray(value, selectedBrandBranches) === -1;
    if (branchNotSelected && selectedBrandBranches.length < 3) {
        selectedBrandBranches.push(value);
        document.getElementById("brand_branches").value = JSON.stringify(selectedBrandBranches)
        var divBranches = document.getElementById("selected_branches_box");
        divBranches.innerHTML = divBranches.innerHTML + "<span style='display:block' id='branch" + value + "'>" + name + "<input type='button' onclick=\"removeBrandBranch('" + value + "')\" value='usuń'></span>";
    } else {
        alert("Możesz wybrać maksymalnie 3 różne branże!");
    }
}

function removeBrandBranch(branchId) {
    var newArray = new Array();
    for (i = 0; i < selectedBrandBranches.length; i++) {
        if (selectedBrandBranches[i] !== branchId) {
            newArray.push(selectedBrandBranches[i]);
        }
    }
    selectedBrandBranches = newArray;
    document.getElementById("brand_branches").value = JSON.stringify(selectedBrandBranches);
    $('#branch' + branchId).remove()
}
