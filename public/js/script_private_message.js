function showSendMessageModal() {
    document.getElementById("send_private_message_modal").style.display = "block";
    document.getElementById("private_message_content").innerHTML = "";
}

function hideSendMessageModal() {
    document.getElementById("private_message_content").innerHTML = "";
    document.getElementById("send_private_message_modal").style.display = "none";
}