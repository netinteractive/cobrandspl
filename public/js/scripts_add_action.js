
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function partnersCountUndefChange() {
    if ($('#action_partners_undef').prop('checked')) {
        $("#action_partners_count").prop('disabled', true);
        $("#action_partners_count").val('');
    } else {
        $("#action_partners_count").prop('disabled', false);
    }
}

function addActionRemoveFile(actionfile_id) {
    $.ajax({
        type: 'POST',
        url: '/actionfile/session/remove',
        data: {
            actionfile_id: actionfile_id
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                document.getElementById("action_file_upload_error").innerHTML = data.error;
            } else {
                document.getElementById("action_files_box").innerHTML = data.html;
            }
        },
        error: function() {
            document.getElementById("action_file_upload_error").innerHTML = 'Wystąpił błąd podczas usuwania pliku';
        },
        dataType: 'html'
    });
}

function editActionRemoveFile(actionfile_id, action_id) {
    $.ajax({
        type: 'POST',
        url: '/actionfile/remove/' + action_id,
        data: {
            actionfile_id: actionfile_id
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                document.getElementById("action_file_upload_error").innerHTML = data.error;
            } else {
                document.getElementById("action_files_box").innerHTML = data.html;
            }
        },
        error: function() {
            document.getElementById("action_file_upload_error").innerHTML = 'Wystąpił błąd podczas usuwania pliku';
        },
        dataType: 'html'
    });
}

function onCategorySelect(id, name) {
    document.getElementById("selected_category_name").innerHTML = name;
    document.getElementById("action_category_hidden").value = id;
}

function showLibrary() {
    $.get("/library/list/init", function(data) {
        $("#library_content").html(data);
    });
    $("#libraryModal").modal();
}

function onLibraryCategoryChange() {
    var category_id = $("#product_category_selector").val();

    $.ajax({
        type: 'GET',
        url: '/library/list/subcategory/' + category_id,
        data: {
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                alert(data.error);
            } else {
                $("#product_subcategory_selector_box").html(data.subcategory_selector);
                $("#product_selector_box").html(data.product_selector);
            }
        },
        error: function() {
            alert("Wystąpił nieznany błąd.");
        },
        dataType: 'html'
    });
}

function onLibrarySubcategoryChange() {
    var subcategory_id = $("#product_subcategory_selector").val();

    $.ajax({
        type: 'GET',
        url: '/library/list/products/' + subcategory_id,
        data: {
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                alert(data.error);
            } else {
                $("#product_selector_box").html(data.product_selector);
            }
        },
        error: function() {
            alert("Wystąpił nieznany błąd.");
        },
        dataType: 'html'
    });
}

function onAddProductFromLibrary() {
    var selected_product_id = $("#product_selector").val();
    if (selected_product_id === null) {
        alert("Musisz wybrać produkt z listy!");
    } else {
        var product_selector = document.getElementById("product_selector");
        var product_id = product_selector.options[product_selector.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: '/action/add/product/session/' + product_id,
            data: {
            },
            success: function(msg) {
                data = $.parseJSON(msg);
                if (data.error) {
                    alert(data.error);
                } else {
                    $("#library_selected_products").html(data.products_list_html);
                }
            },
            error: function() {
                alert("Wystąpił nieznany błąd.");
            },
            dataType: 'html'
        });
    }
    $('#libraryModal').modal('hide');
}

function removeProduct(product_id) {
    $.ajax({
        type: 'POST',
        url: '/action/remove/product/session/' + product_id,
        data: {
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                alert(data.error);
            } else {
                $("#library_selected_products").html(data.products_list_html);
            }
        },
        error: function() {
            alert("Wystąpił nieznany błąd.");
        },
        dataType: 'html'
    });
}

function displayBranches(text) {
    $("#action_branches").html(text);
}

function calculateShare() {
    var budget = $("#action_budget").val();
    var count = $("#action_partners_count").val();
    if (budget && count) {
        var share = budget / count;
        share = share.toFixed(2);
        $("#action_share").html(share);
    }
}

