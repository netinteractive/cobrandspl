function showAddProductModal(add_product_type) {
    tinymce.init({
        selector: "#product_description",
        toolbar: "link | image",
        language: "pl",
        menubar: false,
        statusbar: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality template textcolor paste textcolor"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
                relative_urls: false,
    });
    $('#product_image_upload').fileupload({
        dataType: 'json',
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                document.getElementById("product_image_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                goUpload = false;
            }
            if (uploadFile.size > 2000000) { // 2mb
                document.getElementById("product_image_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }
        },
        done: function(e, data) {
            if (data.result.error) {
                document.getElementById("product_image_upload_error").innerHTML = data.result.error;
            } else if (data.result.success) {
                document.getElementById("product_image_placeholder").innerHTML = '<img src=\'' + data.result.product_image_url + '\' />';
                document.getElementById("product_image_id").value = data.result.product_image_id;
            } else {
                document.getElementById("product_image_upload_error").innerHTML = 'Wystąpił nieznany błąd';
            }
        }
    });
    $("#addProductModal").modal();
    if (add_product_type === 'register') {
        $("#product_add_type").val('register');
    } else if (add_product_type === 'signedin') {
        $("#product_add_type").val('signedin');
    }
}

function showEditProductModal(product_id) {
    tinymce.init({
        selector: "#product_description",
        toolbar: "link | image",
        language: "pl",
        menubar: false,
        statusbar: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality template textcolor paste textcolor"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
                relative_urls: false,
    });
    //resetAddProductModal();
    $('#product_image_upload').fileupload({
        dataType: 'json',
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                document.getElementById("product_image_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                goUpload = false;
            }
            if (uploadFile.size > 2000000) { // 2mb
                document.getElementById("product_image_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }
        },
        done: function(e, data) {
            if (data.result.error) {
                document.getElementById("product_image_upload_error").innerHTML = data.result.error;
            } else if (data.result.success) {
                document.getElementById("product_image_placeholder").innerHTML = '<img src=\'' + data.result.product_image_url + '\' />';
                document.getElementById("product_image_id").value = data.result.product_image_id;
            } else {
                document.getElementById("product_image_upload_error").innerHTML = 'Wystąpił nieznany błąd';
            }
        }
    });
    $.get("/product/" + product_id, function(data) {
        $("#product_id").val(data.product_id);
        $("#product_image_id").val(data.logo_id);
        $("#product_image_placeholder").html("<img src='" + data.logo_url + "' />");
        $("#product_name").val(data.name);
        $("#product_category_selectors").html(data.category_html);
        tinyMCE.get('product_description').setContent(data.description);
        $("#num_of_prices").val(data.price_count);
        if (data.price_count >= 1) {
            $("#product_price1_holder").show();
            $("#product_price1").val(data.price_1);
            $("#product_price1_from").val(data.count_1_from);
            $("#product_price1_to").val(data.count_1_to);
            $("#add_price_button_holder").show();
        }
        if (data.price_count >= 2) {
            $("#product_price2_holder").show();
            $("#product_price2").val(data.price_2);
            $("#product_price2_from").val(data.count_2_from);
            $("#product_price2_to").val(data.count_2_to);
            $("#add_price_button_holder").show();
        }
        if (data.price_count == 3) {
            $("#product_price3_holder").show();
            $("#product_price3").val(data.price_3);
            $("#product_price3_from").val(data.count_3_from);
            $("#product_price3_to").val(data.count_3_to);
            $("#add_price_button_holder").hide();
        }
        $("#product_count").val(data.product_count);
    });
    $("#editProductModal").modal();
}

function addProduct() {
    clearAddProductErrors();
    var addProductType = $("#product_add_type").val();
    var imageId = $("#product_image_id").val();
    var productName = $("#product_name").val();
    var category = $("#product_subcategory_selector").val();
    var description = tinyMCE.get('product_description').getContent();
    var numOfPrices = $("#num_of_prices").val();

    var price1 = parseInt($("#product_price1").val());
    var price1From = parseInt($("#product_price1_from").val());
    var price1To = parseInt($("#product_price1_to").val());

    var price2 = parseInt($("#product_price2").val());
    var price2From = parseInt($("#product_price2_from").val());
    var price2To = parseInt($("#product_price2_to").val());

    var price3 = parseInt($("#product_price3").val());
    var price3From = parseInt($("#product_price3_from").val());
    var price3To = parseInt($("#product_price3_to").val());

    var productCount = $("#product_count").val();

    var validationSuccess = true;

    if (imageId === '') {
        $("#product_image_error").html('Należy dodać zdjęcie produktu');
        validationSuccess = false;
    }

    if (productName === '') {
        $("#product_name_error").html('Należy podać nazwę produktu');
        validationSuccess = false;
    }

    if (category === '') {
        $("#product_category_error").html('Należy wybrać kategorię produktu');
        validationSuccess = false;
    }

    if (description === '') {
        $("#product_description_error").html('Należy uzupełnić opis produktu');
        validationSuccess = false;
    }

    if (numOfPrices == 1) {
        if (!checkPrices(price1, price1From, price1To, 1)) {
            validationSuccess = false;
        }
    } else if (numOfPrices == 2) {
        if (!checkPrices(price1, price1From, price1To, 1)) {
            validationSuccess = false;
        }
        if (!checkPrices(price2, price2From, price2To, 2)) {
            validationSuccess = false;
        }
    } else if (numOfPrices == 3) {
        if (!checkPrices(price1, price1From, price1To, 1)) {
            validationSuccess = false;
        }
        if (!checkPrices(price2, price2From, price2To, 2)) {
            validationSuccess = false;
        }
        if (!checkPrices(price3, price3From, price3To, 3)) {
            validationSuccess = false;
        }
    }

    if (productCount == '') {
        $("#product_count_error").html('Należy uzupelnić to pole');
        validationSuccess = false;
    }

    if (productCount <= 0) {
        $("#product_count_error").html('Wartość musi być większa od 0');
        validationSuccess = false;
    }


    if (!validationSuccess) {
        return false;
    }

    if (numOfPrices == 1) {
        $.ajax({
            type: 'POST',
            url: '/produkt/dodaj',
            data: {
                image_id: imageId,
                add_type: addProductType,
                product_name: productName,
                category_id: category,
                description: description,
                num_of_prices: numOfPrices,
                price_1: price1,
                price_1_from: price1From,
                price_1_to: price1To,
                product_count: productCount

            },
            success: function(msg) {
                addProductSuccess(msg);
            },
            error: function() {
            },
            dataType: 'html'
        });
    } else if (numOfPrices == 2) {
        $.ajax({
            type: 'POST',
            url: '/produkt/dodaj',
            data: {
                image_id: imageId,
                add_type: addProductType,
                product_name: productName,
                category_id: category,
                description: description,
                num_of_prices: numOfPrices,
                price_1: price1,
                price_1_from: price1From,
                price_1_to: price1To,
                price_2: price2,
                price_2_from: price2From,
                price_2_to: price2To,
                product_count: productCount

            },
            success: function(msg) {
                addProductSuccess(msg);
            },
            error: function() {
            },
            dataType: 'html'
        });
    } else if (numOfPrices == 3) {
        $.ajax({
            type: 'POST',
            url: '/produkt/dodaj',
            data: {
                image_id: imageId,
                add_type: addProductType,
                product_name: productName,
                category_id: category,
                description: description,
                num_of_prices: numOfPrices,
                price_1: price1,
                price_1_from: price1From,
                price_1_to: price1To,
                price_2: price2,
                price_2_from: price2From,
                price_2_to: price2To,
                price_3: price3,
                price_3_from: price3From,
                price_3_to: price3To,
                product_count: productCount

            },
            success: function(msg) {
                addProductSuccess(msg);
            },
            error: function() {
            },
            dataType: 'html'
        });
    }
}

function saveProduct(product_id) {
    clearAddProductErrors();
    var productName = $("#product_name").val();
    var category = $("#product_subcategory_selector").val();
    var description = tinyMCE.get('product_description').getContent();
    var numOfPrices = $("#num_of_prices").val();

    var price1 = parseInt($("#product_price1").val());
    var price1From = parseInt($("#product_price1_from").val());
    var price1To = parseInt($("#product_price1_to").val());

    var price2 = parseInt($("#product_price2").val());
    var price2From = parseInt($("#product_price2_from").val());
    var price2To = parseInt($("#product_price2_to").val());

    var price3 = parseInt($("#product_price3").val());
    var price3From = parseInt($("#product_price3_from").val());
    var price3To = parseInt($("#product_price3_to").val());

    var productCount = $("#product_count").val();

    var validationSuccess = true;

    if (productName === '') {
        $("#product_name_error").html('Należy podać nazwę produktu');
        validationSuccess = false;
    }

    if (category === '' || category === 'Wybierz...') {
        $("#product_category_error").html('Należy wybrać kategorię produktu');
        validationSuccess = false;
    }

    if (description === '') {
        $("#product_description_error").html('Należy uzupełnić opis produktu');
        validationSuccess = false;
    }

    if (numOfPrices == 1) {
        if (!checkPrices(price1, price1From, price1To, 1)) {
            validationSuccess = false;
        }
    } else if (numOfPrices == 2) {
        if (!checkPrices(price1, price1From, price1To, 1)) {
            validationSuccess = false;
        }
        if (!checkPrices(price2, price2From, price2To, 2)) {
            validationSuccess = false;
        }
    } else if (numOfPrices == 3) {
        if (!checkPrices(price1, price1From, price1To, 1)) {
            validationSuccess = false;
        }
        if (!checkPrices(price2, price2From, price2To, 2)) {
            validationSuccess = false;
        }
        if (!checkPrices(price3, price3From, price3To, 3)) {
            validationSuccess = false;
        }
    }

    if (productCount == '') {
        $("#product_count_error").html('Należy uzupelnić to pole');
        validationSuccess = false;
    }

    if (productCount <= 0) {
        $("#product_count_error").html('Wartość musi być większa od 0');
        validationSuccess = false;
    }


    if (!validationSuccess) {
        return false;
    }

    if (numOfPrices == 1) {
        $.ajax({
            type: 'POST',
            url: '/produkt/zapisz/' + product_id,
            data: {
                product_name: productName,
                category_id: category,
                description: description,
                num_of_prices: numOfPrices,
                price_1: price1,
                price_1_from: price1From,
                price_1_to: price1To,
                product_count: productCount

            },
            success: function(msg) {
                saveProductSuccess(msg);
            },
            error: function() {
            },
            dataType: 'html'
        });
    } else if (numOfPrices == 2) {
        $.ajax({
            type: 'POST',
            url: '/produkt/zapisz/' + product_id,
            data: {
                product_name: productName,
                category_id: category,
                description: description,
                num_of_prices: numOfPrices,
                price_1: price1,
                price_1_from: price1From,
                price_1_to: price1To,
                price_2: price2,
                price_2_from: price2From,
                price_2_to: price2To,
                product_count: productCount

            },
            success: function(msg) {
                saveProductSuccess(msg);
            },
            error: function() {
            },
            dataType: 'html'
        });
    } else if (numOfPrices == 3) {
        $.ajax({
            type: 'POST',
            url: '/produkt/zapisz/' + product_id,
            data: {
                product_name: productName,
                category_id: category,
                description: description,
                num_of_prices: numOfPrices,
                price_1: price1,
                price_1_from: price1From,
                price_1_to: price1To,
                price_2: price2,
                price_2_from: price2From,
                price_2_to: price2To,
                price_3: price3,
                price_3_from: price3From,
                price_3_to: price3To,
                product_count: productCount

            },
            success: function(msg) {
                saveProductSuccess(msg);
            },
            error: function() {
            },
            dataType: 'html'
        });
    }
}

function saveProductSuccess(msg) {
    data = $.parseJSON(msg);
    if (data.error) {
        var errors = data.error;
        if (errors.error) {
            alert(errors.error);
        }
        if (errors.image_id) {
            $("#product_image_error").html(errors.image_id);
        }
        if (errors.product_name) {
            $("#product_name_error").html(errors.product_name);
        }
        if (errors.category_id) {
            $("#product_category_error").html(errors.category_id);
        }
        if (errors.description) {
            $("#product_description_error").html(errors.description);
        }
        if (errors.num_of_prices) {
            $("#product_price1_error").html(errors.num_of_prices);
        }
        if (errors.price_1) {
            $("#product_price1_error").html(errors.price_1);
        }
        if (errors.price_1_from) {
            $("#product_price1_error").html(errors.price_1_from);
        }
        if (errors.price_1_to) {
            $("#product_price1_error").html(errors.price_1_to);
        }
        if (errors.price_2) {
            $("#product_price2_error").html(errors.price_2);
        }
        if (errors.price_2_from) {
            $("#product_price2_error").html(errors.price_2_from);
        }
        if (errors.price_2_to) {
            $("#product_price2_error").html(errors.price_2_to);
        }
        if (errors.price_3) {
            $("#product_price3_error").html(errors.price_3);
        }
        if (errors.price_3_from) {
            $("#product_price3_error").html(errors.price_3_from);
        }
        if (errors.price_3_to) {
            $("#product_price3_error").html(errors.price_3_to);
        }
        if (errors.product_count) {
            $("#product_count_error").html(errors.product_count);
        }
    } else if (data.success) {
        alert("Produkt został prawidłowo zapisany");
        location.reload();
    } else {
        alert('Wystąpił błąd, spróbuj ponownie później.');
    }
}

function addProductSuccess(msg) {
    data = $.parseJSON(msg);
    if (data.error) {
        var errors = data.error;
        if (errors.error) {
            alert(errors.error);
        }
        if (errors.image_id) {
            $("#product_image_error").html(errors.image_id);
        }
        if (errors.product_name) {
            $("#product_name_error").html(errors.product_name);
        }
        if (errors.category_id) {
            $("#product_category_error").html(errors.category_id);
        }
        if (errors.description) {
            $("#product_description_error").html(errors.description);
        }
        if (errors.num_of_prices) {
            $("#product_price1_error").html(errors.num_of_prices);
        }
        if (errors.price_1) {
            $("#product_price1_error").html(errors.price_1);
        }
        if (errors.price_1_from) {
            $("#product_price1_error").html(errors.price_1_from);
        }
        if (errors.price_1_to) {
            $("#product_price1_error").html(errors.price_1_to);
        }
        if (errors.price_2) {
            $("#product_price2_error").html(errors.price_2);
        }
        if (errors.price_2_from) {
            $("#product_price2_error").html(errors.price_2_from);
        }
        if (errors.price_2_to) {
            $("#product_price2_error").html(errors.price_2_to);
        }
        if (errors.price_3) {
            $("#product_price3_error").html(errors.price_3);
        }
        if (errors.price_3_from) {
            $("#product_price3_error").html(errors.price_3_from);
        }
        if (errors.price_3_to) {
            $("#product_price3_error").html(errors.price_3_to);
        }
        if (errors.product_count) {
            $("#product_count_error").html(errors.product_count);
        }
    } else if (data.success) {
        if (data.type === 'register') {
            $("#products_holder").html(data.products_list_html);
            resetAddProductModal();
            $("#addProductModal").modal('hide');
        } else if (data.type === 'signedin') {
            alert(data.success);
            location.reload();
        }

    } else {
        alert('Wystąpił błąd, spróbuj ponownie później.');
    }

}

/**
 * Czyści okienko dodawania produktu ze wszystkich inputów
 * @returns {undefined}
 */
function resetAddProductModal() {
    clearAddProductErrors();
    $("#product_image_placeholder").html('');
    $("#product_image_id").val('0');
    $("#product_name").val('');
    // przeładowanie listy kategorii
    $.get("/product/list/render", function(data) {
        $("#product_category_selectors").html(data);
    });
    tinyMCE.get('product_description').setContent('');
    $("#product_price1").val('');
    $("#product_price1_from").val('');
    $("#product_price1_to").val('');

    $("#product_price2").val('');
    $("#product_price2_from").val('');
    $("#product_price2_to").val('');

    $("#product_price3").val('');
    $("#product_price3_from").val('');
    $("#product_price3_to").val('');

    $("#num_of_prices").val(1);

    $("#product_price1_holder").show();
    $("#product_price2_holder").hide();
    $("#product_price3_holder").hide();
    $("#add_price_button_holder").show();

    $("#product_count").val('');
}

function clearAddProductErrors() {
    $("#product_image_error").html('');
    $("#product_name_error").html('');
    $("#product_category_error").html('');
    $("#product_price1_error").html('');
    $("#product_price2_error").html('');
    $("#product_price3_error").html('');
    $("#product_description_error").html('');
    $("#product_count_error").html('');
}

function checkPrices(priceMain, priceFrom, priceTo, priceNum) {
    if (isNaN(priceMain) || isNaN(priceFrom) || isNaN(priceTo)) {
        $("#product_price" + priceNum + "_error").html('Należy uzupełnić wszystkie pola');
        return false;
    }
    if (priceFrom > priceTo) {
        $("#product_price" + priceNum + "_error").html("Wartość 'od' nie może być większa od wartości 'do'");
        return false;
    }
    if (priceMain < 0) {
        $("#product_price" + priceNum + "_error").html("Cena musi być większa od 0");
        return false;
    }
    return true;
}


function addNextPrice() {
    var numOfPrices = $("#num_of_prices").val();
    if (numOfPrices == 1) {
        $("#product_price2_holder").show();
        $("#num_of_prices").val(2);
    } else if (numOfPrices == 2) {
        $("#product_price3_holder").show();
        $("#num_of_prices").val(3);
        $("#add_price_button_holder").hide();
    } else if (numOfPrices >= 3) {

    }
}

function removePrice(priceNum) {
    if (priceNum == 2) {
        $("#product_price2").val('');
        $("#product_price2_from").val('');
        $("#product_price2_to").val('');
        $("#product_price2_holder").hide();
        $("#add_price_button_holder").show();
        $("#num_of_prices").val(1);
    } else if (priceNum == 3) {
        $("#product_price3").val('');
        $("#product_price3_from").val('');
        $("#product_price3_to").val('');
        $("#product_price3_holder").hide();
        $("#add_price_button_holder").show();
        $("#num_of_prices").val(2);
    }
}

function reloadProductSubCategoriesList() {
    var mainCategoryId = $("#product_category_selector").val();
    $.ajax({
        type: 'POST',
        url: '/product/subcategories/list/render/' + mainCategoryId,
        data: {
        },
        success: function(msg) {
            $("#product_subcategory_holder").html(msg);
        },
        error: function() {
        },
        dataType: 'html'
    });
}

function removeProductConfirm(product_id) {
    var c = confirm("Czy na pewno chcesz usunąć ten produkt?");
    if (c) {
        removeProduct(product_id);
    }
}

function removeProduct(product_id) {
    $.ajax({
        type: 'POST',
        url: '/produkt/usun/' + product_id,
        data: {
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                var errors = data.error;
                if (errors.error) {
                    alert(errors.error);
                }
            } else if (data.success) {
                alert(data.success);
                window.location.href = "/profile/products/sortby/date/sorttype/desc"
            } else {
                alert('Wystąpił błąd, spróbuj ponownie później.');
            }
        },
        error: function() {
        },
        dataType: 'html'
    });
}