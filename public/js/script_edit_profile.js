function editProfile() {
    document.getElementById("personal_details_show").style.display = "none";
    document.getElementById("personal_details_edit").style.display = "block";
    document.getElementById("edit_branches").value = JSON.stringify(selectedBranches);
}

function cancelEditProfile() {
    document.getElementById("personal_details_show").style.display = "block";
    document.getElementById("personal_details_edit").style.display = "none";
}

function removeBranch(branchId) {
    var newArray = new Array();
    for (i = 0; i < selectedBranches.length; i++) {
        if (selectedBranches[i] !== branchId) {
            newArray.push(selectedBranches[i]);
        }
    }
    selectedBranches = newArray;
    document.getElementById("edit_branches").value = JSON.stringify(selectedBranches);
    $('#branch' + branchId).remove()
}
function onEditBranchSelect() {
    var edit_branch_selector = document.getElementById("edit_branch_selector");
    var value = edit_branch_selector.options[edit_branch_selector.selectedIndex].value;
    var name = edit_branch_selector.options[edit_branch_selector.selectedIndex].innerHTML;
    var branchNotSelected = $.inArray(value, selectedBranches) === -1;
    if (branchNotSelected && selectedBranches.length < 3) {
        selectedBranches.push(value);
        document.getElementById("edit_branches").value = JSON.stringify(selectedBranches)
        var divBranches = document.getElementById("selected_branches_box");
        divBranches.innerHTML = divBranches.innerHTML + "<span style='display:block' id='branch" + value + "'>" + name + "<input type='button' onclick=\"removeBranch('" + value + "')\" value='usuń'></span>";
    }
}


