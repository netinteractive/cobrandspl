function showAddCommentModal(action_id) {
    document.getElementById("comment_content").innerHTML = '';
    document.getElementById("action_id").value = action_id;
    document.getElementById("add_comment_modal").style.display = "block";
}
function selectAllPartners() {
    var checkboxes = new Array();
    checkboxes = document.getElementsByName('selectedPartners[]');

    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type === 'checkbox') {
            checkboxes[i].checked = true;
        }
    }
}

function deselectAllPartners() {
    var checkboxes = new Array();
    checkboxes = document.getElementsByName('selectedPartners[]');

    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type === 'checkbox') {
            checkboxes[i].checked = false;
        }
    }
}

function validatePartners() {
     var checkboxes = new Array();
    checkboxes = document.getElementsByName('selectedPartners[]');

    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type === 'checkbox') {
            if(checkboxes[i].checked) {
                return true;
            }
        }
    }
    alert('Musisz wybrać przynajmniej jednego partnera!');
    return false;
}

function showPartnersModal() {
   document.getElementById("inform_partners_modal").style.display = "block"; 
}

function closePartnersModal() {
   document.getElementById("inform_partners_modal").style.display = "none"; 
}

function onApplyLinkClick() {
    $('#applyModal').modal('show')
}
