function onPackageSelect(price, package_id) {
    if (document.getElementById('package' + package_id).checked) {
        document.getElementById("buyButton").disabled = false;
    } else {
        document.getElementById("buyButton").disabled = true;
    }

    document.getElementById("package_price").innerHTML = price;
}

function hideBuyModal() {
    document.getElementById("buy_package_modal").style.display = "none";
}

function showBuyModal() {
    $('.package_radio').prop('checked', false);
    document.getElementById("buyButton").disabled = true;
    document.getElementById("buy_package_modal").style.display = "block";
}

