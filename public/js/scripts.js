function onAdvancedSearchClick() {
    $("#advanced_search_box").show();
    $("#search_type").val(1);
    $("#search_branch_box").hide();
}

function onHideAdvancedSearchClick() {
    $("#advanced_search_box").hide();
}

function onSearchTypeChange() {
    var searchType = $("#search_type").val();
    if (searchType == 1) {
        $("#search_branch_box").hide();
        $("#search_category_box").show();
        $("#search_city_box").show();
        $("#search_district_box").show();
        $("#search_budget_box").show();
        $("#search_date_box").show();
    } else if (searchType == 2) {
        $("#search_branch_box").show();
        $("#search_category_box").hide();
        $("#search_city_box").hide();
        $("#search_district_box").hide();
        $("#search_budget_box").hide();
        $("#search_date_box").hide();
    }
}


function reloadActionSubCategoriesList() {
    var mainCategoryId = $("#action_category_selector").val();
    $.ajax({
        type: 'POST',
        url: '/action/subcategories/list/render/' + mainCategoryId,
        data: {
        },
        success: function(msg) {
            $("#action_subcategory_holder").html(msg);
        },
        error: function() {
        },
        dataType: 'html'
    });
}

function reloadSubBranchesList() {
    var mainBranchId = $("#branch_selector").val();
    $.ajax({
        type: 'POST',
        url: '/brand/subbranches/list/render/' + mainBranchId,
        data: {
        },
        success: function(msg) {
            $("#brand_subbranch_holder").html(msg);
        },
        error: function() {
        },
        dataType: 'html'
    });
}

