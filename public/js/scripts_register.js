var selectedBranches = new Array();

function onRegistrationBranchSelect() {
    var registration_branch_selector = document.getElementById("registration_branch_selector");
    var value = registration_branch_selector.options[registration_branch_selector.selectedIndex].value;
    var name = registration_branch_selector.options[registration_branch_selector.selectedIndex].innerHTML;
    var branchNotSelected = $.inArray(value, selectedBranches) === -1;
    if (branchNotSelected && selectedBranches.length < 3) {
        selectedBranches.push(value);
        document.getElementById("register_branches").value = JSON.stringify(selectedBranches)
        var divBranches = document.getElementById("selected_branches_box");
        divBranches.innerHTML = divBranches.innerHTML + "<span style='display:block' id='branch" + value + "'>" + name + "<input type='button' onclick=\"removeBranch('" + value + "')\" value='usuń'></span>";
    }
}

function removeBranch(branchId) {
    var newArray = new Array();
    for (i = 0; i < selectedBranches.length; i++) {
        if (selectedBranches[i] !== branchId) {
            newArray.push(selectedBranches[i]);
        }
    }
    selectedBranches = newArray;
    document.getElementById("register_branches").value = JSON.stringify(selectedBranches);
    $('#branch' + branchId).remove()
}

function loadCompanyDataByNip() {
    var nip = $('#register_nip').val();
    var captcha_input = $('#captcha_input').val();
    $.ajax({
        type: 'POST',
        url: '/nip/dane',
        data: {
            nip: nip,
            captcha_input: captcha_input
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                $("#nip_error").innerHTML = data.error;
            } else {
                $("#register_regon").val(data.regon);
                $("#register_companyname").val(data.nazwa);
                $("#register_street").val(data.ulica);
                $("#register_city").val(data.miasto);
                $("#register_postcode").val(data.kod);

                $('select[name=register_district]').val(data.woj);
            }
        },
        error: function() {
            //document.getElementById("action_file_upload_error").innerHTML = 'Wystąpił błąd podczas usuwania pliku';
        },
        dataType: 'html'
    });
    $("#captcha_image").removeAttr("src").attr("src", "/captcha/nip" + "?" + Math.floor(Math.random() * 1000));

}

function addProductError(msg) {

}


/**
 * Usuwanie produktu z listy na formularzy rejestracyjnym
 * @param {type} product_id
 * @returns {undefined}
 */
function removeProductRegister(product_id) {
    $.ajax({
        type: 'POST',
        url: '/rejestracja/produkt/usun/' + product_id,
        data: {
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                alert(data.error);
            } else if (data.success) {
                $("#products_holder").html(data.products_list_html);
            } else {
                alert('Wystąpił nieznany błąd.');
            }
        },
        error: function() {
        },
        dataType: 'html'
    });
}

/**
 * Usuwanie brandu z listy na formularzy rejestracyjnym
 * @param {type} brand_id
 * @returns {undefined}
 */
function removeBrandRegister(brand_id) {
    $.ajax({
        type: 'POST',
        url: '/rejestracja/brand/usun/' + brand_id,
        data: {
        },
        success: function(msg) {
            data = $.parseJSON(msg);
            if (data.error) {
                alert(data.error);
            } else if (data.success) {
                $("#brands_holder").html(data.products_list_html);
            } else {
                alert('Wystąpił nieznany błąd.');
            }
        },
        error: function() {
        },
        dataType: 'html'
    });
}

