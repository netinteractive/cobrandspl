<?php

/**
 * Description of Applicationstate
 *
 * @author damian
 */
class Applicationstate extends Eloquent {

    protected $table = 'applicationstates';
    public $timestamps = false;

    public function applications() {
        return $this->hasMany('Application');
    }

}
