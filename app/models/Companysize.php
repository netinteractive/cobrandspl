<?php

/**
 * Description of Companysize
 *
 * @author damian
 */
class Companysize extends Eloquent {

    protected $table = 'companysizes';

    public function users() {
        return $this->hasMany('User');
    }

}
