<?php

use NetInteractive\Cobrands\Utils\TimeUtils;

/**
 * Description of Packageorder
 *
 * @author damian
 */
class Packageorder extends Eloquent {

    protected $table = 'packageorders';
    public $timestamps = true;

    public function package() {
        return $this->belongsTo('Package');
    }

    public function user() {
        return $this->belongsTo('User');
    }

    public function paymentstatus() {
        return $this->belongsTo('Paymentstatus');
    }

    public function getTimeLeft() {
        if ($this->enddate) {
            return TimeUtils::getTimeDiff($this->enddate);
        } else {
            return '--';
        }
    }

}
