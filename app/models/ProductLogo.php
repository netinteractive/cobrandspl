<?php

/**
 * Description of ProductLogo
 *
 * @author damian
 */
class ProductLogo extends Eloquent {

    protected $table = 'product_logos';
    public $timestamps = true;

    public function products() {
        return $this->hasMany('Product');
    }

    public function getUrl() {
        return route("getProductImage", $this->filename);
    }

    public function getThumbUrl() {
        return route("getProductImageThumb", $this->filename);
    }

}
