<?php

/**
 * Description of ProductCategory
 *
 * @author damian
 */
class ProductCategory extends Eloquent {

    protected $table = 'product_categories';

    public function products() {
        return $this->hasMany('Product');
    }

    public function getChildren() {
        return $this::where('parent_id', $this->id)->orderBy('id', 'ASC')->get();
    }
    
    public function getParent() {
        return $this::find($this->parent_id);
    }

}
