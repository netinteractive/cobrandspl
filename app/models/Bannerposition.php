<?php

/**
 * Description of Bannerposition
 *
 * @author damian
 */
class Bannerposition extends Eloquent {

    protected $table = 'bannerpositions';
    public $timestamps = false;

    public function banners() {
        return $this->hasMany('Banner');
    }

}
