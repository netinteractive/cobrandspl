<?php

class Mailing extends Eloquent {

    protected $table = 'mailings';
    public $timestamps = true;
    protected $fillable = ['email','unsubscription_code'];

}
