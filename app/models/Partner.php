<?php

/**
 * Description of Partner
 *
 * @author damian
 */
class Partner extends Eloquent {

    public function getUserId() {
        return $this->user_id;
    }

    public function getPartnerId() {
        return $this->partner_id;
    }

    public function getBrandId() {
        return $this->brand_id;
    }

    public function getPartnerBrandId() {
        return $this->partner_brand_id;
    }

    public function isInvitationAccepted() {
        return $this->invitation_accepted;
    }

    public function isInvitationRejected() {
        return $this->invitation_rejected;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
    }

    public function setPartnerId($partner_id) {
        $this->partner_id = $partner_id;
    }

    public function setBrandId($brand_id) {
        $this->brand_id = $brand_id;
    }

    public function setPartnerBrandId($partner_brand_id) {
        $this->partner_brand_id = $partner_brand_id;
    }

    public function setInvitationAccepted($invitation_accepted) {
        $this->invitation_accepted = $invitation_accepted;
    }

    public function setInvitationRejected($invitation_rejected) {
        $this->invitation_rejected = $invitation_rejected;
    }

    protected $table = 'partners';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('User', 'user_id');
    }

    public function brand() {
        return $this->belongsTo('Brand', 'brand_id');
    }

    public function partner() {
        return $this->belongsTo('User', 'partner_id');
    }

    public function partnerBrand() {
        return $this->belongsTo('Brand', 'partner_brand_id');
    }

}
