<?php

/**
 * Description of Packagetype
 *
 * @author damian
 */
class Packagetype extends Eloquent {

    protected $table = 'packagetypes';
    public $timestamps = false;

    public function packages() {
        return $this->hasMany('Package');
    }

}
