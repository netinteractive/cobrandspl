<?php

/**
 * Description of District
 *
 * @author damian
 */
class District extends Eloquent {

    protected $table = 'districts';
    public $timestamps = false;

    public function users() {
        return $this->hasMany('User');
    }
    
    public function actions() {
        return $this->hasMany('Action');
    }

}
