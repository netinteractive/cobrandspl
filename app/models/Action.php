<?php

use NetInteractive\Cobrands\Utils\TimeUtils;

/**
 * Description of Action
 *
 * @author damian
 */
class Action extends Eloquent {

    protected $table = 'actions';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('User');
    }

    public function brand() {
        return $this->belongsTo('Brand');
    }

    public function avatar() {
        return $this->belongsTo('Avatar'); // to nie wiedzieć czemu nie działa
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getName() {
        return $this->name;
    }

    public function getCategoryId() {
        return $this->category_id;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getRequiredPartnerCount() {
        return $this->required_partner_count;
    }

    public function getCurrentPartnerCount() {
        return $this->current_partner_count;
    }

    public function getStageDeadlineOne() {
        return $this->stage_deadline_one;
    }

    public function getTimeLeft() {
        return TimeUtils::getTimeDiff($this->stage_deadline_one);
    }

    public function getFormattedStageDeadlineOne() {
        return date("Y-m-d", strtotime($this->stage_deadline_one));
    }

    public function getStageDeadlineTwo() {
        return $this->stage_deadline_two;
    }

    public function getFormattedStageDeadlineTwo() {
        return date("Y-m-d", strtotime($this->stage_deadline_two));
    }

    public function getBudget() {
        return $this->budget;
    }

    public function getFormattedBudget() {
        return number_format($this->budget, 2, ',', ' ') . ' zł';
    }

    public function getActionDateStart() {
        return $this->action_date_start;
    }

    public function getFormattedActionDateStart() {
        return date("Y-m-d", strtotime($this->action_date_start));
    }

    public function getActionDateEnd() {
        return $this->action_date_end;
    }

    public function getFormattedActionDateEnd() {
        return date("Y-m-d", strtotime($this->action_date_end));
    }

    public function getActionstateId() {
        return $this->actionstate_id;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setAvatarId($avatar_id) {
        $this->avatar_id = $avatar_id;
    }

    public function getAvatarId() {
        return $this->avatar_id;
    }

    public function getBrandId() {
        return $this->brand_id;
    }

    public function setBrandId($brand_id) {
        $this->brand_id = $brand_id;
    }

    public function setCategoryId($category_id) {
        $this->category_id = $category_id;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setDescriptionMore($descriptionMore) {
        $this->description_more = $descriptionMore;
    }

    public function setRequiredPartnerCount($required_partner_count) {
        $this->required_partner_count = $required_partner_count;
    }

    public function setStageDeadlineOne($stage_deadline_one) {
        $this->stage_deadline_one = $stage_deadline_one;
    }

    public function setStageDeadlineTwo($stage_deadline_two) {
        $this->stage_deadline_two = $stage_deadline_two;
    }

    public function setBudget($budget) {
        $this->budget = $budget;
    }

    public function setActionDateStart($action_date_start) {
        $this->action_date_start = $action_date_start;
    }

    public function setActionDateEnd($action_date_end) {
        $this->action_date_end = $action_date_end;
    }

    public function setActionstateId($actionstate_id) {
        $this->actionstate_id = $actionstate_id;
    }

    public function category() {
        return $this->belongsTo('Category');
    }

    public function files() {
        return $this->belongsToMany('Actionfile', 'actions_has_actionfiles');
    }

    public function actionstate() {
        return $this->belongsTo('Actionstate');
    }

    public function recommendations() {
        return $this->hasMany('Recommendation');
    }

    public function applications() {
        return $this->hasMany('Application');
    }

    public function comments() {
        return CommentModel::where('action_id', $this->id)->get();
    }

    public function getNameFormatted() {
        return $this->brand->name . "/" . $this->category->getParent()->name . "/" . $this->category->name;
    }

    public function products() {
        return $this->belongsToMany('Product', 'action_has_products');
    }
    
    public function district() {
        return $this->belongsTo('District');
    }

}
