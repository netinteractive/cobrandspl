<?php

/**
 * Tak samo jak przy Abuse, nazwa Comment nie działa i muiałem
 * nazwać CommentModel
 *
 * @author damian
 */
class CommentModel extends Eloquent {

    protected $table = 'comments';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('User', 'author_id');
    }

    public function action() {
        return $this->belongsTo('Action');
    }

    public function getFormattedCreatedDate() {
        return date("d-m-Y h:i:s", strtotime($this->created_at));
    }

}
