<?php

/**
 * Description of Avatar
 *
 * @author damian
 */
class Avatar extends Eloquent {

    protected $table = 'avatars';
    public $timestamps = true;

    public function users() {
        return $this->hasMany('User');
    }

    public function getUrl() {
        return route("getAvatar", $this->filename);
    }

    public function getThumbUrl() {
        return route("getAvatarThumb", $this->filename);
    }

}
