<?php

/**
 * Description of Banner
 *
 * @author damian
 */
class Banner extends Eloquent {

    protected $table = 'banners';
    public $timestamps = true;
    
    public function position() {
        return $this->belongsTo('Bannerposition');
    }

}
