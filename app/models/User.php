<?php

use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;

class User extends SentryUserModel {

    public function district() {
        return $this->belongsTo('District');
    }

    public function avatar() {
        return $this->belongsTo('Avatar');
    }

    public function companySize() {
        return $this->belongsTo('Companysize', 'companysize_id');
    }

    public function branches() {
        return $this->belongsToMany('Branch', 'users_has_branches');
    }

    public function partners() {
        return $this->hasMany('Partner');
    }

    public function getAcceptedPartnersCount() {
        return Partner::where('user_id', $this->id)
                        ->where('invitation_accepted', true)
                        ->where('invitation_rejected', false)
                        ->get()
                        ->count();
    }

    public function actions() {
        return $this->hasMany('Action');
    }

    public function products() {
        return $this->hasMany('Product');
    }

    public function brands() {
        return $this->hasMany('Brand');
    }

    public function recommendations() {
        return $this->hasMany('Recommendation');
    }

    public function getAuthoredRecommendations() {
        return $this->hasMany('Recommendation', 'added_user_id');
    }

    public function applications() {
        return $this->hasMany('Application');
    }

    public function packageorders() {
        return $this->hasMany('Packageorder');
    }

    public function comments() {
        return $this->hasMany('CommentModel');
    }

    public function subscribedToNewsletter() {
        $mailing = Mailing::where('email', $this->email)->first();
        if ($mailing) {
            return true;
        } else {
            return false;
        }
    }

}
