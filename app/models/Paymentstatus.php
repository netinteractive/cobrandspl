<?php

/**
 * Description of Paymentstatus
 *
 * @author damian
 */
class Paymentstatus extends Eloquent {

    protected $table = 'paymentstatus';
    public $timestamps = false;

    public function packages() {
        return $this->hasMany('Package');
    }

}
