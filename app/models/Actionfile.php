<?php

/**
 * Description of Actionfile
 *
 * @author damian
 */
class Actionfile extends Eloquent {

    protected $table = 'actionfiles';
    public $timestamps = true;

    public function actions() {
        return $this->belongsToMany('Action', 'actions_has_actionfiles');
    }

    public function getUrl() {
        return route("getActionfile", $this->filename);
    }

    public function getThumbUrl() {
        return route("getActionfileThumb", $this->filename);
    }

}
