<?php

/**
 * Description of Page
 *
 * @author damian
 */
class Page extends Eloquent {

    protected $table = 'pages';
    public $timestamps = true;
    protected $fillable = array('title', 'content', 'isActive');

}
