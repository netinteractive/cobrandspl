<?php

/**
 * Description of Brand
 *
 * @author damian
 */
class Brand extends Eloquent {

    protected $table = 'brands';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('User');
    }

    public function logo() {
        return $this->belongsTo('Logo');
    }

    public function branches() {
        return $this->belongsToMany('Branch', 'brand_has_branches');
    }
    
    public function actions() {
        return $this->hasMany('Action');
    }
    
    public function applications() {
        return $this->hasMany('Application');
    }

    public function getBranchesAsTextList() {
        $text = '';
        foreach ($this->branches as $branch) {
            $text .= $branch->name . "<br/>";
        }
        return $text;
    }

}
