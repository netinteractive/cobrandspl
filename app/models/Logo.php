<?php

/**
 * Description of Logo
 *
 * @author damian
 */
class Logo extends Eloquent {

    protected $table = 'logos';
    public $timestamps = true;

    public function brands() {
        return $this->hasMany('Brand');
    }

    public function getUrl() {
        return route("getBrandImage", $this->filename);
    }

    public function getThumbUrl() {
        return route("getBrandImageThumb", $this->filename);
    }

}
