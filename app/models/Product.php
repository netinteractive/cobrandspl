<?php

/**
 * Description of Product
 *
 * @author damian
 */
class Product extends Eloquent {

    protected $table = 'products';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('User');
    }

    public function logo() {
        return $this->belongsTo('ProductLogo');
    }

    public function category() {
        return $this->belongsTo('ProductCategory', 'product_category_id');
    }

    public function getFormattedCreatedAtDate() {
        return date("Y-m-d", strtotime($this->created_at));
    }

    public function getFormattedPrice_1() {
        return number_format($this->price_1, 2, ',', ' ') . ' zł';
    }

    public function getFormattedPrice_2() {
        return number_format($this->price_2, 2, ',', ' ') . ' zł';
    }

    public function getFormattedPrice_3() {
        return number_format($this->price_3, 2, ',', ' ') . ' zł';
    }

    public function action() {
        return $this->belongsToMany('Action', 'action_has_branches');
    }

}
