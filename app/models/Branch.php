<?php

/**
 * Description of Branch
 *
 * @author damian
 */
class Branch extends Eloquent {

    protected $table = 'branches';

    public function users() {
        return $this->belongsToMany('User', 'users_has_branches');
    }

    public function brands() {
        return $this->belongsToMany('Brand', 'brand_has_branches');
    }

    public function actions() {
        return $this->hasMany('Action');
    }

    public function getChildren() {
        return $this::where('parent_id', $this->id)->orderBy('id', 'ASC')->get();
    }

}
