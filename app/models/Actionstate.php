<?php

/**
 * Description of Actionstate
 *
 * @author damian
 */
class Actionstate extends Eloquent {

    protected $table = 'actionstates';
    public $timestamps = false;

    public function actions() {
        return $this->hasMany('Action');
    }

}
