<?php

/**
 * Description of Recommendation
 *
 * @author damian
 */
class Recommendation extends Eloquent {

    protected $table = 'recommendations';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('User');
    }

    public function addedUser() {
        return $this->belongsTo('User', 'added_user_id');
    }

    public function action() {
        return $this->belongsTo('Action');
    }

    public function getFormattedCreatedDate() {
        return date("d-m-Y h:i:s", strtotime($this->created_at));
    }

    public function abuses() {
        return $this->hasMany('Abuse');
    }

}
