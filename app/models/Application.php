<?php

/**
 * Description of Application
 *
 * @author damian
 */
class Application extends Eloquent {

    protected $table = 'applications';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('User');
    }

    public function action() {
        return $this->belongsTo('Action');
    }

    public function brand() {
        return $this->belongsTo('Brand');
    }

    public function applicationstate() {
        return $this->belongsTo('Applicationstate');
    }

    public function getFormattedCreatedDate() {
        return date("d-m-Y h:i:s", strtotime($this->created_at));
    }

}
