<?php

/**
 * Description of Category
 *
 * @author damian
 */
class Category extends Eloquent {

    protected $table = 'categories';

    public function actions() {
        return $this->hasMany('Action');
    }

    public function getChildren() {
        return $this::where('parent_id', $this->id)->orderBy('id', 'ASC')->get();
    }
    
    public function getParent() {
        return $this::where('id',$this->parent_id)->first();
    }

}

?>
