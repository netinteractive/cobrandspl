<?php

/**
 * Description of Package
 *
 * @author damian
 */
class Package extends Eloquent {

    protected $table = 'packages';
    public $timestamps = true;

    public function packagetype() {
        return $this->belongsTo('Packagetype');
    }

}
