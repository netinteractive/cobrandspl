<?php

namespace NetInteractive\Cobrands\Utils;
use Log;
/**
 * Description of TimeUtils
 *
 * @author damian
 */
class TimeUtils {

    
    public static function getTimeDiff($date) {
        
        $date = date_create_from_format('Y-m-d H:i:s', $date)->format( 'U' );
        $today = time();
        $remain = $date - $today;
        $d = floor($remain / (24 * 60 * 60));
        $remain_sec = mktime(23, 59, 59);
        $diff = $remain_sec - $today;
        $h = floor($diff / (60 * 60));
        $date_today = date('H:i:s');
        $array = explode(':', $date_today);
        $m = 60 - $array[1];
        $s = 60 - $array[2];
        echo $d . ' dni, ' . $h . ' godzin ' . $m . ' minut.';
    }

}
