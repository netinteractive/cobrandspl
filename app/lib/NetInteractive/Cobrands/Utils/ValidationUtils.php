<?php

namespace NetInteractive\Cobrands\Utils;

/**
 * Description of ValidationUtils
 *
 * @author damian
 */
class ValidationUtils {

    /**
     * Encode html form input
     * @param type $text
     */
    public static function encode($field) {
        return htmlspecialchars($field, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Encode html form input
     * @param type $text
     */
    public static function decode($field) {
        return htmlspecialchars_decode($field, ENT_QUOTES);
    }

}
