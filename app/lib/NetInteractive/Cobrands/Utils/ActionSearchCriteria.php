<?php

namespace NetInteractive\Cobrands\Utils;

/**
 * Description of ActionSearchCriteria
 *
 * @author damian
 */
class ActionSearchCriteria {

    public $category;
    public $district;
    public $sortby;
    public $sorttype;
    public $searchterm;
    
    public $city;
    public $subcategory;
    public $budget_from;
    public $budget_to;
    public $date_from;
    public $date_to;

}
