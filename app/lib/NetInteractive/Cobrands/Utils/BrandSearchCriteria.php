<?php

namespace NetInteractive\Cobrands\Utils;

/**
 * Description of BrandSearchCriteria
 *
 * @author damian
 */
class BrandSearchCriteria {

    public $searchterm;
    public $branch_id;
    public $subbranch_id;

}
