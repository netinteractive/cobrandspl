<?php

namespace NetInteractive\Cobrands\Enums;

/**
 * Description of ActionstateEnum
 *
 * @author damian
 */
abstract class ActionstateEnum {

    const NEW_CREATED = 1;
    const REMOVED = 2;
    const ENDED_AFTER_FIRST_STAGE = 3;
    const SECOND_STAGE_END_WAITING = 4;
    const ENDED_AFTER_SECOND_STAGE = 5;

}
