<?php

namespace NetInteractive\Cobrands\Enums;

/**
 * Description of PackagetypeEnum
 *
 * @author damian
 */
abstract class PackagetypeEnum {

    const SINGLE = 1;
    const MONTHLY = 2;

}
