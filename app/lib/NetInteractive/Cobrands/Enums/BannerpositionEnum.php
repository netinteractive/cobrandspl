<?php

namespace NetInteractive\Cobrands\Enums;

/**
 * Description of BannerpositionEnum
 *
 * @author damian
 */
abstract class BannerpositionEnum {

    const HORIZONTAL = 1;
    const VERTICAL = 2;

}
