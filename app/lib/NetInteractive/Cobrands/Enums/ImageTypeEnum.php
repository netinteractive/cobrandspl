<?php

namespace NetInteractive\Cobrands\Enums;

/**
 * Description of ImageTypeEnum
 *
 * @author damian
 */
abstract class ImageTypeEnum {

    const PRODUCT = 1;
    const BRAND = 2;

}
