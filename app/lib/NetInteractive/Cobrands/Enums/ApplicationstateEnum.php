<?php

namespace NetInteractive\Cobrands\Enums;

/**
 * Description of ApplicationstateEnum
 *
 * @author damian
 */
abstract class ApplicationstateEnum {

    const ACCEPTED = 1;
    const RESERVE = 2;
    const WAITING = 3;

}
