<?php

namespace NetInteractive\Cobrands\Enums;

/**
 * Description of UserTypeEnum
 *
 * @author damian
 */
abstract class UserTypeEnum {

    const REGULAR = 1;
    const PRODUCER = 2;

}
