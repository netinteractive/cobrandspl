<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    "accepted" => "The :attribute must be accepted.",
    "active_url" => "The :attribute is not a valid URL.",
    "after" => "The :attribute must be a date after :date.",
    "alpha" => "The :attribute may only contain letters.",
    "alpha_dash" => "The :attribute may only contain letters, numbers, and dashes.",
    "alpha_num" => "The :attribute may only contain letters and numbers.",
    "array" => "The :attribute must be an array.",
    "before" => "The :attribute must be a date before :date.",
    "between" => array(
        "numeric" => "The :attribute must be between :min and :max.",
        "file" => "The :attribute must be between :min and :max kilobytes.",
        "string" => "The :attribute must be between :min and :max characters.",
        "array" => "The :attribute must have between :min and :max items.",
    ),
    "confirmed" => "The :attribute confirmation does not match.",
    "date" => "The :attribute is not a valid date.",
    "date_format" => "The :attribute does not match the format :format.",
    "different" => "The :attribute and :other must be different.",
    "digits" => "The :attribute must be :digits digits.",
    "digits_between" => "The :attribute must be between :min and :max digits.",
    "email" => "The :attribute must be a valid email address.",
    "exists" => "The selected :attribute is invalid.",
    "image" => "The :attribute must be an image.",
    "in" => "The selected :attribute is invalid.",
    "integer" => "The :attribute must be an integer.",
    "ip" => "The :attribute must be a valid IP address.",
    "max" => array(
        "numeric" => "The :attribute may not be greater than :max.",
        "file" => "The :attribute may not be greater than :max kilobytes.",
        "string" => "The :attribute may not be greater than :max characters.",
        "array" => "The :attribute may not have more than :max items.",
    ),
    "mimes" => "The :attribute must be a file of type: :values.",
    "min" => array(
        "numeric" => "The :attribute must be at least :min.",
        "file" => "The :attribute must be at least :min kilobytes.",
        "string" => "The :attribute must be at least :min characters.",
        "array" => "The :attribute must have at least :min items.",
    ),
    "not_in" => "The selected :attribute is invalid.",
    "numeric" => "The :attribute must be a number.",
    "regex" => "The :attribute format is invalid.",
    "required" => "The :attribute field is required.",
    "required_if" => "The :attribute field is required when :other is :value.",
    "required_with" => "The :attribute field is required when :values is present.",
    "required_with_all" => "The :attribute field is required when :values is present.",
    "required_without" => "The :attribute field is required when :values is not present.",
    "required_without_all" => "The :attribute field is required when none of :values are present.",
    "same" => "The :attribute and :other must match.",
    "size" => array(
        "numeric" => "The :attribute must be :size.",
        "file" => "The :attribute must be :size kilobytes.",
        "string" => "The :attribute must be :size characters.",
        "array" => "The :attribute must contain :size items.",
    ),
    "unique" => "The :attribute has already been taken.",
    "url" => "The :attribute format is invalid.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => array(
        'attribute-name' => array(
            'rule-name' => 'custom-message',
        ),
    ),
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => array(),
    "register" => array(
        'register_companyname_required' => 'Nazwa firmy jest wymagana',
        'register_companyname_min' => 'Minimalna długość nazwy firmy to 2 znaki',
        'register_companyname_max' => 'Maksymalna długość nazwy firmy to 250 znaków',
        'register_companyname_regex' => 'Nazwa firmy zawiera niedozwolone znaki',
        'register_nip_required' => 'Numer NIP jest wymagany',
        'register_nip_min' => 'Numer NIP musi się składać z 10 cyfr',
        'register_nip_max' => 'Numer NIP musi się składać z 10 cyfr',
        'register_nip_regex' => 'Numer NIP musi się składać z 10 cyfr',
        'register_nip_nip' => 'Niepoprawny numer NIP. Musi się składać z 10 cyfr',
        'register_regon_required' => 'Numer REGON jest wymagany',
        'register_regon_min' => 'Minimlana długość numeru REGON to 5 cyfr',
        'register_regon_max' => 'Maksymalna długość numeru REGON to 15 cyfr',
        'register_regon_regex' => 'Numer REGON może zawierać tylko cyfry',
        'register_branches_max' => 'Niepoprawna wartość wybranych branż',
        'register_branches_regex' => 'Niepoprawna wartość wybranych branż',
        'register_street_required' => 'Podanie ulicy jest wymagane',
        'register_street_min' => 'Minimalna długośc nazwy ulicy to 3 znaki',
        'register_street_max' => 'Maksymalna długość nazwy ulicy to 200 znaków',
        'register_street_regex' => 'Nazwa ulicy zawiera niedozwolone znaki',
        'register_postcode_required' => 'Kod pocztowy jest wymagany',
        'register_postcode_min' => 'Minimalna długość kodu pocztowego to 2 znaki',
        'register_postcode_max' => 'Maksymalna długość kodu pocztowego to 200 znaków',
        'register_postcode_regex' => 'Niedozwolone znaki w kodzie pocztowym',
        'register_city_required' => 'Nazwa miasta jest wymagana',
        'register_city_min' => 'Minimalna długośc nazwy miasta to 2 znaki',
        'register_city_max' => 'Maksymalna długość nazwy miasta to 200 znaków',
        'register_city_regex' => 'Nazwa miasta zawiera niedozwolone znaki',
        'register_district_required' => 'Wybranie województwa jest wymagane',
        'register_district_min' => 'Błędne województwo',
        'register_district_max' => 'Błędne województwo',
        'register_district_integer' => 'Błędne województwo',
        'register_companysize_required' => 'Wybranie wielkości firmy jest wymagane',
        'register_companysize_min' => 'Błędna wielkość firmy',
        'register_companysize_max' => 'Błędna wielkość firmy',
        'register_companysize_integer' => 'Błędna wielkość firmy',
        'register_name_required' => 'Podanie imienia jest wymagane',
        'register_name_min' => 'Minimalna długość imienia to 2 znaki',
        'register_name_max' => 'Maksymalna długość imienia to 100 znaków',
        'register_name_alpha' => 'Imię może zawierać tylko litery',
        'register_surnname_required' => 'Podanie nazwiska jest wymagane',
        'register_surnname_min' => 'Minimalna długość nazwiska to 2 znaki',
        'register_surnname_max' => 'Maksymalna długość nawiska to 100 znaków',
        'register_surnname_regex' => 'Nazwisko posiada niedozwolone znaki',
        'register_phone_main_required' => 'Podanie głownego numeru telefonu jest wymagane',
        'register_phone_main_min' => 'Minimalna długość numeru telefonu to 2 znaki',
        'register_phone_main_max' => 'Maksymalna długość numeru telefonu to 45 znaków',
        'register_phone_main_regex' => 'Numer telefonu zawiera niedozwolone znaki',
        'register_phone_alternative_min' => 'Minimalna długość numeru telefonu to 2 znaki',
        'register_phone_alternative_max' => 'Maksymalna długość numeru telefonu to 45 znaków',
        'register_phone_alternative_regex' => 'Numer telefonu zawiera niedozwolone znaki',
        'register_email_required' => 'Podanie adresu e-mail jest wymagane',
        'register_email_email' => 'Niepoprawny adres e-mail',
        'register_email_unique' => 'Podany adres e-mail istnieje już w systemie',
        'register_login_required' => 'Podanie loginu jest wymagane',
        'register_login_alpha_num' => 'Login może zawierać tylko znaki alfanumeryczne',
        'register_login_unique' => 'Podany login już istnieje w systemie',
        'register_login_min' => 'Minimalna długoś loginu to 2 znaki',
        'register_login_max' => 'Maksymalna długośc loginu to 45 znaków',
        'register_pssword_min' => 'Minimalna długość hasła to 5 znaków',
        'register_password_required' => 'Podanie hasła jest wymagane',
        'register_password_confirm_required' => 'Podanie potwierdzenia hasła jest wymagane',
        'register_password_confirm_same' => 'Podane hasła różnią się od siebie',
        'register_description_required' => 'Podanie opisu jest wymagane',
        'register_description_min' => 'Minimalna długość opisu to 10 znaków',
        'register_description_max' => 'Maksymalna długość opisu to 3000 znaków',
        'register_terms_accepted.required' => 'Akceptacja regulaminu jest wymagana'
    )
);
