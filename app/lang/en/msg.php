<?php

return array(
    'home' => array(
        "page_title" => "Cobrands.pl",
        "register_link" => "Zarejestruj się",
        "login_link" => "Zaloguj się",
        "logout_link" => "Wyloguj się"
    ),
    'register' => array(
        "page_title" => "Cobrands.pl - rejestracja",
        "company_data_header" => "Dane firmy",
        "contact_data_header" => "Dane kontaktowe",
        "login_data_header" => "Dane do logowania",
        "companyname_label" => "Nazwa firmy*",
        "nip_label" => "NIP*",
        "regon_label" => "REGON",
        "branches_label" => "Branże* (max. 3):",
        "street_label" => "Ulica*",
        "postcode_city_label" => "Kod i miasto*",
        "district_label" => "Województwo*",
        "companysize_label" => "Wielkość firmy*",
        "contact_person_label" => "Osoba kontaktowa",
        "name_label" => "Imię*",
        "surname_label" => "Nazwisko*",
        "phone_main_label" => "Telefon 1*",
        "phone_alternative_label" => "Telefon 2",
        "email_label" => "E-mail*",
        "login_label" => "Login*",
        "password_label" => "Hasło*",
        "password_confirm_label" => "Powtórz hasło*",
        "description_label" => "Opis działalności firmy*",
        "terms_accepted_label" => "Akceptuję regulamin korzystania z serwisu*",
        "newsletter_label" => "Newsletter",
        "register_button" => "Zarejestruj"
    ),
    'login' => array(
        "page_title" => "Cobrands.pl - logowanie",
        "invalid_credentials" => "Nieprawidłowe dane logowania!",
        "login_required" => "Login jest wymagany!",
        "password_required" => "Hasło jest wymagane!",
        "remember_me_checkbox" => "Zapamiętaj mnie",
        "dont_sign_out_checkbox" => "Nie wylogowuj",
        "login_button" => "Zaloguj"
    ),
    'logout' => array(
        'success' => "Zostałeś prawidłowo wylogowany!",
    )
);
