<?php

return array(
    'home' => array(
        "page_title" => "Cobrands.pl",
        "register_link" => "Zarejestruj się",
        "login_link" => "Zaloguj się",
        "logout_link" => "Wyloguj się",
        "add_action" => "Dodaj akcję"
    ),
    'register' => array(
        "page_title" => "Cobrands.pl - rejestracja",
        "company_data_header" => "Dane firmy",
        "contact_data_header" => "Dane kontaktowe",
        "login_data_header" => "Dane do logowania",
        "companyname_label" => "Nazwa firmy*",
        "nip_label" => "NIP*",
        "regon_label" => "REGON",
        "branches_label" => "Branże* (max. 3):",
        "street_label" => "Ulica*",
        "postcode_city_label" => "Kod i miasto*",
        "district_label" => "Województwo*",
        "companysize_label" => "Wielkość firmy*",
        "contact_person_label" => "Osoba kontaktowa",
        "name_label" => "Osoba kontaktowa*",
        "surname_label" => "Nazwisko*",
        "phone_main_label" => "Telefon 1*",
        "phone_alternative_label" => "Telefon 2",
        "email_label" => "E-mail*",
        "login_label" => "Login*",
        "password_label" => "Hasło*",
        "password_confirm_label" => "Powtórz hasło*",
        "description_label" => "Opis działalności firmy*",
        "terms_accepted_label" => "Akceptuję regulamin korzystania z serwisu*",
        "newsletter_label" => "Newsletter",
        "register_button" => "Zarejestruj"
    ),
    'login' => array(
        "page_title" => "Cobrands.pl - logowanie",
        "invalid_credentials" => "Nieprawidłowe dane logowania!",
        "user_not_accepted" => 'Nie możesz się zalgować gdyż Twój profil jeszcze nie został zweryfikowany!',
        "login_required" => "Login jest wymagany!",
        "password_required" => "Hasło jest wymagane!",
        "remember_me_checkbox" => "Zapamiętaj mnie",
        "dont_sign_out_checkbox" => "Nie wylogowuj",
        "login_button" => "Zaloguj"
    ),
    'logout' => array(
        'success' => "Zostałeś prawidłowo wylogowany!",
    ),
    'add_action' => array(
        'page_title' => "Cobrands.pl - dodaj nową akcję",
        "action_name" => "Nazwa*",
        "action_category" => "Kategoria/Podkategoria*",
        "action_description" => "Opis Akcji*",
        "partners_count" => "Ilość poszukiwanych partnerów*",
        "action_partners_undef" => "nieokreślona",
        "stage_1" => "Etap 1 do:",
        "stage_2" => "Etap 2 do:",
        "partner_aquisition_time" => "Okres pozyskiwania partnerów*",
        "overall_budget" => "Budżet całościowy*",
        "action_date" => "Termin Akcji*"
    ),
    'action' => array(
        'page_title' => 'Cobrands.pl - podgląd akcji'
    ),
    'actions_finished' => array(
        'page_title' => 'Cobrands.pl - akcje zakończone'
    ),
    'profile' => array(
        "invalid_partner" => "Nie znaleziono taiego partnera",
        "partner_saving_error" => "Wystąpił błąd podczas zapisu danych",
        "partner_saving_error" => "Wystąpił błąd podczas usuwania partnera",
        "partner_accepted_successfully" => "Partner został zaakceptowany",
        "partner_rejected_successfully" => "Partner został odrzucony",
        "partner_removed_successfully" => "Partner został usunięty"
    )
);
