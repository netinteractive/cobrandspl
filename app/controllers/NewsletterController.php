<?php

/**
 * Description of NewsletterController
 *
 * @author damian
 */
class NewsletterController extends BaseController {

    public function subscribeToNewsletter() {
        if (Input::has('newsletter_email')) {
            $data = Input::only('newsletter_email');
            $rules = array(
                'newsletter_email' => array('required', 'email', 'unique:mailings,email'),
            );
            $messages = array(
                'newsletter_email.required' => "Aby zapisać się do newslettera wymagane jest podanie adresu e-mail.",
                'newsletter_email.email' => "Wprowadzono niepoprawny e-mail",
                'newsletter_email.unique' => "Podany e-mail jest już zapisany do newslettera"
            );
            $validator = Validator::make($data, $rules, $messages);
            if ($validator->fails()) {
                return Redirect::route('homeView')->withErrors($validator);
            } else if ($validator->passes()) {
                $unsubscription_code = str_random(60);
                Mailing::create(array('email' => Input::get('newsletter_email'), 'unsubscription_code' => $unsubscription_code));
                MailFacade::sendNewsletterSubscriptionEmail(Input::get('newsletter_email'), $unsubscription_code);
                return Redirect::route('homeView')->with('message', "Dziękujemy za zapisanie się do newslettera!");
            }
        } else {
            return Redirect::route('homeView');
        }
    }

    public function unsubscribeFromNewsletter($token) {
        $mailing = Mailing::where('unsubscription_code', $token)->first();
        if ($mailing) {
            $email = $mailing->email;
            if ($mailing->delete()) {
                return Redirect::route('homeView')->with('error', "Podany e-mail " . $email . " został usunięty z newslettera!");
            } else {
                return Redirect::route('homeView')->with('error', "Wypisanie z newslettera nie powiodło się!");
            }
        } else {
            return Redirect::route('homeView')->with('error', "Podano błędny kod wypisania z newslettera!");
        }
    }

}
