<?php

use NetInteractive\Cobrands\Utils\ActionSearchCriteria;
use NetInteractive\Cobrands\Utils\BrandSearchCriteria;

class HomeController extends BaseController {

    public function render() {

        if (Session::has('registrationSuccess')) {
            return View::make('layouts.register.register_success');
        } else {
            $actions = ActionFacade::findActions('all', 'all', 'applications', 'desc', null);
            return View::make('layouts.home')->with('actions', $actions);
        }
    }

    public function search() {
        $search_term = Input::get('search_term');

        if ($search_term == null) {
            $search_term = '';
        }
        $isAdvancedSearch = Input::get('is_advanced_search');
        if ($isAdvancedSearch) {
            $searchType = Input::get('search_type');
            if ($searchType == 1) { // akcje
                $actionSearchCriteria = new ActionSearchCriteria();
                if (Input::get('action_category_id') == 0) {
                    $actionSearchCriteria->category = 'all';
                } else {
                    $actionSearchCriteria->category = Input::get('action_category_id');
                }

                $actionSearchCriteria->subcategory = Input::get('action_subcategory_id');
                $actionSearchCriteria->searchterm = $search_term;
                if (Input::get('search_district') == 0) {
                    $actionSearchCriteria->district = 'all';
                } else {
                    $actionSearchCriteria->district = Input::get('search_district');
                }
                $actionSearchCriteria->city = Input::get('search_city');
                $actionSearchCriteria->budget_from = Input::get('budget_from');
                $actionSearchCriteria->budget_to = Input::get('budget_to');
                $actionSearchCriteria->date_from = Input::get('date_from');
                $actionSearchCriteria->date_to = Input::get('date_to');
                $actionSearchCriteria->sortby = 'latest';
                $actionSearchCriteria->sorttype = 'desc';

                return App::make('ActionController')->actionsListAdvanced($actionSearchCriteria);
            } else if ($searchType == 2) { // brandy
                $brandSearchCriteria = new BrandSearchCriteria();
                $brandSearchCriteria->branch_id = Input::get('branch_id');
                $brandSearchCriteria->subbranch_id = Input::get('subbranch_id');
                $brandSearchCriteria->searchterm = $search_term;

                return App::make('BrandController')->listBrandsAdvanced($brandSearchCriteria);
            }
        } else {
            $category_id = Input::get('search_category_selector');

            if ($category_id == 0) {
                $category_id = 'all';
            }
            return App::make('ActionController')->actionsList($category_id, 'all', 'latest', 'desc', $search_term);
        }
    }

}
