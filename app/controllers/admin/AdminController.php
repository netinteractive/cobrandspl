<?php

/**
 * Description of AdminController
 *
 * @author damian
 */
class AdminController extends BaseController {

    public function render() {
        return View::make('layouts.admin');
    }

}
