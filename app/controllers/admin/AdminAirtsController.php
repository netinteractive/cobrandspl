<?php

class AdminAirtsController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'airts';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $airts = Airt::paginate(10);

        $this->layout->content = View::make('layouts.admin.airts.index')
                ->with('airts', $airts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $this->layout->content = View::make('layouts.admin.airts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make(Input::all(), array(
                    'from' => 'required',
                    'to' => 'required'
                        )
        );

        if ($validator->fails()) {
            return Redirect::to('administrator/airts/create')
                            ->withErrors($validator)
                            ->withInput();
        } else {

            // insert into
            $airt = new Airt;
            $airt->from = Input::get('from');
            $airt->to = Input::get('to');
            $airt->is_active = true;
            $airt->save();

            Session::flash('success-message', 'Przekierowanie zostało dodane!');
            return Redirect::to('administrator/airts');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $airt = Airt::find($id);

        $this->layout->content = View::make('layouts.admin.airts.edit')
                ->with('airt', $airt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), array(
                    'from' => 'required',
                    'to' => 'required'
                        )
        );

        if ($validator->fails()) {
            return Redirect::to('administrator/airts/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // update
            $airt = Airt::find($id);
            $airt->from = Input::get('from');
            $airt->to = Input::get('to');
            $airt->is_active = Input::get('isActive') == '1' ? true : false;
            $airt->save();

            Session::flash('success-message', 'Przekierowanie zostało zaaktualizowane!');
            return Redirect::to('administrator/airts');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $airt = Airt::find($id);
        $airt->delete();

        // redirect
        return Redirect::to('administrator/airts');
    }

    /**
     * Zmienia status isActive
     * 
     * @param int $id
     * @return type
     */
    public function active($id) {
        $newStatus = Input::get('status');
        $airt = Airt::find($id);
        $airt->is_active = $newStatus == '1' ? 1 : 0;
        $airt->save();

        return $airt->is_active ? 1 : 0;
    }

}
