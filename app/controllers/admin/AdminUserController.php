<?php

class AdminUserController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active', 'accept');
    public $curtab = 'users';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $users = User::all();

        $this->layout->content = View::make('layouts.admin.users.index')
                ->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $user = User::find($id);
        $districts = District::all();
        $companysizes = Companysize::all();
        $this->layout->content = View::make('layouts.admin.users.edit')
                ->with('user', $user)->with('districts', $districts)
                ->with('companysizes', $companysizes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'login' => array('required', 'login_unique', 'alpha_num', 'max:45', 'min:2'),
            'companyname' => array('required', 'companyname_unique', 'min:2', 'max:250', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\,\@]+$/'),
            'nip' => array('required', 'min:10', 'max:10', 'regex:/^[0-9]+$/', 'nip'),
            'regon' => array('min:5', 'max:15', 'regex:/^[0-9]+$/'),
            'branches' => array('max:30', 'regex:/^\[[0-9\",]*\]$|/'),
            'street' => array('required', 'min:3', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\/]+$/'),
            'postcode' => array('required', 'min:2', 'max:20', 'regex:/^[A-z0-9\s\-]+$/'),
            'city' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s\.\-]+$/'),
            'district' => array('required', 'min:1', 'max:16', 'integer'),
            'companysize' => array('required', 'min:1', 'max:4', 'integer'),
            'name' => array('required', 'min:2', 'max:100', 'alpha'),
            'surname' => array('required', 'min:2', 'max:100', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\-]+$/'),
            'phone_main' => array('required', 'min:2', 'max:45', 'regex:/^[0-9\s\-\(\)\+]+$/'),
            'phone_alternative' => array('min:2', 'max:45', 'regex:/^[0-9\s\-\(\)\+]+$/'),
            'email' => array('required', 'email', 'email_unique'),
            'description' => array('required', 'min:10', 'max:3000'),
        );
        $messages = array(
            'login.required' => 'Podanie loginu jest wymagane',
            'login.min' => 'Minimalna długość loginu to 2 znaki',
            'login.max' => 'Maksymalna długość loginu to 45 znaków',
            'login.regex' => 'Login zawiera niepoprawne znaki',
            'companyname.required' => Lang::get('validation.register.register_companyname_required'),
            'companyname.min' => Lang::get('validation.register.register_companyname_min'),
            'companyname.max' => Lang::get('validation.register.register_companyname_max'),
            'companyname.regex' => Lang::get('validation.register.register_companyname_regex'),
            'companyname.companyname_unique' => Lang::get('validation.register.register_companyname_unique'),
            'nip.required' => Lang::get('validation.register.register_nip_required'),
            'nip.min' => Lang::get('validation.register.register_nip_min'),
            'nip.max' => Lang::get('validation.register.register_nip_max'),
            'nip.regex' => Lang::get('validation.register.register_nip_regex'),
            'nip.nip' => Lang::get('validation.register.register_nip_nip'),
            'regon.min' => Lang::get('validation.register.register_regon_min'),
            'regon.max' => Lang::get('validation.register.register_regon_max'),
            'regon.regex' => Lang::get('validation.register.register_regon_regex'),
            'branches.max' => Lang::get('validation.register.register_branches_max'),
            'branches.regex' => Lang::get('validation.register.register_branches_regex'),
            'street.required' => Lang::get('validation.register.register_street_required'),
            'street.min' => Lang::get('validation.register.register_street_min'),
            'street.max' => Lang::get('validation.register.register_street_max'),
            'street.regex' => Lang::get('validation.register.register_street_regex'),
            'postcode.required' => Lang::get('validation.register.register_postcode_required'),
            'postcode.min' => Lang::get('validation.register.register_postcode_min'),
            'postcode.max' => Lang::get('validation.register.register_postcode_max'),
            'postcode.regex' => Lang::get('validation.register.register_postcode_regex'),
            'city.required' => Lang::get('validation.register.register_city_required'),
            'city.min' => Lang::get('validation.register.register_city_min'),
            'city.max' => Lang::get('validation.register.register_city_max'),
            'city.regex' => Lang::get('validation.register.register_city_regex'),
            'district.required' => Lang::get('validation.register.register_district_required'),
            'district.min' => Lang::get('validation.register.register_district_min'),
            'district.max' => Lang::get('validation.register.register_district_max'),
            'district.integer' => Lang::get('validation.register.register_district_integer'),
            'companysize.required' => Lang::get('validation.register.register_companysize_required'),
            'companysize.min' => Lang::get('validation.register.register_companysize_min'),
            'companysize.max' => Lang::get('validation.register.register_companysize_max'),
            'companysize.integer' => Lang::get('validation.register.register_companysize_integer'),
            'name.required' => Lang::get('validation.register.register_name_required'),
            'name.min' => Lang::get('validation.register.register_name_min'),
            'name.max' => Lang::get('validation.register.register_name_max'),
            'name.alpha' => Lang::get('validation.register.register_name_alpha'),
            'surnname.required' => Lang::get('validation.register.register_surnname_required'),
            'surnname.min' => Lang::get('validation.register.register_surnname_min'),
            'surnname.max' => Lang::get('validation.register.register_surnname_max'),
            'surnname.regex' => Lang::get('validation.register.register_surnname_regex'),
            'phone_main.required' => Lang::get('validation.register.register_phone_main_required'),
            'phone_main.min' => Lang::get('validation.register.register_phone_main_min'),
            'phone_main.max' => Lang::get('validation.register.register_phone_main_max'),
            'phone_main.regex' => Lang::get('validation.register.register_phone_main_regex'),
            'phone_alternative.min' => Lang::get('validation.register.register_phone_alternative_min'),
            'phone_alternative.max' => Lang::get('validation.register.register_phone_alternative_max'),
            'phone_alternative.regex' => Lang::get('validation.register.register_phone_alternative_regex'),
            'email.required' => Lang::get('validation.register.register_email_required'),
            'email.email' => Lang::get('validation.register.register_email_email'),
            'email.email_unique' => Lang::get('validation.register.register_email_unique'),
            'description.required' => Lang::get('validation.register.register_description_required'),
            'description.min' => Lang::get('validation.register.register_description_min'),
            'description.max' => Lang::get('validation.register.register_description_max')
        );

        Validator::extend('nip', function($attribute, $value, $parameters) {
                    $value = preg_replace("/[^0-9\-]+/", "", $value);
                    if (strlen($value) != 10) {
                        return false;
                    }

                    $arrSteps = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
                    $intSum = 0;
                    for ($i = 0; $i < 9; $i++) {
                        $intSum += $arrSteps[$i] * $value[$i];
                    }
                    $int = $intSum % 11;

                    $intControlNr = ($int == 10) ? 0 : $int;
                    if ($intControlNr == $value[9]) {
                        return true;
                    }
                    return false;
                });

        Validator::extend('companyname_unique', function($attribute, $value, $parameters) use ($id) {
                    $user = User::where('companyname', $value)->where('id', '!=', $id)->get();
                    if ($user->count() > 0) {
                        return false;
                    } else {
                        return true;
                    }
                });

        Validator::extend('login_unique', function($attribute, $value, $parameters) use ($id) {
                    $user = User::where('login', $value)->where('id', '!=', $id)->get();
                    if ($user->count() > 0) {
                        return false;
                    } else {
                        return true;
                    }
                });

        Validator::extend('email_unique', function($attribute, $value, $parameters) use ($id) {
                    $user = User::where('email', $value)->where('id', '!=', $id)->get();
                    if ($user->count() > 0) {
                        return false;
                    } else {
                        return true;
                    }
                });

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::to('administrator/users/' . $id . '/edit')->withErrors($validator)->withInput();
        } else if ($validator->passes()) {
            $newsletter_accepted = false;
            if (Input::has('newsletter_accepted')) {
                if (Input::get('newsletter_accepted') == 'on') {
                    $newsletter_accepted = true;
                }
            }

            $user = User::find($id);
            if (!$user) {
                return Redirect::to('administrator/users/' . $id . '/edit')->with('error-message', 'Błędny użytkownik');
            } else {
                DB::beginTransaction();
                $user->email = Input::get('email');
                $user->companyname = Input::get('companyname');
                $user->name = Input::get('name');
                $user->surname = Input::get('surname');
                $user->street = Input::get('street');
                $user->postcode = Input::get('postcode');
                $user->city = Input::get('city');
                $user->district_id = Input::get('district');
                $user->nip = Input::get('nip');
                $user->regon = Input::get('regon');
                $user->phone_main = Input::get('phone_main');
                $user->phone_alternative = Input::get('phone_alternative');
                $user->description = Input::get('description');
                $user->companysize_id = Input::get('companysize');

                if (!$user->save()) {
                    DB::rollBack();
                    return Redirect::to('administrator/users/' . $id . '/edit')->with('error-message', 'Wystąpił błąd podczas zapisu danych.');
                }

                if ($newsletter_accepted) {
                    $newsletter = Mailing::where('email', $user->email)->get();
                    if (!($newsletter && $newsletter->count())) {
                        if (!Mailing::create(array('email' => $user->email))) {
                            DB::rollBack();
                            return Redirect::to('administrator/users/' . $id . '/edit')->with('error-message', 'Wystąpił błąd podczas zapisu danych.');
                        }
                    }
                } else {
                    $newsletter = Mailing::where('email', $user->email)->first();
                    if ($newsletter) {
                        if (!$newsletter->delete()) {
                            DB::rollBack();
                            return Redirect::to('administrator/users/' . $id . '/edit')->with('error-message', 'Wystąpił błąd podczas zapisu danych.');
                        }
                    }
                }

                /** TODO
                  if (Input::has('edit_branches')) {
                  // usuwanie branż użytkownika
                  if (!DB::table('users_has_branches')->where('user_id', $user->id)->delete()) {
                  DB::rollBack();
                  return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                  }
                  $branches_array = json_decode(Input::get('edit_branches'));
                  foreach ($branches_array as $branch) {
                  if (!DB::table('users_has_branches')->insert(array('user_id' => $user->id, 'branch_id' => intval($branch)))) {
                  DB::rollBack();
                  return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                  }
                  }
                  } else {
                  // usuwanie branż użytkownika
                  if (!DB::table('user_has_branches')->where('user_id', $user->id)->delete()) {
                  DB::rollBack();
                  return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                  }
                  }
                 * */
                DB::commit();
                return Redirect::to('administrator/users/' . $id . '/edit')->with('success-message', 'Dane użytkownika zostały poprawnie zapisane');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $user = User::find($id);
        DB::beginTransaction();
        if ($user->delete()) {
            DB::commit();
            // redirect
            return Redirect::to('administrator/users')->with('success-message', 'Użytkownik został prawidłowo usunięty!');
        } else {
            DB::rollback();
            return Redirect::to('administrator/users')->with('error-message', 'Wystąpił błąd podczas usuwania użytkownika!');
        }
    }

    public function accept($id) {
        $newStatus = Input::get('status');
        $user = User::find($id);
        $user->accepted = $newStatus == '1' ? 1 : 0;
        if ($user->save()) {
            if ($user->accepted) {
                // Wysyłanie maila z potwierdzeniem weryfikacji do użytkownika
                MailFacade::sendAcceptedMessage($user->email, $user->login);
            }
        }

        return $user->accepted ? 1 : 0;
    }

    public function changePassword($id) {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'password' => array('required', 'min:5'),
            'password_confirm' => array('required', 'same:password'),
        );
        $messages = array(
            'password.min' => Lang::get('validation.register.register_password_min'),
            'password.required' => Lang::get('validation.register.register_password_required'),
            'password_confirm.required' => Lang::get('validation.register.register_password_confirm_required'),
            'password_confirm.same' => Lang::get('validation.register.register_password_confirm_same')
        );

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::to('administrator/users/' . $id . '/edit')->withErrors($validator)->withInput();
        } else {
            $user = Sentry::find($id);
            if (!$user) {
                return Redirect::to('administrator/users/' . $id . '/edit')->with('error', 'Błędny użytkownik');
            } else {
                $user->password = Input::get('password');
                if ($user->save()) {
                    return Redirect::to('administrator/users/' . $id . '/edit')->with('success-message', 'Hasło użytkownika zostało zmienione.');
                } else {
                    return Redirect::to('administrator/users/' . $id . '/edit')->with('error-message', 'Wystąpił błąd podczas zmiany hasła użytkownika.');
                }
            }
        }
    }

}
