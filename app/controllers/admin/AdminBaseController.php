<?php

class AdminBaseController extends Controller {

    /**
     * 
     * @var string obecna akcja
     */
    protected $current_action;

    /**
     * Czy obecnie zalogowany user jest adminem
     * 
     * @var boolean
     */
    protected $is_admin = 0;

    /**
     * Lista metod, ktore sa dostepne tylko dla admina(rozwijana w dalszych kontrolerach)
     * 
     * @var array, string 
     */
    protected $admin_actions = array();

    /**
     * Szablon admina
     * 
     * @var string
     */
    protected $admin_layout = 'layouts.admin.admin';

    /**
     * Index strony na ktorej aktualnie sie znajduje user
     * pozwala oznaczyc odpowiednia zakladke w menu jako aktywna
     */
    public $curtab = null;

    /**
     * 
     * @param type $method
     * @param type $parameters
     * @return type
     */
    public function callAction($method, $parameters) {
        $this->current_action = $method;
        return parent::callAction($method, $parameters);
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        /**
         * Nazwa obecnie przetwarzanej akcji
         */
        if (in_array($this->current_action, $this->admin_actions)) {
            if (!$this->is_admin) {
                echo '[PRZEKIEROWANIE DO LOGOWANIA ADMINA]';
                die();
            } else {
                $this->layout = View::make($this->admin_layout);
            }
        } else {
            if (!is_null($this->layout)) {
                $this->layout = View::make($this->layout);
            }
        }

        if (!is_null($this->layout)) {
            $this->layout->content = null;
        }

        View::share('curtab', $this->curtab);
    }

}
