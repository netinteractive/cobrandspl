<?php

class AdminActionsController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'actions';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $actions = Action::all();

        $this->layout->content = View::make('layouts.admin.actions.index')
                ->with('actions', $actions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $action = Action::find($id);
        DB::beginTransaction();
        if ($action->delete()) {
            DB::commit();
            // redirect
            return Redirect::to('administrator/actions')->with('success-message', 'Akcja została prawidłowo usunięta!');
        } else {
            DB::rollback();
            return Redirect::to('administrator/actions')->with('error-message', 'Wystąpił błąd podczas usuwania akcji!');
        }
    }

}
