<?php

class AdminRecommendationsController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'recommendations';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $recommendations = Recommendation::all();

        $this->layout->content = View::make('layouts.admin.recommendations.index')
                ->with('recommendations', $recommendations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $rcm = Recommendation::find($id);

        $this->layout->content = View::make('layouts.admin.recommendations.edit')
                ->with('rcm', $rcm);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), array(
                    'content' => 'required'
                        )
        );
        if ($validator->fails()) {
            return Redirect::to('administrator/recommendations/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // update
            $rcm = Recommendation::find($id);
            $rcm->content = Input::get('content');
            if ($rcm->save()) {
                Session::flash('success-message', 'Treść rekomendacji została zaktualizowana!');
                return Redirect::to('administrator/recommendations');
            } else {
                return Redirect::to('administrator/recommendations')->with('error-message', 'Wystąpił błąd podczas zapisu rekomendacji.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $rcm = Recommendation::find($id);
        if ($rcm->delete()) {
            // redirect
            return Redirect::to('administrator/recommendations')->with('success-message', 'Rekomendacja została prawidłowo usunięta!');
        } else {
            return Redirect::to('administrator/recommendations')->with('error-message', 'Wystąpił błąd podczas usuwania rekomendacji!');
        }
    }

}
