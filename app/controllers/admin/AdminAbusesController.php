<?php

class AdminAbusesController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'abuses';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $abuses = AbuseModel::all();

        $this->layout->content = View::make('layouts.admin.abuses.index')
                ->with('abuses', $abuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $abuse = AbuseModel::find($id);

        $this->layout->content = View::make('layouts.admin.abuses.edit')
                ->with('abuse', $abuse);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), array(
                    'content' => 'required'
                        )
        );
        if ($validator->fails()) {
            return Redirect::to('administrator/abuses/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // update
            $abuse = AbuseModel::find($id);
            $abuse->content = Input::get('content');
            if ($abuse->save()) {
                Session::flash('success-message', 'Treść nadużycia została zaktualizowana!');
                return Redirect::to('administrator/abuses');
            } else {
                return Redirect::to('administrator/abuses')->with('error-message', 'Wystąpił błąd podczas zapisu nadużycia.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $abuse = AbuseModel::find($id);
        if ($abuse->delete()) {
            // redirect
            return Redirect::to('administrator/abuses')->with('success-message', 'Nadużycie zostało prawidłowo usunięte!');
        } else {
            return Redirect::to('administrator/abuses')->with('error-message', 'Wystąpił błąd podczas usuwania nadużycia!');
        }
    }

}
