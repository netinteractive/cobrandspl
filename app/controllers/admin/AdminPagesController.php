<?php

class AdminPagesController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'pages';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $pages = Page::paginate(10);

        $this->layout->content = View::make('layouts.admin.pages.index')
                ->with('pages', $pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $this->layout->content = View::make('layouts.admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make(Input::all(), array(
                    'title' => 'required|max:256',
                    'content' => 'required'
                        )
        );

        if ($validator->fails()) {
            return Redirect::to('administrator/pages/create')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            Page::create(Input::All());

            Session::flash('success-message', 'Strona została dodana!');
            return Redirect::to('administrator/pages');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $view = View::make('layouts.admin.pages.show');

        $page = Page::find($id);

        // jezeli nie znaleziono strony, to wywalamy 404
        if (!$page || !$page->is_active) {
            App::abort(404);
        }
        $view->with('page', $page);

        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $page = Page::find($id);

        $this->layout->content = View::make('layouts.admin.pages.edit')
                ->with('page', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), array(
                    'title' => 'required|max:256',
                    'content' => 'required'
                        )
        );

        if ($validator->fails()) {
            return Redirect::to('administrator/pages/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // update
            $page = Page::find($id);
            $page->title = Input::get('title');
            $page->content = Input::get('content');
            $page->is_active = Input::get('isActive') == '1' ? true : false;
            $page->save();

            Session::flash('success-message', 'Strona została zaaktualizowana!');
            return Redirect::to('administrator/pages');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $page = Page::find($id);
        $page->delete();

        // redirect
        return Redirect::to('administrator/pages');
    }

    /**
     * Zmienia status isActive
     * 
     * @param int $id
     * @return type
     */
    public function active($id) {
        $newStatus = Input::get('status');
        $page = Page::find($id);
        $page->is_active = $newStatus == '1' ? 1 : 0;
        $page->save();

        return $page->is_active ? 1 : 0;
    }

}
