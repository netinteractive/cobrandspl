<?php

class AdminPackagesController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'packages';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $packages = Package::all();

        $this->layout->content = View::make('layouts.admin.packages.index')
                ->with('packages', $packages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $package = Package::find($id);

        $this->layout->content = View::make('layouts.admin.packages.edit')
                ->with('package', $package);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $data = Input::all();
        $rules = array(
            'name' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\,\@]+$/'),
            'price' => array('numeric', 'required')
        );
        $messages = array(
            'name.required' => 'Podanie nazwy pakietu jest wymagane',
            'name.min' => 'Minimalna długość nazwy pakietu to 2 znaki',
            'name.max' => 'Maksymalna długość nazwy pakietu to 200 znaków',
            'name.regex' => 'Nazwa pakietu zawiera niedozwolone znaki',
            'price.numeric' => 'Błędna cena! Wartości podajemy z kropką nie przecinkiem!',
            'price.required' => 'Podanie ceny pakietu jest wymagane'
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('administrator/packages/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // update
            $package = Package::find($id);
            $package->name = Input::get('name');
            $package->price = Input::get('price');
            if ($package->save()) {
                Session::flash('success-message', 'Pakiet został zaktualizowany!');
                return Redirect::to('administrator/packages');
            } else {
                return Redirect::to('administrator/packages')->with('error-message', 'Wystąpił błąd podczas edycji pakietu');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $package = Package::find($id);
        if ($package->delete()) {
            // redirect
            return Redirect::to('administrator/packages')->with('success-message', 'Pakiet został prawidłowo usunięty!');
        } else {
            return Redirect::to('administrator/packages')->with('error-message', 'Wystąpił błąd podczas usuwania pakietu!');
        }
    }

}
