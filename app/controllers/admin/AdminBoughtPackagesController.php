<?php

class AdminBoughtPackagesController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'boughtpackages';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $packageorders = Packageorder::all();

        $this->layout->content = View::make('layouts.admin.boughtpackages.index')
                ->with('packageorders', $packageorders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $po = Packageorder::find($id);

        $this->layout->content = View::make('layouts.admin.boughtpackages.edit')
                ->with('po', $po);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $po = Packageorder::find($id);
        if ($po->delete()) {
            // redirect
            return Redirect::to('administrator/boughtpackages')->with('success-message', 'Zakupiony pakiet został prawidłowo usunięty!');
        } else {
            return Redirect::to('administrator/boughtpackages')->with('error-message', 'Wystąpił błąd podczas usuwania zakupionego pakietu!');
        }
    }

    public function active($id) {
        $newStatus = Input::get('status');
        $po = Packageorder::find($id);
        $po->active = $newStatus == '1' ? 1 : 0;
        $po->save();

        return $po->active ? 1 : 0;
    }

}
