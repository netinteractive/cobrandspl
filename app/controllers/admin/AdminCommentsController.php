<?php

class AdminCommentsController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'comments';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $comments = CommentModel::all();
        $this->layout->content = View::make('layouts.admin.comments.index')
                ->with('comments', $comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $comment = CommentModel::find($id);

        $this->layout->content = View::make('layouts.admin.comments.edit')
                ->with('comment', $comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), array(
                    'content' => 'required'
                        )
        );
        if ($validator->fails()) {
            return Redirect::to('administrator/comments/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // update
            $comment = CommentModel::find($id);
            $comment->content = Input::get('content');
            if ($comment->save()) {
                Session::flash('success-message', 'Treść komentarza została zaktualizowana!');
                return Redirect::to('administrator/comments');
            } else {
                return Redirect::to('administrator/comments')->with('error-message', 'Wystąpił błąd podczas zapisu komentarza.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $comment = CommentModel::find($id);
        if ($comment->delete()) {
            // redirect
            return Redirect::to('administrator/comments')->with('success-message', 'Komentarz został prawidłowo usunięty!');
        } else {
            return Redirect::to('administrator/comments')->with('error-message', 'Wystąpił błąd podczas usuwania komentarza!');
        }
    }

}
