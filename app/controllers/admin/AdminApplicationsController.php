<?php

class AdminApplicationsController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'applications';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $applications = Application::paginate(10);

        $this->layout->content = View::make('layouts.admin.applications.index')
                ->with('applications', $applications);
    }

    public function indexSelected($type) {
        $applications = '';
        if ($type == 'accepted') {
            $applications = Application::where('accepted_by_admin', true)->paginate(10);
        } else if ($type == 'unaccepted') {
            $applications = Application::where('accepted_by_admin', false)->paginate(10);
        } else {
            $applications = Application::paginate(10);
        }

        return View::make('layouts.admin.admin')->with('content', View::make('layouts.admin.applications.index')
                                ->with('applications', $applications));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function accept($id) {
        $newStatus = Input::get('status');
        $app = Application::find($id);
        $app->accepted_by_admin = $newStatus == '1' ? 1 : 0;
        $app->save();

        return $app->accepted_by_admin ? 1 : 0;
    }

}
