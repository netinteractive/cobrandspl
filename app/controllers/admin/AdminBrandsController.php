<?php

/**
 * Description of AdminBrandsController
 *
 * @author damian
 */
class AdminBrandsController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'brands';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $brands = Brand::all();

        $this->layout->content = View::make('layouts.admin.brands.index')
                ->with('brands', $brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $brand = Brand::find($id);
        if (!$brand) {
            App::abort(404);
        } else {
            $this->layout->content = View::make('layouts.admin.brands.edit')
                    ->with('brand', $brand);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        // delete
        $brand = Brand::find($id);
        if ($brand->delete()) {
            // redirect
            return Redirect::to('administrator/brands')->with('success-message', 'Brand został prawidłowo usunięty!');
        } else {
            return Redirect::to('administrator/brands')->with('error-message', 'Wystąpił błąd podczas usuwania brandu!');
        }
    }

}
