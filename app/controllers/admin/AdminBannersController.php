<?php

class AdminBannersController extends AdminBaseController {

    protected $is_admin = 1;
    protected $admin_actions = array('index', 'create', 'edit', 'store', 'active');
    public $curtab = 'banners';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $banners = Banner::all();
        $this->layout->content = View::make('layouts.admin.banners.index')->with('banners', $banners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $banner = Banner::find($id);

        $this->layout->content = View::make('layouts.admin.banners.edit')
                ->with('banner', $banner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), array(
                    'content' => 'required'
                        )
        );
        if ($validator->fails()) {
            return Redirect::to('administrator/banners/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // update
            $banner = Banner::find($id);
            $banner->content = Input::get('content');
            if ($banner->save()) {
                Session::flash('success-message', 'Treść bannera została zaktualizowana!');
                return Redirect::to('administrator/banners');
            } else {
                return Redirect::to('administrator/banners')->with('success-message','Wystąpił błąd podczas zapisu bannera.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
