<?php

/**
 * Description of CategoryController
 *
 * @author damian
 */
class CategoryController extends BaseController {

    public function renderList() {
        $categories = Category::where('is_main', true)->orderBy('id', 'ASC')->get();
        return View::make('layouts.components.categories')
                        ->with('categories', $categories);
    }

}

?>
