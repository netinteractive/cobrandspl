<?php

/**
 * Description of ImageUploadController
 *
 * @author damian
 */
class ImageUploadController extends Controller {

    public function getRegularProductImageFile($filename) {
        return $this->getFile($filename, false, \NetInteractive\Cobrands\Enums\ImageTypeEnum::PRODUCT);
    }

    public function getProductImageThumbFile($filename) {
        return $this->getFile($filename, true, \NetInteractive\Cobrands\Enums\ImageTypeEnum::PRODUCT);
    }
    
    public function getRegularBrandImageFile($filename) {
        return $this->getFile($filename, false, \NetInteractive\Cobrands\Enums\ImageTypeEnum::BRAND);
    }

    public function getBrandImageThumbFile($filename) {
        return $this->getFile($filename, true, \NetInteractive\Cobrands\Enums\ImageTypeEnum::BRAND);
    }

    private function getFile($filename, $isThumb, $type) {
        if ($type == \NetInteractive\Cobrands\Enums\ImageTypeEnum::PRODUCT) {
            $productLogo = ProductLogo::where("filename", $filename)->first();
            if ($productLogo) {
                $path = '';
                if ($isThumb) {
                    $path = Config::get('path.IMAGES_PATH') . $filename . '_thumb.' . $productLogo->filetype;
                } else {
                    $path = Config::get('path.IMAGES_PATH') . $filename . '.' . $productLogo->filetype;
                }

                $content = file_get_contents($path);
                return Response::make($content, 200, array('Content-Type' => 'image/' . $productLogo->filetype, 'Content-Disposition' => 'inline; filename=' . $filename . '.' . $productLogo->filetype));
            } else {
                App::abort(404);
            }
        } else if ($type == \NetInteractive\Cobrands\Enums\ImageTypeEnum::BRAND) {
            $logo = Logo::where("filename", $filename)->first();
            if ($logo) {
                $path = '';
                if ($isThumb) {
                    $path = Config::get('path.IMAGES_PATH') . $filename . '_thumb.' . $logo->filetype;
                } else {
                    $path = Config::get('path.IMAGES_PATH') . $filename . '.' . $logo->filetype;
                }

                $content = file_get_contents($path);
                return Response::make($content, 200, array('Content-Type' => 'image/' . $logo->filetype, 'Content-Disposition' => 'inline; filename=' . $filename . '.' . $logo->filetype));
            } else {
                App::abort(404);
            }
        } else {
            App::abort(404);
        }
    }

    public function uploadProductImage($product_id = null) {
        return $this->uploadImage(\NetInteractive\Cobrands\Enums\ImageTypeEnum::PRODUCT, $product_id);
    }

    public function uploadBrandImage($brand_id = null) {
        return $this->uploadImage(\NetInteractive\Cobrands\Enums\ImageTypeEnum::BRAND, $brand_id);
    }

    private function uploadImage($type, $objectId = null) {
        if ($type == \NetInteractive\Cobrands\Enums\ImageTypeEnum::PRODUCT) {
            $data = Input::all();
            $rules = array(
                'product_image_file' => array('required', 'image', 'max:2000'),
            );
            $messages = array(
                'product_image_file.required' => "Wystąpił błąd! Brak pliku!",
                'product_image_file.image' => "Plik nie jest plikiem gif, jpeg lub png!",
                'product_image_file.max' => "Maksymalny rozmiar pliku to 2MB!"
            );

            $validator = Validator::make($data, $rules, $messages);
            if ($validator->fails()) {
                $response = array(
                    'error' => $validator->messages()->first()
                );
                return Response::json($response);
            } else {

                $file = $data['product_image_file'];
                $mime = $file->getMimeType();
                $fileExtension = '';
                if ($mime == 'image/png') {
                    $fileExtension = 'png';
                } else if ($mime == 'image/jpeg') {
                    $fileExtension = 'jpeg';
                } else if ($mime == 'image/gif') {
                    $fileExtension = 'gif';
                } else {
                    $response = array(
                        'error' => 'Nierozpoznano rodzaju pliku!'
                    );
                    return Response::json($response);
                }
                $shortname = Str::random(30);
                $filename = $shortname . '.' . $fileExtension;
                $thumbname = $shortname . '_thumb' . '.' . $fileExtension;
                // Tworzenie pliku o standardowych wymiarach
                $image_standard = Image::make(Input::file('product_image_file'))->fit(150, 150)->save(Config::get('path.IMAGES_PATH') . $filename, 90);
                // Tworzenie thumba
                $image_thumb = $image_standard->fit(75, 75)->save(Config::get('path.IMAGES_PATH') . $thumbname, 90);

                if ($image_standard && $image_thumb) {
                    DB::beginTransaction();
                    $productLogo = new ProductLogo();
                    $productLogo->filename = $shortname;
                    $productLogo->filetype = $fileExtension;
                    if ($productLogo->save()) {
                        if ($objectId != null) {
                            $product = Product::find($objectId);
                            if ($product && (($product->user_id == Sentry::getUser()->id) || (Sentry::getUser()->inGroup(Sentry::findGroupByName('GROUP_ADMIN'))))) {
                                $product->logo_id = $productLogo->id;
                                if ($product->save()) {
                                    DB::commit();
                                } else {
                                    DB::rollBack();
                                    $response = array(
                                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                                    );
                                    return Response::json($response);
                                }
                            } else {
                                DB::rollBack();
                                $response = array(
                                    'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                                );
                                return Response::json($response);
                            }
                        } else {
                            DB::commit();
                        }

                        Session::put('product_image_url', $productLogo->getUrl());
                        $response = array(
                            'success' => 'success',
                            'product_image_id' => $productLogo->id,
                            'product_image_url' => $productLogo->getUrl()
                        );
                        return Response::json($response);
                    } else {
                        DB::rollBack();
                        $response = array(
                            'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                        );
                        return Response::json($response);
                    }
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                    );
                    return Response::json($response);
                }
            }
        } else if ($type == \NetInteractive\Cobrands\Enums\ImageTypeEnum::BRAND) {
            $data = Input::all();
            $rules = array(
                'brand_image_file' => array('required', 'image', 'max:2000'),
            );
            $messages = array(
                'brand_image_file.required' => "Wystąpił błąd! Brak pliku!",
                'brand_image_file.image' => "Plik nie jest plikiem gif, jpeg lub png!",
                'brand_image_file.max' => "Maksymalny rozmiar pliku to 2MB!"
            );

            $validator = Validator::make($data, $rules, $messages);
            if ($validator->fails()) {
                $response = array(
                    'error' => $validator->messages()->first()
                );
                return Response::json($response);
            } else {

                $file = $data['brand_image_file'];
                $mime = $file->getMimeType();
                $fileExtension = '';
                if ($mime == 'image/png') {
                    $fileExtension = 'png';
                } else if ($mime == 'image/jpeg') {
                    $fileExtension = 'jpeg';
                } else if ($mime == 'image/gif') {
                    $fileExtension = 'gif';
                } else {
                    $response = array(
                        'error' => 'Nierozpoznano rodzaju pliku!'
                    );
                    return Response::json($response);
                }
                $shortname = Str::random(30);
                $filename = $shortname . '.' . $fileExtension;
                $thumbname = $shortname . '_thumb' . '.' . $fileExtension;
                // Tworzenie pliku o standardowych wymiarach
                $image_standard = Image::make(Input::file('brand_image_file'))->fit(150, 150)->save(Config::get('path.IMAGES_PATH') . $filename, 90);
                // Tworzenie thumba
                $image_thumb = $image_standard->fit(75, 75)->save(Config::get('path.IMAGES_PATH') . $thumbname, 90);

                if ($image_standard && $image_thumb) {
                    DB::beginTransaction();
                    $brandLogo = new Logo();
                    $brandLogo->filename = $shortname;
                    $brandLogo->filetype = $fileExtension;
                    if ($brandLogo->save()) {
                        if ($objectId != null) {
                            $brand = Brand::find($objectId);
                            if ($brand && (($brand->user_id == Sentry::getUser()->id) || Sentry::getUser()->inGroup(Sentry::findGroupByName('GROUP_ADMIN')))) {
                                $brand->logo_id = $brandLogo->id;
                                if ($brand->save()) {
                                    DB::commit();
                                } else {
                                    DB::rollBack();
                                    $response = array(
                                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                                    );
                                    return Response::json($response);
                                }
                            } else {
                                DB::rollBack();
                                $response = array(
                                    'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                                );
                                return Response::json($response);
                            }
                        } else {
                            DB::commit();
                        }

                        Session::put('brand_image_url', $brandLogo->getUrl());
                        $response = array(
                            'success' => 'success',
                            'brand_image_id' => $brandLogo->id,
                            'brand_image_url' => $brandLogo->getUrl()
                        );
                        return Response::json($response);
                    } else {
                        DB::rollBack();
                        $response = array(
                            'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                        );
                        return Response::json($response);
                    }
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                    );
                    return Response::json($response);
                }
            }
        } else {
            $response = array(
                'error' => 'Wystąpił błąd podczas zapisywania pliku!'
            );
            return Response::json($response);
        }
    }

}
