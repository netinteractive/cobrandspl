<?php

/**
 * Description of AbuseController
 *
 * @author damian
 */
class AbuseController extends BaseController {

    /**
     * Dodawanie nadżycia do rekomendacji
     */
    public function add() {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'recommendation_id' => array('required', 'integer'),
            'abuse_content' => array('required', 'min:10', 'max:3000',)
        );
        $messages = array(
            'recommendation_id.required' => 'Niepoprawny identyfikator rekomendacji',
            'recommendation_id.integer' => 'Niepoprawny identyfikator rekomendacji',
            'abuse_content.required' => 'Treść nadużycia jest wymagana',
            'abuse_content.min' => 'Minimalna długość treści nadużycia to 10 znaków',
            'abuse_content.max' => 'Maksymalna długość treści nadużycia to 3000 znaków',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::route('myProfileView')->withErrors($validator)->with('showModal', Input::get('recommendation_id'))->withInput();
        } else {
            // sprawdzamy czy użytkownik może dodać nadużycie do tej rekomendacji
            if (AbuseFacade::isAbuseAllowedForRecommendation(Input::get('recommendation_id'))) {
                $abuse = new AbuseModel();
                $abuse->recommendation_id = Input::get('recommendation_id');
                $abuse->content = Input::get('abuse_content');

                if ($abuse->save()) {
                    return Redirect::route('myProfileView')->with('message', 'Nadużycie zostało wysłane.');
                } else {
                    return Redirect::route('myProfileView')->with('error', 'Wystąpił błąd podczas dodawania rekomendacji. Spróbuj ponownie później.');
                }
            } else {
                return Redirect::route('myProfileView')->with('error', 'Nie mozna dodać nadużycia do tej rekomendacji.');
            }
        }
    }

}
