<?php

use NetInteractive\Cobrands\Utils\BrandSearchCriteria;

/**
 * Description of BrandController
 *
 * @author damian
 */
class BrandController extends Controller {

    /**
     * Dodawanie branda
     * Brandy dodane podczas rejestracji najpierw są przypisywane do usera o id=1
     * i są nieaktywne. Dopiero po prawidłowej rejestracji są przypisywane do nowo
     * zarejestrowanego id
     */
    public function addBrand() {
        $data = Input::all();
        $rules = array(
            'image_id' => array('required', 'integer'),
            'brand_name' => array('required', 'unique:brands,name', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\,\@]+$/'),
            'brand_branches' => array('max:30', 'regex:/^\[[0-9\",]*\]$|/', 'required'),
            'description' => array('required', 'min:10', 'max:4000'),
        );
        $messages = array(
            'image_id.required' => 'Dodanie logo brandu jest wymagane',
            'image_id.integer' => 'Błędny identyfikator loga brandu',
            'brand_name.required' => 'Podanie nzwy brandu jest wymagane',
            'brand_name.unique' => 'Brand o takiej nazwie już istnieje',
            'brand_name.min' => 'Minimalna długość nazwy brandu to 2 znaki',
            'brand_name.max' => 'Maksymalna długość nazwy brandu to 200 znaków',
            'brand_name.regex' => 'Nazwa brandu zawiera niedozwolone znaki',
            'brand_branches.max' => 'Błędnie wybrane branże',
            'brand_branches.regex' => 'Błędnie wybrane branże',
            'brand_branches.required' => 'Wymagane jest wybranie przynajmniej jednej branży',
            'description.required' => 'Podanie opisu brandu jest wymagana',
            'description.min' => 'Minimalna długość opisu brandu to 10 znaków',
            'description.max' => 'Maksymalna długość opisu brandu to 4000 znaków',
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $response = array(
                'error' => $validator->messages()
            );
            return Response::json($response);
        } else {
            // rodzaj: czy rejestracja, czy dodawanie własnego brandu
            $addType = strip_tags(Input::get('add_type'));

            // tworzenie brandu
            $brand = new Brand();
            $brand->name = strip_tags(Input::get('brand_name'));
            $brand->logo_id = strip_tags(Input::get('image_id'));
            $brand->accepted = true;
            if ($addType == 'register') {
                $brand->user_id = 1; // 'niczyje' brandy przypisujemy najpierw do admina
                $brand->active = false;
            } else if ($addType == 'signedin' && Sentry::check()) {
                $brand->user_id = Sentry::getUser()->id;
                $brand->active = true;
            } else {
                $response = array(
                    'error' => array('error' => 'Nieprawidłowy identyfikator użytkownika.')
                );
                return Response::json($response);
            }

            $brand->description = NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('description'));

            if ($brand->save()) {
                // przypisanie branż do brandu
                if (Input::has('brand_branches')) {
                    $branches_array = json_decode(Input::get('brand_branches'));
                    foreach ($branches_array as $branch) {
                        $brand->branches()->attach(intval($branch));
                    }
                }

                if ($addType == 'register') {
                    // zapisujemy ten brand do sesji użytkownika
                    $this->storeBrandInSession($brand);
                    $this->forgetBrandImage();
                    $response = array(
                        'success' => 'Brand został prawidłowo dodany',
                        'brand_id' => $brand->id,
                        'brand_name' => $brand->name,
                        'brands_list_html' => View::make('layouts.register.brands_list')->render(),
                        'type' => 'register'
                    );
                    return Response::json($response);
                } else if ($addType == 'signedin') {
                    $this->forgetBrandImage();
                    $response = array(
                        'success' => 'Brand został prawidłowo dodany',
                        'type' => 'signedin',
                        'brands_list_html' => View::make('layouts.profile.components.brands_list')->render(),
                        'action_brands_list_html' => App::make('ActionController')->renderActionBrandsRadioList(Sentry::getUser(), Input::old('action_selected_brand'))
                    );
                    return Response::json($response);
                } else {
                    $response = array(
                        'error' => array('error' => 'Wystąpił błąd podczas zapisu brandu. Spróbuj ponownie później.')
                    );
                    return Response::json($response);
                }
            } else {
                $response = array(
                    'error' => array('error' => 'Wystąpił błąd podczas zapisu brandu. Spróbuj ponownie później.')
                );
                return Response::json($response);
            }
        }
    }

    /**
     * Zapisywanie brandu
     * @param type $brand_id
     */
    public function saveBrand($brand_id) {
        $brand = Brand::where('accepted', true)->where('active', true)->where('id', $brand_id)->first();
        if (!$brand) {
            App::abort(404);
        }
        if ($brand->user_id == Sentry::getUser()->id || (Sentry::getUser()->inGroup(Sentry::findGroupByName('GROUP_ADMIN')))) {
            
        } else {
            App::abort(404);
        }

        $data = Input::all();
        $rules = array(
            'brand_name' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\,\@]+$/'),
            'brand_branches' => array('max:30', 'regex:/^\[[0-9\",]*\]$|/', 'required'),
            'description' => array('required', 'min:10', 'max:4000'),
        );
        $messages = array(
            'brand_name.required' => 'Podanie nzwy brandu jest wymagane',
            'brand_name.min' => 'Minimalna długość nazwy brandu to 2 znaki',
            'brand_name.max' => 'Maksymalna długość nazwy brandu to 200 znaków',
            'brand_name.regex' => 'Nazwa brandu zawiera niedozwolone znaki',
            'brand_branches.max' => 'Błędnie wybrane branże',
            'brand_branches.regex' => 'Błędnie wybrane branże',
            'brand_branches.required' => 'Wymagane jest wybranie przynajmniej jednej branży',
            'description.required' => 'Podanie opisu brandu jest wymagana',
            'description.min' => 'Minimalna długość opisu brandu to 10 znaków',
            'description.max' => 'Maksymalna długość opisu brandu to 4000 znaków',
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $response = array(
                'error' => $validator->messages()
            );
            return Response::json($response);
        } else {
            $brand->name = strip_tags(Input::get('brand_name'));

            $brand->description = NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('description'));

            if ($brand->save()) {

                // usuwanie branż brandu
                if (!DB::table('brand_has_branches')->where('brand_id', $brand->id)->delete()) {
                    DB::rollBack();
                    $response = array(
                        'error' => array('error' => 'Wystąpił błąd podczas zapisu brandu. Spróbuj ponownie później.')
                    );
                }
                $branches_array = json_decode(Input::get('brand_branches'));
                foreach ($branches_array as $branch) {
                    if (!DB::table('brand_has_branches')->insert(array('brand_id' => $brand->id, 'branch_id' => intval($branch)))) {
                        DB::rollBack();
                        $response = array(
                            'error' => array('error' => 'Wystąpił błąd podczas zapisu brandu. Spróbuj ponownie później.')
                        );
                    }
                }

                $response = array(
                    'success' => 'Brand został prawidłowo zapisany'
                );
                return Response::json($response);
            } else {
                $response = array(
                    'error' => array('error' => 'Wystąpił błąd podczas zapisu brandu. Spróbuj ponownie później.')
                );
                return Response::json($response);
            }
        }
    }

    /**
     * Podgląd brandu
     * @param type $brand_id
     */
    public function show($brand_id) {
        $brand = Brand::where('accepted', true)->where('active', true)->where('id', $brand_id)->first();
        if ($brand && !Request::ajax()) {
            return View::make('layouts.brands.show')->with('brand', $brand);
        } else if (Request::ajax() && $brand && Sentry::check() && ($brand->user_id == Sentry::getUser()->id || (Sentry::getUser()->inGroup(Sentry::findGroupByName('GROUP_ADMIN'))) )) {
            $branchIds = array();
            $branchNames = array();
            foreach ($brand->branches as $brandBranch) {
                array_push($branchIds, $brandBranch->id);
                array_push($branchNames, $brandBranch->name);
            }

            $response = array(
                'brand_id' => $brand->id,
                'logo_id' => $brand->logo->id,
                'logo_url' => $brand->logo->getUrl(),
                'branch_ids' => $branchIds,
                'branch_names' => $branchNames,
                'name' => $brand->name,
                'description' => \NetInteractive\Cobrands\Utils\ValidationUtils::decode($brand->description)
            );
            return Response::json($response);
        } else {
            App::abort(404);
        }
    }
    
    public function showActions($brand_id) {
        $brand = Brand::where('accepted', true)->where('active', true)->where('id', $brand_id)->first();
        if ($brand ) {
            $actions = Action::where('brand_id',$brand->id)->paginate(2);
            return View::make('layouts.actions_list')->with('actions', $actions);
        } else {
            App::abort(404);
        }
    }

    /**
     * Zapisuje brand w sesji
     * @param Brand $brand
     */
    private function storeBrandInSession($brand) {
        if (Session::has('register_brands')) {
            $register_brands = Session::get('register_brands');
            array_push($register_brands, $brand);
            Session::put('register_brands', $register_brands);
        } else {
            $register_brands = array();
            $register_brands[0] = $brand;
            Session::put('register_brands', $register_brands);
        }
    }

    private function forgetBrandImage() {
        Session::forget('brand_image_url');
    }

    /**
     * Usuwanie brandu z formularza rejestracji
     * @param type $brand_id
     */
    public function removeBrandFromRegistrationForm($brand_id) {
        if ($this->removeBrandFromSession($brand_id)) {
            // sprawdzamy czy taki brand może zostać usunięty z formularza
            // rejestracji
            $brand = Brand::where('id', $brand_id)->where('active', false)->where('user_id', 1)->first();
            if ($brand) {
                if ($brand->delete()) {
                    $response = array(
                        'success' => 'Brand został prawidłowo usunięty',
                        'brands_list_html' => View::make('layouts.register.brands_list')->render()
                    );
                    return Response::json($response);
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas usuwania brandu'
                    );
                    return Response::json($response);
                }
            } else {
                $response = array(
                    'error' => 'Taki brand nie istnieje'
                );
                return Response::json($response);
            }
        } else {
            $response = array(
                'error' => 'Taki brand nie istnieje'
            );
            return Response::json($response);
        }
    }

    /**
     * Usuwanie brandu z sesji
     * @param type $brand_id id brandu
     * @return boolean czy usunięto czy też nie
     */
    public function removeBrandFromSession($brand_id) {
        if (Session::has('register_brands')) {
            $register_brands = Session::get('register_brands');
            $temp = array();
            foreach ($register_brands as $rb) {
                if ($rb->id != $brand_id) {
                    array_push($temp, $rb);
                }
            }
            Session::put('register_brands', $temp);
            return true;
        } else {
            return false;
        }
    }

    public function removeBrand($brand_id) {
        $brand = Brand::find($brand_id);
        if ($brand) {
            if ($brand->user_id != Sentry::getUser()->id) {
                App::abort(404);
            } else {
                if ($brand->delete()) {
                    $response = array(
                        'success' => 'Brand został prawidłowo usunięty',
                        'brands_list_html' => View::make('layouts.profile.components.brands_list')->render()
                    );
                    return Response::json($response);
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas usuwania brandu'
                    );
                    return Response::json($response);
                }
            }
        } else {
            App::abort(404);
        }
    }

    public function listBrands() {
        $brands = Brand::where('accepted', true)->where('active', true)->paginate(2);
        return View::make('layouts.brands.brands_list')->with('brands', $brands);
    }

    public function listBrandsAdvanced(BrandSearchCriteria $brandSearchCriteria) {
        $brands = Brand::where('accepted', true)->where('active', true);

        if ($brandSearchCriteria->branch_id != 0) {
            if ($brandSearchCriteria->subbranch_id != 0) {
                $subbranch_id = $brandSearchCriteria->subbranch_id;
                $brands->whereHas('branches', function($q) use ($subbranch_id) {
                    $q->where('branch_id', $subbranch_id);
                });
            } else {
                $branch_id = $brandSearchCriteria->branch_id;
                $brands->whereHas('branches', function($q) use ($branch_id) {
                    $q->whereRaw("branch_id IN (SELECT id FROM branches WHERE parent_id=$branch_id)");
                });
                //$brands->whereRaw("category_id IN (SELECT id FROM categories WHERE parent_id=$category_id)");
            }
        }
        
        if ($brandSearchCriteria->searchterm != null) {
            $brands->whereRaw("((brands.name LIKE '%$brandSearchCriteria->searchterm%') OR (brands.description LIKE '%$brandSearchCriteria->searchterm%'))");
        }

        $brands = $brands->paginate(2);
        return View::make('layouts.brands.brands_list')->with('brands', $brands);
    }

}
