<?php

/**
 * Description of BranchCategoryController
 *
 * @author damian
 */
class BranchCategoryController extends Controller {

    public function renderMainBranchesList($branch_id) {
        return View::make('layouts.components.brand_branches.main_list')->with('branch_id', $branch_id);
    }

    public function renderSubBranchesList($mainBranchId, $subbranchId = null) {
        return View::make('layouts.components.brand_branches.sub_list')
                        ->with('mainBranchId', $mainBranchId)
                        ->with('subbranchId', $subbranchId);
    }

    public function renderWholeList() {
        return View::make('layouts.components.brand_branches.category_selector');
    }

}
