<?php

/**
 * Description of LibraryController
 *
 * @author damian
 */
class LibraryController extends BaseController {

    public function renderLibrarySelector() {
        // znajdujemy wszsytkie nadkategorie dostępnych produktów
        $productCategories = ProductCategory::whereRaw('id IN (select distinct pc.parent_id from product_categories as pc
right join products as p on p.product_category_id=pc.id and p.active=true
order by pc.parent_id ASC)')->get();
        return View::make('layouts.library.init_list')
                        ->with('productCategories', $productCategories);
    }

    public function renderOnCategorySelection($categoryId) {
        $category = ProductCategory::find($categoryId);
        if ($category) {
            $subCategories = ProductCategory::whereRaw("id IN (select distinct pc.id from product_categories as pc
inner join products as p on p.product_category_id=pc.id and p.active=true and pc.parent_id=$categoryId
order by pc.parent_id ASC)")->get();
            ;
            $products = Product::whereRaw("id IN (select distinct p.id from products as p
inner join product_categories as pc on pc.id=p.product_category_id and pc.parent_id=$categoryId)")->where('active', true)->get();
            $response = array(
                'subcategory_selector' => View::make('layouts.library.subcategory_list')->with('subCategories', $subCategories)->render(),
                'product_selector' => View::make('layouts.library.products_list')->with('products', $products)->render()
            );
            return Response::json($response);
        } else {
            App::abort(404);
        }
    }

    public function renderOnSubcategorySelection($subcategoryId) {
        $category = ProductCategory::find($subcategoryId);
        if ($category) {

            $products = Product::where('product_category_id', $category->id)->where('active', true)->get();
            $response = array(
                'product_selector' => View::make('layouts.library.products_list')->with('products', $products)->render()
            );
            return Response::json($response);
        } else {
            App::abort(404);
        }
    }

}
