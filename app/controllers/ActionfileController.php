<?php

/**
 * Description of ActionfileController
 *
 * @author damian
 */
class ActionfileController extends BaseController {

    public function getRegularFile($filename) {
        return $this->getFile($filename, false);
    }

    public function getThumbFile($filename) {
        return $this->getFile($filename, true);
    }

    private function getFile($filename, $isThumb) {
        $actionfile = Actionfile::where('filename', $filename)->first();
        if ($actionfile) {
            $path = '';
            // jeśli jest to plik graficzny
            if ($actionfile->filetype == 'png' || $actionfile->filetype == 'jpeg' || $actionfile->filetype == 'gif') {
                if ($isThumb) {
                    $path = Config::get('path.ACTIONFILES_PATH') . $filename . '_thumb.' . $actionfile->filetype;
                } else {
                    $path = Config::get('path.ACTIONFILES_PATH') . $filename . '.' . $actionfile->filetype;
                }

                $content = file_get_contents($path);
                return Response::make($content, 200, array('Content-Type' => "$actionfile->mime", 'Content-Disposition' => 'inline; filename=' . $actionfile->original_filename));
            } else {
                // jeśli jest to dokument
                if ($isThumb) {
                    if ($actionfile->filetype == 'doc' || $actionfile->filetype == 'docx') {
                        $path = Config::get('path.ACTIONFILES_PATH') . 'doc_icon.png';
                    } else if ($actionfile->filetype == 'xls' || $actionfile->filetype == 'xlsx') {
                        $path = Config::get('path.ACTIONFILES_PATH') . 'xls_icon.png';
                    } else {
                        $path = Config::get('path.ACTIONFILES_PATH') . 'pdf_icon.png';
                    }

                    $content = file_get_contents($path);
                    return Response::make($content, 200, array('Content-Type' => "$actionfile->mime", 'Content-Disposition' => 'inline; filename=' . $actionfile->original_filename));
                } else {
                    $path = Config::get('path.ACTIONFILES_PATH') . $filename . '.' . $actionfile->filetype;
                    $content = file_get_contents($path);
                    return Response::make($content, 200, array('Content-Type' => "$actionfile->mime", 'Content-Disposition' => 'inline; filename=' . $actionfile->original_filename));
                }
            }
        } else {
            App::abort(404);
        }
    }

    /**
     * Podczas dodawania akcji usuwa dodany plik z sesji
     * @return type
     */
    public function removeFileFromSession() {
        if (Input::has('actionfile_id')) {
            if (Session::has('action_files')) {
                $action_file_id = Input::get('actionfile_id');
                $action_files = Session::get('action_files');
                $temp = array();
                foreach ($action_files as $af) {
                    if ($af->id != $action_file_id) {
                        array_push($temp, $af);
                    }
                }
                Session::put('action_files', $temp);
                $response = array(
                    'success' => 'success',
                    'html' => View::make('layouts.actions.add.action_files')->render()
                );
                return Response::json($response);
            } else {
                $response = array(
                    'error' => 'Wystąpił błąd podczas usuwania pliku!'
                );
                return Response::json($response);
            }
        } else {
            $response = array(
                'error' => 'Wystąpił błąd podczas usuwania pliku!'
            );
            return Response::json($response);
        }
    }

    /**
     * Usuwanie pliku
     * @param type $action_id
     */
    public function removeFile($action_id) {
        if (Input::has('actionfile_id')) {
            $action = Action::find($action_id);
            if ($action) {
                if ($action->user_id == Sentry::getUser()->id) {
                    $action_file_id = Input::get('actionfile_id');
                    $actionfile = Actionfile::find($action_file_id);
                    if ($actionfile->delete()) {
                        $response = array(
                            'success' => 'success',
                            'html' => View::make('layouts.actions.show.action_files')->with('action',$action)->render()
                        );
                        return Response::json($response);
                    } else {
                        $response = array(
                            'error' => 'Usuwanie pliku się nie powiodło!'
                        );
                        return Response::json($response);
                    }
                } else {
                    $response = array(
                        'error' => 'Nie masz uprawnień do tej akcji!'
                    );
                    return Response::json($response);
                }
            } else {
                $response = array(
                    'error' => 'Akcja nie istnieje!'
                );
                return Response::json($response);
            }
        } else {
            $response = array(
                'error' => 'Wystąpił błąd podczas usuwania pliku!'
            );
            return Response::json($response);
        }
    }

}
