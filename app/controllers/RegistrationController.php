<?php

/**
 * Description of RegistrationController
 *
 * @author damian
 */
class RegistrationController extends BaseController {

    public function render() {
        $districts_list = District::lists('name', 'id');
        $companysizes_list = Companysize::lists('name', 'id');
        return View::make('layouts.register.register')
                        ->with('districts_list', $districts_list)
                        ->with('companysizes_list', $companysizes_list)
                        ->with('producer', false);
    }

    public function renderProducerRegisterView() {
        $districts_list = District::lists('name', 'id');
        $companysizes_list = Companysize::lists('name', 'id');
        return View::make('layouts.register.register')
                        ->with('districts_list', $districts_list)
                        ->with('companysizes_list', $companysizes_list)
                        ->with('producer', true);
    }

    /**
     * Generowanie captchy dla pobierania danych firmy na podstawie
     * NIPu na stronie rejestracji
     */
    public function renderCaptchaNip() {
        if (!Session::has('nipHash')) {
            $hash = str_random(20);
            Session::put('nipHash', $hash);
        }
        $cookie_file = Config::get('path.CAPTCHA_COOKIE_PATH') . Session::get('nipHash') . '.txt';
        file_put_contents($cookie_file, ' ');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://stat.gov.pl/regon/');
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        curl_exec($ch);
        curl_setopt($ch, CURLOPT_URL, 'http://stat.gov.pl/regon/Captcha.jpg');
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);

        $store = curl_exec($ch);
        header('Content-Type: image/jpeg');
        header('Content-Length: ' . strlen($store));
        echo $store;
    }

    /**
     * Pobiera informacje o firmie na podstawie NIPu
     */
    public function retrieveCompanyDetailsByNip() {
        $captcha_input = strip_tags(Input::get('captcha_input'));
        $nip = strip_tags(Input::get('nip'));

        if (!Session::has('nipHash')) {
            $hash = str_random(10);
            Session::put('nipHash', $hash);
        }
        $cookie_file = Config::get('path.CAPTCHA_COOKIE_PATH') . Session::get('nipHash') . '.txt';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://stat.gov.pl/regon/');
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'queryTypeRBSet=1nip&' . urlencode($captcha_input) . '11=' . urlencode($nip) . '&verifCodeTF=' . urlencode($captcha_input));

        $store = curl_exec($ch);
        $msg = '';

        if (preg_match('/<table id="dbResponse01T" >(.*)<\/table>/', $store, $match)) {
            $dom = new DOMDocument("1.0", "utf-8");
            $msg = '<meta content="text/html; charset=UTF-8" http-equiv="content-type" />' . $match[0];
            @$dom->loadHTML($msg);
            //the table by its tag name
            $tables = $dom->getElementsByTagName('table');
            $firstTableRows = $tables->item(0)->getElementsByTagName('tr');
            $regon = $firstTableRows->item(0)->getElementsByTagName('td')->item(1)->nodeValue;
            $nazwa = $firstTableRows->item(2)->getElementsByTagName('td')->item(1)->nodeValue;

            $secondTableRows = $tables->item(1)->getElementsByTagName('tr');
            $wojewodztwo = mb_strtolower(trim($secondTableRows->item(0)->getElementsByTagName('td')->item(1)->nodeValue));
            $ulica = trim(preg_replace('/,[^,]*$/', '', $secondTableRows->item(3)->getElementsByTagName('td')->item(1)->nodeValue));
            $poczta_miasto = explode(' ', trim($secondTableRows->item(4)->getElementsByTagName('td')->item(1)->nodeValue), 2);

            if ($wojewodztwo == 'dolnośląskie') {
                $wojewodztwo = 1;
            } else if ($wojewodztwo == 'kujawsko-pomorskie') {
                $wojewodztwo = 2;
            } else if ($wojewodztwo == 'lubelskie') {
                $wojewodztwo = 3;
            } else if ($wojewodztwo == 'lubuskie') {
                $wojewodztwo = 4;
            } else if ($wojewodztwo == 'łódzkie') {
                $wojewodztwo = 5;
            } else if ($wojewodztwo == 'małopolskie') {
                $wojewodztwo = 6;
            } else if ($wojewodztwo == 'mazowieckie') {
                $wojewodztwo = 7;
            } else if ($wojewodztwo == 'opolskie') {
                $wojewodztwo = 8;
            } else if ($wojewodztwo == 'podkarpackie') {
                $wojewodztwo = 9;
            } else if ($wojewodztwo == 'podlaskie') {
                $wojewodztwo = 10;
            } else if ($wojewodztwo == 'pomorskie') {
                $wojewodztwo = 11;
            } else if ($wojewodztwo == 'śląskie') {
                $wojewodztwo = 12;
            } else if ($wojewodztwo == 'świętokrzyskie') {
                $wojewodztwo = 13;
            } else if ($wojewodztwo == 'warmińsko-mazurskie') {
                $wojewodztwo = 14;
            } else if ($wojewodztwo == 'wielkopolskie') {
                $wojewodztwo = 15;
            } else if ($wojewodztwo == 'zachodniopomorskie') {
                $wojewodztwo = 16;
            }

            $response = array(
                'regon' => $regon,
                'nazwa' => $nazwa,
                'woj' => $wojewodztwo,
                'ulica' => $ulica,
                'kod' => $poczta_miasto[0],
                'miasto' => $poczta_miasto[1]
            );
            return Response::json($response);
        } else if (preg_match('/<p class="messageP">(.*?)<\/p>/', $store, $match)) {
            $response = array(
                'error' => 'Twoja przeglądarka przesłała niepoprawne dane'
            );
            return Response::json($response);
        } else if (preg_match('/<select id="criterion0TF" name="criterion0TF"(.*?)<\/select>/', $store, $match)) {
            $response = array(
                'error' => 'Dla podanego numeru NIP występuje więcej niż jeden podmiot.'
            );
            return Response::json($response);
        } else {
            $response = array(
                'error' => 'Wystąpił nieznany błąd.'
            );
            return Response::json($response);
        }
    }

    public function registerUser() {
        return $this->register(\NetInteractive\Cobrands\Enums\UserTypeEnum::REGULAR);
    }

    public function registerProducer() {
        return $this->register(\NetInteractive\Cobrands\Enums\UserTypeEnum::PRODUCER);
    }

    public function register($userType) {
        $routeBack = '';
        $is_producer = false;
        if ($userType == \NetInteractive\Cobrands\Enums\UserTypeEnum::REGULAR) {
            $routeBack = 'registerView';
            $is_producer = false;
        } else if ($userType == \NetInteractive\Cobrands\Enums\UserTypeEnum::PRODUCER) {
            $routeBack = 'registerProducerView';
            $is_producer = true;
        }
        Input::flash();
        $data = Input::all();
        $rules = array(
            'register_companyname' => array('required', 'unique:users,companyname', 'min:2', 'max:250', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0"-9\s\.\-\,\@]+$/'),
            'register_nip' => array('required', 'min:10', 'max:10', 'regex:/^[0-9]+$/', 'nip'),
            'register_regon' => array('required', 'min:5', 'max:15', 'regex:/^[0-9]+$/'),
            'register_street' => array('required', 'min:3', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\/]+$/'),
            'register_postcode' => array('required', 'min:2', 'max:20', 'regex:/^[A-z0-9\s\-]+$/'),
            'register_city' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s\.\-]+$/'),
            'register_district' => array('required', 'min:1', 'max:16', 'integer'),
            'register_companysize' => array('required', 'min:1', 'max:4', 'integer'),
            'register_name' => array('required', 'min:2', 'max:100', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0"-9\s\.\-\,\@]+$/'),
            'register_phone_main' => array('required', 'min:2', 'max:45', 'regex:/^[0-9\s\-\(\)\+]+$/'),
            'register_phone_alternative' => array('min:2', 'max:45', 'regex:/^[0-9\s\-\(\)\+]+$/'),
            'register_email' => array('required', 'email', 'unique:users,email'),
            'register_password' => array('required', 'min:5'),
            'register_password_confirm' => array('required', 'same:register_password'),
            'register_terms_accepted' => array('required', 'accepted'),
            'register_description' => array('required', 'min:2', 'max:3000'),
        );
        $messages = array(
            'register_companyname.required' => Lang::get('validation.register.register_companyname_required'),
            'register_companyname.min' => Lang::get('validation.register.register_companyname_min'),
            'register_companyname.max' => Lang::get('validation.register.register_companyname_max'),
            'register_companyname.regex' => Lang::get('validation.register.register_companyname_regex'),
            'register_companyname.unique' => Lang::get('validation.register.register_companyname_unique'),
            'register_nip.required' => Lang::get('validation.register.register_nip_required'),
            'register_nip.min' => Lang::get('validation.register.register_nip_min'),
            'register_nip.max' => Lang::get('validation.register.register_nip_max'),
            'register_nip.regex' => Lang::get('validation.register.register_nip_regex'),
            'register_nip.nip' => Lang::get('validation.register.register_nip_nip'),
            'register_regon.required' => Lang::get('validation.register.register_regon_required'),
            'register_regon.min' => Lang::get('validation.register.register_regon_min'),
            'register_regon.max' => Lang::get('validation.register.register_regon_max'),
            'register_regon.regex' => Lang::get('validation.register.register_regon_regex'),
            'register_street.required' => Lang::get('validation.register.register_street_required'),
            'register_street.min' => Lang::get('validation.register.register_street_min'),
            'register_street.max' => Lang::get('validation.register.register_street_max'),
            'register_street.regex' => Lang::get('validation.register.register_street_regex'),
            'register_postcode.required' => Lang::get('validation.register.register_postcode_required'),
            'register_postcode.min' => Lang::get('validation.register.register_postcode_min'),
            'register_postcode.max' => Lang::get('validation.register.register_postcode_max'),
            'register_postcode.regex' => Lang::get('validation.register.register_postcode_regex'),
            'register_city.required' => Lang::get('validation.register.register_city_required'),
            'register_city.min' => Lang::get('validation.register.register_city_min'),
            'register_city.max' => Lang::get('validation.register.register_city_max'),
            'register_city.regex' => Lang::get('validation.register.register_city_regex'),
            'register_district.required' => Lang::get('validation.register.register_district_required'),
            'register_district.min' => Lang::get('validation.register.register_district_min'),
            'register_district.max' => Lang::get('validation.register.register_district_max'),
            'register_district.integer' => Lang::get('validation.register.register_district_integer'),
            'register_companysize.required' => Lang::get('validation.register.register_companysize_required'),
            'register_companysize.min' => Lang::get('validation.register.register_companysize_min'),
            'register_companysize.max' => Lang::get('validation.register.register_companysize_max'),
            'register_companysize.integer' => Lang::get('validation.register.register_companysize_integer'),
            'register_name.required' => Lang::get('validation.register.register_name_required'),
            'register_name.min' => Lang::get('validation.register.register_name_min'),
            'register_name.max' => Lang::get('validation.register.register_name_max'),
            'register_name.regex' => Lang::get('validation.register.register_name_alpha'),
            'register_phone_main.required' => Lang::get('validation.register.register_phone_main_required'),
            'register_phone_main.min' => Lang::get('validation.register.register_phone_main_min'),
            'register_phone_main.max' => Lang::get('validation.register.register_phone_main_max'),
            'register_phone_main.regex' => Lang::get('validation.register.register_phone_main_regex'),
            'register_phone_alternative.min' => Lang::get('validation.register.register_phone_alternative_min'),
            'register_phone_alternative.max' => Lang::get('validation.register.register_phone_alternative_max'),
            'register_phone_alternative.regex' => Lang::get('validation.register.register_phone_alternative_regex'),
            'register_email.required' => Lang::get('validation.register.register_email_required'),
            'register_email.email' => Lang::get('validation.register.register_email_email'),
            'register_email.unique' => Lang::get('validation.register.register_email_unique'),
            'register_password.min' => Lang::get('validation.register.register_password_min'),
            'register_password.required' => Lang::get('validation.register.register_password_required'),
            'register_password_confirm.required' => Lang::get('validation.register.register_password_confirm_required'),
            'register_password_confirm.same' => Lang::get('validation.register.register_password_confirm_same'),
            'register_terms_accepted.required' => Lang::get('validation.register.register_terms_accepted_required'),
            'register_terms_accepted.accepted' => Lang::get('validation.register.register_terms_accepted_accepted'),
            'register_description.required' => "Opis działalności firmy jest wymagany",
            'register_description.min' => "Minimalna długość opisu działalności firmy to 2 znaki",
            'register_description.max' => "Maksymalna długość działalności firmy to 3000 znaków",
        );

        Validator::extend('nip', function($attribute, $value, $parameters) {
            $value = preg_replace("/[^0-9\-]+/", "", $value);
            if (strlen($value) != 10) {
                return false;
            }

            $arrSteps = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
            $intSum = 0;
            for ($i = 0; $i < 9; $i++) {
                $intSum += $arrSteps[$i] * $value[$i];
            }
            $int = $intSum % 11;

            $intControlNr = ($int == 10) ? 0 : $int;
            if ($intControlNr == $value[9]) {
                return true;
            }
            return false;
        });

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            // sprawdzamy czy user sobie już załadował avatar jeśli tak
            //  to go przekazujemy spowrotem
            if (Input::has('avatar_id')) {

                $avatar = Avatar::find(Input::get('avatar_id'));
                if ($avatar) {
                    return Redirect::route($routeBack)->withErrors($validator)->withInput()->with('avatar_url', $avatar->getUrl());
                } else {
                    return Redirect::route($routeBack)->withErrors($validator)->withInput();
                }
            } else {
                return Redirect::route($routeBack)->withErrors($validator)->withInput();
            }
        } else if ($validator->passes()) {
            $terms_accepted = false;
            if (Input::has('register_terms_accepted')) {
                if (Input::get('register_terms_accepted') == 'on') {
                    $terms_accepted = true;
                }
            }
            $newsletter_accepted = false;
            if (Input::has('register_newsletter_accepted')) {
                if (Input::get('register_newsletter_accepted') == 'on') {
                    $newsletter_accepted = true;
                }
            }

            $avatar_id = 1;
            if (Input::has('avatar_id')) {
                $avatar = Avatar::find(Input::get('avatar_id'));
                if ($avatar) {
                    $avatar_id = $avatar->id;
                }
            }
            DB::beginTransaction();
            $user = Sentry::register(array(
                        'email' => Input::get('register_email'),
                        'password' => Input::get('register_password'),
                        'companyname' => Input::get('register_companyname'),
                        'name' => Input::get('register_name'),
                        'street' => Input::get('register_street'),
                        'postcode' => Input::get('register_postcode'),
                        'city' => Input::get('register_city'),
                        'district_id' => Input::get('register_district'),
                        'nip' => Input::get('register_nip'),
                        'regon' => Input::get('register_regon'),
                        'phone_main' => Input::get('register_phone_main'),
                        'phone_alternative' => Input::get('register_phone_alternative'),
                        'terms_accepted' => $terms_accepted,
                        'companysize_id' => Input::get('register_companysize'),
                        'avatar_id' => $avatar_id,
                        'is_producer' => $is_producer,
                        'description' => \NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('register_description'))
            ));

            if ($user) {
                $userGroup = $group = Sentry::findGroupByName('GROUP_USER');
                $user->addGroup($userGroup);
                if ($newsletter_accepted) {
                    $newsletter = Mailing::where('email', $user->email)->get();
                    if (!($newsletter && $newsletter->count())) {
                        Mailing::create(array('email' => $user->email));
                    }
                }

                // pobieranie dodanych produktów lub brandów
                if ($user->is_producer) {
                    // zapisywanie produktów do użytkownika
                    if (Session::has('register_products')) {
                        $register_products = Session::get('register_products');
                        foreach ($register_products as $rf) {
                            $product = Product::find($rf->id);
                            if ($product) {
                                if (!$product->active && $product->user_id == 1) {
                                    $product->user_id = $user->id;
                                    $product->active = true;
                                    if ($product->save()) {
                                        
                                    } else {
                                        DB::rollback();
                                        return Redirect::route($routeBack)->with('error', 'Nie można zapisać produktu ' . $product->name);
                                    }
                                } else {
                                    DB::rollback();
                                    return Redirect::route($routeBack)->with('error', 'Nie można dodać produktu ' . $product->name);
                                }
                            } else {
                                DB::rollback();
                                return Redirect::route($routeBack)->with('error', 'Jeden z produktów jest błędny.');
                            }
                        }
                    }
                } else {
                    // zapisywanie brandów do użytkownika
                    if (Session::has('register_brands')) {
                        $register_brands = Session::get('register_brands');
                        foreach ($register_brands as $rb) {
                            $brand = Brand::find($rb->id);
                            if ($brand) {
                                if (!$brand->active && $brand->user_id == 1) {
                                    $brand->user_id = $user->id;
                                    $brand->active = true;
                                    if ($brand->save()) {
                                        
                                    } else {
                                        DB::rollback();
                                        return Redirect::route($routeBack)->with('error', 'Nie można zapisać brandu ' . $brand->name);
                                    }
                                } else {
                                    DB::rollback();
                                    return Redirect::route($routeBack)->with('error', 'Nie można dodać brand ' . $brand->name);
                                }
                            } else {
                                DB::rollback();
                                return Redirect::route($routeBack)->with('error', 'Jeden z brandów jest błędny.');
                            }
                        }
                    }
                }
                if (Session::has('register_products')) {
                    Session::forget('register_products');
                }
                if (Session::has('register_brands')) {
                    Session::forget('register_brands');
                }
                DB::commit();
                $activationCode = $user->getActivationCode();

                MailFacade::sendActivationEmail($user->email, $user->login, $activationCode);
                return Redirect::route('homeView')->with('registrationSuccess', true);
            } else {
                DB::rollback();
                return Redirect::route($routeBack)->with('error', 'Rejestracja nie powiodła się. Spróbuj jeszcze raz.');
            }
        }
    }

}
