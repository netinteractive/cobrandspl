<?php

use NetInteractive\Cobrands\Enums\PackagetypeEnum;
use NetInteractive\Cobrands\Enums\PaymentstatusEnum;
use Netinteractive\Payments\Payments;
use Netinteractive\Payments\Methods\TransferujMethod;

/**
 * Description of PackageController
 *
 * @author damian
 */
class PackageController extends BaseController {

    public function renderPayView() {
        if (Input::has('selected_package_id')) {
            $package = Package::find(Input::get('selected_package_id'));
            if ($package) {
                $payment = Payments::instance();
                $payment->setMethod(new TransferujMethod());
                $payment->setAmount($package->price);
                $payment->setName(Sentry::getUser()->companyname);
                $payment->setEmail(Sentry::getUser()->email);
                $payment->setDescription('Opłata za pakiet: ' . $package->name);
                $payment->setUserId(Sentry::getUser()->id);
                $payment->setProductId($package->id);
                $payment->setRedirectSuccess('/package/pay/success');
                $payment->setRedirectError('/package/pay/error');
                $button = $payment->getButton();
                return View::make('layouts.profile.pay')
                                ->with('package', $package)
                                ->with('button', $button);
            } else {
                return Redirect::route('myProfileView')->with('error', 'Wybrany rodzaj pakietu nie istnieje');
            }
        } else {
            return Redirect::route('myProfileView')->with('error', 'Zakup pakietu nie mógł zostać zrealizowany');
        }
    }

    /**
     * Zakup pakietu przez użytkownika
     * @return type
     */
    public function buy() {
        if (Input::has('selected_package_id')) {
            $package = Package::find(Input::get('selected_package_id'));
            if ($package) {
                $po = new Packageorder();
                $po->user_id = Sentry::getUser()->id;
                $po->package_id = $package->id;
                if ($package->packagetype->id == PackagetypeEnum::SINGLE) {
                    $po->startdate = null;
                    $po->enddate = null;
                } else if ($package->packagetype->id == PackagetypeEnum::MONTHLY) {
                    $po->startdate = Carbon::now();
                    $po->enddate = Carbon::now()->addMonth();
                } else {
                    return Redirect::route('myProfileView')->with('error', 'Nieznany rodzaj pakietu. Akcja anulowana.');
                }
                $po->active = false;
                $po->used = false;
                $po->price = $package->price;
                $po->paymentstatus_id = PaymentstatusEnum::COMPLETE;
                if ($po->save()) {
                    return Redirect::route('myProfileView')->with('message', 'Wybrany rodzaj pakietu został zakupiony!');
                } else {
                    return Redirect::route('myProfileView')->with('error', 'Zakup pakietu nie mógł zostać zrealizowany. Spróbuj ponownie później.');
                }
            } else {
                return Redirect::route('myProfileView')->with('error', 'Wybrany rodzaj pakietu nie istnieje');
            }
        } else {
            return Redirect::route('myProfileView')->with('error', 'Zakup pakietu nie mógł zostać zrealizowany');
        }
    }

}
