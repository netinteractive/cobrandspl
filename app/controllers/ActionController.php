<?php

use NetInteractive\Cobrands\Enums\ActionstateEnum;
use NetInteractive\Cobrands\Facades\ActionFacadeImplementation as ActionResponse;
use NetInteractive\Cobrands\Facades\PartnerFacadeImplementation as PartnerResponse;
use NetInteractive\Cobrands\Utils\ActionSearchCriteria;

/**
 * Description of ActionController
 *
 * @author damian
 */
class ActionController extends BaseController {

    public function renderActionAddView() {
        $districts_list = District::lists('name', 'id');
        return View::make('layouts.actions.add_action')
                        ->with('districts_list', $districts_list);
    }

    public function renderActionEditView($action_id) {
        $action = Action::find($action_id);
        if ($action) {
            if ($action->user_id == Sentry::getUser()->id) {
                $category_name = 'Wybierz kategorię';
                $category = Category::find($action->category_id);
                if ($category) {
                    $category_name = $category->name;
                }
                Session::flash('category_name', $category_name);
                $districts_list = District::lists('name', 'id');
                return View::make('layouts.actions.edit')
                                ->with('action', $action)
                                ->with('districts_list', $districts_list);
            } else {
                return Redirect::route('actionView', $action_id)->with('error', 'Nie masz uprawnień do edycji tej akcji!');
            }
        } else {
            return Redirect::route('homeView')->with('error', 'Nie ma takiej akcji!');
        }
    }

    public function renderActionView($action_id) {
        $action = Action::find($action_id);
        if (Request::ajax()) {
            if ($action) {
                return Response::json($action);
            } else {
                return Response::json(array('error' => 'Nie ma takiej akcji!'));
            }
        } else {
            if ($action) {
                return View::make('layouts.actions.action')->with('action', $action);
            } else {
                App::abort(404);
            }
        }
    }

    public function renderActionsFinishedView() {
        $actions = Action::where('actionstate_id', ActionstateEnum::ENDED_AFTER_FIRST_STAGE)
                ->orWhere('actionstate_id', ActionstateEnum::ENDED_AFTER_SECOND_STAGE)
                ->paginate(2);
        return View::make('layouts.actions.finished')->with('actions', $actions);
    }

    public function actionAddAction() {
        if (Sentry::check()) {
            $data = Input::all();
            $rules = array(
                'action_category' => array('required', 'digits_between:1,85'),
                'action_city' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s\.\-]+$/'),
                'action_district' => array('required', 'min:1', 'max:16', 'integer'),
                'action_description' => array('required', 'min:2', 'max:4000'),
                'action_description_more' => array('min:2', 'max:4000'),
                'action_partners_count' => array('sometimes', 'integer', 'min:1', 'max:10000'),
                'action_stage_1_date' => array('required', 'date'),
                'action_stage_2_date' => array('required', 'date', 'after:action_stage_1_date'),
                'action_budget' => array('required', 'numeric'),
                'action_date_from' => array('required', 'date'),
                'action_date_to' => array('required', 'date'),
                'action_selected_brand' => array('required', 'integer', 'brand')
            );
            $messages = array(
                'action_category.required' => Lang::get('validation.add_action.action_branch_required'),
                'action_category.digits_between' => Lang::get('validation.add_action.action_branch_digits_between'),
                'action_description.required' => Lang::get('validation.add_action.action_description_required'),
                'action_description.min' => Lang::get('validation.add_action.action_description_min'),
                'action_description.max' => Lang::get('validation.add_action.action_description_max'),
                'action_description_more.min' => "Minimalna długość tekstu to 2 znaki",
                'action_description_more.max' => "Maksymalna długość tekstu to 4000 znaków",
                'action_partners_count.required' => Lang::get('validation.add_action.action_partners_count_required_without'),
                'action_partners_count.integer' => Lang::get('validation.add_action.action_partners_count_integer'),
                'action_partners_count.min' => Lang::get('validation.add_action.action_partners_count_min'),
                'action_partners_count.max' => Lang::get('validation.add_action.action_partners_count_max'),
                'action_stage_1_date.required' => Lang::get('validation.add_action.action_stage_1_date_required'),
                'action_stage_1_date.date' => Lang::get('validation.add_action.action_stage_1_date_date'),
                'action_stage_2_date.required' => Lang::get('validation.add_action.action_stage_2_date_required'),
                'action_stage_2_date.date' => Lang::get('validation.add_action.action_stage_2_date_date'),
                'action_stage_2_date.after' => 'Data zakończenia drugiego etapu musi występować po dacie zakończenia pierwszego etapu',
                'action_budget.required' => Lang::get('validation.add_action.action_budget_required'),
                'action_budget.numeric' => Lang::get('validation.add_action.action_budget_numeric'),
                'action_date_from.required' => Lang::get('validation.add_action.action_date_from_required'),
                'action_date_from.date' => Lang::get('validation.add_action.action_date_from_date'),
                'action_date_to.required' => Lang::get('validation.add_action.action_date_to_required'),
                'action_date_to.date' => Lang::get('validation.add_action.action_date_to_date'),
                'action_selected_brand.required' => "Wybranie brandu jest wymagane",
                'action_selected_brand.integer' => "Wybrany brand jest błędny",
                'action_selected_brand.brand' => "Wybrany kod brandu jest błędny",
                'action_city.required' => "Wymagane jest podanie nazwy miasta",
                'action_city.min' => "Minimalna długość nazwy miasta to 2 znaki",
                'action_city.max' => "Maksymalna długość nazwy miasta to 200 znaków",
                'action_city.regex' => "Nazwa miasta zawiera nieprawidłowe znaki",
                'action_district.required' => "Wybranie województwa jest wymagane",
                'action_district.min' => "Nieprawidłowe województwo",
                'action_district.max' => "Nieprawidłowe województwo",
                'action_district.integer' => "Nieprawidłowe województwo",
            );

            Validator::extend('brand', function($attribute, $value, $parameters) {
                $brand = Brand::where('id', $value)->where('user_id', Sentry::getUser()->id)->first();
                if ($brand) {
                    return true;
                } else {
                    return false;
                }
            });


            $validator = Validator::make($data, $rules, $messages);

            if ($validator->fails()) {
                $category_name = 'Wybierz kategorię';
                if (Input::has('action_category')) {
                    $category = Category::find(Input::get('action_category'));
                    if ($category) {
                        $category_name = $category->name;
                    }
                }
                // sprawdzamy czy user sobie już załadował avatar jeśli tak
                //  to go przekazujemy spowrotem
                if (Input::has('avatar_id')) {
                    $avatar = Avatar::find(Input::get('avatar_id'));
                    if ($avatar) {
                        return Redirect::route('actionAddView')->withErrors($validator)->withInput()->with('category_name', $category_name)->with('avatar_url', $avatar->getUrl());
                    } else {
                        return Redirect::route('actionAddView')->withErrors($validator)->with('category_name', $category_name)->withInput();
                    }
                } else {
                    return Redirect::route('actionAddView')->withErrors($validator)->with('category_name', $category_name)->withInput();
                }
            } else if ($validator->passes()) {
                $action = new Action();
                $action->setUserId(Sentry::getUser()->id);
                $action->setBrandId(Input::get('action_selected_brand'));
                $action->setCategoryId(Input::get('action_category'));
                $action->setDescription(\NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('action_description')));
                if (Input::has('action_description_more')) {
                    $action->setDescriptionMore(\NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('action_description_more')));
                }
                if(Input::has('action_partners_count')) {
                    $action->setRequiredPartnerCount(Input::get('action_partners_count'));
                } else {
                    $action->setRequiredPartnerCount(1);
                }

                $action->setStageDeadlineOne(Input::get('action_stage_1_date'));
                $action->setStageDeadlineTwo(Input::get('action_stage_2_date'));
                $action->setBudget(Input::get('action_budget'));
                $action->setActionDateStart(Input::get('action_date_from'));
                $action->setActionDateEnd(Input::get('action_date_to'));
                $action->setActionstateId(ActionstateEnum::NEW_CREATED);

                $action->city = strip_tags(Input::get('action_city'));
                $action->district_id = strip_tags(Input::get('action_district'));

                if (Input::has('avatar_id')) {
                    $action->setAvatarId(Input::get('avatar_id'));
                }

                DB::beginTransaction();
                if ($action->save()) {
                    // załadowywanie plików z sesji
                    if (Session::has('action_files')) {
                        foreach (Session::get('action_files') as $af) {
                            // if (!$action->files()->attach($af->id)) { // to nie chce działać
                            if (!DB::table('actions_has_actionfiles')->insert(array('action_id' => $action->id, 'actionfile_id' => $af->id))) {
                                DB::rollBack();
                                return Redirect::route('actionAddView')->with('error', 'Wystąpił błąd i akcja nie mogła zostać dodana!')->withInput();
                            }
                        }
                        Session::forget('action_files');
                    }

                    if (Session::has('action_products')) {
                        foreach (Session::get('action_products') as $ap) {
                            if (!DB::table('action_has_products')->insert(array('action_id' => $action->id, 'product_id' => $ap->id))) {
                                DB::rollBack();
                                return Redirect::route('actionAddView')->with('error', 'Wystąpił błąd i akcja nie mogła zostać dodana!')->withInput();
                            }
                        }
                        Session::forget('action_products');
                    }
                    DB::commit();
                    $url = URL::route('actionView', array('action_id' => $action->id, '#poinformuj_partnerow'));
                    return Redirect::to($url)->with('message', 'Akcja została dodana!');
                } else {
                    return Redirect::route('actionAddView')->with('error', 'Wystąpił błąd i akcja nie mogła zostać dodana!')->withInput();
                }
            }
        } else {
            return Redirect::route('actionAddView')->with('error', 'Wystąpił błąd i Twoja akcja nie mogła zostać utworzona!');
        }
    }

    public function actionSaveAction($action_id) {
        $action = Action::find($action_id);
        if ($action) {
            if ($action->user_id == Sentry::getUser()->id) {
                $data = Input::all();
                $rules = array(
                    'action_city' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s\.\-]+$/'),
                    'action_district' => array('required', 'min:1', 'max:16', 'integer'),
                    'action_category' => array('required', 'digits_between:1,85'),
                    'action_description' => array('required', 'min:2', 'max:4000'),
                    'action_partners_count' => array('required_without:action_partners_undef', 'integer', 'min:1', 'max:10000'),
                    'action_partners_undef' => array('required_without:action_partners_count'),
                    'action_stage_1_date' => array('required', 'date'),
                    'action_stage_2_date' => array('required', 'date', 'after:action_stage_1_date'),
                    'action_budget' => array('required', 'numeric'),
                    'action_date_from' => array('required', 'date'),
                    'action_date_to' => array('required', 'date'),
                );
                $messages = array(
                    'action_category.required' => Lang::get('validation.add_action.action_branch_required'),
                    'action_category.digits_between' => Lang::get('validation.add_action.action_branch_digits_between'),
                    'action_description.required' => Lang::get('validation.add_action.action_description_required'),
                    'action_description.min' => Lang::get('validation.add_action.action_description_min'),
                    'action_description.max' => Lang::get('validation.add_action.action_description_max'),
                    'action_partners_count.required_without' => Lang::get('validation.add_action.action_partners_count_required_without'),
                    'action_partners_count.integer' => Lang::get('validation.add_action.action_partners_count_integer'),
                    'action_partners_count.min' => Lang::get('validation.add_action.action_partners_count_min'),
                    'action_partners_count.max' => Lang::get('validation.add_action.action_partners_count_max'),
                    'action_partners_undef.required_without' => Lang::get('validation.add_action.action_partners_undef_required_without'),
                    'action_stage_1_date.required' => Lang::get('validation.add_action.action_stage_1_date_required'),
                    'action_stage_1_date.date' => Lang::get('validation.add_action.action_stage_1_date_date'),
                    'action_stage_2_date.required' => Lang::get('validation.add_action.action_stage_2_date_required'),
                    'action_stage_2_date.date' => Lang::get('validation.add_action.action_stage_2_date_date'),
                    'action_stage_2_date.after' => 'Data zakończenia drugiego etapu musi występować po dacie zakończenia pierwszego etapu',
                    'action_budget.required' => Lang::get('validation.add_action.action_budget_required'),
                    'action_budget.numeric' => Lang::get('validation.add_action.action_budget_numeric'),
                    'action_date_from.required' => Lang::get('validation.add_action.action_date_from_required'),
                    'action_date_from.date' => Lang::get('validation.add_action.action_date_from_date'),
                    'action_date_to.required' => Lang::get('validation.add_action.action_date_to_required'),
                    'action_date_to.date' => Lang::get('validation.add_action.action_date_to_date')
                );

                $validator = Validator::make($data, $rules, $messages);

                if ($validator->fails()) {
                    $category_name = 'Wybierz kategorię';
                    if (Input::has('action_category')) {
                        $category = Category::find(Input::get('action_category'));
                        if ($category) {
                            $category_name = $category->name;
                        }
                    }
                    return Redirect::route('actionEditView', $action->id)->withErrors($validator)->with('category_name', $category_name)->withInput();
                } else if ($validator->passes()) {
                    $action->setBrandId(Input::get('action_selected_brand'));
                    $action->setCategoryId(Input::get('action_category'));
                    $action->setDescription(\NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('action_description')));
                    if (Input::has('action_description_more')) {
                        $action->setDescriptionMore(\NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('action_description_more')));
                    }
                    $action->setRequiredPartnerCount(Input::get('action_partners_count'));

                    $action->setStageDeadlineOne(Input::get('action_stage_1_date'));
                    $action->setStageDeadlineTwo(Input::get('action_stage_2_date'));
                    $action->setBudget(Input::get('action_budget'));
                    $action->setActionDateStart(Input::get('action_date_from'));
                    $action->setActionDateEnd(Input::get('action_date_to'));

                    $action->city = strip_tags(Input::get('action_city'));
                    $action->district_id = strip_tags(Input::get('action_district'));

                    if ($action->save()) {
                        return Redirect::route('actionEditView', $action->id)->with('message', 'Akcja została prawidłowo zapisana!');
                    } else {
                        return Redirect::route('actionAddView')->with('error', 'Wystąpił błąd i akcja nie mogła zostać dodana!')->withInput();
                    }
                }
            } else {
                return Redirect::route('actionView', $action_id)->with('error', 'Nie masz uprawnień do edycji tej akcji!');
            }
        } else {
            return Redirect::route('homeView')->with('error', 'Nie ma takiej akcji!');
        }
    }

    public function actionsList($category, $district, $sortby, $sorttype, $searchterm = null) {
        if (Request::ajax()) {
            return ActionFacade::findActionsRestful($category, $district, $sortby, $sorttype, $searchterm);
        } else if (!Request::ajax()) {
            $actions = ActionFacade::findActions($category, $district, $sortby, $sorttype, $searchterm);
            return View::make('layouts.actions_list')->with('actions', $actions);
        }
    }

    public function actionsListAdvanced(ActionSearchCriteria $actionSearchCriteria) {
        $actions = ActionFacade::findActionsAdvanced($actionSearchCriteria);
        return View::make('layouts.actions_list')->with('actions', $actions);
    }

    public function applyToAction($action_id,$user_id) {
        if (Input::has('apply_brand') && Input::has('visibility_type')) {
            $brand_id = Input::get('apply_brand');
            $is_visible = true;
            if (Input::get('visibility_type') == 'invisible') {
                $is_visible = false;
            }
            $response = ActionFacade::applyToAction($action_id,$user_id, $brand_id, $is_visible);
            if ($response != ActionResponse::APPLY_SUCCESSFULL) {
                if (Request::ajax()) {
                    $response = array(
                        'error' => $response
                    );
                    return Response::json($response);
                } else {
                    return Redirect::route('actionsList', array('all', 'all', 'latest', 'desc'))->with('error', $response);
                }
            } else {
                if (Request::ajax()) {
                    $response = array(
                        'success' => $response
                    );
                    return Response::json($response);
                } else {
                    return Redirect::route('actionsList', array('all', 'all', 'latest', 'desc'))->with('message', $response);
                }
            }
        } else {
            return Redirect::route('actionsList', array('all', 'all', 'latest', 'desc'))->with('message', $response);
        }
    }

    public function uploadAvatarAction($action_id = null) {
        $data = Input::all();
        $rules = array(
            'avatarfile' => array('required', 'image', 'max:2000'),
        );
        $messages = array(
            'avatarfile.required' => "Wystąpił błąd! Brak pliku!",
            'avatarfile.image' => "Plik nie jest plikiem gif, jpeg lub png!",
            'avatarfile.max' => "Maksymalny rozmiar pliku to 2MB!"
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $response = array(
                'error' => $validator->messages()->first()
            );
            return Response::json($response);
        } else {

            // Sprawdzamy czy użytkownik zmienia logo akcji
            $logoChange = false;
            $action = '';
            if ($action_id != null) {
                $logoChange = true;
                $action = Action::find($action_id);
                if (!$action) {
                    $response = array(
                        'error' => 'Podana akcja nie istnieje!'
                    );
                    return Response::json($response);
                }
                if (Sentry::getUser()->id != $action->user_id) {
                    $response = array(
                        'error' => 'Nie można zmienić loga nieswojej akcji!'
                    );
                    return Response::json($response);
                }
            }

            $file = $data['avatarfile'];
            $mime = $file->getMimeType();
            $fileExtension = '';
            if ($mime == 'image/png') {
                $fileExtension = 'png';
            } else if ($mime == 'image/jpeg') {
                $fileExtension = 'jpeg';
            } else if ($mime == 'image/gif') {
                $fileExtension = 'gif';
            } else {
                $response = array(
                    'error' => 'Nierozpoznano rodzaju pliku!'
                );
                return Response::json($response);
            }
            $shortname = Str::random(30);
            $filename = $shortname . '.' . $fileExtension;
            $thumbname = $shortname . '_thumb' . '.' . $fileExtension;
            // Tworzenie pliku o standardowych wymiarach
            $image_standard = Image::make(Input::file('avatarfile'))->fit(600, 300)->save(Config::get('path.AVATARS_PATH') . $filename, 90);
            // Tworzenie thumba
            $image_thumb = $image_standard->fit(75, 75)->save(Config::get('path.AVATARS_PATH') . $thumbname, 90);

            if ($image_standard && $image_thumb) {
                $avatar = new Avatar();
                $avatar->filename = $shortname;
                $avatar->filetype = $fileExtension;
                if ($avatar->save()) {
                    if ($logoChange && $action) {
                        $action->avatar_id = $avatar->id;
                        if ($action->save()) {
                            $response = array(
                                'success' => 'success',
                                'avatar_id' => $avatar->id,
                                'avatar_url' => $avatar->getUrl(),
                                'avatar_thumb_url' => $avatar->getThumbUrl()
                            );
                            return Response::json($response);
                        } else {
                            $response = array(
                                'error' => 'Nie udało się zmienić logo akcji!'
                            );
                            return Response::json($response);
                        }
                    } else {
                        $response = array(
                            'success' => 'success',
                            'avatar_id' => $avatar->id,
                            'avatar_url' => $avatar->getUrl()
                        );
                        return Response::json($response);
                    }
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                    );
                    return Response::json($response);
                }
            } else {
                $response = array(
                    'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                );
                return Response::json($response);
            }
        }
    }

    function uploadFileAction($action_id = null) {
        $data = Input::all();
        $rules = array(
            'action_file' => array('required', 'max:10000'),
        );
        $messages = array(
            'action_file.required' => "Wystąpił błąd! Brak pliku!",
            'action_file.max' => "Maksymalny rozmiar pliku to 10MB!"
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $response = array(
                'error' => $validator->messages()->first()
            );
            return Response::json($response);
        } else {
            // Sprawdzamy czy użytkownik dodaje plik do istniejącej akcji
            $actionChange = false;
            $action = '';
            if ($action_id != null) {
                $actionChange = true;
                $action = Action::find($action_id);
                if (!$action) {
                    $response = array(
                        'error' => 'Podana akcja nie istnieje!'
                    );
                    return Response::json($response);
                }
                if (Sentry::getUser()->id != $action->user_id) {
                    $response = array(
                        'error' => 'Nie można dodać plików do nieswojej akcji!'
                    );
                    return Response::json($response);
                }
            }

            // sprawdzamy czy nie dodano za dużo plików (więcej niż 5)
            // edycja akcji
            if ($actionChange) {
                if ($action->files()->count() >= 5) {
                    $response = array(
                        'error' => 'Maksymalna liczba plików jaką można załadować wynosi 5!'
                    );
                    return Response::json($response);
                }
            } else {
                // przy dodawaniu nowej akcji
                if (Session::has('action_files')) {
                    if (sizeof(Session::get('action_files')) >= 5) {
                        $response = array(
                            'error' => 'Maksymalna liczba plików jaką można załadować wynosi 5!'
                        );
                        return Response::json($response);
                    }
                }
            }

            $file = $data['action_file'];
            $mime = $file->getMimeType();
            $originalExtension = $file->getClientOriginalExtension();
            $fileExtension = '';
            if ($mime == 'image/png') {
                $fileExtension = 'png';
            } else if ($mime == 'image/jpeg') {
                $fileExtension = 'jpeg';
            } else if ($mime == 'image/gif') {
                $fileExtension = 'gif';
            } else if ($mime == 'application/pdf' && $originalExtension == 'pdf') {
                $fileExtension = 'pdf';
            } else if ($mime == 'application/msword' && $originalExtension == 'doc') {
                $fileExtension = 'doc';
            } else if ($mime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                $fileExtension = 'docx';
            } else if ($mime == 'application/vnd.ms-excel' || ($mime == 'application/msword' && $originalExtension == 'xls')) {
                $fileExtension = 'xls';
            } else if ($mime == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                $fileExtension = 'xlsx';
            } else {
                $response = array(
                    'error' => 'Nierozpoznano rodzaju pliku!'
                );
                return Response::json($response);
            }

            $originalName = $file->getClientOriginalName();
            $shortname = Str::random(30);
            $filename = $shortname . '.' . $fileExtension;

            // jeśli plik jest zdjęciem to tworzymy miniaturkę i normalny duży plik
            if ($fileExtension == 'png' || $fileExtension == 'jpeg' || $fileExtension == 'gif') {
                $thumbname = $shortname . '_thumb' . '.' . $fileExtension;
                // Tworzenie pliku o standardowych wymiarach
                $image_standard = Image::make(Input::file('action_file'))->save(Config::get('path.ACTIONFILES_PATH') . $filename, 90);
                // Tworzenie thumba
                $image_thumb = $image_standard->fit(75, 75)->save(Config::get('path.ACTIONFILES_PATH') . $thumbname, 90);

                if ($image_standard && $image_thumb) {
                    $actionfile = new Actionfile();
                    $actionfile->filename = $shortname;
                    $actionfile->filetype = $fileExtension;
                    $actionfile->original_filename = $originalName;
                    $actionfile->mimetype = $mime;
                    DB::beginTransaction();
                    if ($actionfile->save()) {
                        if ($actionChange && $action) {
                            // jak zwykle attach() nie chce działać
                            if (DB::table('actions_has_actionfiles')->insert(array('action_id' => $action->id, 'actionfile_id' => $actionfile->id))) {
                                DB::commit();
                                $response = array(
                                    'success' => 'success',
                                    'html' => View::make('layouts.actions.show.action_files')->with('action', $action)->render()
                                );
                                return Response::json($response);
                            } else {
                                DB::rollBack();
                                $response = array(
                                    'error' => 'Nie udało się załadować pliku!'
                                );
                                return Response::json($response);
                            }
                        } else {
                            DB::commit();
                            $this->storeFileInSession($actionfile);
                            $response = array(
                                'success' => 'success',
                                'html' => View::make('layouts.actions.add.action_files')->render()
                            );
                            return Response::json($response);
                        }
                    } else {
                        DB::rollBack();
                        $response = array(
                            'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                        );
                        return Response::json($response);
                    }
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                    );
                    return Response::json($response);
                }
            } else {
                // jesli jest to dokument to go po prostu załadowywujemy
                if (Input::file('action_file')->move(Config::get('path.ACTIONFILES_PATH'), $filename)) {
                    $actionfile = new Actionfile();
                    $actionfile->filename = $shortname;
                    $actionfile->filetype = $fileExtension;
                    $actionfile->original_filename = $originalName;
                    $actionfile->mimetype = $mime;

                    DB::beginTransaction();
                    if ($actionfile->save()) {
                        if ($actionChange && $action) {
                            if (DB::table('actions_has_actionfiles')->insert(array('action_id' => $action->id, 'actionfile_id' => $actionfile->id))) {
                                DB::commit();
                                $response = array(
                                    'success' => 'success',
                                    'html' => View::make('layouts.actions.show.action_files')->with('action', $action)->render()
                                );
                                return Response::json($response);
                            } else {
                                DB::rollBack();
                                $response = array(
                                    'error' => 'Nie udało się załadować pliku!'
                                );
                                return Response::json($response);
                            }
                        } else {
                            DB::commit();
                            $this->storeFileInSession($actionfile);
                            $response = array(
                                'success' => 'success',
                                'html' => View::make('layouts.actions.add.action_files')->render()
                            );
                            return Response::json($response);
                        }
                    } else {
                        DB::rollBack();
                        $response = array(
                            'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                        );
                        return Response::json($response);
                    }
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                    );
                    return Response::json($response);
                }
            }
        }
    }

    /**
     * Zapisuje załadowany plik w sesji
     * @param type $actionfile
     */
    private function storeFileInSession($actionfile) {
        // tylko przy dodawaniu akcji na czysto
        if (Session::has('action_files')) {
            // tu zastąpić Session::push
            $action_files = Session::get('action_files');
            array_push($action_files, $actionfile);
            Session::put('action_files', $action_files);
        } else {
            $action_files = array();
            $action_files[0] = $actionfile;
            Session::put('action_files', $action_files);
        }
    }

    /**
     * Wysłanie wiadomości do partnerów na temat akcji
     * @param type $action_id id akcji, której się tyczy wiadomość
     * @return type
     */
    public function informPartnersAction($action_id) {
        if (Input::has('selectedPartners') && Input::has('message')) {
            $response = PartnerFacade::informPartners(Input::get('selectedPartners'), Input::get('message'), $action_id);
            if ($response != PartnerResponse::INFORM_SUCCESS) {
                return Redirect::route('actionView', $action_id)->with('error', $response);
            } else {
                return Redirect::route('actionView', $action_id)->with('message', $response);
            }
        } else {
            return Redirect::route('actionView', $action_id)->with('error', 'Wystąpił błąd, spróbuj ponownie później.');
        }
    }

    public function addProduct($product_id) {
        $product = Product::where('id', $product_id)->where('active', true)->first();
        if (!$product) {
            App::abort(404);
        } else {
            if (Session::has('action_products')) {
                $selected_products = Session::get('action_products');
                foreach ($selected_products as $sp) {
                    if ($sp->id == $product_id) {
                        $response = array(
                            'error' => "Ten produkt już został wybrany!"
                        );
                        return Response::json($response);
                    }
                }
                array_push($selected_products, $product);
                Session::put('action_products', $selected_products);
            } else {
                $selected_products = array();
                $selected_products[0] = $product;
                Session::put('action_products', $selected_products);
            }

            $response = array(
                'products_list_html' => View::make('layouts.actions.products_list')->render()
            );
            return Response::json($response);
        }
    }

    public function removeProduct($product_id) {
        $product = Product::where('id', $product_id)->where('active', true)->first();
        if ($product) {
            if (Session::has('action_products')) {
                $selected_products = Session::get('action_products');
                $temp = array();
                foreach ($selected_products as $sp) {
                    if ($sp->id != $product_id) {
                        array_push($temp, $sp);
                    }
                }
                Session::put('action_products', $temp);
                $response = array(
                    'products_list_html' => View::make('layouts.actions.products_list')->render()
                );
                return Response::json($response);
            } else {
                $response = array(
                    'error' => 'Lista produktów jest pusta!'
                );
                return Response::json($response);
            }
        } else {
            App::abort(404);
        }
    }

    public function renderActionBrandsRadioList($user, $selected_id) {
        return View::make('layouts.actions.action_brands')
                        ->with('user', $user)
                        ->with('selected_id', $selected_id)
                        ->render();
    }

}
