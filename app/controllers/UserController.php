<?php

/**
 * Description of UserController
 *
 * @author damian
 */
use NetInteractive\Cobrands\Facades\UserFacadeImplementation as UserResponse;

class UserController extends BaseController {

    public function activateAccount($code) {
        $user = User::where('activation_code', '=', $code)->where('activated', '=', false);
        if ($user->count()) {
            $user = $user->first();
            try {
                if ($user->attemptActivation($code)) {
                    return Redirect::route('homeView')->with('message', 'Twoje konto zostało aktywowane. Możesz się teraz zalogować.');
                } else {
                    return Redirect::route('homeView')->with('error', 'Wystąpił błąd i Twoje konto nie mogło zostać aktywowane!');
                }
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                return Redirect::route('homeView')->with('error', 'Wystąpił błąd i Twoje konto nie mogło zostać aktywowane!');
            } catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e) {
                return Redirect::route('homeView')->with('error', 'Wystąpił błąd i Twoje konto nie mogło zostać aktywowane!');
            }
        } else {
            return Redirect::route('homeView')->with('error', 'Wystąpił błąd i Twoje konto nie mogło zostać aktywowane!');
        }
    }

    public function listPartners() {
        $partners = UserFacade::getAllPartners();
        return View::make('layouts.partners.partners_list')->with('partners', $partners);
    }

    public function showPartner($id) {
        $user = User::find($id);
        if ($user) {
            return View::make('layouts.partners.partner_profile')->with('user', $user);
        } else {
            App::abort(404);
        }
    }

    public function showPartnerActions($id) {
        $user = User::find($id);
        if ($user) {
            $activeActions = UserFacade::getUserActiveActions($id);
            $endedActions = UserFacade::getUserFinishedActions($id);

            return View::make('layouts.partners.partner_actions')
                            ->with('endedActions', $endedActions)
                            ->with('activeActions', $activeActions)
                            ->with('username', $user->login);
        } else {
            App::abort(404);
        }
    }

    public function partnerAddAction() {
        $brand_id = Input::get('brand_id');
        $partner_brand_id = Input::get('partner_brand_id');
        if (!$brand_id || !$partner_brand_id) {
            if (Request::ajax()) {
                $response = array(
                    'error' => 'Błędne parametry!'
                );
                return Response::json($response);
            } else {
                return Redirect::route('listBrands')->with('error', 'Błędne parametry!');
            }
        } else {
            $userBrand = Brand::find($brand_id);
            $partnerBrand = Brand::find($partner_brand_id);
            if (!$userBrand || !$partnerBrand) {
                if (Request::ajax()) {
                    $response = array(
                        'error' => 'Takie brandy nie istnieją!'
                    );
                    return Response::json($response);
                } else {
                    return Redirect::route('listBrands')->with('error', 'Takie brandy nie istniejąs!');
                }
            } else {
                if (PartnerFacade::isPartnerBrandOfAnotherBrand($brand_id, $partner_brand_id)) {
                    if (Request::ajax()) {
                        $response = array(
                            'error' => 'Ten brand jest już Twoim partnerem!'
                        );
                        return Response::json($response);
                    } else {
                        return Redirect::route('listBrands')->with('error', 'Ten brand jest już Twoim partnerem!');
                    }
                } else {
                    switch ($response = UserFacade::addPartnerBrand($userBrand->id, $partnerBrand->user->id, $partnerBrand->id)) {
                        case UserResponse::PARTNER_ADD_ERROR:
                            if (Request::ajax()) {
                                $response = array(
                                    'error' => $response
                                );
                                return Response::json($response);
                            } else {
                                return Redirect::route('listBrands')->with('message', $response);
                            }
                        case UserResponse::PARTNER_ADD_SUCCESS:
                            if (Request::ajax()) {
                                $response = array(
                                    'success' => $response
                                );
                                return Response::json($response);
                            } else {
                                return Redirect::route('listBrands')->with('message', $response);
                            }
                    }
                }
            }
        }
    }

    public function sendMessage($user_id) {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'user_id' => array('required', 'integer'),
            'private_message_content' => array('required', 'min:10', 'max:3000',)
        );
        $messages = array(
            'user_id.required' => 'Niepoprawny identyfikator użytkownika',
            'user_id.integer' => 'Niepoprawny identyfikator użytkownika',
            'private_message_content.required' => 'Treść wiadomości jest wymagana',
            'private_message_content.min' => 'Minimalna długość wiadomości to 10 znaków',
            'private_message_content.max' => 'Maksymalna długość wiadomości to 3000 znaków',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            if (Input::has('user_id')) {
                return Redirect::route('showPartner', Input::get('user_id'))->withErrors($validator)->with('showMessageModal', true)->withInput();
            } else {
                return Redirect::route('homeView')->with('error', 'Nieprawidłowy identyfikator użytkownika');
            }
        } else {
            if (Sentry::check()) {
                $user = User::find(Input::get('user_id'));
                if (!$user) {
                    return Redirect::route('homeView')->with('error', 'Użytkownik, do którego chcesz wysłać wiadomość nie istnieje!');
                } else {
                    if ($user->id == Sentry::getUser()->id) {
                        return Redirect::route('homeView')->with('error', 'Nie możesz wysłać wiadomości do samego siebie!');
                    } else {
                        MailFacade::sendPrivateMessage($user->email, Sentry::getUser()->login, Input::get('private_message_content'), $user->login);
                        return Redirect::route('showPartner', Input::get('user_id'))->with('message', 'Wiadomość została wysłana do użytkownika!');
                    }
                }
            } else {
                return Redirect::route('homeView')->with('error', 'Aby wysłać wiadomość musisz się najpierw zalogować!');
            }
        }
    }

}
