<?php

use NetInteractive\Cobrands\Facades\PartnerFacadeImplementation as Response;

/**
 * Description of ProfileController
 *
 * @author damian
 */
class ProfileController extends BaseController {

    public function renderMyProfileView() {
        $user = Sentry::getUser();

        $districts = District::all();
        $companysizes = Companysize::all();
        return View::make('layouts.profile.my_profile')->with('user', $user)
                        ->with('districts', $districts)
                        ->with('companysizes', $companysizes);
    }

    public function renderMyActionsView() {
        return View::make('layouts.profile.my_actions');
    }

    public function renderMyApplicationsView() {
        return View::make('layouts.profile.my_applications');
    }

    public function renderPackagePaySuccess() {
        return Redirect::route('myProfileView')->with('message', 'Wybrany rodzaj pakietu został zakupiony!');
    }

    public function renderPackagePayError() {
        return Redirect::route('myProfileView')->with('error', 'Zakup pakietu nie mógł zostać zrealizowany. Spróbuj ponownie później.');
    }

    public function renderMyProductsView($sortby, $sorttype) {
        $products = ProductFacade::findMyProducts($sortby, $sorttype);
        return View::make('layouts.profile.my_products')
                        ->with('products', $products)
                        ->with('sortby', $sortby)
                        ->with('sorttype', $sorttype);
    }

    public function renderMyPartnersView() {
        $waitingPartners = PartnerFacade::getCurrentUserWaitingPartners();
        $acceptedPartners = PartnerFacade::getCurrentUserAcceptedPartners();
        return View::make('layouts.profile.my_partners')
                        ->with('waitingPartners', $waitingPartners)
                        ->with('acceptedPartners', $acceptedPartners);
    }

    public function saveAction() {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'edit_companyname' => array('required', 'companyname_unique', 'min:2', 'max:250', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\,\@]+$/'),
            'edit_nip' => array('required', 'min:10', 'max:10', 'regex:/^[0-9]+$/', 'nip'),
            'edit_regon' => array('min:5', 'max:15', 'regex:/^[0-9]+$/'),
            'edit_branches' => array('max:30', 'regex:/^\[[0-9\",]*\]$|/'),
            'edit_street' => array('required', 'min:3', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\/]+$/'),
            'edit_postcode' => array('required', 'min:2', 'max:20', 'regex:/^[A-z0-9\s\-]+$/'),
            'edit_city' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s\.\-]+$/'),
            'edit_district' => array('required', 'min:1', 'max:16', 'integer'),
            'edit_companysize' => array('required', 'min:1', 'max:4', 'integer'),
            'edit_name' => array('required', 'min:2', 'max:100', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0"-9\s\.\-\,\@]+$/'),
            'edit_phone_main' => array('required', 'min:2', 'max:45', 'regex:/^[0-9\s\-\(\)\+]+$/'),
            'edit_phone_alternative' => array('min:2', 'max:45', 'regex:/^[0-9\s\-\(\)\+]+$/'),
            'edit_email' => array('required', 'email', 'email_unique'),
            'edit_description' => array('required', 'min:10', 'max:3000'),
        );
        $messages = array(
            'edit_companyname.required' => Lang::get('validation.register.register_companyname_required'),
            'edit_companyname.min' => Lang::get('validation.register.register_companyname_min'),
            'edit_companyname.max' => Lang::get('validation.register.register_companyname_max'),
            'edit_companyname.regex' => Lang::get('validation.register.register_companyname_regex'),
            'edit_companyname.companyname_unique' => Lang::get('validation.register.register_companyname_unique'),
            'edit_nip.required' => Lang::get('validation.register.register_nip_required'),
            'edit_nip.min' => Lang::get('validation.register.register_nip_min'),
            'edit_nip.max' => Lang::get('validation.register.register_nip_max'),
            'edit_nip.regex' => Lang::get('validation.register.register_nip_regex'),
            'edit_nip.nip' => Lang::get('validation.register.register_nip_nip'),
            'edit_regon.min' => Lang::get('validation.register.register_regon_min'),
            'edit_regon.max' => Lang::get('validation.register.register_regon_max'),
            'edit_regon.regex' => Lang::get('validation.register.register_regon_regex'),
            'edit_branches.max' => Lang::get('validation.register.register_branches_max'),
            'edit_branches.regex' => Lang::get('validation.register.register_branches_regex'),
            'edit_street.required' => Lang::get('validation.register.register_street_required'),
            'edit_street.min' => Lang::get('validation.register.register_street_min'),
            'edit_street.max' => Lang::get('validation.register.register_street_max'),
            'edit_street.regex' => Lang::get('validation.register.register_street_regex'),
            'edit_postcode.required' => Lang::get('validation.register.register_postcode_required'),
            'edit_postcode.min' => Lang::get('validation.register.register_postcode_min'),
            'edit_postcode.max' => Lang::get('validation.register.register_postcode_max'),
            'edit_postcode.regex' => Lang::get('validation.register.register_postcode_regex'),
            'edit_city.required' => Lang::get('validation.register.register_city_required'),
            'edit_city.min' => Lang::get('validation.register.register_city_min'),
            'edit_city.max' => Lang::get('validation.register.register_city_max'),
            'edit_city.regex' => Lang::get('validation.register.register_city_regex'),
            'edit_district.required' => Lang::get('validation.register.register_district_required'),
            'edit_district.min' => Lang::get('validation.register.register_district_min'),
            'edit_district.max' => Lang::get('validation.register.register_district_max'),
            'edit_district.integer' => Lang::get('validation.register.register_district_integer'),
            'edit_companysize.required' => Lang::get('validation.register.register_companysize_required'),
            'edit_companysize.min' => Lang::get('validation.register.register_companysize_min'),
            'edit_companysize.max' => Lang::get('validation.register.register_companysize_max'),
            'edit_companysize.integer' => Lang::get('validation.register.register_companysize_integer'),
            'edit_name.required' => Lang::get('validation.register.register_name_required'),
            'edit_name.min' => Lang::get('validation.register.register_name_min'),
            'edit_name.max' => Lang::get('validation.register.register_name_max'),
            'edit_name.alpha' => Lang::get('validation.register.register_name_alpha'),
            'edit_phone_main.required' => Lang::get('validation.register.register_phone_main_required'),
            'edit_phone_main.min' => Lang::get('validation.register.register_phone_main_min'),
            'edit_phone_main.max' => Lang::get('validation.register.register_phone_main_max'),
            'edit_phone_main.regex' => Lang::get('validation.register.register_phone_main_regex'),
            'edit_phone_alternative.min' => Lang::get('validation.register.register_phone_alternative_min'),
            'edit_phone_alternative.max' => Lang::get('validation.register.register_phone_alternative_max'),
            'edit_phone_alternative.regex' => Lang::get('validation.register.register_phone_alternative_regex'),
            'edit_email.required' => Lang::get('validation.register.register_email_required'),
            'edit_email.email' => Lang::get('validation.register.register_email_email'),
            'edit_email.email_unique' => Lang::get('validation.register.register_email_unique'),
            'edit_description.required' => Lang::get('validation.register.register_description_required'),
            'edit_description.min' => Lang::get('validation.register.register_description_min'),
            'edit_description.max' => Lang::get('validation.register.register_description_max')
        );

        Validator::extend('nip', function($attribute, $value, $parameters) {
            $value = preg_replace("/[^0-9\-]+/", "", $value);
            if (strlen($value) != 10) {
                return false;
            }

            $arrSteps = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
            $intSum = 0;
            for ($i = 0; $i < 9; $i++) {
                $intSum += $arrSteps[$i] * $value[$i];
            }
            $int = $intSum % 11;

            $intControlNr = ($int == 10) ? 0 : $int;
            if ($intControlNr == $value[9]) {
                return true;
            }
            return false;
        });

        Validator::extend('companyname_unique', function($attribute, $value, $parameters) {
            $user = User::where('companyname', $value)->where('id', '!=', Input::get('user_id'))->get();
            if ($user->count() > 0) {
                return false;
            } else {
                return true;
            }
        });

        Validator::extend('email_unique', function($attribute, $value, $parameters) {
            $user = User::where('email', $value)->where('id', '!=', Input::get('user_id'))->get();
            if ($user->count() > 0) {
                return false;
            } else {
                return true;
            }
        });

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::route('myProfileView')->withErrors($validator)->withInput()->with('showEdit', true);
        } else if ($validator->passes()) {
            $newsletter_accepted = false;
            if (Input::has('edit_newsletter_accepted')) {
                if (Input::get('edit_newsletter_accepted') == 'on') {
                    $newsletter_accepted = true;
                }
            }

            $user = User::find(Input::get('user_id'));
            if (!$user) {
                return Redirect::route('myProfileView')->with('error', 'Błędny użytkownik');
            } else {
                DB::beginTransaction();
                $user->email = strip_tags(Input::get('edit_email'));
                $user->companyname = strip_tags(Input::get('edit_companyname'));
                $user->name = strip_tags(Input::get('edit_name'));
                $user->street = strip_tags(Input::get('edit_street'));
                $user->postcode = strip_tags(Input::get('edit_postcode'));
                $user->city = strip_tags(Input::get('edit_city'));
                $user->district_id = Input::get('edit_district');
                $user->nip = Input::get('edit_nip');
                $user->regon = Input::get('edit_regon');
                $user->phone_main = strip_tags(Input::get('edit_phone_main'));
                $user->phone_alternative = strip_tags(Input::get('edit_phone_alternative'));
                $user->description = \NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('edit_description'));
                $user->companysize_id = Input::get('edit_companysize');

                if (!$user->save()) {
                    DB::rollBack();
                    return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                }

                if ($newsletter_accepted) {
                    $newsletter = Mailing::where('email', $user->email)->get();
                    if (!($newsletter && $newsletter->count())) {
                        if (!Mailing::create(array('email' => $user->email))) {
                            DB::rollBack();
                            return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                        }
                    }
                } else {
                    $newsletter = Mailing::where('email', $user->email)->first();
                    if ($newsletter) {
                        if (!$newsletter->delete()) {
                            DB::rollBack();
                            return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                        }
                    }
                }
                if (Input::has('edit_branches')) {
                    // usuwanie branż użytkownika
                    if (!DB::table('users_has_branches')->where('user_id', $user->id)->delete()) {
                        DB::rollBack();
                        return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                    }
                    $branches_array = json_decode(Input::get('edit_branches'));
                    foreach ($branches_array as $branch) {
                        if (!DB::table('users_has_branches')->insert(array('user_id' => $user->id, 'branch_id' => intval($branch)))) {
                            DB::rollBack();
                            return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                        }
                    }
                } else {
                    // usuwanie branż użytkownika
                    if (!DB::table('user_has_branches')->where('user_id', $user->id)->delete()) {
                        DB::rollBack();
                        return Redirect::route('myProfileView')->with('showEdit', true)->with('error', 'Wystąpił błąd podczas zapisu danych.');
                    }
                }

                DB::commit();
                return Redirect::route('myProfileView')->with('message', 'Twoje dane zostały poprawnie zapisane');
            }
        }
    }

    public function changePassword() {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'edit_password' => array('required', 'min:5'),
            'edit_password_confirm' => array('required', 'same:edit_password'),
        );
        $messages = array(
            'edit_password.min' => Lang::get('validation.register.register_password_min'),
            'edit_password.required' => Lang::get('validation.register.register_password_required'),
            'edit_password_confirm.required' => Lang::get('validation.register.register_password_confirm_required'),
            'edit_password_confirm.same' => Lang::get('validation.register.register_password_confirm_same')
        );

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::route('myProfileView')->withErrors($validator)->withInput()->with('showEdit', true);
        } else {
            $user = Sentry::find(Input::get('user_id'));
            if (!$user) {
                return Redirect::route('myProfileView')->with('error', 'Błędny użytkownik');
            } else {
                $user->password = Input::get('edit_password');
                if ($user->save()) {
                    return Redirect::route('myProfileView')->with('message', 'Twoje hasło zostało zmienione.');
                } else {
                    return Redirect::route('myProfileView')->with('error', 'Wystąpił błąd podczas zmiany hasła.');
                }
            }
        }
    }

    public function partnerAcceptAction($user_id,$brand_id) {
        switch ($response = PartnerFacade::acceptUserPartner($user_id,$brand_id)) {
            case Response::INVALID_PARTNER:
            case Response::SAVING_ERROR:
                return Redirect::route('myPartnersView')->with('error', Lang::get($response));
            case Response::PARTNER_ACCEPTED_SUCCESSFULLY:
                return Redirect::route('myPartnersView')->with('message', Lang::get($response));
        }
    }

    public function partnerRejectAction($user_id,$brand_id) {
        switch ($response = PartnerFacade::rejectUserPartner($user_id,$brand_id)) {
            case Response::INVALID_PARTNER:
            case Response::SAVING_ERROR:
                return Redirect::route('myPartnersView')->with('error', Lang::get($response));
            case Response::PARTNER_REJECTED_SUCCESSFULLY:
                return Redirect::route('myPartnersView')->with('message', Lang::get($response));
        }
    }

    public function partnerRemoveAction($user_id,$brand_id) {
        switch ($response = PartnerFacade::removeUserPartner($user_id,$brand_id)) {
            case Response::INVALID_PARTNER:
            case Response::REMOVAL_ERROR:
                return Redirect::route('myPartnersView')->with('error', Lang::get($response));
            case Response::PARTNER_REMOVED_SUCCESSFULLY:
                return Redirect::route('myPartnersView')->with('message', Lang::get($response));
        }
    }

}
