<?php

/**
 * Description of ProductController
 *
 * @author damian
 */
class ProductController extends Controller {

    /**
     * Dodawanie produktu
     * Produkty dodane podczas rejestracji najpierw są przypisywane do usera o id=1
     * i są nieaktywne. Dopiero po prawidłowej rejestracji są przypisywane do nowo
     * zarejestrowanego id
     */
    public function addProduct() {
        $data = Input::all();
        $rules = array(
            'image_id' => array('required', 'integer'),
            'product_name' => array('required', 'unique:products,name', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\,\@]+$/'),
            'category_id' => array('required', 'digits_between:1,85'),
            'description' => array('required', 'min:10', 'max:4000'),
            'num_of_prices' => array('required', 'integer', 'min:1', 'max:3'),
            'price_1' => array('required', 'numeric', 'min:1'),
            'price_1_from' => array('required', 'integer'),
            'price_1_to' => array('required', 'integer'),
            'price_2' => array('required_if:num_of_prices,==,2', 'numeric', 'min:1'),
            'price_2_from' => array('required_if:num_of_prices,==,2', 'integer'),
            'price_2_to' => array('required_if:num_of_prices,==,2', 'integer'),
            'price_3' => array('required_if:num_of_prices,==,3', 'numeric', 'min:1'),
            'price_3_from' => array('required_if:num_of_prices,==,3', 'integer'),
            'price_3_to' => array('required_if:num_of_prices,==,3', 'integer'),
            'product_count' => array('required', 'integer')
        );
        $messages = array(
            'image_id.required' => 'Dodanie zdjęcia produktu jest wymagane',
            'image_id.integer' => 'Błędny identyfikator zdjęcia produktu',
            'product_name.required' => 'Podanie nzwy produktu jest wymagane',
            'product_name.unique' => 'Produkt o takiej nazwie już istnieje',
            'product_name.min' => 'Minimalna długość nazwy produktu to 2 znaki',
            'product_name.max' => 'Maksymalna długość nazwy produktu to 200 znaków',
            'product_name.regex' => 'Nazwa produktu zawiera niedozwolone znaki',
            'category_id.required' => 'Wybranie kategorii jest wymagane',
            'category_id.digits_between' => 'Wybrano nieprawidłową kategorię',
            'description.required' => 'Podanie opisu produktu jest wymagana',
            'description.min' => 'Minimalna długość opisu produktu to 10 znaków',
            'description.max' => 'Maksymalna długość opisu produktu to 4000 znaków',
            'num_of_prices.required' => 'Nieprawidłowa liczba cen produktów',
            'num_of_prices.integer' => 'Nieprawidłowa liczba cen produktów',
            'num_of_prices.min' => 'Nieprawidłowa liczba cen produktów',
            'num_of_prices.max' => 'Nieprawidłowa liczba cen produktów',
            'price_1.required' => 'Podanie ceny produktu jest wymagane',
            'price_1.numeric' => 'Błędna cena produktu1',
            'price_1.min' => 'Minimalna cena produktu to 1 zł.',
            'price_1_from.required' => 'Wymagane jest podanie liczby szt. od',
            'price_1_from.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_1_to.required' => 'Wymagane jest podanie liczby szt. do',
            'price_1_to.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_2.required_if' => 'Podanie drugiej ceny produktu jest wymagane',
            'price_2.numeric' => 'Błędna cena produktu2',
            'price_2.min' => 'Minimalna cena produktu to 1 zł.',
            'price_2_from.required_if' => 'Wymagane jest podanie liczby szt. od',
            'price_2_from.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_2_to.required_if' => 'Wymagane jest podanie liczby szt. do',
            'price_2_to.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_3.required_if' => 'Podanie trzeciej ceny produktu jest wymagane',
            'price_3.numeric' => 'Błędna cena produktu3',
            'price_3.min' => 'Minimalna cena produktu to 1 zł.',
            'price_3_from.required_if' => 'Wymagane jest podanie liczby szt. od',
            'price_3_from.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_3_to.required_if' => 'Wymagane jest podanie liczby szt. do',
            'price_3_to.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'product_count.required' => 'Wymagane jest podanie liczby sztuk produktu',
            'product_count.integer' => 'Liczba sztuk produktu musi być liczbą całkowitą'
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $response = array(
                'error' => $validator->messages()
            );
            return Response::json($response);
        } else {
            // rodzaj: czy rejestracja, czy dodawanie własnego produktu
            $addType = strip_tags(Input::get('add_type'));
            // jeszcze sprawdzamy czy liczby sztuk 'od' nie są większe od 'do'
            $numOfPrices = Input::get('num_of_prices');
            if ($numOfPrices >= 1) {
                $price1From = intval(Input::get('price_1_from'));
                $price1To = intval(Input::get('price_1_to'));
                if ($price1From > $price1To) {
                    $response = array(
                        'error' => array('price_1' => 'Liczba sztuk od nie może być większa od liczby sztuk do')
                    );
                    return Response::json($response);
                }
            }
            if ($numOfPrices >= 2) {
                $price2From = intval(Input::get('price_2_from'));
                $price2To = intval(Input::get('price_2_to'));
                if ($price2From > $price2To) {
                    $response = array(
                        'error' => array('price_2' => 'Liczba sztuk od nie może być większa od liczby sztuk do')
                    );
                    return Response::json($response);
                }
            }
            if ($numOfPrices == 3) {
                $price3From = intval(Input::get('price_3_from'));
                $price3To = intval(Input::get('price_3_to'));
                if ($price3From > $price3To) {
                    $response = array(
                        'error' => array('price_3' => 'Liczba sztuk od nie może być większa od liczby sztuk do')
                    );
                    return Response::json($response);
                }
            }

            // tworzenie produktu
            $product = new Product();
            $product->name = strip_tags(Input::get('product_name'));
            $product->logo_id = strip_tags(Input::get('image_id'));
            if ($addType == 'register') {
                $product->user_id = 1; // 'niczyje' produkty przypisujemy najpierw do admina
                $product->active = false;
            } else if ($addType == 'signedin' && Sentry::check() && Sentry::getUser()->is_producer) {
                $product->user_id = Sentry::getUser()->id;
                $product->active = true;
            } else {
                $response = array(
                    'error' => array('error' => 'Nieprawidłowy identyfikator użytkownika.')
                );
                return Response::json($response);
            }
            $product->product_category_id = strip_tags(Input::get('category_id'));
            $product->description = NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('description'));
            $product->product_count = intval(strip_tags(Input::get('product_count')));
            $product->price_count = intval(strip_tags(Input::get('num_of_prices')));
            if ($product->price_count >= 1) {
                $product->price_1 = intval(strip_tags(Input::get('price_1')));
                $product->count_1_from = intval(strip_tags(Input::get('price_1_from')));
                $product->count_1_to = intval(strip_tags(Input::get('price_1_to')));
            }
            if ($product->price_count >= 2) {
                $product->price_2 = intval(strip_tags(Input::get('price_2')));
                $product->count_2_from = intval(strip_tags(Input::get('price_2_from')));
                $product->count_2_to = intval(strip_tags(Input::get('price_2_to')));
            }
            if ($product->price_count == 3) {
                $product->price_3 = intval(strip_tags(Input::get('price_3')));
                $product->count_3_from = intval(strip_tags(Input::get('price_3_from')));
                $product->count_3_to = intval(strip_tags(Input::get('price_3_to')));
            }

            if ($product->save()) {
                if ($addType == 'register') {
                    // zapisujemy ten produkt do sesji użytkownika
                    $this->storeProductInSession($product);
                    $this->forgetProductImage();
                    $response = array(
                        'success' => 'Produkt został prawidłowo dodany',
                        'product_id' => $product->id,
                        'product_name' => $product->name,
                        'products_list_html' => View::make('layouts.register.products_list')->render(),
                        'type' => 'register'
                    );
                    return Response::json($response);
                } else if ($addType == 'signedin') {
                    $this->forgetProductImage();
                    $response = array(
                        'success' => 'Produkt został prawidłowo dodany',
                        'type' => 'signedin'
                    );
                    return Response::json($response);
                } else {
                    $response = array(
                        'error' => array('error' => 'Wystąpił błąd podczas zapisu produktu. Spróbuj ponownie później.')
                    );
                    return Response::json($response);
                }
            } else {
                $response = array(
                    'error' => array('error' => 'Wystąpił błąd podczas zapisu produktu. Spróbuj ponownie później.')
                );
                return Response::json($response);
            }
        }
    }

    public function saveProduct($product_id) {
        $product = Product::find($product_id);
        if (!$product) {
            App::abort(404);
        }
        if ($product->user_id == Sentry::getUser()->id || (Sentry::getUser()->inGroup(Sentry::findGroupByName('GROUP_ADMIN')))) {
            
        } else {
            App::abort(404);
        }
        $data = Input::all();
        $rules = array(
            'product_name' => array('required', 'min:2', 'max:200', 'regex:/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9\s\.\-\,\@]+$/'),
            'category_id' => array('required', 'digits_between:1,85'),
            'description' => array('required', 'min:10', 'max:4000'),
            'num_of_prices' => array('required', 'integer', 'min:1', 'max:3'),
            'price_1' => array('required', 'numeric', 'min:1'),
            'price_1_from' => array('required', 'integer'),
            'price_1_to' => array('required', 'integer'),
            'price_2' => array('required_if:num_of_prices,==,2', 'numeric', 'min:1'),
            'price_2_from' => array('required_if:num_of_prices,==,2', 'integer'),
            'price_2_to' => array('required_if:num_of_prices,==,2', 'integer'),
            'price_3' => array('required_if:num_of_prices,==,3', 'numeric', 'min:1'),
            'price_3_from' => array('required_if:num_of_prices,==,3', 'integer'),
            'price_3_to' => array('required_if:num_of_prices,==,3', 'integer'),
            'product_count' => array('required', 'integer')
        );
        $messages = array(
            'product_name.required' => 'Podanie nzwy produktu jest wymagane',
            'product_name.min' => 'Minimalna długość nazwy produktu to 2 znaki',
            'product_name.max' => 'Maksymalna długość nazwy produktu to 200 znaków',
            'product_name.regex' => 'Nazwa produktu zawiera niedozwolone znaki',
            'category_id.required' => 'Wybranie kategorii jest wymagane',
            'category_id.digits_between' => 'Wybrano nieprawidłową kategorię',
            'description.required' => 'Podanie opisu produktu jest wymagana',
            'description.min' => 'Minimalna długość opisu produktu to 10 znaków',
            'description.max' => 'Maksymalna długość opisu produktu to 4000 znaków',
            'num_of_prices.required' => 'Nieprawidłowa liczba cen produktów',
            'num_of_prices.integer' => 'Nieprawidłowa liczba cen produktów',
            'num_of_prices.min' => 'Nieprawidłowa liczba cen produktów',
            'num_of_prices.max' => 'Nieprawidłowa liczba cen produktów',
            'price_1.required' => 'Podanie ceny produktu jest wymagane',
            'price_1.numeric' => 'Błędna cena produktu1',
            'price_1.min' => 'Minimalna cena produktu to 1 zł.',
            'price_1_from.required' => 'Wymagane jest podanie liczby szt. od',
            'price_1_from.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_1_to.required' => 'Wymagane jest podanie liczby szt. do',
            'price_1_to.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_2.required_if' => 'Podanie drugiej ceny produktu jest wymagane',
            'price_2.numeric' => 'Błędna cena produktu2',
            'price_2.min' => 'Minimalna cena produktu to 1 zł.',
            'price_2_from.required_if' => 'Wymagane jest podanie liczby szt. od',
            'price_2_from.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_2_to.required_if' => 'Wymagane jest podanie liczby szt. do',
            'price_2_to.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_3.required_if' => 'Podanie trzeciej ceny produktu jest wymagane',
            'price_3.numeric' => 'Błędna cena produktu3',
            'price_3.min' => 'Minimalna cena produktu to 1 zł.',
            'price_3_from.required_if' => 'Wymagane jest podanie liczby szt. od',
            'price_3_from.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'price_3_to.required_if' => 'Wymagane jest podanie liczby szt. do',
            'price_3_to.integer' => 'Liczba sztuk musi być liczbą całkowitą',
            'product_count.required' => 'Wymagane jest podanie liczby sztuk produktu',
            'product_count.integer' => 'Liczba sztuk produktu musi być liczbą całkowitą'
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $response = array(
                'error' => $validator->messages()
            );
            return Response::json($response);
        } else {
            // jeszcze sprawdzamy czy liczby sztuk 'od' nie są większe od 'do'
            $numOfPrices = Input::get('num_of_prices');
            if ($numOfPrices >= 1) {
                $price1From = intval(Input::get('price_1_from'));
                $price1To = intval(Input::get('price_1_to'));
                if ($price1From > $price1To) {
                    $response = array(
                        'error' => array('price_1' => 'Liczba sztuk od nie może być większa od liczby sztuk do')
                    );
                    return Response::json($response);
                }
            }
            if ($numOfPrices >= 2) {
                $price2From = intval(Input::get('price_2_from'));
                $price2To = intval(Input::get('price_2_to'));
                if ($price2From > $price2To) {
                    $response = array(
                        'error' => array('price_2' => 'Liczba sztuk od nie może być większa od liczby sztuk do')
                    );
                    return Response::json($response);
                }
            }
            if ($numOfPrices == 3) {
                $price3From = intval(Input::get('price_3_from'));
                $price3To = intval(Input::get('price_3_to'));
                if ($price3From > $price3To) {
                    $response = array(
                        'error' => array('price_3' => 'Liczba sztuk od nie może być większa od liczby sztuk do')
                    );
                    return Response::json($response);
                }
            }

            // zapisywanie produktu
            $product->name = strip_tags(Input::get('product_name'));
            $product->product_category_id = strip_tags(Input::get('category_id'));
            $product->description = NetInteractive\Cobrands\Utils\ValidationUtils::encode(Input::get('description'));
            $product->product_count = intval(strip_tags(Input::get('product_count')));
            $product->price_count = intval(strip_tags(Input::get('num_of_prices')));
            if ($product->price_count >= 1) {
                $product->price_1 = intval(strip_tags(Input::get('price_1')));
                $product->count_1_from = intval(strip_tags(Input::get('price_1_from')));
                $product->count_1_to = intval(strip_tags(Input::get('price_1_to')));
                $product->price_2 = null;
                $product->count_2_from = null;
                $product->count_2_to = null;
                $product->price_3 = null;
                $product->count_3_from = null;
                $product->count_3_to = null;
            }
            if ($product->price_count >= 2) {
                $product->price_2 = intval(strip_tags(Input::get('price_2')));
                $product->count_2_from = intval(strip_tags(Input::get('price_2_from')));
                $product->count_2_to = intval(strip_tags(Input::get('price_2_to')));
            }
            if ($product->price_count == 3) {
                $product->price_3 = intval(strip_tags(Input::get('price_3')));
                $product->count_3_from = intval(strip_tags(Input::get('price_3_from')));
                $product->count_3_to = intval(strip_tags(Input::get('price_3_to')));
            }

            if ($product->save()) {
                $response = array(
                    'success' => 'Produkt został prawidłowo zapisany'
                );
                return Response::json($response);
            } else {
                $response = array(
                    'error' => array('error' => 'Wystąpił błąd podczas zapisu produktu. Spróbuj ponownie później.')
                );
                return Response::json($response);
            }
        }
    }

    /**
     * Usuwanie produktu z formularza rejestracji
     * @param type $product_id
     */
    public function removeProductFromRegistrationForm($product_id) {
        if ($this->removeProductFromSession($product_id)) {
            // sprawdzamy czy taki produkt może zostać usunięty z formularza
            // rejestracji
            $product = Product::where('id', $product_id)->where('active', false)->where('user_id', 1)->first();
            if ($product) {
                if ($product->delete()) {
                    $response = array(
                        'success' => 'Produkt został prawidłowo usunięty',
                        'products_list_html' => View::make('layouts.register.products_list')->render()
                    );
                    return Response::json($response);
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas usuwania produktu'
                    );
                    return Response::json($response);
                }
            } else {
                $response = array(
                    'error' => 'Taki produkt nie istnieje'
                );
                return Response::json($response);
            }
        } else {
            $response = array(
                'error' => 'Taki produkt nie istnieje'
            );
            return Response::json($response);
        }
    }

    /**
     * Zapisuje produkt w sesji
     * @param Product $product
     */
    private function storeProductInSession($product) {
        if (Session::has('register_products')) {
            $register_products = Session::get('register_products');
            array_push($register_products, $product);
            Session::put('register_products', $register_products);
        } else {
            $register_products = array();
            $register_products[0] = $product;
            Session::put('register_products', $register_products);
        }
    }

    /**
     * Usuwanie produktu z sesji
     * @param type $product_id id produktu
     * @return boolean czy usunięto czy też nie
     */
    public function removeProductFromSession($product_id) {
        if (Session::has('register_products')) {
            $register_products = Session::get('register_products');
            $temp = array();
            foreach ($register_products as $rf) {
                if ($rf->id != $product_id) {
                    array_push($temp, $rf);
                }
            }
            Session::put('register_products', $temp);
            return true;
        } else {
            return false;
        }
    }

    private function forgetProductImage() {
        Session::forget('product_image_url');
    }

    public function productsList($category, $sortby, $sorttype, $searchterm = null) {
        if (Request::ajax()) {
            return ProductFacade::findProductsRestful($category, $sortby, $sorttype, $searchterm);
        } else if (!Request::ajax()) {
            $products = ProductFacade::findProducts($category, $sortby, $sorttype, $searchterm);
            return View::make('layouts.products_list')
                            ->with('products', $products)
                            ->with('sortby', $sortby)
                            ->with('sorttype', $sorttype);
        }
    }

    public function renderShowView($product_id) {
        $product = Product::find($product_id);
        if (!Request::ajax() && $product) {
            return View::make('layouts.products.show')
                            ->with('product', $product);
        } else if (Request::ajax() && $product && ($product->user_id == Sentry::getUser()->id || (Sentry::getUser()->inGroup(Sentry::findGroupByName('GROUP_ADMIN'))) )) {
            $category_id = $product->category->getParent()->id;
            $subcategory_id = $product->category->id;
            $response = array(
                'product_id' => $product->id,
                'logo_id' => $product->logo->id,
                'logo_url' => $product->logo->getUrl(),
                'name' => $product->name,
                'category_html' => View::make('layouts.components.product_categories.category_selector_selected')->with('category_id', $category_id)->with('subcategory_id', $subcategory_id)->render(),
                'description' => \NetInteractive\Cobrands\Utils\ValidationUtils::decode($product->description),
                'price_count' => $product->price_count,
                'price_1' => $product->price_1,
                'count_1_from' => $product->count_1_from,
                'count_1_to' => $product->count_1_to,
                'price_2' => $product->price_2,
                'count_2_from' => $product->count_2_from,
                'count_2_to' => $product->count_2_to,
                'price_3' => $product->price_3,
                'count_3_from' => $product->count_3_from,
                'count_3_to' => $product->count_3_to,
                'product_count' => $product->product_count
            );
            return Response::json($response);
        } else {
            App::abort(404);
        }
    }

    public function removeProduct($product_id) {
        $product = Product::find($product_id);
        if ($product) {
            if ($product->user_id != Sentry::getUser()->id) {
                App::abort(404);
            } else {
                if ($product->delete()) {
                    $response = array(
                        'success' => 'Produkt został prawidłowo usunięty'
                    );
                    return Response::json($response);
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas usuwania produktu'
                    );
                    return Response::json($response);
                }
            }
        } else {
            App::abort(404);
        }
    }

}
