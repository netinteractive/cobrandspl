<?php

/**
 * Description of CommentControler
 *
 * @author damian
 */
class CommentController extends BaseController {

    public function add() {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'action_id' => array('required', 'integer'),
            'comment_content' => array('required', 'min:10', 'max:3000',)
        );
        $messages = array(
            'action_id.required' => 'Niepoprawny identyfikator akcji',
            'action_id.integer' => 'Niepoprawny identyfikator akcji',
            'comment_content.required' => 'Treść rekomendacji jest wymagana',
            'comment_content.min' => 'Minimalna długość rekomendacji to 10 znaków',
            'comment_content.max' => 'Maksymalna długość rekomendacji to 3000 znaków',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            if (Input::has('action_id')) {
                return Redirect::route('actionView', array(Input::get('action_id'), 'false'))->withErrors($validator)->with('showModal', Input::get('action_id'))->withInput();
            } else {
                return Redirect::route('homeView')->withErrors($validator)->with('error', 'Dodanie komentarza do akcji było niemozliwe');
            }
        } else {
            // sprawdzamy czy użytkownik może dodać komentarz do tej akcji
            if (ActionFacade::isAllowedForCommenting(Input::get('action_id')) && ActionFacade::isAllowedForCommentingByLoggedUser(Input::get('action_id'))) {
                $comment = new CommentModel();
                $comment->author_id = Sentry::getUser()->id;
                $comment->action_id = Input::get('action_id');
                $comment->content = Input::get('comment_content');
                if ($comment->save()) {
                    return Redirect::route('actionView', array(Input::get('action_id'), 'false'))->with('message', 'Komentarz do akcji został prawidłowo dodany.');
                } else {
                    return Redirect::route('actionView', array(Input::get('action_id'), 'false'))->with('error', 'Dodanie koemntarza do tej akcji nie powiodło się. Spróbuj ponownie później.');
                }
            } else {
                return Redirect::route('actionView', array(Input::get('action_id'), 'false'))->with('error', 'Dodanie koemntarza do tej akcji jest niemozliwe!');
            }
        }
    }

}
