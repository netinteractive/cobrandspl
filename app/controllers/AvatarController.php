<?php

/**
 * Description of AvatarController
 *
 * @author damian
 */
class AvatarController extends BaseController {

    public function getRegularFile($filename) {
        return $this->getFile($filename, false);
    }

    public function getThumbFile($filename) {
        return $this->getFile($filename, true);
    }

    private function getFile($filename, $isThumb) {
        $avatar = Avatar::where("filename", $filename)->first();
        if ($avatar) {
            $path = '';
            if ($isThumb) {
                $path = Config::get('path.AVATARS_PATH') . $filename . '_thumb.' . $avatar->filetype;
            } else {
                $path = Config::get('path.AVATARS_PATH') . $filename . '.' . $avatar->filetype;
            }

            $content = file_get_contents($path);
            return Response::make($content, 200, array('Content-Type' => 'image/' . $avatar->filetype, 'Content-Disposition' => 'inline; filename=' . $filename . '.' . $avatar->filetype));
        } else {
            App::abort(404);
        }
    }

    public function uploadFile($user_id = null) {
        $data = Input::all();
        $rules = array(
            'avatarfile' => array('required', 'image', 'max:2000'),
        );
        $messages = array(
            'avatarfile.required' => "Wystąpił błąd! Brak pliku!",
            'avatarfile.image' => "Plik nie jest plikiem gif, jpeg lub png!",
            'avatarfile.max' => "Maksymalny rozmiar pliku to 2MB!"
        );

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $response = array(
                'error' => $validator->messages()->first()
            );
            return Response::json($response);
        } else {

            // Sprawdzamy czy użytkownik zmienia swoje własne logo
            $avatarChange = false;
            $user = '';
            if ($user_id != null) {
                $avatarChange = true;
                $user = User::find($user_id);
                if (!$user) {
                    $response = array(
                        'error' => 'Podany użytkownik nie istnieje!'
                    );
                    return Response::json($response);
                }
                if (Sentry::getUser()->id != $user->id) {
                    $response = array(
                        'error' => 'Nie można zmienić avatara innego użytkownika!'
                    );
                    return Response::json($response);
                }
            }

            $file = $data['avatarfile'];
            $mime = $file->getMimeType();
            $fileExtension = '';
            if ($mime == 'image/png') {
                $fileExtension = 'png';
            } else if ($mime == 'image/jpeg') {
                $fileExtension = 'jpeg';
            } else if ($mime == 'image/gif') {
                $fileExtension = 'gif';
            } else {
                $response = array(
                    'error' => 'Nierozpoznano rodzaju pliku!'
                );
                return Response::json($response);
            }
            $shortname = Str::random(30);
            $filename = $shortname . '.' . $fileExtension;
            $thumbname = $shortname . '_thumb' . '.' . $fileExtension;
            // Tworzenie pliku o standardowych wymiarach
            $image_standard = Image::make(Input::file('avatarfile'))->fit(150, 150)->save(Config::get('path.AVATARS_PATH') . $filename, 90);
            // Tworzenie thumba
            $image_thumb = $image_standard->fit(75, 75)->save(Config::get('path.AVATARS_PATH') . $thumbname, 90);

            if ($image_standard && $image_thumb) {
                $avatar = new Avatar();
                $avatar->filename = $shortname;
                $avatar->filetype = $fileExtension;
                if ($avatar->save()) {
                    if ($avatarChange && $user) {
                        $user->avatar_id = $avatar->id;
                        if ($user->save()) {
                            $response = array(
                                'success' => 'success',
                                'avatar_id' => $avatar->id,
                                'avatar_url' => $avatar->getUrl(),
                                'avatar_thumb_url' => $avatar->getThumbUrl()
                            );
                            return Response::json($response);
                        } else {
                            $response = array(
                                'error' => 'Nie udało się zmienić avatara!'
                            );
                            return Response::json($response);
                        }
                    } else {
                        $response = array(
                            'success' => 'success',
                            'avatar_id' => $avatar->id,
                            'avatar_url' => $avatar->getUrl()
                        );
                        return Response::json($response);
                    }
                } else {
                    $response = array(
                        'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                    );
                    return Response::json($response);
                }
            } else {
                $response = array(
                    'error' => 'Wystąpił błąd podczas zapisywania pliku!'
                );
                return Response::json($response);
            }
        }
    }

    public function copyAvatarToBrand($avatar_id) {
        $avatar = Avatar::find($avatar_id);
        if ($avatar) {
            if (!copy(Config::get('path.AVATARS_PATH') . $avatar->filename . "." . $avatar->filetype, Config::get('path.IMAGES_PATH') . $avatar->filename . "." . $avatar->filetype)) {
                $response = array(
                    'error' => 'Wystąpił błąd podczas kopiowania pliku!'
                );
                return Response::json($response);
            }

            if (!copy(Config::get('path.AVATARS_PATH') . $avatar->filename . "_thumb." . $avatar->filetype, Config::get('path.IMAGES_PATH') . $avatar->filename . "_thumb." . $avatar->filetype)) {
                $response = array(
                    'error' => 'Wystąpił błąd podczas kopiowania pliku!'
                );
                return Response::json($response);
            }

            $brandLogo = new Logo();
            $brandLogo->filename = $avatar->filename;
            $brandLogo->filetype = $avatar->filetype;
            if ($brandLogo->save()) {
                $response = array(
                    'success' => 'Plik został prawidłowo skopiowany!',
                    'brand_image_id' => $brandLogo->id,
                    'brand_image_url' => $brandLogo->getUrl()
                );
                return Response::json($response);
            }
        } else {
            App::abort(404);
        }
    }

}
