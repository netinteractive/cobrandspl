<?php

/**
 * Description of ProductCategoryController
 *
 * @author damian
 */
class ProductCategoryController extends BaseController {

    public function renderMainProductCategoriesList($category_id) {
        return View::make('layouts.components.product_categories.main_list')->with('category_id',$category_id);
    }

    public function renderSubProductCategoriesList($mainProductCategoryId,$subcategoryId=null) {
        return View::make('layouts.components.product_categories.sub_list')
                ->with('mainProductCategoryId', $mainProductCategoryId)
                ->with('subcategoryId',$subcategoryId);
    }
    
    public function renderWholeList() {
        return View::make('layouts.components.product_categories.category_selector');
    }

}
