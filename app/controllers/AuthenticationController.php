<?php

/**
 * Description of AuthenticationController
 *
 * @author damian
 */
class AuthenticationController extends BaseController {

    public function render() {
        return View::make('layouts.login');
    }

    public function renderPasswordResetView() {
        return View::make('layouts.password_reset_form');
    }

    public function sendPasswordResetCode() {
        if (Input::has('reminder_email')) {
            $user_email = Input::get('reminder_email');

            $user = User::where('email', $user_email)->where('activated', true)->where('accepted', true)->first();
            if ($user && $user->count()) {
                $resetCode = $user->getResetPasswordCode();
                MailFacade::sendPasswordResetEmail($user->email, $user->login, $resetCode);
                return Redirect::route('homeView')->with('message', 'Na podany adres e-mail zostal wysłany link do zresetowania hasła');
            } else {
                return Redirect::route('passwordResetView')->with('error', 'Podany adres e-mail nie istnieje w systemie');
            }
        } else {
            return Redirect::route('passwordResetView')->with('error', 'Wystąpił błąd. Spróbuj ponownie.');
        }
    }

    public function renderNewPasswordView($reset_token) {
        return View::make('layouts.password_new_form')->with('password_reset_token', $reset_token);
    }

    public function resetPassword() {
        $data = Input::only('reset_email', 'reset_password', 'reset_password_confirmation', 'password_reset_token');
        $rules = array(
            'reset_email' => array('required', 'email', 'exists:users,email'),
            'reset_password' => array('required', 'min:5'),
            'reset_password_confirmation' => array('required', 'same:reset_password'),
            'password_reset_token' => array('required')
        );
        $messages = array(
            'reset_email.required' => 'Podanie adresu e-mail jest wymagane',
            'reset_email.email' => 'Niepoprawny adres e-mail',
            'reset_email.exists' => 'Podany adres e-mail nie istnieje w systemie',
            'reset_password.required' => 'Podanie nowego hasła jest wymagane',
            'reset_password.min' => 'Minimalna długość hasła to 5 znaków',
            'reset_password_confirmation.required' => 'Potwierdzenia hasła jest wymagane',
            'reset_password_confirmation.same' => 'Podane hasła są różne',
            'reset_password_reset_token.required' => 'Nieprawidłowy kod',
        );

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::route('newPasswordView', Input::get('password_reset_token'))->withErrors($validator)->withInput();
        } else if ($validator->passes()) {

            $user_email = Input::get('reset_email');
            $user = User::where('email', $user_email)->where('activated', true)->first();
            if ($user->count()) {
                // trochę to dziwne, ale inaczej wyskakuje błąd "hashera"
                $user = Sentry::findUserById($user->id);
                $reset_code = Input::get('password_reset_token');
                $password = Input::get('reset_password');
                if ($user->checkResetPasswordCode($reset_code)) {
                    if ($user->attemptResetPassword($reset_code, $password)) {
                        return Redirect::route('homeView')->with('message', 'Hasło zostało zmienione')->withInput();
                    } else {
                        return Redirect::route('newPasswordView', Input::get('password_reset_token'))->with('error', 'Resetowanie hasła się nie powiodło')->withInput();
                    }
                } else {
                    return Redirect::route('newPasswordView', Input::get('password_reset_token'))->with('error', 'Podany kod jest nieprawidłowy')->withInput();
                }
            } else {
                return Redirect::route('newPasswordView', Input::get('password_reset_token'))->with('error', 'Nieprawidłowy adres e-mail')->withInput();
            }
        }
    }

    public function login() {
        if (Input::has('login_email') && Input::has('login_password')) {
            $user = User::where('email', Input::get('login_email'))->first();
            if ($user) {
                if ($user->accepted == false) {
                    return Redirect::back()->withInput()->with('error', Lang::get('msg.login.user_not_accepted'));
                }
            }
            try {
                $credentials = array(
                    'email' => Input::get('login_email'),
                    'password' => Input::get('login_password'),
                    'accepted' => true // użytkownik jest zaakceptowany przez admina
                );
                if (Input::has('remember_me')) {
                    if (Input::get('remember_me') == "on") {
                        Sentry::authenticate($credentials, true);
                    }
                } else {
                    Sentry::authenticate($credentials, false);
                }
            } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
                return Redirect::back()->withInput()->with('error', Lang::get('msg.login.login_required'));
            } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
                return Redirect::back()->withInput()->with('error', Lang::get('msg.login.password_required'));
            } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
                return Redirect::back()->withInput()->with('error', Lang::get('msg.login.invalid_credentials'));
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                return Redirect::back()->withInput()->with('error', Lang::get('msg.login.invalid_credentials'));
            } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
                return Redirect::back()->withInput()->with('error', Lang::get('msg.login.invalid_credentials'));
            } catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
                return Redirect::back()->withInput()->with('error', Lang::get('msg.login.invalid_credentials'));
            } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
                return Redirect::back()->withInput()->with('error', Lang::get('msg.login.invalid_credentials'));
            }
            return Redirect::intended('/');
        } else {
            return Redirect::back()->with('error', Lang::get('msg.login.invalid_credentials'));
        }
    }

    public function logout() {
        Sentry::logout();
        return Redirect::to('/')->with('message', Lang::get('msg.logout.success'));
    }

}
