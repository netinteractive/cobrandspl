<?php

/**
 * Description of RecommendationController
 *
 * @author damian
 */
class RecommendationController extends BaseController {

    public function add() {
        Input::flash();
        $data = Input::all();
        $rules = array(
            'action_id' => array('required', 'integer'),
            'recommendation_content' => array('required', 'min:10', 'max:3000',)
        );
        $messages = array(
            'action_id.required' => 'Niepoprawny identyfikator akcji',
            'action_id.integer' => 'Niepoprawny identyfikator akcji',
            'recommendation_content.required' => 'Treść rekomendacji jest wymagana',
            'recommendation_content.min' => 'Minimalna długość rekomendacji to 10 znaków',
            'recommendation_content.max' => 'Maksymalna długość rekomendacji to 3000 znaków',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::route('myApplicationsView')->withErrors($validator)->with('showModal', Input::get('action_id'))->withInput();
        } else {
            // sprawdzamy czy użytkownik może dodać rekomendację
            if (ActionFacade::isAddRecommendationAllowed(Input::get('action_id'))) {
                $recom = new Recommendation();
                $recom->action_id = Input::get('action_id');
                $recom->user_id = Action::find(Input::get('action_id'))->user_id;
                $recom->added_user_id = Sentry::getUser()->id;
                $recom->content = Input::get('recommendation_content');
                if ($recom->save()) {
                    return Redirect::route('myApplicationsView')->with('message', 'Rekomendacja została prawidłowo dodana.');
                } else {
                    return Redirect::route('myApplicationsView')->with('error', 'Zapisanie rekomendacji się nie powiodło!');
                }
            } else {
                return Redirect::route('myApplicationsView')->with('error', 'Dodanie rekomendacji jest niemożliwe.');
            }
        }
    }

}
