<?php

/**
 * Description of ActionCategoryController
 *
 * @author damian
 */
class ActionCategoryController extends Controller {

    public function renderMainActionCategoriesList($category_id) {
        return View::make('layouts.components.action_categories.main_list')->with('category_id', $category_id);
    }

    public function renderActionSubCategoriesList($mainActionCategoryId, $subcategoryId = null) {
        return View::make('layouts.components.action_categories.sub_list')
                        ->with('mainActionCategoryId', $mainActionCategoryId)
                        ->with('subcategoryId', $subcategoryId);
    }

    public function renderWholeList() {
        return View::make('layouts.components.product_categories.category_selector');
    }

}
