<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::filter('ajax', function() {
    if (Request::ajax() === false)
        return Response::error('500');
});

Route::get('/', array(
    'as' => 'homeView',
    'uses' => 'HomeController@render'
));

// Wyszukiwarka na stronie głównej
Route::post('/search', array(
    'before' => 'csrf',
    'as' => 'search',
    'uses' => 'HomeController@search'
));

// Generowanie captchy do rejestracji
Route::get('/captcha/nip', array(
    'as' => 'captchaNip',
    'uses' => 'RegistrationController@renderCaptchaNip'
));

// Pobieranie danych firmy na podst NIPu
Route::post('/nip/dane', array(
    'uses' => 'RegistrationController@retrieveCompanyDetailsByNip'
));

// Renderowanie listy podkategorii produktu
Route::post('/product/subcategories/list/render/{mainProductCategoryId}', array(
    'before' => 'ajax/csrf',
    'uses' => 'ProductCategoryController@renderSubProductCategoriesList'
))->where('mainProductCategoryId', '[0-9]+');

// Renderowanie listy podkategorii akcji
Route::post('/action/subcategories/list/render/{mainActionCategoryId}', array(
    'before' => 'ajax/csrf',
    'uses' => 'ActionCategoryController@renderActionSubCategoriesList'
))->where('mainActionCategoryId', '[0-9]+');

// Renderowanie listy podbranż
Route::post('/brand/subbranches/list/render/{mainBranchId}', array(
    'before' => 'ajax/csrf',
    'uses' => 'BranchCategoryController@renderSubBranchesList'
))->where('mainBranchId', '[0-9]+');

// Renderowanie całej listy kategorii produktu
Route::get('/product/list/render', array(
    'uses' => 'ProductCategoryController@renderWholeList'
));


Route::filter('checkAdminAccess', function($route, $request) {
    if (!Sentry::getUser()->inGroup(Sentry::findGroupByName('GROUP_ADMIN'))) {
        return Redirect::route('homeView')->with('error', 'Nie masz dostępu do tej części serwisu!');
    }
});

Route::group(array('before' => 'auth|checkAdminAccess'), function() {
    Route::get('administrator', function() {
        return Redirect::to('administrator/dashboard');
    });

    Route::post('administrator/users/{id}/accept', 'AdminUserController@accept')->where('id', '\d+');
    Route::post('administrator/users/{id}/changePassword', array('uses' => 'AdminUserController@changePassword', 'as' => 'changePasswordAdmin'))->where('id', '\d+');
    Route::resource('administrator/users', 'AdminUserController');
    Route::resource('administrator/dashboard', 'AdminDashboardController');

    Route::post('administrator/pages/{id}/active', 'AdminPagesController@active')->where('id', '\d+');
    Route::resource('administrator/pages', 'AdminPagesController');

    Route::post('administrator/airts/{id}/active', 'AdminAirtsController@active')->where('id', '\d+');
    Route::resource('administrator/airts', 'AdminAirtsController');
    Route::resource('administrator/actions', 'AdminActionsController');
    Route::resource('administrator/packages', 'AdminPackagesController');
    Route::post('administrator/boughtpackages/{id}/active', 'AdminBoughtPackagesController@active')->where('id', '\d+');
    Route::resource('administrator/boughtpackages', 'AdminBoughtPackagesController');
    Route::get('administrator/applications/{type}', 'AdminApplicationsController@indexSelected')->where('type', 'all|accepted|unaccepted');
    Route::post('administrator/applications/{id}/accept', 'AdminApplicationsController@accept')->where('id', '\d+');
    //Route::resource('administrator/applications', 'AdminApplicationsController');
    Route::resource('administrator/recommendations', 'AdminRecommendationsController');
    Route::resource('administrator/comments', 'AdminCommentsController');
    Route::resource('administrator/banners', 'AdminBannersController');
    Route::resource('administrator/abuses', 'AdminAbusesController');
    Route::resource('administrator/products', 'AdminProductsController');
    Route::resource('administrator/brands', 'AdminBrandsController');
});

Route::group(array('before' => 'auth'), function() {

    Route::get('/action/add', array(
        'as' => 'actionAddView',
        'uses' => 'ActionController@renderActionAddView'
    ));

    Route::get('/action/edit/{action_id}', array(
        'as' => 'actionEditView',
        'uses' => 'ActionController@renderActionEditView'
    ))->where('action_id', '[0-9]+');

    Route::put('/action', array(
        'before' => 'csrf',
        'as' => 'actionAddAction',
        'uses' => 'ActionController@actionAddAction'
    ));

    // pobieranie listy kategorii z biblioteki
    Route::get('/library/list/init', array(
        'before' => 'ajax',
        'as' => 'renderLibraryInitList',
        'uses' => 'LibraryController@renderLibrarySelector'
    ));

    Route::get('/library/list/subcategory/{category_id}', array(
        'before' => 'ajax',
        'as' => 'renderLibrarySubcategoryList',
        'uses' => 'LibraryController@renderOnCategorySelection'
    ))->where('category_id', '[0-9]+');

    Route::get('/library/list/products/{subcategory_id}', array(
        'before' => 'ajax',
        'as' => 'renderLibrarySubcategoryList',
        'uses' => 'LibraryController@renderOnSubcategorySelection'
    ))->where('subcategory_id', '[0-9]+');

    // Dodawanie produktu do akcji (do sesji)
    Route::post('/action/add/product/session/{product_id}', array(
        'before' => 'csrf|ajax',
        'as' => 'actionAddProduct',
        'uses' => 'ActionController@addProduct'
    ))->where('product_id', '[0-9]+');

    // Dodawanie produktu do akcji (do sesji)
    Route::post('/action/remove/product/session/{product_id}', array(
        'before' => 'csrf|ajax',
        'as' => 'actionRemoveProduct',
        'uses' => 'ActionController@removeProduct'
    ))->where('product_id', '[0-9]+');

    // zapisywanie akcji
    Route::put('/action/save/{action_id}', array(
        'before' => 'csrf',
        'as' => 'actionSaveAction',
        'uses' => 'ActionController@actionSaveAction'
    ))->where('action_id', '[0-9]+');

    Route::post('/action/avatar/upload/{action_id?}', array(
        'as' => 'actionUploadAvatar',
        'uses' => 'ActionController@uploadAvatarAction'
    ))->where('action_id', '[0-9]+');

    Route::post('/action/file/upload/{action_id?}', array(
        'as' => 'actionUploadFile',
        'uses' => 'ActionController@uploadFileAction'
    ))->where('action_id', '[0-9]+');

    Route::put('/action/file/upload/{action_id?}', array(
        'as' => 'actionUploadFile',
        'uses' => 'ActionController@uploadFileAction'
    ))->where('action_id', '[0-9]+');

    Route::get('/actions/list/finished', array(
        'as' => 'actionFinishedView',
        'uses' => 'ActionController@renderActionsFinishedView'
    ));

    // Zgłaszanie się do akcji
    Route::post('/action/{action_id}/apply/{user_id}', array(
                'before' => 'csrf',
                'as' => 'applyToAction',
                'uses' => 'ActionController@applyToAction'
            ))->where('action_id', '[0-9]+')
            ->where('user_id', '[0-9]+');

    // Informowanie partnerów o akcji
    Route::post('/action/{action_id}/partners/inform', array(
        'before' => 'csrf',
        'as' => 'actionInformPartners',
        'uses' => 'ActionController@informPartnersAction'
    ))->where('action_id', '[0-9]+');

    // Widok profilu
    Route::get('/profile', array(
        'as' => 'myProfileView',
        'uses' => 'ProfileController@renderMyProfileView'
    ));

    Route::post('/profile/save', array(
        'as' => 'myProfileSaveAction',
        'uses' => 'ProfileController@saveAction'
    ));

    Route::post('/profile/password/change', array(
        'as' => 'myProfilePasswordChangeAction',
        'uses' => 'ProfileController@changePassword'
    ));

    Route::get('/profile/actions', array(
        'as' => 'myActionsView',
        'uses' => 'ProfileController@renderMyActionsView'
    ));
    Route::get('/profile/applications', array(
        'as' => 'myApplicationsView',
        'uses' => 'ProfileController@renderMyApplicationsView'
    ));

    Route::get('/profile/partners', array(
        'as' => 'myPartnersView',
        'uses' => 'ProfileController@renderMyPartnersView'
    ));

    Route::get('/profile/products/sortby/{sortby}/sorttype/{sorttype}', array(
                'as' => 'myProductsView',
                'uses' => 'ProfileController@renderMyProductsView'
            ))->where('sortby', 'date|price')
            ->where('sorttype', 'asc|desc');

    Route::post('/profile/partners/{user_id}/brand/{brand_id}/accept', array(
        'before' => 'csrf',
        'as' => 'partnerAcceptAction',
        'uses' => 'ProfileController@partnerAcceptAction'
    ))->where('partner_id', '[0-9]+')
            ->where('brand_id', '[0-9]+');


    Route::post('/profile/partners/{user_id}/brand/{brand_id}/reject', array(
        'before' => 'csrf',
        'as' => 'partnerRejectAction',
        'uses' => 'ProfileController@partnerRejectAction'
    ))->where('partner_id', '[0-9]+')
            ->where('brand_id', '[0-9]+');

    Route::post('/profile/partners/{user_id}/brand/{brand_id}/remove', array(
        'before' => 'csrf',
        'as' => 'partnerRemoveAction',
        'uses' => 'ProfileController@partnerRemoveAction'
    ))->where('partner_id', '[0-9]+')->where('brand_id', '[0-9]+');

    // Dodawanie partnera
    Route::post('/partner/add', array(
        'before'=>'csrf',
        'as' => 'partnerAddAction',
        'uses' => 'UserController@partnerAddAction'
    ))->where('partner_id', '[0-9]+');

    // Wysyłanie wiadomości do partnera
    Route::post('/partner/{partner_id}/message/send', array(
        'before' => 'csrf',
        'as' => 'partnerSendMessage',
        'uses' => 'UserController@sendMessage'
    ))->where('partner_id', '[0-9]+');

    // Dodawanie rekomendacji
    Route::post('/recommendation/add', array(
        'before' => 'csrf',
        'as' => 'addRecommendation',
        'uses' => 'RecommendationController@add'
    ));

    // Zakup pakietu
    Route::post('/package/pay', array(
        'before' => 'csrf',
        'as' => 'payPackage',
        'uses' => 'PackageController@renderPayView'
    ));

    Route::get('/package/pay/success', array(
        'as' => 'paySuccess',
        'uses' => 'ProfileController@renderPackagePaySuccess'
    ));

    Route::get('/package/pay/error', array(
        'as' => 'payError',
        'uses' => 'ProfileController@renderPackagePayError'
    ));

    // Dodawanie nadużyia do rekomendacji
    Route::post('/abuse/add', array(
        'before' => 'csrf',
        'as' => 'addAbuse',
        'uses' => 'AbuseController@add'
    ));

    // Dodawanie nadużyia do rekomendacji
    Route::post('/comment/add', array(
        'before' => 'csrf',
        'as' => 'addComment',
        'uses' => 'CommentController@add'
    ));

    // Pliki akcji
    Route::get('/actionfile/{filename}', array(
        'as' => 'getActionfile',
        'uses' => 'ActionfileController@getRegularFile'
    ));

    Route::get('/actionfile/thumb/{filename}', array(
        'as' => 'getActionfileThumb',
        'uses' => 'ActionfileController@getThumbFile'
    ));

    // usuwanie plików akcji podczas dodawania
    Route::post('/actionfile/session/remove', array(
        'as' => 'actionFileSessionRemove',
        'uses' => 'ActionfileController@removeFileFromSession'
    ));

    // usuwanie plików z utworzonej już akcji
    Route::post('/actionfile/remove/{actionfile_id}', array(
        'as' => 'actionFileRemove',
        'uses' => 'ActionfileController@removeFile'
    ));

    Route::get('/products/list/category/{category}/sortby/{sortby}/sorttype/{sorttype}/{searchterm?}', array(
                'as' => 'productsList',
                'uses' => 'ProductController@productsList'
            ))->where('category', '[0-9]+|all')
            ->where('sortby', 'date|price')
            ->where('sorttype', 'asc|desc');

    Route::get('/product/{product_id}', array(
        'as' => 'showProductView',
        'uses' => 'ProductController@renderShowView'
    ))->where('product_id', '[0-9]+');

    Route::get('/product/{product_id}', array(
        'as' => 'showProductView',
        'uses' => 'ProductController@renderShowView'
    ))->where('product_id', '[0-9]+');
});

Route::get('/login', array(
    'as' => 'loginView',
    'uses' => 'AuthenticationController@render'
));

Route::post('/login', array(
    'before' => 'csrf',
    'as' => 'loginAction',
    'uses' => 'AuthenticationController@login'
));

Route::get('/logout', array(
    'as' => 'logoutAction',
    'uses' => 'AuthenticationController@logout'
));

Route::get('/rejestracja', array(
    'as' => 'registerView',
    'uses' => 'RegistrationController@render'
));

Route::post('/rejestracja', array(
    'before' => 'csrf',
    'as' => 'registerUserAction',
    'uses' => 'RegistrationController@registerUser'
));

Route::post('/rejestracja/producent', array(
    'before' => 'csrf',
    'as' => 'registerProducerAction',
    'uses' => 'RegistrationController@registerProducer'
));

Route::get('/rejestracja/producent', array(
    'as' => 'registerProducerView',
    'uses' => 'RegistrationController@renderProducerRegisterView'
));

Route::post('/produkt/dodaj', array(
    'before' => 'ajax|csrf',
    'as' => 'addProduct',
    'uses' => 'ProductController@addProduct'
));

Route::post('/produkt/zapisz/{product_id}', array(
    'before' => 'ajax|csrf',
    'as' => 'saveProduct',
    'uses' => 'ProductController@saveProduct'
))->where('product_id', '[0-9]+');

Route::post('/produkt/usun/{product_id}', array(
    'before' => 'ajax|csrf',
    'as' => 'removeProduct',
    'uses' => 'ProductController@removeProduct'
))->where('product_id', '[0-9]+');

// Widok podglądu brandu
Route::get('/brand/{brand_id}', array(
    'as' => 'showBrand',
    'uses' => 'BrandController@show'
))->where('brand_id', '[0-9]+');

// Lista podglądu akcji brandu
Route::get('/brand/{brand_id}/actions', array(
    'as' => 'showBrandActions',
    'uses' => 'BrandController@showActions'
))->where('brand_id', '[0-9]+');

Route::post('/brand/dodaj', array(
    'before' => 'ajax|csrf',
    'as' => 'addBrand',
    'uses' => 'BrandController@addBrand'
));

Route::post('/brand/usun/{brand_id}', array(
    'before' => 'ajax|csrf',
    'as' => 'removeBrand',
    'uses' => 'BrandController@removeBrand'
))->where('brand_id', '[0-9]+');

Route::post('/brand/zapisz/{brand_id}', array(
    'before' => 'ajax|csrf',
    'as' => 'saveBrand',
    'uses' => 'BrandController@saveBrand'
))->where('brand_id', '[0-9]+');

Route::post('/rejestracja/produkt/usun/{product_id}', array(
    'before' => 'ajax|csrf',
    'as' => 'registerRemoveProduct',
    'uses' => 'ProductController@removeProductFromRegistrationForm'
))->where('product_id', '[0-9]+');

Route::post('/rejestracja/brand/usun/{brand_id}', array(
    'before' => 'ajax|csrf',
    'as' => 'registerRemoveBrand',
    'uses' => 'BrandController@removeBrandFromRegistrationForm'
))->where('brand_id', '[0-9]+');

Route::get('/activate/{code}', array(
    'as' => 'activateAction',
    'uses' => 'UserController@activateAccount'
));

Route::get('/password/reset', array(
    'as' => 'passwordResetView',
    'uses' => 'AuthenticationController@renderPasswordResetView'
));

Route::post('/password/sendcode', array(
    'as' => 'sendPasswordResetCodeAction',
    'uses' => 'AuthenticationController@sendPasswordResetCode'
));

Route::get('/password/reset/{code}', array(
    'as' => 'newPasswordView',
    'uses' => 'AuthenticationController@renderNewPasswordView'
));

Route::post('/password/reset', array(
    'as' => 'resetPasswordAction',
    'uses' => 'AuthenticationController@resetPassword'
));

Route::post('/newsletter/subscribe', array(
    'as' => 'newsletterSubscribeAction',
    'uses' => 'NewsletterController@subscribeToNewsletter'
));

Route::get('/newsletter/unsubscribe/{code}', array(
    'as' => 'newsletterUnsubscribeAction',
    'uses' => 'NewsletterController@unsubscribeFromNewsletter'
));

// Avatary
Route::get('/avatar/{filename}', array(
    'as' => 'getAvatar',
    'uses' => 'AvatarController@getRegularFile'
));

Route::get('/avatar/thumb/{filename}', array(
    'as' => 'getAvatarThumb',
    'uses' => 'AvatarController@getThumbFile'
));

Route::post('/avatar/upload/{user_id?}', array(
    'as' => 'uploadAvatar',
    'uses' => 'AvatarController@uploadFile'
))->where('user_id', '[0-9]+');

// Ładowanie zdjęć produktów
Route::post('/image/product/upload/{product_id?}', array(
    'before' => 'ajax|csrf',
    'as' => 'uploadProductImage',
    'uses' => 'ImageUploadController@uploadProductImage'
))->where('product_id', '[0-9]+');

// Ładowanie zdjęć produktów przez PUT
Route::put('/image/product/upload/{product_id?}', array(
    'before' => 'ajax|csrf',
    'as' => 'uploadProductImage',
    'uses' => 'ImageUploadController@uploadProductImage'
))->where('product_id', '[0-9]+');

//Pobieranie zdjęć produktów
Route::get('/image/product/{filename}', array(
    'as' => 'getProductImage',
    'uses' => 'ImageUploadController@getRegularProductImageFile'
));

Route::get('/image/product/thumb/{filename}', array(
    'as' => 'getProductImageThumb',
    'uses' => 'ImageUploadController@getProductImageThumbFile'
));

// Ładowanie zdjęć brandów
Route::post('/image/brand/upload/{brand_id?}', array(
    'before' => 'ajax|csrf',
    'as' => 'uploadBrandImage',
    'uses' => 'ImageUploadController@uploadBrandImage'
))->where('brand_id', '[0-9]+');

Route::put('/image/brand/upload/{brand_id?}', array(
    'before' => 'ajax|csrf',
    'as' => 'uploadBrandImage',
    'uses' => 'ImageUploadController@uploadBrandImage'
))->where('brand_id', '[0-9]+');

//Pobieranie zdjęć brandów
Route::get('/image/brand/{filename}', array(
    'as' => 'getBrandImage',
    'uses' => 'ImageUploadController@getRegularBrandImageFile'
));

Route::get('/image/brand/thumb/{filename}', array(
    'as' => 'getBrandImageThumb',
    'uses' => 'ImageUploadController@getBrandImageThumbFile'
));

// Kopiowanie avataru do branda
Route::post('/image/brand/copy/{avatar_id}', array(
    'before' => 'ajax|csrf',
    'as' => 'copyAvatarToBrandImage',
    'uses' => 'AvatarController@copyAvatarToBrand'
))->where('avatar_id', '[0-9]+');

// Listy akcji
Route::get('/actions/list/category/{category}/district/{district}/sortby/{sortby}/sorttype/{sorttype}/{searchterm?}', array(
            'as' => 'actionsList',
            'uses' => 'ActionController@actionsList'
        ))->where('category', '[0-9]+|all')
        ->where('district', '[0-9]+|all')
        ->where('sortby', 'latest|ending|applications|budget')
        ->where('sorttype', 'asc|desc');

// Listy partnerów
Route::get('/partners', array(
    'as' => 'listPartners',
    'uses' => 'UserController@listPartners'
));

// Listy aktywnych brandów
Route::get('/brands', array(
    'as' => 'listBrands',
    'uses' => 'BrandController@listBrands'
));

// Listy partnerów
Route::get('/selector/categories', array(
    'as' => 'listCategories',
    'uses' => 'CategoryController@renderList'
));

Route::get('/partner/{id}', array(
    'as' => 'showPartner',
    'uses' => 'UserController@showPartner'
))->where('id', '[0-9]+|all');

Route::get('/partner/{id}/actions', array(
    'as' => 'showPartnerActions',
    'uses' => 'UserController@showPartnerActions'
))->where('id', '[0-9]+|all');

// widok akcji
Route::get('/action/{action_id}', array(
    'as' => 'actionView',
    'uses' => 'ActionController@renderActionView'
))->where('action_id', '[0-9]+');

Route::get('{name}', function($name) {

    $airt = Airt::where('from', $name)->where('is_active', 1)->first();
    $page = Page::where('title', $airt->to)->first();
    if ($airt && $page) {
        return View::make('layouts.airt')->with('content', $page->content);
    } else {
        App::abort(404);
    }
});


