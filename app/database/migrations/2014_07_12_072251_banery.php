<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Banery extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('bannerpositions', function($table) {
            $table->increments('id');
            $table->string('name', 60);
            $table->string('human_name', 100);
        });

        Schema::create('banners', function($table) {
            $table->increments('id');
            $table->unsignedInteger('position_id')->index('`fk_banners_positions_idx`');
            $table->foreign('position_id')->references('id')->on('bannerpositions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('content', 60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
