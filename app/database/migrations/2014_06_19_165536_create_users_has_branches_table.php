<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersHasBranchesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users_has_branches', function(Blueprint $table) {
            $table->unsignedInteger('user_id')->index('`fk_users_has_branches_users_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('branch_id')->index('`fk_users_has_branches_branches_idx`');
            $table->foreign('branch_id')->references('id')->on('branches')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->primary(['user_id', 'branch_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users_has_branches');
    }

}
