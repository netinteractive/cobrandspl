<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartnersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('partners', function(Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('user_id')->index('`fk_partners_users_1_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('brand_id')->index('`fk_partners_brands_1_idx`');
            $table->foreign('brand_id')->references('id')->on('brands')->onUpdate('CASCADE')->onDelete('CASCADE');
            
            $table->unsignedInteger('partner_id')->index('`fk_partners_users_2_idx`');
            $table->foreign('partner_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('partner_brand_id')->index('`fk_partners_brands_2_idx`');
            $table->foreign('partner_brand_id')->references('id')->on('brands')->onUpdate('CASCADE')->onDelete('CASCADE');
            
            $table->boolean('invitation_accepted')->default(0);
            $table->boolean('invitation_rejected')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('partners');
    }

}
