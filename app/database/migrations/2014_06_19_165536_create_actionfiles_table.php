<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionfilesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('actionfiles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('filename', 200);
            $table->string('original_filename', 300);
            $table->string('filetype', 20);
            $table->string('mimetype', 300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('actionfiles');
    }

}
