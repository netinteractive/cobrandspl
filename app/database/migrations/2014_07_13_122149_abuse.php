<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Abuse extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('abuses', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recommendation_id')->index('`fk_abuses_recommendations_idx`');
            $table->foreign('recommendation_id')->references('id')->on('recommendations')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('abuses');
    }

}
