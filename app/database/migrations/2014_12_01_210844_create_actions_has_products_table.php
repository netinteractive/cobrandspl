<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsHasProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('action_has_products', function(Blueprint $table) {
            $table->unsignedInteger('action_id')->index('`fk_action_has_products_actions_idx`');
            $table->foreign('action_id')->references('id')->on('actions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('product_id')->index('`fk_action_has_products_products_idx`');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->primary(['action_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
