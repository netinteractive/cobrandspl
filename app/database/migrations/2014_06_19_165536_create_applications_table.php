<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('applications', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index('`fk_applications_users_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('action_id')->index('`fk_applications_actions_idx`');
            $table->foreign('action_id')->references('id')->on('actions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('applicationstate_id')->index('`fk_applications_applicationstate_idx`');
            $table->foreign('applicationstate_id')->references('id')->on('applicationstates')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->boolean('accepted_by_applicant');
            $table->boolean('accepted_by_admin')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('applications');
    }

}
