<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('brands', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index('`fk_actions_users_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('name', 150)->unique();
            $table->text('description');
            $table->unsignedInteger('logo_id')->nullable()->index('`fk_brands_logos_idx`');
            $table->foreign('logo_id')->references('id')->on('logos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->boolean('accepted')->default(false);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
