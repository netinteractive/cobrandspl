<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comment extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('comments', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('action_id')->index('`fk_comments_actions_idx`');
            $table->foreign('action_id')->references('id')->on('actions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('author_id')->index('`fk_comments_users_idx`');
            $table->foreign('author_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('abuses');
    }

}
