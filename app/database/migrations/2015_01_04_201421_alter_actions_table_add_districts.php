<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActionsTableAddDistricts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('actions', function(Blueprint $table) {
            $table->unsignedInteger('district_id')->unsigned()->index('`fk_users_districts_idx`');
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
