<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecommendationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('recommendations', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('action_id')->index('`fk_recommendations_actions_idx`');
            $table->foreign('action_id')->references('id')->on('actions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('user_id')->index('`fk_recommendations_users_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('added_user_id')->index('`fk_recommendations_users_idx2`');
            $table->foreign('added_user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('recommendations');
    }

}
