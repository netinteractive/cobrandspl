<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200)->unique();
            $table->unsignedInteger('user_id')->index('`fk_actions_users_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('product_category_id')->index('`fk_actions_product_categories_idx`');
            $table->foreign('product_category_id')->references('id')->on('product_categories')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('description');
            $table->boolean('active')->default(false);
            $table->unsignedInteger('product_count')->default(0);
            $table->unsignedInteger('logo_id')->nullable()->index('`fk_products_logos_idx`');
            $table->foreign('logo_id')->references('id')->on('product_logos')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('price_count');
            $table->decimal('price_1', 10, 2);
            $table->unsignedInteger('count_1_from');
            $table->unsignedInteger('count_1_to');
            $table->decimal('price_2', 10, 2)->nullable();
            $table->unsignedInteger('count_2_from')->nullable();
            $table->unsignedInteger('count_2_to')->nullable();
            $table->decimal('price_3', 10, 2)->nullable();
            $table->unsignedInteger('count_3_from')->nullable();
            $table->unsignedInteger('count_3_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
