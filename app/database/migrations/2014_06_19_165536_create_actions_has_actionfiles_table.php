<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionsHasActionfilesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('actions_has_actionfiles', function(Blueprint $table) {
            $table->unsignedInteger('action_id')->index('`fk_actions_has_actionfiles_actions_1_idx`');
            $table->foreign('action_id')->references('id')->on('actions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('actionfile_id')->index('`fk_actions_has_actionfiles_actionfiles_2_idx`');
            $table->foreign('actionfile_id')->references('id')->on('actionfiles')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->primary(['action_id', 'actionfile_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('actions_has_actionfiles');
    }

}
