<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('actions', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index('`fk_actions_users_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('category_id')->index('`fk_actions_categories_idx`');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('description')->nullable();
            $table->text('description_more')->nullable();
            $table->integer('required_partner_count')->unsigned();
            $table->integer('current_partner_count')->unsigned()->default(0);
            $table->dateTime('stage_deadline_one');
            $table->dateTime('stage_deadline_two');
            $table->decimal('budget', 10);
            $table->dateTime('action_date_start');
            $table->dateTime('action_date_end');
            $table->unsignedInteger('actionstate_id')->index('`fk_actions_actionstate_idx`');
            $table->foreign('actionstate_id')->references('id')->on('actionstates')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('avatar_id')->nullable()->index('`fk_users_avatars_idx`');
            $table->foreign('avatar_id')->references('id')->on('avatars')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('actions');
    }

}
