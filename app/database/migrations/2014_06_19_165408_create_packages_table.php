<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('packages', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200);
            $table->decimal('price', 10, 2);
            $table->unsignedInteger('packagetype_id')->index('`fk_packages_packagetypes_idx`');
            $table->foreign('packagetype_id')->references('id')->on('packagetypes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('packages');
    }

}
