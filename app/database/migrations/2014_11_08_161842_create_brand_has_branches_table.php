<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandHasBranchesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('brand_has_branches', function(Blueprint $table) {
            $table->unsignedInteger('brand_id')->index('`fk_brands_has_branches_brands_idx`');
            $table->foreign('brand_id')->references('id')->on('brands')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('branch_id')->index('`fk_brands_has_branches_branches_idx`');
            $table->foreign('branch_id')->references('id')->on('branches')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->primary(['brand_id', 'branch_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
