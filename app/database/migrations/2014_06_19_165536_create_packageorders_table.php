<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackageordersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('packageorders', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index('`fk_packageorders_users_idx`');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('package_id')->index('`fk_packageorders_packages_idx`');
            $table->foreign('package_id')->references('id')->on('packages')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->dateTime('startdate')->nullable();
            $table->dateTime('enddate')->nullable();
            $table->boolean('active');
            $table->boolean('used')->default(false);
            $table->decimal('price', 10, 2);
            $table->unsignedInteger('paymentstatus_id')->index('`fk_packageorders_paymentstatus_idx`');
            $table->foreign('paymentstatus_id')->references('id')->on('paymentstatus')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('packageorders');
    }

}
