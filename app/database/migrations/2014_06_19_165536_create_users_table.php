<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function(Blueprint $table) {
            $table->string('companyname', 250)->unique();
            $table->unsignedInteger('avatar_id')->nullable()->index('`fk_users_avatars_idx`');
            $table->foreign('avatar_id')->references('id')->on('avatars')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('name', 100);
            $table->string('street', 200);
            $table->string('postcode', 20);
            $table->string('city', 200);
            $table->unsignedInteger('district_id')->unsigned()->index('`fk_users_districts_idx`');
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('nip', 10);
            $table->string('regon', 14)->nullable();
            $table->string('phone_main', 45);
            $table->string('phone_alternative', 45)->nullable();
            $table->boolean('terms_accepted')->default(0);
            $table->unsignedInteger('companysize_id')->index('`fk_users_companysizes_idx`');
            $table->foreign('companysize_id')->references('id')->on('companysizes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('description', 3000);
            $table->boolean('accepted')->default(0);
            $table->boolean('is_producer')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }

}
