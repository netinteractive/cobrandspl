<?php

/**
 * Description of BrandTableSeeder
 *
 * @author damian
 */
class BrandTableSeeder extends Seeder {

    public function run() {
        DB::table('brands')->delete();
        $brand = Brand::create(array(
                    'user_id' => 2,
                    'name' => 'Coca-cola',
                    'description' => '&lt;p&gt;Marka napoj&amp;oacute;w gazowych tych od św. Mikołaja&lt;/p&gt;',
                    'logo_id' => 2,
                    'accepted' => true,
                    'active' => true
        ));
        $brand->branches()->attach(56);
        $brand->branches()->attach(57);

        $brand = Brand::create(array(
                    'user_id' => 2,
                    'name' => 'Pepsi',
                    'description' => '&lt;p&gt;Marka napoj&amp;oacute;w gazownych &lt;strong&gt;lepkich i czarnych&lt;/strong&gt;&lt;/p&gt;',
                    'logo_id' => 3,
                    'accepted' => true,
                    'active' => true
        ));
        $brand->branches()->attach(83);
        $brand->branches()->attach(84);
        $brand->branches()->attach(85);

        $brand = Brand::create(array(
                    'user_id' => 3,
                    'name' => 'Toyota',
                    'description' => '&lt;p&gt;Solidne samochody&lt;/p&gt;',
                    'logo_id' => 4,
                    'accepted' => true,
                    'active' => true
        ));
        $brand->branches()->attach(198);
        $brand->branches()->attach(199);
        
        $brand = Brand::create(array(
                    'user_id' => 3,
                    'name' => 'Lexus',
                    'description' => '&lt;p&gt;Jeszcze solidniejsze samochody niż toyota&lt;/p&gt;',
                    'logo_id' => 5,
                    'accepted' => true,
                    'active' => true
        ));
        $brand->branches()->attach(249);
        $brand->branches()->attach(250);
        $brand->branches()->attach(251);
        
        $brand = Brand::create(array(
                    'user_id' => 4,
                    'name' => 'Panasonic',
                    'description' => '&lt;p&gt;Marka sprzętu elektronicznego najlepszej jakości&lt;/p&gt;',
                    'logo_id' => 6,
                    'accepted' => true,
                    'active' => true
        ));
        $brand->branches()->attach(65);
        $brand->branches()->attach(66);
        
        $brand = Brand::create(array(
                    'user_id' => 4,
                    'name' => 'Philips',
                    'description' => '&lt;p&gt;Bardzo porządny sprzęt elektroniczny&lt;/p&gt;',
                    'logo_id' => 7,
                    'accepted' => true,
                    'active' => true
        ));
        $brand->branches()->attach(19);
        $brand->branches()->attach(20);
        $brand->branches()->attach(21);
        
        $brand = Brand::create(array(
                    'user_id' => 5,
                    'name' => 'Orion',
                    'description' => '&lt;p&gt;Wymarła marka sprzętu elektronicznego godnego polecenia&lt;/p&gt;',
                    'logo_id' => 8,
                    'accepted' => true,
                    'active' => true
        ));
        $brand->branches()->attach(45);
        $brand->branches()->attach(46);
    }

}
