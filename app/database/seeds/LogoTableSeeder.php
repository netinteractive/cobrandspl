<?php

/**
 * Description of LogoTableSeeder
 *
 * @author damian
 */
class LogoTableSeeder extends Seeder {

    public function run() {
        DB::table('logos')->delete();
        Logo::create(array('filename' => 'default', 'filetype' => 'png'));
        Logo::create(array('filename' => 'S4jV2pwHUzH1Bg8Pb3K8msV8IfEUKz', 'filetype' => 'jpeg'));
        Logo::create(array('filename' => 'aLqCyYgSeZGtMfaTkQCw7ARvWMNc4Y', 'filetype' => 'jpeg'));
        Logo::create(array('filename' => 'wwFN6xtlLeZTLSLyKh8Pv1OI5ka67U', 'filetype' => 'jpeg'));
        Logo::create(array('filename' => 'wZBdov4qqc0sd8qGVyPCXKbRZ8TvWG', 'filetype' => 'jpeg'));
        Logo::create(array('filename' => 'ZwnabvZreQwKrW4QpNXKXTx6hedsE7', 'filetype' => 'jpeg'));
        Logo::create(array('filename' => 'BLwu5md7TmaXwEWLyfvYwPBt2QbeDm', 'filetype' => 'jpeg'));
        Logo::create(array('filename' => 'dshZirgmG6uItikH0Ci3NvpLu140lZ', 'filetype' => 'png'));
    }

}
