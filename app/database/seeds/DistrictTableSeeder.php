<?php

class DistrictTableSeeder extends Seeder {

    public function run() {
        DB::table('districts')->delete();
        District::create(array('name' => "dolnośląskie"));
        District::create(array('name' => "kujawsko-pomorskie"));
        District::create(array('name' => "lubelskie"));
        District::create(array('name' => "lubuskie"));
        District::create(array('name' => "łódzkie"));
        District::create(array('name' => "małopolskie"));
        District::create(array('name' => "mazowieckie"));
        District::create(array('name' => "opolskie"));
        District::create(array('name' => "podkarpackie"));
        District::create(array('name' => "podlaskie"));
        District::create(array('name' => "pomorskie"));
        District::create(array('name' => "śląskie"));
        District::create(array('name' => "świętokrzyskie"));
        District::create(array('name' => "warmińsko-mazurskie"));
        District::create(array('name' => "wielkopolskie"));
        District::create(array('name' => "zachodniopomorskie"));
    }

}

?>
