<?php

/**
 * Description of GroupTableSeeder
 *
 * @author damian
 */
class GroupTableSeeder extends Seeder {

    public function run() {

        Sentry::createGroup(array(
            'name' => 'GROUP_USER',
            'permissions' => array(
                'admin' => 0,
                'users' => 1,
            ),
        ));

        Sentry::createGroup(array(
            'name' => 'GROUP_ADMIN',
            'permissions' => array(
                'admin' => 1,
                'users' => 1,
            ),
        ));
    }

}
