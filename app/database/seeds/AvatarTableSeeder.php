<?php

/**
 * Description of AvatarTableSeeder
 *
 * @author damian
 */
class AvatarTableSeeder extends Seeder {

    public function run() {
        DB::table('avatars')->delete();
        Avatar::create(array('filename' => 'no_avatar', 'filetype' => 'png'));
        Avatar::create(array('filename' => 'avatar_1', 'filetype' => 'png'));
        Avatar::create(array('filename' => 'avatar_2', 'filetype' => 'png'));
        Avatar::create(array('filename' => 'avatar_3', 'filetype' => 'png'));
        Avatar::create(array('filename' => 'avatar_4', 'filetype' => 'png'));
    }

}
