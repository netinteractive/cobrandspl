<?php

/**
 * Description of PackageTableSeeder
 *
 * @author damian
 */
class PackageTableSeeder extends Seeder {

    public function run() {
        DB::table('packages')->delete();
        Package::create(array('name' => 'Dodanie jednej akcji', 'price' => 10.99, 'packagetype_id' => 1));
        Package::create(array('name' => 'Abonament miesięczny', 'price' => 199.99, 'packagetype_id' => 2));
    }

}
