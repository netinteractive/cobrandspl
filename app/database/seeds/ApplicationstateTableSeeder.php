<?php

/**
 * Description of ApplicationstateTableSeeder
 *
 * @author damian
 */
class ApplicationstateTableSeeder extends Seeder {

    public function run() {
        DB::table('applicationstates')->delete();
        Applicationstate::create(array('name' => 'ACCEPTED', 'human_name' => 'Zaakceptowany'));
        Applicationstate::create(array('name' => 'RESERVE', 'human_name' => 'Rezerwowy'));
        Applicationstate::create(array('name' => 'WAITING', 'human_name' => 'Oczekuje'));
    }

}
