<?php

/**
 * Description of ProductLogoTableSeeder
 *
 * @author damian
 */
class ProductLogoTableSeeder extends Seeder {

    public function run() {
        DB::table('product_logos')->delete();
        ProductLogo::create(array('filename' => 'no_logo', 'filetype' => 'png'));
        ProductLogo::create(array('filename' => 'bHM6uUosdrebmm40Oo4o2MSm462sGM', 'filetype' => 'jpeg'));
        ProductLogo::create(array('filename' => 'C5Nh1jj3sjNkC8ZMMAHlKE5129ZpNe', 'filetype' => 'jpeg'));
        ProductLogo::create(array('filename' => 'sJSDdAu3iGGthPFR4qRobEZmXADB8l', 'filetype' => 'jpeg'));
        ProductLogo::create(array('filename' => 'ZIplgP0OyYVtn9hzwtKsDlR3p5h8Vz', 'filetype' => 'jpeg'));
    }

}
