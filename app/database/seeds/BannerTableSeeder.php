<?php

/**
 * Description of BannerTableSeeder
 *
 * @author damian
 */
class BannerTableSeeder extends Seeder {

    public function run() {
        DB::table('banners')->delete();
        // Baner poziomy
        Banner::create(array('position_id' => 1, 'content' => "<span style='font-size:10px'>Reklamy</span><br/><img style='margin-right:15px' src='http://lorempixel.com/200/100/' /><img style='margin-right:15px' src='http://lorempixel.com/220/100/' /><img src='http://lorempixel.com/205/100/' />"));
        // Baner pionowy
        Banner::create(array('position_id' => 2, 'content' => "<span style='font-size:10px'>Reklamy</span><br/><img style='margin-bottom:15px' src='http://lorempixel.com/150/200/' /><br/><img style='margin-bottom:15px' src='http://lorempixel.com/150/210/' /><br/><img src='http://lorempixel.com/150/205/' /><br/>"));
    }

}
