<?php

/**
 * Description of ProductCategoryTableSeeder
 *
 * @author damian
 */
class ProductCategoryTableSeeder extends Seeder {

    public function run() {
        DB::table('product_categories')->delete();
        $pc = ProductCategory::create(array('name' => 'SUROWCE I ENERGETYKA', 'is_main' => true));

        $first_level = ProductCategory::create(array('name' => 'Paliwa', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Węgiel', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Benzyna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Gaz LPG', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Biopaliwa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Oleje', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Surowce do produkcji', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Kruszywo', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Tworzywa sztuczne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Mineralne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Metale', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Surówka', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Żelazne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Kolorowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Szlachetne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Drewno', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Surowe drewno', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Półprodukty', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Produkty stolarskie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Zaopatrzenie w energię', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Energia elektryczna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Dystrybucja paliw', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Energia odnawialna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $pc->id));

        $pc = ProductCategory::create(array('name' => 'PRZEMYSŁ', 'is_main' => true));
        $first_level = ProductCategory::create(array('name' => 'Elementy metalowe', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Produkty ze stali', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Pozostałe metale', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Maszyny i akcesoria', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Ogólne przeznaczenie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Automatyka przemysłowa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Elektronarzędzia', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Rolnictwo i leśnictwo', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Specjalne przeznaczenie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Obróbka metali', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Naprawa i instalacja', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Wynajem i dierżawa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Narzędzia', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Akcesoria', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Tekstylia i skóra', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Tkaniny', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Wyroby włókiennicze', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Odzież', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Produkty skórzane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Obróbka metali', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Obróbka skrawaniem', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Obróbka plastyczna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Obróbka cieplna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Obróbka chemiczna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Chemia przemysłowa', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Chemia organiczna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Chemia nieorganiczna', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Rolnictwo i spożywczy', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Mięsny', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Rybny', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Mleczarski', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Olejarski', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Młynarski', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Owocowo-warzywny', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Przetwórstwo spożywcze', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $pc->id));

        $pc = ProductCategory::create(array('name' => 'ARTYKUŁY POWRZECHNEGO UŻYTKU', 'is_main' => true));
        $first_level = ProductCategory::create(array('name' => 'Opakowania', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Opakowania papierowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Opakowania plastikowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Opakowania metalowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Opakowania szklane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Opakowania drewniane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Usługi pakowania', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Etykiety', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Folie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Taśmy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Artykuły spożywcze', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Mięsa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Ryby', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Owoce i warzywa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Oleje i tłuszcze', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Wyroby mleczarskie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Pieczywo', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Napoje', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Słodycze', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Farmacja i medycyna', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Leki', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Artykuły medyczne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Sprzęt medyczny', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Chemia gospodarcza', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Farby i lakiery', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Środki czyszczące', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Wyposażenie domu', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Ceramika i szkło', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Uroda', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Dla dziecka', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Ogród', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Meble', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Narzędzia', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Motoryzacja', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Dom i ogród', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $pc->id));

        $pc = ProductCategory::create(array('name' => 'ELEKTRONIKA I IT', 'is_main' => true));
        $first_level = ProductCategory::create(array('name' => 'Komputery', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Laptopy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Stacjonarne, PC', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Serwery', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Urządzenia sieciowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Tablety', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Podzespoły i akcesoria', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Oprogramowanie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'RTV i AGD', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Sprzęt audio', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Sprzęt wideo', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'AGD drobne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'AGD duże', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Telekomunikacja', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Telefony i centrale', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Internet', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Usługi telekomunikacyjne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Akcesoria', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Usługi informatyczne', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Hosting', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Oprogramowanie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Części elektroniczne', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Mikroprocesory', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Silniki', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Obwody drukowane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Oporniki', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Układy scalone', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Mierniki', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Cewki, kondensatory, diody', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $pc->id));

        $pc = ProductCategory::create(array('name' => 'LOGISTYKA I POJAZDY', 'is_main' => true));
        $first_level = ProductCategory::create(array('name' => 'Samochody', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Osobowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Ciężarowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Nadwozia i przyczepy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Konserwacja i naprawa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Części i akcesoria', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Wynajem i dzierżawa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Pojazdy specjalistyczne', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Łodzie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Tabor szynowy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Budowlane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Rolnicze', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Pojazdy przemysłowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Konserwacja i naprawa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Części i akcesoria', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Poczta i kurier', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Przesyłki duże', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Przesyłki małe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Logistyka', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Transport lądowy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Transport wodny', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Transport lotniczy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Magazynowanie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Produkty do transportu', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $pc->id));

        $pc = ProductCategory::create(array('name' => 'BUDOWNICTWO I NIERUCHOMOŚCI', 'is_main' => true));
        $first_level = ProductCategory::create(array('name' => 'Usługi budowlane', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Projekty budowlane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Roboty budowlane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Instalacje elektryczne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Instalacje wodno-kanalizacyjne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Wykończenia i remonty', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Maszyny budowlane', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Materiały budowlane', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Materiały konstrukcyjne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Piasek i kruszywa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Beton i podobne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Izolacja', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Materiały wykończeniowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Materiały instalacyjne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Narzędzia', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Obługa nieruchomości', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Ochrona', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Sprzątanie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Konserwacja nieruchomości', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Zarządzanie nieruchomością', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Pośrednictwo nieruchomości', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Biura', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Rzeczoznawcy majątkowi', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $pc->id));


        $pc = ProductCategory::create(array('name' => 'BIURO I USŁUGI DLA FIRMY', 'is_main' => true));
        $first_level = ProductCategory::create(array('name' => 'Doradztwo i szkolenia', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Działalność prawnicza', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Rachunkowość i księgowość', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Doradztwo zarządcze, audyt', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Rekrutacja', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Szkolenia', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Usługi specjalistyczne', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Ochrona', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Projektowanie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Usługi finansowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Ubezpieczenia', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Usługi medyczne', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Katering', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Call Center', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Reklama i marketing', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'PR i Komunikacja', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Eventy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Badania marketingowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Produkcja reklamy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Dystrybucja reklamy', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Materiały reklamowe', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Gadżety', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Gazetki i ulotki', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Podróże służbowe', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Hotele, pensjonaty', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Przejazdy i przeloty', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Materiały biurowe', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Pieczątki i akcesoria', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Wizytówki', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Zszywacze', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Materiały piśmiennicze', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Papier', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Sprzęt biurowy', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Bindowanie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Kserokopiarki i drukarki', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Tonery', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Sprzęt do prezentacji', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Wynajem i dzierżawa', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $pc->id));

        $pc = ProductCategory::create(array('name' => 'EDUKACJA I ROZRYWKA', 'is_main' => true));

        $first_level = ProductCategory::create(array('name' => 'Edukacja', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Materiały naukowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Oświata', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));

        $first_level = ProductCategory::create(array('name' => 'Sport', 'is_main' => false, 'parent_id' => $pc->id));
        ProductCategory::create(array('name' => 'Sporty letnie', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Sporty zimowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Imprezy firmowe', 'is_main' => false, 'parent_id' => $first_level->id));
        ProductCategory::create(array('name' => 'Inne', 'is_main' => false, 'parent_id' => $first_level->id));
    }

}
