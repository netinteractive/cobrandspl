<?php

/**
 * Description of ActionstateTableSeeder
 *
 * @author damian
 */
class ActionstateTableSeeder extends Seeder {

    public function run() {
        DB::table('actionstates')->delete();
        Actionstate::create(array('name' => 'NEW_CREATED'));
        Actionstate::create(array('name' => 'REMOVED'));
        Actionstate::create(array('name' => 'ENDED_AFTER_FIRST_STAGE'));
        Actionstate::create(array('name' => 'SECOND_STAGE_END_WAITING'));
        Actionstate::create(array('name' => 'ENDED_AFTER_SECOND_STAGE'));
    }

}
