<?php

/**
 * Description of CategoryTableSeeder
 *
 * @author damian
 */
class CategoryTableSeeder extends Seeder {

    public function run() {
        DB::table('categories')->delete();
        $category = Category::create(array('name' => 'INTERNET MARKETING', 'is_main' => true));
        Category::create(array('name' => 'reklama graficzna', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'search engine optimization (SEO)', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'search engine marketing (SEM)', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'zarządzanie stroną internetową firmy', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'e-mail marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'social media marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'viral marketing / buzz marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'crowdsourcing marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'reklama na posrtalach oferujących zakupy grupowe', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'referral marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'affiliate marketing', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'MOBILE MARKETING', 'is_main' => true));
        Category::create(array('name' => 'komunikacja poprzez SMS/MMS', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'wykorzystanie aplikacji mobilnych do promocji', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'in-game marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'kody QR', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'voice mailing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'location based marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'dedykowane reklamy display pod urządzenia mobilne', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'komunikacja poprzez bluetooth', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'OUTBOUND MARKETING', 'is_main' => true));
        Category::create(array('name' => 'marketing bezpośredni', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'telemarketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'kampanie email-marketingowe', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'kampanie wysyłkowe pocztą tradycyjną', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'door-to-door marketing', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'INBOUND MARKETING', 'is_main' => true));
        Category::create(array('name' => 'content marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'infolinie/telecentrum', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'EVENT MARKETING', 'is_main' => true));
        Category::create(array('name' => 'obecność z wykładem na konferencji lub seminarium branżowym', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'prezentacja na własnym stoisku na targach (trade show marketing)', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'organizacja festiwali, koncertów, pikników dla klientów', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'wyjazdy "lojalnościowe" dla kluczowych klientów', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'sponsoring wydarzeń organizowanych przez inne podmioty', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'ATL (above the line)', 'is_main' => true));
        Category::create(array('name' => 'reklama telewizyjna', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'reklama radiowa', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'reklama prasowa', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'outdoor marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'product placement', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'BTL (below the line)', 'is_main' => true));
        Category::create(array('name' => 'ambush marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'eco marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'guerilla marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'augmented reality marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'point-of-sale marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'merchandising', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'MARKETING PRODUKTOWY/BRAND MARKETING', 'is_main' => true));
        Category::create(array('name' => 'new product development', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'test-driven marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'rebranding', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'promotion marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'sampling', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'konkursy, quizy', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'marketing lojalnościowy', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'cross- i up-selling', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'certyfikacja', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'customer experience management', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'vertical marketing', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'MARKETING ANALITYCZNY', 'is_main' => true));
        Category::create(array('name' => 'database marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'closed loop marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'behavioral marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'geomarketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'neuromarketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'badania marketingowe', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'benchmarking', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'remarketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'real-time marketing', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'B2B MARKETING', 'is_main' => true));
        Category::create(array('name' => 'relationship marketing (account-based marketing, one-to-one marketing)', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'channel marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'cooperative marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'niche marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'yellow-pages / katalogi branżowe', 'is_main' => false, 'parent_id' => $category->id));

        $category = Category::create(array('name' => 'PUBLIC RELATIONS (PR)', 'is_main' => true));
        Category::create(array('name' => 'media relations', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'article marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'lobbying', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'celebrity marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'czarny PR', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'ord-of-mouth marketing', 'is_main' => false, 'parent_id' => $category->id));
        Category::create(array('name' => 'komunikacja wewnętrzna', 'is_main' => false, 'parent_id' => $category->id));
    }

}

?>
