<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $this->call('BannerpositionTableSeeder');
        $this->call('BannerTableSeeder');
        $this->call('DistrictTableSeeder');
        $this->call('CompanysizeTableSeeder');
        $this->call('BranchTableSeeder');
        $this->call('CategoryTableSeeder');
        $this->call('ProductCategoryTableSeeder');
        $this->call('GroupTableSeeder');
        $this->call('AvatarTableSeeder');
        $this->call('LogoTableSeeder');
        $this->call('UserTableSeeder');
        $this->call('ActionstateTableSeeder');
        $this->call('ApplicationstateTableSeeder');
        $this->call('PackagetypeTableSeeder');
        $this->call('PackageTableSeeder');
        $this->call('PaymentstatusTableSeeder');
        $this->call('PackageorderTableSeeder');
        $this->call('BrandTableSeeder');
        $this->call('ActionTableSeeder');
        $this->call('ApplicationTableSeeder');
        $this->call('PartnerTableSeeder');
        $this->call('RecommendationTableSeeder');
        $this->call('CommentTableSeeder');
        $this->call('AbuseTableSeeder');
        $this->call('ProductLogoTableSeeder');
        $this->call('ProductTableSeeder');
    }

}
