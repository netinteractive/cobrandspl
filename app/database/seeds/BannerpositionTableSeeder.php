<?php

/**
 * Description of BannerpositionTableSeeder
 *
 * @author damian
 */
class BannerpositionTableSeeder extends Seeder {

    public function run() {
        DB::table('bannerpositions')->delete();
        // Baner poziomy
        Bannerposition::create(array('name' => 'HORIZONTAL', 'human_name' => 'Baner poziomy'));
        Bannerposition::create(array('name' => 'VARTICAL', 'human_name' => 'Baner pionowy'));
    }

}
