<?php

/**
 * Description of ActionTableSeeder
 *
 * @author damian
 */
class ActionTableSeeder extends Seeder {

    public function run() {
        DB::table('actions')->delete();
        Action::create(array(
            'user_id' => 2,
            'category_id' => 6,
            'description' => \NetInteractive\Cobrands\Utils\ValidationUtils::encode('In a est elit. Curabitur volutpat, nulla nec convallis posuere, felis lectus ultricies orci, eget tincidunt ligula arcu nec orci. Nam sollicitudin at ipsum vel viverra. Proin nunc nibh, semper non dapibus id, cursus at tortor. Ut a mauris faucibus, tincidunt est eget, fringilla augue. Nulla facilisi. Proin lorem tellus, volutpat non aliquam sed, tempor id eros. In consequat nunc sapien, sit amet semper tellus semper ut. Vestibulum rutrum porttitor mi in ornare. Vestibulum et cursus ligula. Maecenas a imperdiet magna.'),
            'required_partner_count' => 2,
            'current_partner_count' => 2,
            'stage_deadline_one' => Carbon::now()->addWeek(),
            'stage_deadline_two' => Carbon::now()->addWeeks(3),
            'budget' => 2500.50,
            'action_date_start' => Carbon::now()->addMonth(),
            'action_date_end' => Carbon::now()->addMonth()->addWeek(),
            'actionstate_id' => 1,
            'brand_id' => 2,
            'district_id' => 3,
            'city' => 'Warszawa'
        ));

        Action::create(array(
            'user_id' => 3,
            'category_id' => 58,
            'description' => \NetInteractive\Cobrands\Utils\ValidationUtils::encode('In a est elit. Curabitur volutpat, nulla nec convallis posuere, felis lectus ultricies orci, eget tincidunt ligula arcu nec orci. Nam sollicitudin at ipsum vel viverra. Proin nunc nibh, semper non dapibus id, cursus at tortor. Ut a mauris faucibus, tincidunt est eget, fringilla augue. Nulla facilisi. Proin lorem tellus, volutpat non aliquam sed, tempor id eros. In consequat nunc sapien, sit amet semper tellus semper ut. Vestibulum rutrum porttitor mi in ornare. Vestibulum et cursus ligula. Maecenas a imperdiet magna.'),
            'required_partner_count' => 1,
            'stage_deadline_one' => Carbon::now()->subWeeks(5),
            'stage_deadline_two' => Carbon::now()->subWeeks(4),
            'budget' => 45000.45,
            'action_date_start' => Carbon::now()->addWeek(),
            'action_date_end' => Carbon::now()->addWeeks(2),
            'actionstate_id' => 3,
            'brand_id' => 4,
            'district_id' => 6,
            'city' => 'Pekin'
        ));

        Action::create(array(
            'user_id' => 2,
            'category_id' => 23,
            'description' => \NetInteractive\Cobrands\Utils\ValidationUtils::encode('In a est elit. Curabitur volutpat, nulla nec convallis posuere, felis lectus ultricies orci, eget tincidunt ligula arcu nec orci. Nam sollicitudin at ipsum vel viverra. Proin nunc nibh, semper non dapibus id, cursus at tortor. Ut a mauris faucibus, tincidunt est eget, fringilla augue. Nulla facilisi. Proin lorem tellus, volutpat non aliquam sed, tempor id eros. In consequat nunc sapien, sit amet semper tellus semper ut. Vestibulum rutrum porttitor mi in ornare. Vestibulum et cursus ligula. Maecenas a imperdiet magna.'),
            'required_partner_count' => 1,
            'stage_deadline_one' => Carbon::now()->subWeeks(5),
            'stage_deadline_two' => Carbon::now()->subWeeks(4),
            'budget' => 46000.45,
            'action_date_start' => Carbon::now()->addWeek(),
            'action_date_end' => Carbon::now()->addWeeks(2),
            'actionstate_id' => 3,
            'brand_id' => 1,
            'district_id' => 1,
            'city' => 'Barcelona'
        ));

        Action::create(array(
            'user_id' => 4,
            'category_id' => 55,
            'description' => \NetInteractive\Cobrands\Utils\ValidationUtils::encode('In a est elit. Curabitur volutpat, nulla nec convallis posuere, felis lectus ultricies orci, eget tincidunt ligula arcu nec orci. Nam sollicitudin at ipsum vel viverra. Proin nunc nibh, semper non dapibus id, cursus at tortor. Ut a mauris faucibus, tincidunt est eget, fringilla augue. Nulla facilisi. Proin lorem tellus, volutpat non aliquam sed, tempor id eros. In consequat nunc sapien, sit amet semper tellus semper ut. Vestibulum rutrum porttitor mi in ornare. Vestibulum et cursus ligula. Maecenas a imperdiet magna.'),
            'required_partner_count' => 1,
            'stage_deadline_one' => Carbon::now()->subWeeks(5),
            'stage_deadline_two' => Carbon::now()->subWeeks(4),
            'budget' => 600000.99,
            'action_date_start' => Carbon::now()->addWeek(),
            'action_date_end' => Carbon::now()->addWeeks(2),
            'actionstate_id' => 3,
            'brand_id' => 6,
            'district_id' => 8,
            'city' => 'Kraków'
        ));
    }

}
