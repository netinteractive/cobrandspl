<?php

/**
 * Description of PackageorderTableSeeder
 *
 * @author damian
 */
class PackageorderTableSeeder extends Seeder {

    public function run() {
        DB::table('packageorders')->delete();
        Packageorder::create(array(
            'user_id' => 2,
            'package_id' => 1,
            'active' => false,
            'price' => 10.99,
            'paymentstatus_id' => 1
        ));

        Packageorder::create(array(
            'user_id' => 2,
            'package_id' => 1,
            'active' => false,
            'price' => 10.99,
            'paymentstatus_id' => 1
        ));

        Packageorder::create(array(
            'user_id' => 4,
            'package_id' => 2,
            'active' => true,
            'startdate' => Carbon::now(),
            'enddate' => Carbon::now()->addMonth(),
            'price' => 199.99,
            'paymentstatus_id' => 1
        ));

        Packageorder::create(array(
            'user_id' => 4,
            'package_id' => 2,
            'active' => false,
            'startdate' => Carbon::now()->subMonths(5),
            'enddate' => Carbon::now()->subMonths(4),
            'price' => 199.99,
            'paymentstatus_id' => 1
        ));

        Packageorder::create(array(
            'user_id' => 5,
            'package_id' => 1,
            'active' => false,
            'price' => 10.99,
            'paymentstatus_id' => 1
        ));

        Packageorder::create(array(
            'user_id' => 5,
            'package_id' => 1,
            'active' => true,
            'price' => 10.99,
            'paymentstatus_id' => 1
        ));
    }

}
