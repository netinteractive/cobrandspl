<?php

/**
 * Description of CommentTableSeeder
 *
 * @author damian
 */
class CommentTableSeeder extends Seeder {

    public function run() {
        DB::table('comments')->delete();
        CommentModel::create(array('action_id' => 1, 'author_id' => 2, 'content' => 'Jakiś tam testowy koemntarzyk to akcji'));
        CommentModel::create(array('action_id' => 1, 'author_id' => 2, 'content' => 'Jakiś tam KOLEJNY testowy komentarzyk do tej samej akcji'));
        CommentModel::create(array('action_id' => 2, 'author_id' => 3, 'content' => 'Inny komentarz'));
    }

}

?>
