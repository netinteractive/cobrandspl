<?php

/**
 * Description of RecommendationTableSeeder
 *
 * @author damian
 */
class RecommendationTableSeeder extends Seeder {

    public function run() {
        DB::table('recommendations')->delete();
        Recommendation::create(array(
            'action_id' => 1,
            'user_id' => 2,
            'added_user_id' => 3,
            'content' => 'Duis consectetur hendrerit nisi at mattis. Etiam pulvinar odio in congue varius. Nullam varius ante eget rutrum laoreet. Curabitur dapibus volutpat dui, id dapibus justo accumsan vitae. Aenean mollis accumsan congue. Maecenas commodo ac leo eu adipiscing. Quisque adipiscing sapien at mi mattis luctus. Quisque at mauris mi. Nulla bibendum nisl eget nibh pharetra, eleifend hendrerit magna commodo.'));
        Recommendation::create(array(
            'action_id' => 3,
            'user_id' => 2,
            'added_user_id' => 4,
            'content' => 'Nunc congue ipsum venenatis vestibulum fringilla. Vivamus sodales adipiscing nisl. Phasellus adipiscing nulla eu est aliquet, et tempus mi sollicitudin. Sed vitae turpis in ante feugiat semper. Phasellus tristique dignissim lacus in tristique. Proin vel risus at eros dictum congue at eu neque. Nunc vulputate, massa id rutrum lacinia, nibh ligula laoreet justo, blandit tincidunt mi ipsum sit amet dui. Aenean tincidunt ultrices risus a congue. Etiam sed nibh sed augue ultrices condimentum a id sem. Nulla dapibus, est in volutpat molestie, nisl nulla sollicitudin es'));
    }

}
