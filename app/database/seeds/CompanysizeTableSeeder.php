<?php

/**
 * Description of CompanysizeTableSeeder
 *
 * @author damian
 */
class CompanysizeTableSeeder extends Seeder {

    public function run() {
        DB::table('companysizes')->delete();
        Companysize::create(array('name' => "Mikro (zatrudnienie mniej niż 10 pracowników rocznie)"));
        Companysize::create(array('name' => "Mała (zatrudnienie mniej niż 50 pracowników rocznie)"));
        Companysize::create(array('name' => "Średnia (zatrudnienie mniej niż 250 pracowników rocznie)"));
        Companysize::create(array('name' => "Duża (zatrudnienie 250 i wiecej pracowników rocznie)"));
    }

}
