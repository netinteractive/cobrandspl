<?php

/**
 * Description of PaymentstatusTableSeeder
 *
 * @author damian
 */
class PaymentstatusTableSeeder extends Seeder {

    public function run() {
        DB::table('paymentstatus')->delete();
        Paymentstatus::create(array('name' => 'COMPLETE', 'human_name' => 'zapłacone'));
        Paymentstatus::create(array('name' => 'PENDING', 'human_name' => 'oczekuje'));
    }

}
