<?php

/**
 * Description of PackagetypeTableSeeder
 *
 * @author damian
 */
class PackagetypeTableSeeder extends Seeder {

    public function run() {
        DB::table('packagetypes')->delete();
        Packagetype::create(array('name' => 'SINGLE','human_name'=>'jednorazowy'));
        Packagetype::create(array('name' => 'MONTHLY','human_name'=>'miesięczny'));
    }

}
