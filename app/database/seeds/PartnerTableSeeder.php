<?php

/**
 * Description of PartnerTableSeeder
 *
 * @author damian
 */
class PartnerTableSeeder extends Seeder {

    public function run() {
        DB::table('partners')->delete();

        // tutaj create nie chce działać

        $partner = new Partner();
        $partner->user_id = 2;
        $partner->partner_id = 3;
        $partner->brand_id = 1;
        $partner->partner_brand_id = 4;
        $partner->invitation_accepted = false;
        $partner->invitation_rejected = false;
        $partner->save();

        $partner = new Partner();
        $partner->user_id = 2;
        $partner->partner_id = 4;
        $partner->brand_id = 1;
        $partner->partner_brand_id = 6;
        $partner->invitation_accepted = false;
        $partner->invitation_rejected = false;
        $partner->save();

        $partner = new Partner();
        $partner->user_id = 2;
        $partner->partner_id = 5;
        $partner->brand_id = 2;
        $partner->partner_brand_id = 7;
        $partner->invitation_accepted = false;
        $partner->invitation_rejected = false;
        $partner->save();

        $partner = new Partner();
        $partner->user_id = 3;
        $partner->partner_id = 5;
        $partner->brand_id = 4;
        $partner->partner_brand_id = 7;
        $partner->invitation_accepted = false;
        $partner->invitation_rejected = false;
        $partner->save();

        $partner = new Partner();
        $partner->user_id = 4;
        $partner->partner_id = 3;
        $partner->brand_id = 6;
        $partner->partner_brand_id = 3;
        $partner->invitation_accepted = false;
        $partner->invitation_rejected = false;
        $partner->save();
    }

}
