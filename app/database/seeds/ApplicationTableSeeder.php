<?php

/**
 * Description of ApplicationTableSeeder
 *
 * @author damian
 */
class ApplicationTableSeeder extends Seeder {

    public function run() {
        DB::table('applications')->delete();
        Application::create(array('user_id' => 3,'brand_id'=>3, 'action_id' => 1, 'applicationstate_id' => 3, 'accepted_by_applicant' => false));
        Application::create(array('user_id' => 2,'brand_id'=>1, 'action_id' => 2, 'applicationstate_id' => 3, 'accepted_by_applicant' => false));
        Application::create(array('user_id' => 5,'brand_id'=>7, 'action_id' => 1, 'applicationstate_id' => 3, 'accepted_by_applicant' => false));
    }

}
