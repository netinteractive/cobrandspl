<?php

/**
 * Description of ProductTableSeeder
 *
 * @author damian
 */
class ProductTableSeeder extends Seeder {

    public function run() {
        DB::table('products')->delete();
        Product::create(
                array(
                    'name' => 'Krzesło do jadalni',
                    'user_id' => 2,
                    'product_category_id' => 105,
                    'description' => '&lt;p&gt;Fenomenalne krzesło do jadalni. &lt;strong&gt;JEDYNE TAKIE&lt;/strong&gt;.&lt;/p&gt;',
                    'active' => true,
                    'product_count' => 300,
                    'logo_id' => 2,
                    'price_count' => 3,
                    'price_1' => 70.0,
                    'count_1_from' => 1,
                    'count_1_to' => 50,
                    'price_2' => 65.0,
                    'count_2_from' => 51,
                    'count_2_to' => 150,
                    'price_3' => 60.0,
                    'count_3_from' => 151,
                    'count_3_to' => 300,
        ));

        Product::create(
                array(
                    'name' => 'Telefon stacjonarny',
                    'user_id' => 2,
                    'product_category_id' => 133,
                    'description' => '&lt;p&gt;Nowy telefon stacjonarny. Wspaniały w podr&amp;oacute;ży i w domu. &lt;strong&gt;OKAZJA&lt;/strong&gt;&lt;/p&gt;',
                    'active' => true,
                    'product_count' => 2000,
                    'logo_id' => 3,
                    'price_count' => 2,
                    'price_1' => 250.0,
                    'count_1_from' => 1,
                    'count_1_to' => 200,
                    'price_2' => 225.0,
                    'count_2_from' => 201,
                    'count_2_to' => 1000,
                    'price_3' => null,
                    'count_3_from' => null,
                    'count_3_to' => null,
        ));

        Product::create(
                array(
                    'name' => 'Telefon komórkowy',
                    'user_id' => 2,
                    'product_category_id' => 133,
                    'description' => '&lt;p&gt;Nowy telefon kom&amp;oacute;rkowy. Bateria trzyma 3 tygodnie i wszystkie gry chodzą.&lt;/p&gt;',
                    'active' => true,
                    'product_count' => 10000,
                    'logo_id' => 4,
                    'price_count' => 1,
                    'price_1' => 350.0,
                    'count_1_from' => 1,
                    'count_1_to' => 1000,
                    'price_2' => null,
                    'count_2_from' => null,
                    'count_2_to' => null,
                    'price_3' => null,
                    'count_3_from' => null,
                    'count_3_to' => null,
        ));

        Product::create(
                array(
                    'name' => 'Fenomenalny młotek',
                    'user_id' => 2,
                    'product_category_id' => 105,
                    'description' => '&lt;p&gt;Młotek do wbijania gwoździ. Przebieg: ok. 300 gwoździ.&lt;/p&gt;',
                    'active' => true,
                    'product_count' => 9999,
                    'logo_id' => 5,
                    'price_count' => 2,
                    'price_1' => 20.0,
                    'count_1_from' => 1,
                    'count_1_to' => 100,
                    'price_2' => 15.0,
                    'count_2_from' => 1001,
                    'count_2_to' => 9999,
                    'price_3' => null,
                    'count_3_from' => null,
                    'count_3_to' => null,
        ));
    }

}
