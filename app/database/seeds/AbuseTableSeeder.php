<?php

/**
 * Description of AbuseTableSeeder
 *
 * @author damian
 */
class AbuseTableSeeder extends Seeder {

    public function run() {
        DB::table('abuses')->delete();
        AbuseModel::create(array('recommendation_id' => 1, 'content' => 'Jakiś tam testowy tekst nadużycia'));
        AbuseModel::create(array('recommendation_id' => 2, 'content' => 'Jakiś tam INNY testowy tekst nadużycia'));
        AbuseModel::create(array('recommendation_id' => 1, 'content' => 'Jakiś tam testowy tekst nadużycia kolejny'));
    }

}

?>
