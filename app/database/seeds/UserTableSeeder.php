<?php

/**
 * Description of UserSeeder
 *
 * @author damian
 */
class UserTableSeeder extends Seeder {

    public function run() {
        $user = Sentry::register(array(
                    'email' => 'admin@cobrand.pl',
                    'password' => '12qwaszxcv',
                    'companyname' => 'Cobrands.pl',
                    'name' => 'Damian Winiarski',
                    'street' => 'Testowa 4/5',
                    'postcode' => '00-777',
                    'city' => 'Warszawa',
                    'district_id' => 7,
                    'nip' => '5251007278',
                    'phone_main' => '555666777',
                    'phone_alternative' => '',
                    'companysize_id' => 1,
                    'activated' => true,
                    'accepted' => true,
                    'terms_accepted' => true,
                    'avatar_id' => 1,
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vehicula quam non nibh congue, eu efficitur ipsum sollicitudin. Aliquam tempor, nibh eu venenatis placerat, libero lorem congue turpis, ut sollicitudin dolor lorem a nibh. Nam nec dolor vel augue efficitur congue quis vitae nulla. Curabitur vehicula mauris eget mi laoreet sagittis. Nulla bibendum mi quis lorem tincidunt, et imperdiet diam sollicitudin. Etiam a varius dui. In non purus accumsan, eleifend sem vitae, laoreet magna. Donec volutpat tellus quis leo viverra rhoncus. '
        ));

        $adminGroup = $group = Sentry::findGroupByName('GROUP_ADMIN');
        $user->addGroup($adminGroup);

        Mailing::create(array('email' => $user->email));

        $user = Sentry::register(array(
                    'email' => 'marek@cobrand.pl',
                    'password' => '12qwaszxcv',
                    'companyname' => 'Monsanto',
                    'name' => 'Jan Kowalski',
                    'street' => 'Starożytna 5 m.5',
                    'postcode' => '03-567',
                    'city' => 'Łódź',
                    'district_id' => 5,
                    'nip' => '5251007272',
                    'phone_main' => '+33 88976543',
                    'phone_alternative' => '',
                    'companysize_id' => 2,
                    'activated' => true,
                    'accepted' => true,
                    'terms_accepted' => true,
                    'avatar_id' => 2,
                    'description' => 'Pellentesque fringilla dignissim massa vitae tempus. Nunc ut malesuada lorem. Nunc iaculis tellus mauris, sed volutpat metus finibus eu. Donec quis sapien mauris. Nulla id mi a enim eleifend condimentum. Vivamus sagittis auctor dolor. Fusce cursus ullamcorper volutpat. Ut mattis viverra posuere. Aliquam hendrerit felis et metus ultricies, in molestie lorem auctor. Cras eget vestibulum lorem. Aliquam erat volutpat. Donec ante risus, rutrum vitae feugiat vel, imperdiet in lectus. '
        ));

        // Find the group using the group id
        $userGroup = $group = Sentry::findGroupByName('GROUP_USER');

        // Assign the group to the user
        $user->addGroup($userGroup);
        Mailing::create(array('email' => $user->email));

        $user = Sentry::register(array(
                    'email' => 'damian_winiarski@o2.pl',
                    'password' => '12qwaszxcv',
                    'companyname' => 'Dell',
                    'name' => 'Arkadiusz Cobrandski',
                    'street' => 'Kaszubska 4/3',
                    'postcode' => '44-998',
                    'city' => 'Kielce',
                    'district_id' => 2,
                    'nip' => '5251997272',
                    'phone_main' => '22 33445566',
                    'phone_alternative' => '',
                    'companysize_id' => 2,
                    'activated' => true,
                    'accepted' => true,
                    'terms_accepted' => true,
                    'avatar_id' => 3,
                    'description' => 'Fusce vitae augue non massa bibendum tristique vitae sit amet nulla. In ut tincidunt urna. Sed commodo diam nec ante tempor rutrum. Sed ullamcorper nulla est. Aenean condimentum tincidunt lorem. Donec odio ipsum, mollis id sem et, fermentum aliquet dui. Curabitur malesuada lacus sem, vitae varius ante mattis nec. Aenean non tellus imperdiet, luctus dui vitae, mollis nibh. Proin laoreet, magna et ullamcorper gravida, ex purus tempor metus, a tincidunt orci sem et tortor. Etiam rhoncus felis vitae ex auctor euismod. '
        ));

        // Find the group using the group id
        $userGroup = $group = Sentry::findGroupByName('GROUP_USER');

        // Assign the group to the user
        $user->addGroup($userGroup);
        Mailing::create(array('email' => $user->email));

        $user = Sentry::register(array(
                    'email' => 'jarek@cobrand.pl',
                    'password' => '12qwaszxcv',
                    'companyname' => 'Placki Sp. z o.o.',
                    'name' => 'Janek Twardy',
                    'street' => 'Naleśnicza 4',
                    'postcode' => '33-987',
                    'city' => 'Poznań',
                    'district_id' => 15,
                    'nip' => '6661997272',
                    'phone_main' => '5678 456 789',
                    'phone_alternative' => '',
                    'companysize_id' => 2,
                    'activated' => true,
                    'accepted' => true,
                    'terms_accepted' => true,
                    'avatar_id' => 4,
                    'description' => 'Ut aliquet ante non suscipit eleifend. Nam molestie nunc quam, in cursus mauris ornare at. Nam bibendum ligula nibh, a feugiat nulla laoreet ac. Morbi faucibus non mi sit amet ornare. Vestibulum id consectetur est. Maecenas ut pharetra velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed tristique ligula libero, vel fringilla justo venenatis non. Vivamus tempor erat nec tempus ornare. Quisque est tellus, maximus ac molestie id, elementum eget arcu. '
        ));

        // Find the group using the group id
        $userGroup = $group = Sentry::findGroupByName('GROUP_USER');

        // Assign the group to the user
        $user->addGroup($userGroup);
        Mailing::create(array('email' => $user->email));

        $user = Sentry::register(array(
                    'email' => 'henryk@cobrand.pl',
                    'password' => '12qwaszxcv',
                    'companyname' => 'Szrotex',
                    'name' => 'Henryk Rzeźbiarz',
                    'street' => 'ul. Szrotu 15/4',
                    'postcode' => '66-900',
                    'city' => 'Wrocław',
                    'district_id' => 1,
                    'nip' => '6661007272',
                    'phone_main' => '555 222 333',
                    'phone_alternative' => '',
                    'companysize_id' => 2,
                    'activated' => true,
                    'accepted' => true,
                    'terms_accepted' => true,
                    'avatar_id' => 5,
                    'description' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean massa felis, molestie sed lectus sed, vestibulum pharetra tellus. Cras interdum condimentum sapien quis faucibus. Etiam mollis ipsum a est placerat blandit. Ut id dui accumsan, commodo ex vel, auctor mauris. Vivamus sed ex arcu. Sed sit amet ante vitae odio ultrices dapibus ut a quam. '
        ));

        // Find the group using the group id
        $userGroup = $group = Sentry::findGroupByName('GROUP_USER');

        // Assign the group to the user
        $user->addGroup($userGroup);
        Mailing::create(array('email' => $user->email));
    }

}
