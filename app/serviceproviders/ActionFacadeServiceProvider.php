<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of BranchFacadeServiceProvider
 *
 * @author damian
 */
class ActionFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('actionFacade', function() {
            return new \NetInteractive\Cobrands\Facades\ActionFacadeImplementation;
        });
    }

}
