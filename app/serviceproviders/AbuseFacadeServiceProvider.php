<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of AbuseFacadeServiceProvider
 *
 * @author damian
 */
class AbuseFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('abuseFacade', function() {
            return new \NetInteractive\Cobrands\Facades\AbuseFacadeImplementation;
        });
    }

}
