<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of CategoryFacadeServiceProvider
 *
 * @author damian
 */
class CategoryFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('categoryFacade', function() {
            return new \NetInteractive\Cobrands\Facades\CategoryFacadeImplementation;
        });
    }

}
