<?php

use Illuminate\Support\ServiceProvider;

class UserFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('userFacade', function() {
            return new \NetInteractive\Cobrands\Facades\UserFacadeImplementation;
        });
    }

}
