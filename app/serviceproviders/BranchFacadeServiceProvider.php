<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of BranchFacadeServiceProvider
 *
 * @author damian
 */
class BranchFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('branchFacade', function() {
            return new \NetInteractive\Cobrands\Facades\BranchFacadeImplementation;
        });
    }

}
