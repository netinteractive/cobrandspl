<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of ProductCategoryFacadeServiceProvider
 *
 * @author damian
 */
class ProductCategoryFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('productCategoryFacade', function() {
            return new \NetInteractive\Cobrands\Facades\ProductCategoryFacadeImplementation;
        });
    }

}
