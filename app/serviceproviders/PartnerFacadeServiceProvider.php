<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of PartnerFacadeServiceProvider
 *
 * @author damian
 */
class PartnerFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('partnerFacade', function() {
            return new \NetInteractive\Cobrands\Facades\PartnerFacadeImplementation;
        });
    }

}
