<?php

use NetInteractive\Cobrands\Enums\PackagetypeEnum;
use NetInteractive\Cobrands\Enums\PaymentstatusEnum;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log as Log;

/*
 * Description of EventServiceProvider
 *
 * @author damian
 */

class PaymentEventServiceProvider extends ServiceProvider {

    public function register() {
        // Odpowiedź na zdarzenie zakupu pakietu
        Event::listen('eloquent.saved: Netinteractive\Payments\PaymentModel', function($payment) {
            if ($payment->status == 'accepted') {

                $product_id = $payment->product_id;
                $package = Package::find($product_id);
                if ($package) {

                    if ($package->packagetype->id == PackagetypeEnum::MONTHLY) {
                        // sprawdzamy czy user nie ma aktywnego pakietu miesięcznego
                        // jeśli ma to przedłużamy subskrypcję o miesiąc
                        $currentPo = Packageorder::where('user_id', Sentry::getUser()->id)->whereHas('package', function($q) {
                                    $q->where('packagetype_id', PackagetypeEnum::MONTHLY);
                                })->where('enddate', '>=', Carbon::now())->first();
                        if ($currentPo) {
                            $date = new Carbon($currentPo->enddate);
                            $currentPo->enddate = $date->addMonth();
                            $currentPo->save();
                        } else {
                            $po = new Packageorder();
                            $po->user_id = Sentry::getUser()->id;
                            $po->package_id = $package->id;
                            $po->startdate = Carbon::now();
                            $po->enddate = Carbon::now()->addMonth();
                            $po->active = true;
                            $po->used = false;
                            $po->price = $package->price;
                            $po->paymentstatus_id = PaymentstatusEnum::COMPLETE;
                            $po->save();
                        }
                    } else if ($package->packagetype->id == PackagetypeEnum::SINGLE) {
                        $po = new Packageorder();
                        $po->user_id = Sentry::getUser()->id;
                        $po->package_id = $package->id;
                        $po->startdate = null;
                        $po->enddate = null;
                        $po->active = true;
                        $po->used = false;
                        $po->price = $package->price;
                        $po->paymentstatus_id = PaymentstatusEnum::COMPLETE;
                        $po->save();
                    }
                }
            }

            if ($payment->status == 'confirmed') {
                // akcja która ma sie wykonać w przypadku przyjścia potwierdzenia z serwera
            }
        });
    }

}

?>
