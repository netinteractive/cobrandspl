<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of BannerFacadeServiceProvider
 *
 * @author damian
 */
class BannerFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('bannerFacade', function() {
            return new \NetInteractive\Cobrands\Facades\BannerFacadeImplementation;
        });
    }

}
