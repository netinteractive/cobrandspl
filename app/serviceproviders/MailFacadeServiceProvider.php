<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of MailFacadeServiceProvider
 *
 * @author damian
 */
class MailFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('mailFacade', function() {
            return new \NetInteractive\Cobrands\Facades\MailFacadeImplementation;
        });
    }

}
