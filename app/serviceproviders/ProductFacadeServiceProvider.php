<?php

use Illuminate\Support\ServiceProvider;

/**
 * Description of ProductFacadeServiceProvider
 *
 * @author damian
 */
class ProductFacadeServiceProvider extends ServiceProvider {

    public function register() {
        App::bind('productFacade', function() {
            return new \NetInteractive\Cobrands\Facades\ProductFacadeImplementation;
        });
    }

}
