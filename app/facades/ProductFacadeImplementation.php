<?php

namespace NetInteractive\Cobrands\Facades;

use Product;
use Sentry;

/**
 * Description of ProductFacadeImplementation
 *
 * @author damian
 */
class ProductFacadeImplementation {

    public function findProducts($category, $sortby, $sorttype, $searchterm) {
        return $this->getProductsByCritera($category, $sortby, $sorttype, $searchterm, null);
    }

    public function findMyProducts($sortby, $sorttype) {
        return $this->getProductsByCritera('all', $sortby, $sorttype, null, Sentry::getUser()->id);
    }

    public function findProductsRestful($category, $sortby, $sorttype, $searchterm) {
        return Response::json($this->getProductsByCritera($category, $sortby, $sorttype, $searchterm, null));
    }

    private function getProductsByCritera($category, $sortby, $sorttype, $searchterm, $user_id = null) {
        $products = '';

        if ($category == 'all') {
            $products = Product::where('active', true);
        } else {
            $products = Product::where('active', true)->where('product_category_id', $category);
        }
        
        if($user_id != null) {
            $products->where('user_id',$user_id);
        }

        if ($searchterm != null) {
            $products->whereRaw("((products.name LIKE '%$searchterm%') OR (products.description LIKE '%$searchterm%'))");
        }

        if ($sortby == 'date') {
            $products->orderBy('created_at', $sorttype);
        } else if ($sortby == 'price') {
            $products->orderBy('price_1', $sorttype);
        }
        return $products->paginate(2);
    }

}
