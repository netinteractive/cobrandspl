<?php

namespace NetInteractive\Cobrands\Facades;

use Sentry;
use Partner;
use DB;
use Log;
use Action;
use User;
use MailFacade;
use Brand;

/**
 * Description of PartnerFacadeImplementation
 *
 * @author damian
 */
class PartnerFacadeImplementation {

    const INVALID_PARTNER = "msg.profile.invalid_partner";
    const SAVING_ERROR = "msg.profile.partner_saving_error";
    const REMOVAL_ERROR = "msg.profile.partner_remove_error";
    const PARTNER_ACCEPTED_SUCCESSFULLY = "msg.profile.partner_accepted_successfully";
    const PARTNER_REJECTED_SUCCESSFULLY = "msg.profile.partner_rejected_successfully";
    const PARTNER_REMOVED_SUCCESSFULLY = "msg.profile.partner_removed_successfully";
    const INFORM_NOT_ACCEPTED_ERROR = "Jeden z wybranych użytkowników nie jest Twoim zaakceptowanym partnerem";
    const INFORM_NO_SUCH_ACTION_ERROR = "Podana akcja nie istnieje!";
    const INFORM_SUCCESS = "Wiadomości zostały prawidłowo wysłane!";

    public function getCurrentUserWaitingPartners() {
        return Partner::where('partner_id', Sentry::getUser()->id)->where('invitation_accepted', false)->where('invitation_rejected', false)->get();
    }

    public function getCurrentUserAcceptedPartners() {
        return Partner::where('partner_id', Sentry::getUser()->id)->where('invitation_accepted', true)->where('invitation_rejected', false)->get();
    }

    public function acceptUserPartner($user_id, $brand_id) {
        $partner = Partner::where('partner_id', Sentry::getUser()->id)
                ->where('user_id', $user_id)
                ->where('brand_id', $brand_id)
                ->where('invitation_accepted', false)
                ->where('invitation_rejected', false)
                ->first();
        if ($partner) {
            DB::beginTransaction();
            // oznaczanie partnera jako zaakceptowanego
            $partner->setInvitationAccepted(true);
            if ($partner->save()) {
                // dodawanie aktualnego użytkownika jako partnera
                // tego, który go zaprosił
                $newPartner = new Partner();
                $newPartner->setUserId(Sentry::getUser()->id);
                $newPartner->setBrandId($partner->partnerBrand->id);
                $newPartner->setPartnerId($partner->getUserId());
                $newPartner->setPartnerBrandId($brand_id);
                $newPartner->setInvitationAccepted(true);
                $newPartner->setInvitationRejected(false);
                if ($newPartner->save()) {
                    DB::commit();
                    return self::PARTNER_ACCEPTED_SUCCESSFULLY;
                } else {
                    DB::rollback();
                    return self::SAVING_ERROR;
                }
            } else {
                DB::rollback();
                return self::SAVING_ERROR;
            }
        } else {
            return self::INVALID_PARTNER;
        }
    }

    public function rejectUserPartner($user_id, $brand_id) {
        $partner = Partner::where('partner_id', Sentry::getUser()->id)
                ->where('user_id', $user_id)
                ->where('brand_id', $brand_id)
                ->where('invitation_accepted', false)
                ->where('invitation_rejected', false)
                ->first();
        if ($partner) {
            // oznaczanie partnera jako odrzuconego
            $partner->setInvitationRejected(true);
            if ($partner->save()) {
                return self::PARTNER_REJECTED_SUCCESSFULLY;
            } else {
                return self::INVALID_PARTNER;
            }
        }
    }

    public function removeUserPartner($user_id, $brand_id) {
        $partner = Partner::where('partner_id', Sentry::getUser()->id)
                        ->where('user_id', $user_id)
                        ->where('brand_id', $brand_id)
                        ->where('invitation_accepted', true)->first();
        $contrPartner = Partner::where('user_id', Sentry::getUser()->id)
                        ->where('partner_id', $partner->user_id)
                        ->where('partner_brand_id', $partner->brand_id)
                        ->where('invitation_accepted', true)->first();
        if ($partner && $contrPartner) {
            DB::beginTransaction();
            if ($partner->delete() && $contrPartner->delete()) {
                DB::commit();
                return self::PARTNER_REMOVED_SUCCESSFULLY;
            } else {
                DB::rollback();
                return self::REMOVAL_ERROR;
            }
        } else {
            return self::INVALID_PARTNER;
        }
    }

    /**
     * Czy dany brand jest już partnerem innego branda
     * @param 
     * @return boolean
     */
    public function isPartnerBrandOfAnotherBrand($brand_id, $partner_brand_id) {
        if (Sentry::check()) {
            $partner = Partner::whereRaw("(partners.brand_id=$brand_id AND partners.partner_brand_id=$partner_brand_id  AND partners.invitation_rejected=false) OR (partners.brand_id=$brand_id AND partners.partner_brand_id=$partner_brand_id AND partners.invitation_rejected=false)")->first();
            if ($partner) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Sprawdza czy użytkownik posiada jakiekolwiek wolen brandy,
     * który nie polubiły jeszcze tego branda
     * @param type $brand_id
     */
    public function isHaveAnyBrandsLeftToLikeThisBrand($brand_id) {
        if (Sentry::check()) {
            $auth_user_id = Sentry::getUser()->id;
            $brand = Brand::whereRaw("brands.id not in (select p.brand_id from partners as p where p.partner_brand_id=$brand_id and p.user_id=$auth_user_id) AND brands.user_id=$auth_user_id AND brands.accepted=true AND brands.active=true")->first();
            if ($brand) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Lista wolnych brandów usera,
     * który nie polubiły jeszcze tego branda
     * @param type $brand_id
     */
    public function brandsLeftToLikeThisBrand($brand_id) {
        if (Sentry::check()) {
            $auth_user_id = Sentry::getUser()->id;
            $brands = Brand::whereRaw("brands.id not in (select p.brand_id from partners as p where p.partner_brand_id=$brand_id and p.user_id=$auth_user_id) AND brands.user_id=$auth_user_id AND brands.accepted=true AND brands.active=true")->get();
            return $brands;
        } else {
            return '';
        }
    }

    /**
     * Czy użytkownik jest zaakceptowanym brandem obecnego użytkownika
     * @param type $user_id
     * @return boolean
     */
    public function isAcceptedBrandOfCurrentUser($brand_id) {
        if (Sentry::check()) {
            $auth_user_id = Sentry::getUser()->id;
            $partner = Partner::whereRaw("(partners.user_id=$auth_user_id AND partners.partner_brand_id=$brand_id  AND partners.invitation_rejected=false AND partners.invitation_accepted=true) OR (partners.brand_id=$brand_id AND partners.partner_id=$auth_user_id  AND partners.invitation_rejected=false  AND partners.invitation_accepted=true)")->first();
            if ($partner) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Informowanie partnerów o akcji
     * @param array $partner_id_array tablica z id partnerów
     * @param string $message_content zawartość wiadomości do partnerów
     * @param int $action_id id akcji
     * @return type
     */
    public function informPartners($brand_id_array, $message_content, $action_id) {
        $action = Action::find($action_id);
        if (!$action) {
            return self::INFORM_NO_SUCH_ACTION_ERROR;
        }
        foreach ($brand_id_array as $brand_id) {
            if (!self::isAcceptedBrandOfCurrentUser($brand_id)) {
                return self::INFORM_NOT_ACCEPTED_ERROR;
            }
        }

        foreach ($brand_id_array as $brand_id) {
            $brand = Brand::find($brand_id);
            MailFacade::sendInformAboutActionMessage($brand->user->email, $brand->user->login, $message_content, $action, Sentry::getUser()->login);
        }
        return self::INFORM_SUCCESS;
    }

    /**
     * Zwraca liczbę znajmowych od branda
     * @param type $brand_id
     */
    public function getBrandContactsNumber($brand_id) {
        $partners = Partner::where('brand_id', $brand_id)->where('invitation_accepted', true)->get();
        return count($partners);
    }

}
