<?php

namespace NetInteractive\Cobrands\Facades;

use Banner;
use NetInteractive\Cobrands\Enums\BannerpositionEnum;

/**
 * Description of BannerFacadeImplementation
 *
 * @author damian
 */
class BannerFacadeImplementation {

    public function getHorizontalBanner() {
        return Banner::where('position_id', BannerpositionEnum::HORIZONTAL)->first();
    }

    public function getVerticalBanner() {
        return Banner::where('position_id', BannerpositionEnum::VERTICAL)->first();
    }

}
