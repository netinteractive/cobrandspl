<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of BannerFacade
 *
 * @author damian
 */
class BannerFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'bannerFacade';
    }

}
