<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of AbuseFacade
 *
 * @author damian
 */
class AbuseFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'abuseFacade';
    }

}
