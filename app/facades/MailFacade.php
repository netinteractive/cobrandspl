<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of MailFacade
 *
 * @author damian
 */
class MailFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'mailFacade';
    }

}
