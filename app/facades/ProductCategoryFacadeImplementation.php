<?php

namespace NetInteractive\Cobrands\Facades;

use ProductCategory;

/**
 * Description of ProductCategoryFacadeImplementation
 *
 * @author damian
 */
class ProductCategoryFacadeImplementation {

    public function getMainCategories() {
        return ProductCategory::where('is_main', true)->orderBy('id', 'ASC')->get();
    }

    public function getSubCategories($mainCategoryId) {
        return ProductCategory::where('is_main', false)->where('parent_id', $mainCategoryId)->orderBy('id', 'ASC')->get();
    }

}
