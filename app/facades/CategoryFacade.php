<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of CategoryFacade
 *
 * @author damian
 */
class CategoryFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'categoryFacade';
    }

}
