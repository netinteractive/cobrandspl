<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of ProductFacade
 *
 * @author damian
 */
class ProductFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'productFacade';
    }

}
