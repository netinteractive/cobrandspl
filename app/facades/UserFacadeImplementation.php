<?php

namespace NetInteractive\Cobrands\Facades;

use User;
use NetInteractive\Cobrands\Enums\ActionstateEnum;
use Action;
use Partner;
use Sentry;
use DB;
use Log;
use Packageorder;
use Carbon;

class UserFacadeImplementation {

    const PARTNER_ADD_ERROR = "Nie udało się dodać brandu partnera!";
    const PARTNER_ADD_SUCCESS = "Brand partner zosał prawidłowo dodany!";

    public function getRegisteredUsersCount() {
        return User::all()->count();
    }

    public function getAllPartners() {
        return User::where('id', '!=', 1)->paginate(2);
    }

    public function getUserActiveActions($user_id) {
        $user = User::find($user_id);
        return Action::where('user_id', $user->id)
                        ->where('actionstate_id', ActionstateEnum::NEW_CREATED)
                        ->orWhere('actionstate_id', ActionstateEnum::SECOND_STAGE_END_WAITING)->get();
    }

    public function getUserAppliedActiveActions($user_id) {
        $user = User::find($user_id);
        return Action::whereRaw('(actionstate_id = ? OR actionstate_id = ?)', array(ActionstateEnum::NEW_CREATED, ActionstateEnum::SECOND_STAGE_END_WAITING))
                        ->whereHas('applications', function($q) use ($user_id) {
                            $q->where('user_id', $user_id);
                        })->get();
    }

    public function getUserFinishedActions($user_id) {
        $user = User::find($user_id);
        return Action::where('user_id', $user->id)
                        ->where('actionstate_id', ActionstateEnum::ENDED_AFTER_FIRST_STAGE)
                        ->orWhere('actionstate_id', ActionstateEnum::ENDED_AFTER_SECOND_STAGE)->get();
    }

    public function getUserAppliedFinishedActions($user_id) {
        $user = User::find($user_id);
        return Action::whereRaw('(actionstate_id = ? OR actionstate_id = ?)', array(ActionstateEnum::ENDED_AFTER_FIRST_STAGE, ActionstateEnum::ENDED_AFTER_SECOND_STAGE))
                        ->whereHas('applications', function($q) use ($user_id) {
                            $q->where('user_id', $user_id);
                        })->get();
    }

    public function addPartnerBrand($brand_id, $partner_brand_id, $partner_id) {
        if ($partner_id == Sentry::getUser()->id) {
            return self::PARTNER_ADD_ERROR;
        }
        $partner = new Partner();
        $partner->setUserId(Sentry::getUser()->id);
        $partner->setBrandId($brand_id);
        $partner->setPartnerId($partner_id);
        $partner->setPartnerBrandId($partner_brand_id);

        if ($partner->save()) {
            return self::PARTNER_ADD_SUCCESS;
        } else {
            return self::PARTNER_ADD_ERROR;
        }
    }

    public function retrieveTopTen() {
        $users_action_counts = DB::select(DB::raw("select u.id as user_id,count(a.id) as action_count, concat(u.companyname,' (',count(a.id),')') as link from users u left join actions a on (a.user_id=u.id) group by u.id order by action_count DESC LIMIT 10;"));
        return $users_action_counts;
    }

    public function getUserActiveMonthlyPackages($user_id) {
        return Packageorder::where('user_id', $user_id)
                        ->where('active', true)
                        ->where('startdate', '<=', Carbon::now())
                        ->where('enddate', '>=', Carbon::now())
                        ->where('used', false)
                        ->get();
    }

    public function getUserActiveSinglePackages($user_id) {
        return Packageorder::where('user_id', $user_id)
                        ->where('active', true)
                        ->where('startdate', null)
                        ->where('enddate', null)
                        ->where('used', false)
                        ->get();
    }

}

?>
