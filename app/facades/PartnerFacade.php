<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of PartnerFacade
 *
 * @author damian
 */
class PartnerFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'partnerFacade';
    }

}
