<?php

namespace NetInteractive\Cobrands\Facades;

use Sentry;
use Recommendation;
use AbuseModel;

/**
 * Description of AbuseFacadeImplementation
 *
 * @author damian
 */
class AbuseFacadeImplementation {

    /**
     * Sprawdzenie czy zalogowany użykownik może zgłosić nadużycie
     * do danej rekomendacji
     * @param type $recommendation_id
     */
    public function isAbuseAllowedForRecommendation($recommendation_id) {
        if (!Sentry::check()) {
            return false;
        }

        // Sprawdzanie czy rekomendacja jest przypisana do zalogowanego użytkownika
        $recom = Recommendation::where('id', $recommendation_id)
                ->where('user_id', Sentry::getUser()->id)
                ->first();

        if (!$recom) {
            return false;
        }

        // sprawdzanie czy zalogowany user już nie złożył nadużycia
        // do tej rekomendacji
        $abuse = AbuseModel::where('recommendation_id', $recom->id)->first();
        if ($abuse) {
            return false;
        }

        return true;
    }

}
