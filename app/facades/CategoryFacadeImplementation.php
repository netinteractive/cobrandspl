<?php

namespace NetInteractive\Cobrands\Facades;

use Category;

/**
 * Description of CategoryFacadeImplementation
 *
 * @author damian
 */
class CategoryFacadeImplementation {

    public function getMainCategories() {
        return Category::where('is_main', true)->orderBy('id', 'ASC')->get();
    }

    public function getSubCategories($mainCategoryId) {
        return Category::where('is_main', false)->where('parent_id', $mainCategoryId)->orderBy('id', 'ASC')->get();
    }
}
