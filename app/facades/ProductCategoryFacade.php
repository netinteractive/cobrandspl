<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of ProductCategoryFacade
 *
 * @author damian
 */
class ProductCategoryFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'productCategoryFacade';
    }

}
