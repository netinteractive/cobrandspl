<?php

namespace NetInteractive\Cobrands\Facades;

use Mail;
use URL;

/**
 * Metody do wysyłania e-maili
 *
 * @author damian
 */
class MailFacadeImplementation {

    /**
     * Wysyłanie maila z linkiem aktywacyjnym po rejestracji
     * @param type $to adres e-mail adresata
     * @param type $username nazwa użytkownika adresata
     * @param type $activation_code kod aktywujący
     */
    public function sendActivationEmail($to, $username, $activation_code) {
        Mail::send('emails.activation', array('link' => URL::route('activateAction', $activation_code), 'username' => $username), function($message) use ($to, $username) {
            $message->from('info@tekstyzdupy.pl', 'Cobrands.pl')
                    ->to($to, $username)
                    ->subject('Rejstracja w serwisie Cobrands.pl');
        });
    }

    /**
     * Mail z potwierdzeniem chęci zmiany hasła
     * @param type $to adres e-mail adresata
     * @param type $username nazwa użytkownika adresata
     * @param type $reset_code kod resetujący hasło
     */
    public function sendPasswordResetEmail($to, $username, $reset_code) {
        Mail::send('emails.reminder', array('link' => URL::route('newPasswordView', $reset_code), 'username' => $username), function($message) use ($to, $username) {
            $message->from('info@tekstyzdupy.pl', 'Cobrands.pl')
                    ->to($to, $username)
                    ->subject('Reset hasła w serwisie Cobrands.pl');
        });
    }

    /**
     * Mail wysyłany po zapisaniu się na newsletter
     * @param type $to adres e-mail adresata
     * @param type $unsubscription_code kod do wypisania się z newslettera
     */
    public function sendNewsletterSubscriptionEmail($to, $unsubscription_code) {
        Mail::send('emails.newsletter', array('link' => URL::route('newsletterUnsubscribeAction', $unsubscription_code)), function($message) use ($to) {
            $message->from('info@tekstyzdupy.pl', 'Cobrands.pl')
                    ->to($to)
                    ->subject('Reset hasła w serwisie Cobrands.pl');
        });
    }

    /**
     * Mail z wiadomością prywatną do partnera
     * @param type $to adres e-mail adresata
     * @param type $authorname nazwa użytkownika autora wiadomości
     * @param type $content treść wiadomości
     * @param type $username nazwa użytkownika adresata wiadomości
     */
    public function sendPrivateMessage($to, $authorname, $content, $username) {
        Mail::send('emails.private_message', array('authorname' => $authorname, 'content' => $content, 'username' => $username), function($message) use ($to, $authorname) {
            $message->from('info@tekstyzdupy.pl', 'Cobrands.pl')
                    ->to($to)
                    ->subject('Wiadomość od użytkownika:' . $authorname);
        });
    }

    /**
     * Mail wysyłany po weryfikacji profilu przez administratora
     * @param type $to adres e-mail adresata
     * @param type $username nazwa użytkownika adresata wiadomości
     */
    public function sendAcceptedMessage($to, $username) {
        Mail::send('emails.accepted', array('username' => $username), function($message) use ($to, $username) {
            $message->from('info@tekstyzdupy.pl', 'Cobrands.pl')
                    ->to($to, $username)
                    ->subject('Twoje konto zostało zweryfikowane!');
        });
    }

    /**
     * Wysyłanie wiadomości informującej o akcji
     * @param type $to adres e-mail użytkownika
     * @param type $username login użytkownika
     * @param type $message_content treść wiadomości
     * @param Action $action akcja
     * @param type $authorname login użytkownika wysyłającego
     */
    public function sendInformAboutActionMessage($to, $username, $message_content, $action,$authorname) {
        Mail::send('emails.action_inform', array('username' => $username,'action'=>$action,'message_content'=>$message_content,'authorname'=>$authorname), function($message) use ($to, $username,$action) {
            $message->from('info@tekstyzdupy.pl', 'Cobrands.pl')
                    ->to($to, $username)
                    ->subject('Wiadomość dotycząca akcji '.$action->getNameFormatted());
        });
    }

}
