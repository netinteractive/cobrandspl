<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of ActionFacade
 *
 * @author damian
 */
class ActionFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'actionFacade';
    }

}
