<?php

namespace NetInteractive\Cobrands\Facades;

use NetInteractive\Cobrands\Enums\ActionstateEnum;
use NetInteractive\Cobrands\Enums\ApplicationstateEnum;
use NetInteractive\Cobrands\Utils\ActionSearchCriteria;
use Action;
use Response;
use Application;
use User;
use Sentry;
use Recommendation;
use Carbon;
use DB;
use Brand;
use Log;

/**
 * Description of ActionFacadeImplementation
 *
 * @author damian
 */
class ActionFacadeImplementation {

    const APPLY_USER_NOT_FOUND = 'Nie ma takiego użytkownika.';
    const APPLY_ACTION_UNAVAILABLE = 'Nie ma takiej akcji lub nie ma możliwości zgłoszenia się tej akcji.';
    const APPLY_USER_ALREADY_APPLIED = 'Użytkownik już zgłosił się do tej akcji.';
    const APPLY_USER_APPLICATION_ALLOWED = 'Użytkownik może aplikować do tej akcji.';
    const APPLY_SUCCESSFULL = 'Zgłoszenie zostało prawidłowo wysłane.';
    const APPLY_ERROR = 'Wystąpił błąd podczas próby wysłania zgłoszenia. Spróbuj ponownie później.';
    const APPLY_BRAND_ERROR = 'Podany brand nie istnieje lub nie należy do zalogowanego użytkownika';
    const APPLY_ACTION_TOO_MANY_APPLICATIONS = 'Akcja już posiada wymaganą liczbę zgłoszeń';

    public function getTotalActionsCount() {
        return Action::all()->count();
    }

    public function getFinishedActionsCount() {
        return Action::where('actionstate_id', ActionstateEnum::ENDED_AFTER_FIRST_STAGE)
                        ->orWhere('actionstate_id', ActionstateEnum::ENDED_AFTER_SECOND_STAGE)
                        ->get()
                        ->count();
    }

    public function getActiveActionsCount() {
        return Action::where('actionstate_id', ActionstateEnum::NEW_CREATED)
                        ->orWhere('actionstate_id', ActionstateEnum::SECOND_STAGE_END_WAITING)
                        ->get()
                        ->count();
    }

    public function findActions($category, $district, $sortby, $sorttype, $searchterm) {
        $actionCriteria = new ActionSearchCriteria();
        $actionCriteria->category = $category;
        $actionCriteria->district = $district;
        $actionCriteria->sortby = $sortby;
        $actionCriteria->sorttype = $sorttype;
        $actionCriteria->searchterm = $searchterm;
        return $this->getActionsByCritera($actionCriteria);
    }

    public function findActionsRestful($category, $district, $sortby, $sorttype, $searchterm) {
        $actionCriteria = new ActionSearchCriteria();
        $actionCriteria->category = $category;
        $actionCriteria->district = $district;
        $actionCriteria->sortby = $sortby;
        $actionCriteria->sorttype = $sorttype;
        $actionCriteria->searchterm = $searchterm;
        return Response::json($this->getActionsByCritera($actionCriteria));
    }

    public function findActionsAdvanced(ActionSearchCriteria $actionSearchCriteria) {
        return $this->getActionsByCritera($actionSearchCriteria);
    }

    private function getActionsByCritera(ActionSearchCriteria $actionCriteria) {
        $actions = '';

        if ($actionCriteria->category == 'all') {
            $actions = Action::where('actionstate_id', ActionstateEnum::NEW_CREATED);
        } else {
            if ($actionCriteria->subcategory != 0) {
                $actions = Action::where('actionstate_id', ActionstateEnum::NEW_CREATED)->where('category_id', $actionCriteria->subcategory);
            } else {
                $category_id = $actionCriteria->category;
                $actions = Action::where('actionstate_id', ActionstateEnum::NEW_CREATED)->whereRaw("category_id IN (SELECT id FROM categories WHERE parent_id=$category_id)");
            }
        }

        if ($actionCriteria->district != 'all') {
            $actions->where('district_id', $actionCriteria->district);
        }

        if ($actionCriteria->city != null) {
            $actions->where('city', 'LIKE', "%$actionCriteria->city%");
        }

        if ($actionCriteria->budget_from != null) {
            $actions->where('budget', '>', $actionCriteria->budget_from);
        }

        if ($actionCriteria->budget_to != null) {
            $actions->where('budget', '<', $actionCriteria->budget_to);
        }

        if ($actionCriteria->date_from != null) {
            $actions->where('action_date_start', '>', $actionCriteria->date_from);
        }

        if ($actionCriteria->date_to != null) {
            $actions->where('action_date_end', '<', $actionCriteria->date_to);
        }

        if ($actionCriteria->searchterm != null) {
            $actions->whereRaw("(actions.description LIKE '%$actionCriteria->searchterm%')");
        }

        if ($actionCriteria->sortby == 'latest') {
            $actions->orderBy('created_at', $actionCriteria->sorttype);
        } else if ($actionCriteria->sortby == 'ending') {
            $actions->orderBy('stage_deadline_one', $actionCriteria->sorttype);
        } else if ($actionCriteria->sortby == 'applications') {
            $actions->orderBy('current_partner_count', $actionCriteria->sorttype);
        } else if ($actionCriteria->sortby == 'budget') {
            $actions->orderBy('budget', $actionCriteria->sorttype);
        }
        return $actions->paginate(2);
    }

    /**
     * Sprawdzenie czy użytkownik może aplikować do akcji
     * na potrzeby np. wyświetlania elementów wizualnych
     * @param type $user_id
     * @param type $action_id
     * @return boolean
     */
    public function checkIfUserCanApply($user_id, $action_id) {
        $canApply = false;
        switch ($response = $this->returnActionApplicationAllowance($action_id,$user_id)) {
            case self::APPLY_ACTION_UNAVAILABLE:
            case self::APPLY_USER_ALREADY_APPLIED:
            case self::APPLY_USER_NOT_FOUND:
                $canApply = false;
                break;
            case self::APPLY_USER_APPLICATION_ALLOWED:
                $canApply = true;
                break;
            default:
                $canApply = false;
        }
        return $canApply;
    }

    /**
     * Pomocnicza metoda, która wstępnie sprawdza czy użytkownik istnieje,
     * czy akcja istnieje i czy użytkownik już nie zgłosił się do akcji wcześniej
     * @param type $user_id
     * @param type $action_id
     * @return type
     */
    private function returnActionApplicationAllowance($action_id,$user_id, $brand_id = null,$is_visible=null) {
        // sprawdzanie czy taki użytkownik istnieje i jest zaakceptowany
        $user = User::find($user_id);
        if (!$user) {
            return self::APPLY_USER_NOT_FOUND;
        }
        if ($brand_id != null) {
            // sprawdzanie czy brand należy do usera
            Log::info('brand'.$brand_id.'user'.$user_id);
            $brand = Brand::where('id', $brand_id)->where('user_id', $user_id)->first();
            if (!$brand) {
                return self::APPLY_BRAND_ERROR;
            }
        }

        // sprawdzanie czy akcja istnieje i czy istnieje możlwość zgłaszania się
        // do niej
        $action = Action::where('id', $action_id)
                ->where('actionstate_id', ActionstateEnum::NEW_CREATED)
                ->whereRaw('(actions.stage_deadline_one > now() )')
                ->first();
        if (!$action) {
            return self::APPLY_ACTION_UNAVAILABLE;
        }
        
        // jeśli liczba zgłosoznych jes już wystarczająca
        if($action->required_partner_count == $action->current_partner_count) {
            return self::APPLY_ACTION_TOO_MANY_APPLICATIONS;
        }
        
        // teraz sprawdzamy czy użytkownik już się nie zgłosił do tej akcji
        $application = Application::where('user_id', $user_id)->where('action_id', $action_id)->first();
        if ($application) {
            return self::APPLY_USER_ALREADY_APPLIED;
        }
        return self::APPLY_USER_APPLICATION_ALLOWED;
    }

    public function applyToAction($action_id,$user_id, $brand_id, $is_visible) {
        $response = $this->returnActionApplicationAllowance($action_id,$user_id, $brand_id);
        if ($response != self::APPLY_USER_APPLICATION_ALLOWED) {
            return $response;
        } else {
            // nie ma zastrzeżeń wiec zgłaszamy użytkownika do akcji
            $application = new Application();
            $application->user_id = $user_id;
            $application->action_id = $action_id;
            $application->applicationstate_id = ApplicationstateEnum::WAITING;
            $application->accepted_by_applicant = false;
            $application->visible = $is_visible;
            $application->brand_id = $brand_id;

            $action = Action::find($action_id);
            $action->current_partner_count = $action->current_partner_count + 1;
            DB::beginTransaction();
            if ($application->save() && $action->save()) {
                DB::commit();
                return self::APPLY_SUCCESSFULL;
            } else {
                DB::rollBack();
                return self::APPLY_ERROR;
            }
        }
    }

    /**
     * Zwraca true lub false w zależności czy dany użytkownik może dodać
     * rekomendację do danej akcji czy nie
     * @param type $action_id
     * @return boolean
     */
    public function isAddRecommendationAllowed($action_id) {
        // czy użytkownik jest zalogowany
        if (!Sentry::check()) {
            return false;
        }

        // czy akcja istnieje, jest zakończona i nie należy do zalogowanego usera
        $action = Action::where('id', $action_id)->where('user_id', '!=', Sentry::getUser()->id)
                ->where('actionstate_id', ActionstateEnum::ENDED_AFTER_FIRST_STAGE)
                ->orWhere('actionstate_id', ActionstateEnum::ENDED_AFTER_SECOND_STAGE)
                ->first();
        if (!$action) {
            return false;
        }

        // czy użytkownik już czasem nie dodał rekomendacji do tej akcji
        $recom = Recommendation::where('action_id', $action_id)->where('added_user_id', Sentry::getUser()->id)->first();
        if ($recom) {
            return false;
        }

        return true;
    }

    /**
     * Sprawdza czy jest możliwość zakończenia akcji przez czasem.
     * Tylko jeśli ilość zgłoszonych partnerów jest wyższa od wymaganej lub jeśli
     * ilość partnerów jest niezdefiniowana
     * @param type $action_id
     */
    public function isActionEndingAllowed($action_id) {
        // czy użytkownik jest zalogowany
        if (!Sentry::check()) {
            return false;
        }
        // czy akcja istnieje, jest aktywna, należy do zalogowanego usera,
        // ma zgloszonych więcej lub tyle samo partnerów nić potrzeba lub 
        // ma nieokreśloną liczbę wymaganych partnerów (0)
        $action = Action::where('id', $action_id)
                        ->where('user_id', Sentry::getUser()->id)
                        ->whereRaw('(actionstate_id = ? OR actionstate_id = ?)', array(ActionstateEnum::NEW_CREATED, ActionstateEnum::SECOND_STAGE_END_WAITING))
                        ->whereRaw('((actions.current_partner_count >= actions.required_partner_count) OR (actions.required_partner_count = 0))')->first();

        if (!$action) {
            return false;
        }
        return true;
    }

    /**
     * Sprawdzenie czy minął termin realizacji akcji
     */
    public function isAllowedForCommenting($action_id) {
        $action = Action::find($action_id);
        if ($action) {
            if (($action->getActionDateEnd() < Carbon::now()) && ($action->actionstate_id == ActionstateEnum::ENDED_AFTER_FIRST_STAGE || $action->actionstate_id == ActionstateEnum::ENDED_AFTER_SECOND_STAGE)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Sprawdzanie czy użytkownik był uczestnikiem akcji
     * @param type $action_id
     * @return boolean
     */
    public function isAllowedForCommentingByLoggedUser($action_id) {
        if ($this->isAllowedForCommenting($action_id)) {
            if (Sentry::check()) {
                $app = Application::where('user_id', Sentry::getUser()->id)
                        ->where('action_id', $action_id)
                        ->where('applicationstate_id', ApplicationstateEnum::ACCEPTED)
                        ->first();
                if ($app) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
