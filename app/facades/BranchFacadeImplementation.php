<?php

namespace NetInteractive\Cobrands\Facades;

use Branch;

/**
 * Description of BranchFacadeImplementation
 *
 * @author damian
 */
class BranchFacadeImplementation {

    public function getMainBranches() {
        return Branch::where('is_main', true)->orderBy('id', 'ASC')->get();
    }
    
    public function getSubBranches($mainBranchId) {
        return Branch::where('is_main', false)->where('parent_id', $mainBranchId)->orderBy('id', 'ASC')->get();
    }

}
