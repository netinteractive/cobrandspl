<?php

use Illuminate\Support\Facades\Facade;

/**
 * Description of BranchFacade
 *
 * @author damian
 */
class BranchFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'branchFacade';
    }

}
