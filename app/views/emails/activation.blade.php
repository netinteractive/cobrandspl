Witaj {{ $username }}!<br /><br/>
Twoje konto na portalu <a href="http://www.cobrands.pl">www.cobrands.pl</a>  zostało utworzone. Zapraszamy.<br/><br/>

<a href="{{ $link }}">Aby aktywować konto kliknij na ten link</a><br/><br/>
lub przejdź na podany niżej adres:<br/>
<a href="{{ $link }}">{{ $link }}</a><br/><br/>