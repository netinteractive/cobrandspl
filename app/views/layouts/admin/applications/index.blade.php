@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj zgłoszeniami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-thumbs-up"></i>Zgłoszenia
        </div>
    </div>

    <div class="portlet-body">
        <div class="form-actions top fluid " style='margin-bottom:15px'>
            <div class="col-md-9">
                <a href='/administrator/applications/all'>Wszystkie</a>
                <a href='/administrator/applications/accepted'>Zaakceptowane</a>
                <a href='/administrator/applications/unaccepted'>Niezaakceptowane</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="applications-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Użytkownik</td>
                        <td>Akcja</td>
                        <td>Status</td>
                        <td>Data zgłoszenia</td>
                        <td>Akceptacja</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($applications as $app)
                    <tr rel="{{ $app->id }}">
                        <td>{{ $app->id }}</td>
                        <td>{{ $app->user->companyname }}</td>
                        <td>{{ $app->action->getNameFormatted()}}</td>
                        <td>{{ $app->applicationstate->human_name }}</td>
                        <td>{{ $app->getFormattedCreatedDate() }}</td>
                        <td>
                            @if($app->accepted_by_admin)
                            <a class="change-status-btn btn btn-xs btn-danger hidden" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @else
                            <a class="change-status-btn btn btn-xs btn-danger" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success hidden" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        {{ $applications->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function() {

        $(function() {
            $('#applications-list tbody tr').each(function(i, h) {

                //id rekordu
                var id = $(this).attr('rel');

                // przycisk zmiany statusu
                $(h).find('.change-status-btn').click(function() {
                    var status = $(this).attr('status');

                    $.post('/administrator/applications/' + id + '/accept', {status: status}, function(newStatus) {
                        var statusRevert = newStatus == '1' ? '0' : '1';

                        $(h).find('.change-status-btn[status=' + newStatus + ']').addClass('hidden');
                        $(h).find('.change-status-btn[status=' + statusRevert + ']').removeClass('hidden');
                    });
                });
            });
        });
    };
</script>
<style>.hidden {display: none;}</style>