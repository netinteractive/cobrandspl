<h3 class="page-title">
    Edycja banera
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Baner - {{ $banner->position->human_name }}
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($banner, array('url' => array('administrator/banners', $banner->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="submit" class="btn green">Zapisz</button>
                {{ HTML::link('administrator/banners', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-1 control-label">Treść</label>
                <div class="col-md-11">
                    {{ Form::textarea('content', null, array('class'=>'form-control')) }}
                </div>
            </div>

        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>
