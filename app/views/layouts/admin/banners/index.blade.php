@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj banerami reklamowymi
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-money"></i>Banery reklamowe
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="banners-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Pozycja</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($banners as $banner)
                    <tr rel="{{ $banner->id }}">
                        <td>{{ $banner->id }}</td>
                        <td>{{ $banner->position->human_name }}</td>
                        <td>
                            <div class="">
                                <a href="banners/{{ $banner->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>.hidden {display: none;}</style>