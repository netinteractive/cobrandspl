<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 3.0.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Cobrands.pl - panel administracyjny</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="_token" content="<?= csrf_token() ?>"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->

        {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') }}
        {{ HTML::style('global/plugins/font-awesome/css/font-awesome.min.css') }}
        {{ HTML::style('global/plugins/simple-line-icons/simple-line-icons.min.css') }}
        {{ HTML::style('global/plugins/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('global/plugins/uniform/css/uniform.default.css') }}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        {{ HTML::style('global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}
        {{ HTML::style('global/plugins/fullcalendar/fullcalendar/fullcalendar.css') }}
        {{ HTML::style('global/plugins/jqvmap/jqvmap/jqvmap.css') }}
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        {{ HTML::style('admin/pages/css/tasks.css') }}
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        {{ HTML::style('global/css/components.css') }}
        {{ HTML::style('global/css/plugins.css') }}
        {{ HTML::style('admin/layout/css/layout.css') }}
        {{ HTML::style('admin/layout/css/themes/default.css', array('id'=>'style_color')) }}
        {{ HTML::style('admin/layout/css/custom.css') }}
        {{ HTML::style('global/plugins/data-tables/DT_bootstrap.css') }}
        {{ HTML::style('admin/css/extra.css') }}
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{route('homeView')}}">
                        {{ HTML::image('admin/layout/img/logo.png', 'logo', array('class'=>'logo-default')) }}
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <div class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </div>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="../../admin/layout/img/avatar3_small.jpg"/>
                                <span class="username">
                                    {{Sentry::getUser()->login}} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{route('logoutAction')}}">
                                        <i class="fa fa-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu" data-auto-scroll="false" data-auto-speed="200">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler">
                            </div>
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <li>
                            <div class="voffset3"></div>
                        </li>
                        <li class="start { @if($curtab=='dashboard')active @endif }">
                            <a href="/administrator/dashboard">
                                <i class="fa fa-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='users')active @endif }">
                            <a href="/administrator/users">
                                <i class="fa fa-user"></i>
                                <span class="title">Użytkownicy</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='actions')active @endif }">
                            <a href="/administrator/actions">
                                <i class="fa fa-gavel"></i>
                                <span class="title">Akcje</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='packages')active @endif }">
                            <a href="/administrator/packages">
                                <i class="fa fa-archive"></i>
                                <span class="title">Pakiety</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='boughtpackages')active @endif }">
                            <a href="boughtpackages">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="title">Zakupione pakiety</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='products')active @endif }">
                            <a href="/administrator/products">
                                <i class="fa fa-bullhorn"></i>
                                <span class="title">Produkty</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='brands')active @endif }">
                            <a href="/administrator/brands">
                                <i class="fa fa-gears"></i>
                                <span class="title">Brandy</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='recommendations')active @endif }">
                            <a href="/administrator/recommendations">
                                <i class="fa fa-quote-right"></i>
                                <span class="title">Rekomendacje</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='abuses')active @endif }">
                            <a href="/administrator/abuses">
                                <i class="fa fa-quote-right"></i>
                                <span class="title">Nadużycia</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='comments')active @endif }">
                            <a href="/administrator/comments">
                                <i class="fa fa-comments"></i>
                                <span class="title">Edycja komentarzy</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='banners')active @endif }">
                            <a href="/administrator/banners">
                                <i class="fa fa-money"></i>
                                <span class="title">Banery reklamowe</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='pages')active @endif }">
                            <a href="/administrator/pages">
                                <i class="fa fa-edit"></i>
                                <span class="title">Strony statyczne</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="start { @if($curtab=='airts')active @endif }">
                            <a href="/administrator/airts">
                                <i class="fa fa-exchange"></i>
                                <span class="title">Przekierowania</span>
                                <span class="selected"></span>
                            </a>
                        </li>

                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    {{ $content }}
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
                2014 &copy; Metronic by keenthemes.
            </div>
            <div class="page-footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        {{ HTML::script('global/plugins/respond.min.js') }}
        {{ HTML::script('global/plugins/excanvas.min.js') }} 
        <![endif]-->
        {{ HTML::script('global/plugins/jquery-1.11.0.min.js') }}
        {{ HTML::script('global/plugins/jquery-migrate-1.2.1.min.js') }}
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        {{ HTML::script('global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}
        {{ HTML::script('global/plugins/bootstrap/js/bootstrap.min.js') }}
        {{ HTML::script('global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
        {{ HTML::script('global/plugins/jquery-slimscroll/jquery.slimscroll.js') }}
        {{ HTML::script('global/plugins/jquery.blockui.min.js') }}
        {{ HTML::script('global/plugins/jquery.cokie.min.js') }}
        {{ HTML::script('global/plugins/uniform/jquery.uniform.min.js') }}
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::script('global/plugins/jqvmap/jqvmap/jquery.vmap.js') }}
        {{ HTML::script('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}
        {{ HTML::script('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}
        {{ HTML::script('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}
        {{ HTML::script('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}
        {{ HTML::script('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}
        {{ HTML::script('global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}
        {{ HTML::script('global/plugins/flot/jquery.flot.min.js') }}
        {{ HTML::script('global/plugins/flot/jquery.flot.resize.min.js') }}
        {{ HTML::script('global/plugins/flot/jquery.flot.categories.min.js') }}
        {{ HTML::script('global/plugins/jquery.pulsate.min.js') }}
        {{ HTML::script('global/plugins/bootstrap-daterangepicker/moment.min.js') }}
        {{ HTML::script('global/plugins/bootstrap-daterangepicker/daterangepicker.js') }}
        {{ HTML::script('global/scripts/metronic.js') }}
        {{ HTML::script('admin/layout/scripts/layout.js') }}
        {{ HTML::script('global/plugins/data-tables/jquery.dataTables.min.js') }}
        {{ HTML::script('global/plugins/data-tables/DT_bootstrap.js') }}
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        {{ HTML::script('global/plugins/fullcalendar/fullcalendar/fullcalendar.min.js') }}
        {{ HTML::script('global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}
        {{ HTML::script('global/plugins/jquery.sparkline.min.js') }}
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        {{ HTML::script('global/scripts/metronic.js') }}
        {{ HTML::script('admin/layout/scripts/layout.js') }}
        {{ HTML::script('admin/pages/scripts/index.js') }}
        {{ HTML::script('admin/pages/scripts/tasks.js') }}
        {{ HTML::script('global/plugins/tinymce/tinymce.min.js') }}
        <script type="text/javascript" src="{{URL::asset('js/vendor/jquery.ui.widget.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.iframe-transport.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.fileupload.js')}}"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout
                Index.init();
                Index.initDashboardDaterange();
                Index.initJQVMAP(); // init index page's custom scripts
                Index.initCalendar(); // init index page's custom scripts
                Index.initCharts(); // init index page's custom scripts
                Index.initChat();
                Index.initMiniCharts();
                Index.initIntro();
                Tasks.initDashboardWidget();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    }
                });
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>