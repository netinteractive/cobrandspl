<h3 class="page-title">
    Edycja brandu
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div id="alert-holder">

</div>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Edytuj brand
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($brand, array('url' => array('administrator/brands', $brand->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="button" class="btn green" onclick="saveBrand({{$brand->id}})">Zapisz</button>
                {{ HTML::link('administrator/brands', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">

            <div class="form-group">
                <div class="col-md-2">

                </div>
                <div class="col-md-10">
                    <div id="brand_image_placeholder" class="brand-image-placeholder">
                        <img src="{{$brand->logo->getUrl()}}" />
                    </div>
                    <button type="button" class="btn green" onclick="$('#brand_image_upload').click()">Zmień</button>
                    <input id="brand_image_upload" type="file" name="brand_image_file" data-url="{{ route('uploadBrandImage',$brand->id) }}" style="display:none"/>
                    <span class="validation-error" id="brand_image_upload_error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nazwa</label>
                <div class="col-md-10">
                    <input type="text" name="brand_name" id="brand_name" value="{{$brand->name}}" class="form-control"/>
                    <span class="validation-error" id="brand_name_error">{{ $errors->first('brand_name')}}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Branże (max. 3)</label>
                <div class="col-md-10">
                    <select onchange="onBrandBranchSelect()" id="registration_branch_selector">
                        @foreach(BranchFacade::getMainBranches() as $mainBranch)
                        <optgroup label="{{ $mainBranch->name }}">
                            @foreach($mainBranch->getChildren() as $child)
                            <option value="{{ $child->id }}">{{ $child->name }}</option>
                            @endforeach
                        </optgroup>
                        @endforeach
                    </select>
                    <input type="hidden" name="brand_branches" value="" id="brand_branches"/>
                    <div id="selected_branches_box">
                    </div>
                    <span class="validation-error" id="brand_name_error">{{ $errors->first('brand_name')}}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Opis brandu</label>
                <div class="col-md-10">
                    <textarea name="brand_description" id="brand_description" maxlength="3000">{{$brand->description}}</textarea>
                    <span class="validation-error" id="brand_description_error">{{ $errors->first('brand_description')}}</span>
                </div>
            </div>
        </div>
        <div class="form-actions bottom fluid ">
            <div class="col-md-9">
                <button type="button" class="btn green" onclick="saveBrand({{$brand->id}})">Zapisz</button>
                {{ HTML::link('administrator/brands', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>
<script>
    var selectedBrandBranches = new Array();
                    function initFileUpload() {

                    $('#brand_image_upload').fileupload({
                    dataType: 'json',
                            add: function(e, data) {
                            var goUpload = true;
                                    var uploadFile = data.files[0];
                                    if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                            document.getElementById("brand_image_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                                    goUpload = false;
                            }
                            if (uploadFile.size > 2000000) { // 2mb
                            document.getElementById("brand_image_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                                    goUpload = false;
                            }
                            if (goUpload == true) {
                            data.submit();
                            }
                            },
                            done: function(e, data) {
                            if (data.result.error) {
                            document.getElementById("brand_image_upload_error").innerHTML = data.result.error;
                            } else if (data.result.success) {
                            document.getElementById("brand_image_placeholder").innerHTML = '<img src=\'' + data.result.brand_image_url + '\' />';
                                    document.getElementById("brand_image_id").value = data.result.brand_image_id;
                            } else {
                            document.getElementById("brand_image_upload_error").innerHTML = 'Wystąpił nieznany błąd';
                            }
                            }
                    });
                    }

            function initEditor() {
            tinymce.init({
            selector: "#brand_description",
                    toolbar: "link | image",
                    language: "pl",
                    menubar: false,
                    statusbar: false,
                    plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                            "table contextmenu directionality template textcolor paste textcolor"
                    ],
                    toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
                    relative_urls: false,
            });
            }


            function clearAddBrandErrors() {
            $("#brand_image_error").html('');
                    $("#brand_name_error").html('');
                    $("#brand_branches_error").html('');
                    $("#brand_description_error").html('');
            }

            function removeBrandBranch(branchId) {
            var newArray = new Array();
                    for (i = 0; i < selectedBrandBranches.length; i++) {
            if (selectedBrandBranches[i] !== branchId) {
            newArray.push(selectedBrandBranches[i]);
            }
            }
            selectedBrandBranches = newArray;
                    document.getElementById("brand_branches").value = JSON.stringify(selectedBrandBranches);
                    $('#branch' + branchId).remove()
                    }

function onBrandBranchSelect() {
    var registration_branch_selector = document.getElementById("registration_branch_selector");
    var value = registration_branch_selector.options[registration_branch_selector.selectedIndex].value;
    var name = registration_branch_selector.options[registration_branch_selector.selectedIndex].innerHTML;
    var branchNotSelected = $.inArray(value, selectedBrandBranches) === -1;
    if (branchNotSelected && selectedBrandBranches.length < 3) {
        selectedBrandBranches.push(value);
        document.getElementById("brand_branches").value = JSON.stringify(selectedBrandBranches)
        var divBranches = document.getElementById("selected_branches_box");
        divBranches.innerHTML = divBranches.innerHTML + "<span class='brand-branch' id='branch" + value + "'>" + name + "<input type='button' onclick=\"removeBrandBranch('" + value + "')\" value='x' class='delete-btn btn btn-xs red remove-branch-button'></span>";
    } else {
        alert("Możesz wybrać maksymalnie 3 różne branże!");
    }
}
function saveBrandSuccess(msg) {
    data = $.parseJSON(msg);
    if (data.error) {
        var errors = data.error;
        if (errors.error) {
            alert(errors.error);
        }
        if (errors.image_id) {
            $("#brand_image_error").html(errors.image_id);
        }
        if (errors.product_name) {
            $("#brand_name_error").html(errors.brand_name);
        }
        if (errors.brand_branches) {
            $("#brand_branches_error").html(errors.brand_branches);
        }
        if (errors.description) {
            $("#brand_description_error").html(errors.description);
        }
    } else if (data.success) {
        $("#alert-holder").html("<div class=\"alert alert-success\">"+data.success+"</div>");
        $(window).scrollTop(0);
    } else {
        $("#alert-holder").html("<div class=\"alert alert-error\">Wystąpił błąd. Spróbuj ponownie póżniej</div>");
        $(window).scrollTop(0);
    }
}
function saveBrand(brand_id) {
    clearAddBrandErrors();
    var addBrandType = $("#brand_add_type").val();
    var imageId = $("#brand_image_id").val();
    var brandName = $("#brand_name").val();
    var brandBranches = $("#brand_branches").val();
    var description = tinyMCE.get('brand_description').getContent();

    var validationSuccess = true;

    if (imageId === '') {
        $("#brand_image_error").html('Należy dodać logo brandu');
        validationSuccess = false;
    }

    if (brandName === '') {
        $("#brand_name_error").html('Należy podać nazwę brandu');
        validationSuccess = false;
    }

    if (description === '') {
        $("#brand_description_error").html('Należy uzupełnić opis brandu');
        validationSuccess = false;
    }

    if (selectedBrandBranches.length === 0) {
        $("#brand_branches_error").html('Należy wybrać przynajmniej jedną branżę.');
        validationSuccess = false;
    }

    if (!validationSuccess) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/brand/zapisz/' + brand_id,
        data: {
            image_id: imageId,
            brand_name: brandName,
            brand_branches: brandBranches,
            description: description

        },
        success: function(msg) {
            saveBrandSuccess(msg);
        },
        error: function() {
        },
        dataType: 'html'
    });
}
            window.onload = function() {
            initFileUpload();
                    initEditor();
                    $.get("/brand/{{$brand->id}}", function(data) {
                    $("#brand_id").val(data.brand_id);
                            selectedBrandBranches = new Array();
                            for (var branch_id = 0; branch_id < data.branch_ids.length; branch_id++) {
                    var value = data.branch_ids[branch_id].toString();
                            var name = data.branch_names[branch_id];
                            selectedBrandBranches.push(value);
                            document.getElementById("brand_branches").value = JSON.stringify(selectedBrandBranches)
                            var divBranches = document.getElementById("selected_branches_box");
                            divBranches.innerHTML = divBranches.innerHTML + "<span class='brand-branch' id='branch" + value + "'>" + name + "<input type='button' onclick=\"removeBrandBranch('" + value + "')\" value='x' class='delete-btn btn btn-xs red remove-branch-button'></span>";
                    }
                    });
            };




</script>
