@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif
@if (Session::has('error-message'))
<div class="alert alert-danger">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif
<h3 class="page-title">
    Zarządzaj produktami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-archive"></i>Produkty
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="products-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Nazwa</td>
                        <td>Właściciel</td>
                        <td>L. szt. na stanie</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr rel="{{ $product->id }}">
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{$product->user->companyname}}</td>
                        <td>{{ $product->product_count}}</td>
                        <td>
                            <div class="">
                                <a href="products/{{ $product->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                                {{ Form::open(array('url' => 'administrator/products/' . $product->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>.hidden {display: none;}</style>
<script>
    function initTable() {

        $('#products-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć ten produkt?');
        });
        initTable();
    };




</script>