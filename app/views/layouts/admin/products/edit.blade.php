<h3 class="page-title">
    Edycja produktu
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div id="alert-holder">
    
</div>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Edytuj produkt
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($product, array('url' => array('administrator/products', $product->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="button" class="btn green" onclick="saveProduct({{$product->id}})">Zapisz</button>
                {{ HTML::link('administrator/products', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">

            <div class="form-group">
                <div class="col-md-2">

                </div>
                <div class="col-md-10">
                    <div id="product_image_placeholder" class="product-image-placeholder">
                        <img src="{{$product->logo->getUrl()}}" />
                    </div>
                    <button type="button" class="btn green" onclick="$('#product_image_upload').click()">Zmień</button>
                    <input id="product_image_upload" type="file" name="product_image_file" data-url="{{ route('uploadProductImage',$product->id) }}" style="display:none"/>
                    <span class="validation-error" id="product_image_upload_error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nazwa</label>
                <div class="col-md-10">
                    <input type="text" name="product_name" id="product_name" value="{{$product->name}}" class="form-control"/>
                    <span class="validation-error" id="product_name_error">{{ $errors->first('product_name')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-6">
                    <div id="product_category_selectors">

                    </div>
                    <span class="validation-error" id="product_category_error">{{ $errors->first('product_category')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Opis produktu</label>
                <div class="col-md-10">
                    <textarea name="product_description" id="product_description" maxlength="3000">{{$product->description}}</textarea>
                    <span class="validation-error" id="product_description_error">{{ $errors->first('product_description')}}</span>
                </div>
            </div>
            <div id="product_prices_holder" class="form-group">
                <div id="product_price1_holder" class="col-md-12">
                    <label for="product_price1" class="col-md-2 control-label">Cena produktu</label>
                    <div class="col-md-10">
                        <input style="width:70px;display:inline" type="text" name="product_price1" maxlength="6" id="product_price1" value="{{$product->price_1}}" placeholder="PLN" class="form-control"/>
                        od:<input style="width:50px;display:inline" type="text" name="product_price1_from" maxlength="4" id="product_price1_from" value="{{$product->count_1_from}}" placeholder="szt." class="form-control"/>
                        do:<input style="width:50px;display:inline" type="text" name="product_price1_to" maxlength="4" id="product_price1_to" value="{{$product->count_1_to}}" placeholder="szt." class="form-control"/>
                        <br/>
                        <span class="validation-error" id="product_price1_error"></span>
                    </div>
                </div>
                <div id="product_price2_holder" class="col-md-12" @if($product->price_count >=2) style="display:block" @else style="display:none" @endif>
                     <label for="product_price2" class="col-md-2 control-label">Cena produktu</label>
                    <div class="col-md-10">
                        <input style="width:70px;display:inline" type="text" name="product_price2" maxlength="6" id="product_price2" value="{{$product->price_2}}" placeholder="PLN" class="form-control"/>
                        od:<input style="width:50px;display:inline" type="text" name="product_price2_from" maxlength="4" id="product_price2_from" value="{{$product->count_2_from}}" placeholder="szt." class="form-control"/>
                        do:<input style="width:50px;display:inline" type="text" name="product_price2_to" maxlength="4" id="product_price2_to" value="{{$product->count_2_to}}" placeholder="szt." class="form-control"/>
                        <button type="button" value="usuń" onclick="removePrice(2)" class="btn green">usuń</button>
                        <br/>
                        <span class="validation-error" id="product_price2_error"></span>
                    </div>
                </div>
                <div id="product_price3_holder" class="col-md-12" @if($product->price_count ==3) style="display:block" @else style="display:none" @endif>
                     <label for="product_price3" class="col-md-2 control-label">Cena produktu</label>
                    <div class="col-md-10">
                        <input style="width:70px;display:inline" type="text" name="product_price3" maxlength="6" id="product_price3" value="{{$product->price_3}}" placeholder="PLN" class="form-control"/>
                        od:<input style="width:50px;display:inline" type="text" name="product_price3_from" maxlength="4" id="product_price3_from" value="{{$product->count_3_from}}" placeholder="szt." class="form-control"/>
                        do:<input style="width:50px;display:inline" type="text" name="product_price3_to" maxlength="4" id="product_price3_to" value="{{$product->count_3_to}}" placeholder="szt." class="form-control"/>
                        <button type="button" value="usuń" onclick="removePrice(3)" class="btn green">usuń</button>
                        <br/>
                        <span class="validation-error" id="product_price3_error"></span>
                    </div>
                </div>
                <div id="add_price_button_holder" class="col-md-12" @if($product->price_count <3) style="display:block" @else style="display:none" @endif>
                     <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <input type="hidden" id="num_of_prices" name="num_of_prices" value="{{$product->price_count}}" />
                        <button type="button" value="+" id="add_price_button" onclick="addNextPrice()" class="btn green"/><span id="addPriceLabel">Dodaj kolejną cenę</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    <label for="product_count" class="col-md-12 control-label">Ilość produktów na stanie:</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="product_count" maxlength="9" id="product_count" value="{{$product->product_count}}" placeholder="szt." class="form-control"/>
                    <br/>
                    <span class="validation-error" id="product_count_error">{{ $errors->first('product_count')}}</span>
                </div>
            </div>

        </div>
        <div class="form-actions bottom fluid ">
            <div class="col-md-9">
                <button type="button" class="btn green" onclick="saveProduct({{$product->id}})">Zapisz</button>
                {{ HTML::link('administrator/products', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>
<script>
    function initFileUpload() {

        $('#product_image_upload').fileupload({
            dataType: 'json',
            add: function(e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                    document.getElementById("product_image_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                    goUpload = false;
                }
                if (uploadFile.size > 2000000) { // 2mb
                    document.getElementById("product_image_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                    goUpload = false;
                }
                if (goUpload == true) {
                    data.submit();
                }
            },
            done: function(e, data) {
                if (data.result.error) {
                    document.getElementById("product_image_upload_error").innerHTML = data.result.error;
                } else if (data.result.success) {
                    document.getElementById("product_image_placeholder").innerHTML = '<img src=\'' + data.result.product_image_url + '\' />';
                    document.getElementById("product_image_id").value = data.result.product_image_id;
                } else {
                    document.getElementById("product_image_upload_error").innerHTML = 'Wystąpił nieznany błąd';
                }
            }
        });

    }

    function initEditor() {
        tinymce.init({
            selector: "#product_description",
            toolbar: "link | image",
            language: "pl",
            menubar: false,
            statusbar: false,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality template textcolor paste textcolor"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
                    relative_urls: false,
        });
    }

    function addNextPrice() {
        var numOfPrices = $("#num_of_prices").val();
        if (numOfPrices == 1) {
            $("#product_price2_holder").show();
            $("#num_of_prices").val(2);
        } else if (numOfPrices == 2) {
            $("#product_price3_holder").show();
            $("#num_of_prices").val(3);
            $("#add_price_button_holder").hide();
        } else if (numOfPrices >= 3) {

        }
    }

    function removePrice(priceNum) {
        if (priceNum == 2) {
            $("#product_price2").val('');
            $("#product_price2_from").val('');
            $("#product_price2_to").val('');
            $("#product_price2_holder").hide();
            $("#add_price_button_holder").show();
            $("#num_of_prices").val(1);
        } else if (priceNum == 3) {
            $("#product_price3").val('');
            $("#product_price3_from").val('');
            $("#product_price3_to").val('');
            $("#product_price3_holder").hide();
            $("#add_price_button_holder").show();
            $("#num_of_prices").val(2);
        }
    }

    function clearAddProductErrors() {
        $("#product_image_error").html('');
        $("#product_name_error").html('');
        $("#product_category_error").html('');
        $("#product_price1_error").html('');
        $("#product_price2_error").html('');
        $("#product_price3_error").html('');
        $("#product_description_error").html('');
        $("#product_count_error").html('');
    }
    
    function checkPrices(priceMain, priceFrom, priceTo, priceNum) {
    if (isNaN(priceMain) || isNaN(priceFrom) || isNaN(priceTo)) {
        $("#product_price" + priceNum + "_error").html('Należy uzupełnić wszystkie pola');
        return false;
    }
    if (priceFrom > priceTo) {
        $("#product_price" + priceNum + "_error").html("Wartość 'od' nie może być większa od wartości 'do'");
        return false;
    }
    if (priceMain < 0) {
        $("#product_price" + priceNum + "_error").html("Cena musi być większa od 0");
        return false;
    }
    return true;
}

function saveProductSuccess(msg) {
    data = $.parseJSON(msg);
    if (data.error) {
        var errors = data.error;
        if (errors.error) {
            $("#alert-holder").html("<div class=\"alert alert-error\">"+errors.error+"</div>");
            $(window).scrollTop(0);
        }
        if (errors.image_id) {
            $("#product_image_error").html(errors.image_id);
        }
        if (errors.product_name) {
            $("#product_name_error").html(errors.product_name);
        }
        if (errors.category_id) {
            $("#product_category_error").html(errors.category_id);
        }
        if (errors.description) {
            $("#product_description_error").html(errors.description);
        }
        if (errors.num_of_prices) {
            $("#product_price1_error").html(errors.num_of_prices);
        }
        if (errors.price_1) {
            $("#product_price1_error").html(errors.price_1);
        }
        if (errors.price_1_from) {
            $("#product_price1_error").html(errors.price_1_from);
        }
        if (errors.price_1_to) {
            $("#product_price1_error").html(errors.price_1_to);
        }
        if (errors.price_2) {
            $("#product_price2_error").html(errors.price_2);
        }
        if (errors.price_2_from) {
            $("#product_price2_error").html(errors.price_2_from);
        }
        if (errors.price_2_to) {
            $("#product_price2_error").html(errors.price_2_to);
        }
        if (errors.price_3) {
            $("#product_price3_error").html(errors.price_3);
        }
        if (errors.price_3_from) {
            $("#product_price3_error").html(errors.price_3_from);
        }
        if (errors.price_3_to) {
            $("#product_price3_error").html(errors.price_3_to);
        }
        if (errors.product_count) {
            $("#product_count_error").html(errors.product_count);
        }
    } else if (data.success) {
        $("#alert-holder").html("<div class=\"alert alert-success\">"+data.success+"</div>");
        $(window).scrollTop(0);
    } else {
        $("#alert-holder").html("<div class=\"alert alert-error\">Wystąpił błąd. Spróbuj ponownie póżniej</div>");
        $(window).scrollTop(0);
    }
}

    function saveProduct(product_id) {
        clearAddProductErrors();
        var productName = $("#product_name").val();
        var category = $("#product_subcategory_selector").val();
        var description = tinyMCE.get('product_description').getContent();
        var numOfPrices = $("#num_of_prices").val();

        var price1 = parseInt($("#product_price1").val());
        var price1From = parseInt($("#product_price1_from").val());
        var price1To = parseInt($("#product_price1_to").val());

        var price2 = parseInt($("#product_price2").val());
        var price2From = parseInt($("#product_price2_from").val());
        var price2To = parseInt($("#product_price2_to").val());

        var price3 = parseInt($("#product_price3").val());
        var price3From = parseInt($("#product_price3_from").val());
        var price3To = parseInt($("#product_price3_to").val());

        var productCount = $("#product_count").val();

        var validationSuccess = true;

        if (productName === '') {
            $("#product_name_error").html('Należy podać nazwę produktu');
            validationSuccess = false;
        }

        if (category === '' || category === 'Wybierz...') {
            $("#product_category_error").html('Należy wybrać kategorię produktu');
            validationSuccess = false;
        }

        if (description === '') {
            $("#product_description_error").html('Należy uzupełnić opis produktu');
            validationSuccess = false;
        }

        if (numOfPrices == 1) {
            if (!checkPrices(price1, price1From, price1To, 1)) {
                validationSuccess = false;
            }
        } else if (numOfPrices == 2) {
            if (!checkPrices(price1, price1From, price1To, 1)) {
                validationSuccess = false;
            }
            if (!checkPrices(price2, price2From, price2To, 2)) {
                validationSuccess = false;
            }
        } else if (numOfPrices == 3) {
            if (!checkPrices(price1, price1From, price1To, 1)) {
                validationSuccess = false;
            }
            if (!checkPrices(price2, price2From, price2To, 2)) {
                validationSuccess = false;
            }
            if (!checkPrices(price3, price3From, price3To, 3)) {
                validationSuccess = false;
            }
        }

        if (productCount == '') {
            $("#product_count_error").html('Należy uzupelnić to pole');
            validationSuccess = false;
        }

        if (productCount <= 0) {
            $("#product_count_error").html('Wartość musi być większa od 0');
            validationSuccess = false;
        }


        if (!validationSuccess) {
            return false;
        }

        if (numOfPrices == 1) {
            $.ajax({
                type: 'POST',
                url: '/produkt/zapisz/' + product_id,
                data: {
                    product_name: productName,
                    category_id: category,
                    description: description,
                    num_of_prices: numOfPrices,
                    price_1: price1,
                    price_1_from: price1From,
                    price_1_to: price1To,
                    product_count: productCount

                },
                success: function(msg) {
                    saveProductSuccess(msg);
                },
                error: function() {
                },
                dataType: 'html'
            });
        } else if (numOfPrices == 2) {
            $.ajax({
                type: 'POST',
                url: '/produkt/zapisz/' + product_id,
                data: {
                    product_name: productName,
                    category_id: category,
                    description: description,
                    num_of_prices: numOfPrices,
                    price_1: price1,
                    price_1_from: price1From,
                    price_1_to: price1To,
                    price_2: price2,
                    price_2_from: price2From,
                    price_2_to: price2To,
                    product_count: productCount

                },
                success: function(msg) {
                    saveProductSuccess(msg);
                },
                error: function() {
                },
                dataType: 'html'
            });
        } else if (numOfPrices == 3) {
            $.ajax({
                type: 'POST',
                url: '/produkt/zapisz/' + product_id,
                data: {
                    product_name: productName,
                    category_id: category,
                    description: description,
                    num_of_prices: numOfPrices,
                    price_1: price1,
                    price_1_from: price1From,
                    price_1_to: price1To,
                    price_2: price2,
                    price_2_from: price2From,
                    price_2_to: price2To,
                    price_3: price3,
                    price_3_from: price3From,
                    price_3_to: price3To,
                    product_count: productCount

                },
                success: function(msg) {
                    saveProductSuccess(msg);
                },
                error: function() {
                },
                dataType: 'html'
            });
        }
    }
function reloadProductSubCategoriesList() {
    var mainCategoryId = $("#product_category_selector").val();
    $.ajax({
        type: 'POST',
        url: '/product/subcategories/list/render/' + mainCategoryId,
        data: {
        },
        success: function(msg) {
            $("#product_subcategory_holder").html(msg);
        },
        error: function() {
        },
        dataType: 'html'
    });
}
    window.onload = function() {
        initFileUpload();
        initEditor();
        $.get("/product/" + {{$product->id}}, function(data) {
            $("#product_category_selectors").html(data.category_html);
        });
    };




</script>
