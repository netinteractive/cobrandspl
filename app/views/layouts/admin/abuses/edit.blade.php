<h3 class="page-title">
    Edycja nadużycia
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Edytuj nadużycie
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($abuse, array('url' => array('administrator/abuses', $abuse->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="submit" class="btn green">Zapisz</button>
                {{ HTML::link('administrator/abuses', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-2 control-label">Data dodania</label>
                <div class="col-md-10">
                    {{ $abuse->getFormattedCreatedDate() }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Autor</label>
                <div class="col-md-10">
                    {{ $abuse->recommendation->user->companyname }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Rekomendacja</label>
                <div class="col-md-10">
                    <a href='/administrator/recommendations/{{$abuse->recommendation->id}}/edit'>Link</a>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Treść</label>
                <div class="col-md-10">
                    {{ Form::textarea('content', null, array('class'=>'form-control')) }}
                </div>
            </div>

        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>
