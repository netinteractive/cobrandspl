@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif
@if (Session::has('error-message'))
<div class="alert alert-danger">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj nadużyciami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-comments"></i>Zgłoszone nadużycia
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="abuses-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Zgłaszający</td>
                        <td>Rekomendacja</td>
                        <td>Treść</td>
                        <td>Data dodania</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($abuses as $abuse)
                    <tr rel="{{ $abuse->id }}">
                        <td>{{ $abuse->id }}</td>
                        <td>{{ $abuse->recommendation->user->companyname }}</td>
                        <td><a href='/administrator/recommendations/{{$abuse->recommendation->id}}/edit'>Link</a></td>
                        <td>{{ $abuse->content }}</td>
                        <td>{{ $abuse->getFormattedCreatedDate() }}</td>
                        <td>
                            <div class="">
                                <a href="abuses/{{ $abuse->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                                {{ Form::open(array('url' => 'administrator/abuses/' . $abuse->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
<style>.hidden {display: none;}</style>
<script>
    function initTable() {

        $('#abuses-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć to nadużycie?');
        });
        initTable();
    };




</script>