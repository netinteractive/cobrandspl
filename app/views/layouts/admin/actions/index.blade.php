@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

@if (Session::has('error-message'))
<div class="alert alert-danger">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj akcjami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gavel"></i>Akcje
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="actions-list">
                <thead>
                    <tr>
                        <td>id</td>
                        <td>Nazwa</td>
                        <td>Zgłoszonych</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($actions as $action)
                    <tr rel="{{ $action->id }}">
                        <td>{{ $action->id }}</td>
                        <td>{{{ $action->getNameFormatted() }}}</td>
                        <td>{{{ $action->current_partner_count }}}</td>
                        <td>
                            <div class="">
                                <a href="actions/{{ $action->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                                {{ Form::open(array('url' => 'administrator/actions/' . $action->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>.hidden {display: none;}</style>
<script>
    function initTable() {

        $('#actions-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć tę akcję? Spowoduje to także usunięcie powiązanych danych!');
        });
        initTable();
    };




</script>