<h3 class="page-title">
    Edycja zamówień
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Edycja zamówienia
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($po, array('url' => array('administrator/boughtpackages', $po->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="submit" class="btn green">Zapisz</button>
                {{ HTML::link('administrator/boughtpackages', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">
            

        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>
