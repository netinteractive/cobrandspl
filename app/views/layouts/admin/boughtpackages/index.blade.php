@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj zakupionymi pakietami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-shopping-cart"></i>Zakupione pakiety
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="boughtpackages-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Użytkownik (firma)</td>
                        <td>Pakiet</td>
                        <td>Ważny od</td>
                        <td>Ważny do</td>
                        <td>Cena</td>
                        <td>Status płatności</td>
                        <td>Użyty</td>
                        <td>Aktywny</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($packageorders as $po)
                    <tr rel="{{ $po->id }}">
                        <td>{{ $po->id }}</td>
                        <td>{{ $po->user->login }} ({{$po->user->companyname}})</td>
                        <td>{{ $po->package->name }}</td>
                        <td>
                            @if($po->startdate != null)
                            {{ date("d-m-Y H:i:s",strtotime($po->startdate)) }}
                            @else
                            {{ '-' }}
                            @endif
                        </td>
                        <td>
                            @if($po->enddate != null)
                            {{ date("d-m-Y H:i:s",strtotime($po->enddate)) }}
                            @else
                            {{ '-' }}
                            @endif
                        </td>
                        <td>
                            {{ number_format($po->price,2,',',' ') }} zł
                        </td>
                        <td>
                            {{ $po->paymentstatus->human_name }}
                        </td>
                        <td>
                            @if($po->used)
                            <span class="glyphicon glyphicon-ok"></span>
                            @else
                            <span class="glyphicon glyphicon-remove"></span>
                            @endif
                        </td>
                        <td>
                            @if($po->active)
                            <a class="change-status-btn btn btn-xs btn-danger hidden" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @else
                            <a class="change-status-btn btn btn-xs btn-danger" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success hidden" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @endif
                        </td>
                        <td>
                            <div class="">
                                {{ Form::open(array('url' => 'administrator/boughtpackages/' . $po->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    function initTable() {

        $('#boughtpackages-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć ten zamówiony pakiet?');
        });
        initTable();
        $(function() {
            $('#boughtpackages-list tbody tr').each(function(i, h) {

                //id rekordu
                var id = $(this).attr('rel');

                // przycisk zmiany statusu
                $(h).find('.change-status-btn').click(function() {
                    var status = $(this).attr('status');

                    $.post('boughtpackages/' + id + '/active', {status: status}, function(newStatus) {
                        var statusRevert = newStatus == '1' ? '0' : '1';

                        $(h).find('.change-status-btn[status=' + newStatus + ']').addClass('hidden');
                        $(h).find('.change-status-btn[status=' + statusRevert + ']').removeClass('hidden');
                    });
                });
            });
        });
    };
</script>
<style>.hidden {display: none;}</style>