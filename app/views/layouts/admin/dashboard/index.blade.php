@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

<h3 class="page-title">
    Panel administracyjny
</h3>

<div class="voffset3"></div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="users" class="more">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ User::all()->count()}}
                    </div>
                    <div class="desc">
                        Użytkownicy
                    </div>
                </div>
                <a href="users" class="more">
                    Przejdź do użytkowników<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="actions" class="more">
            <div class="dashboard-stat red-intense">
                <div class="visual">
                    <i class="fa fa-gavel"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ Action::all()->count()}}
                    </div>
                    <div class="desc">
                        Akcje
                    </div>
                </div>
                <a href="actions" class="more">
                    Przejdź do akcji<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="packages" class="more">
            <div class="dashboard-stat green-haze">
                <div class="visual">
                    <i class="fa fa-archive"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ Package::all()->count()}}
                    </div>
                    <div class="desc">
                        Pakiety
                    </div>
                </div>
                <a href="packages" class="more">
                    Przejdź do pakietów<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="boughtpackages" class="more">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ Packageorder::all()->count()}}
                    </div>
                    <div class="desc">
                        Zakupione pakiety
                    </div>
                </div>
                <a href="boughtpackages" class="more">
                    Przejdź do zakupionych pakietów<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="applications" class="more">
            <div class="dashboard-stat blue-steel">
                <div class="visual">
                    <i class="fa fa-thumbs-up"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ Application::all()->count()}}
                    </div>
                    <div class="desc">
                        Zgłoszenia
                    </div>
                </div>
                <a href="applications" class="more">
                    Przejdź do akceptacji zgłoszeń<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="recommendations" class="more">
            <div class="dashboard-stat purple-studio">
                <div class="visual">
                    <i class="fa fa-quote-right"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ Recommendation::all()->count()}}
                    </div>
                    <div class="desc">
                        Rekomendacje
                    </div>
                </div>
                <a href="recommendations" class="more">
                    Przejdź do rekomendacji<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="comments" class="more">
            <div class="dashboard-stat red-sunglo">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ CommentModel::all()->count() }}
                    </div>
                    <div class="desc">
                        Komentarze
                    </div>
                </div>
                <a href="comments" class="more">
                    Przejdź do edycji komentarzy<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </a>
    </div>
</div>

<style>.hidden {display: none;}</style>