@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj przekierowaniami
</h3>

<a href="airts/create" class="btn green">Nowe przekierowanie <i class="fa fa-plus"></i></a>
<div class="voffset3"></div>
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-exchange"></i>Przekierowania
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="airts-list">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Z
                        </th>
                        <th>
                            Do
                        </th>
                        <th>
                            Status
                        </th>
                        <th>

                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($airts as $airt)
                    <tr rel="{{ $airt->id }}">
                        <td rowspan="1" colspan="1" style="width: 29px">{{ $airt->id }}</td>
                        <td>{{{ $airt->from }}}</td>
                        <td>{{{ $airt->to }}}</td>
                        <td>
                            @if($airt->isActive)
                            <a class="change-status-btn btn btn-xs btn-danger hidden" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @else
                            <a class="change-status-btn btn btn-xs btn-danger" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success hidden" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @endif
                        </td>
                        <td>
                            <div class="">
                                <a href="airts/{{ $airt->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>

                                {{ Form::open(array('url' => 'administrator/airts/' . $airt->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        {{ $airts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

<script>
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć to przekierowanie?');
        });

        $(function() {
            (function() {
                $('#airts-list tbody tr').each(function(i, h) {

                    //id rekordu
                    var id = $(this).attr('rel');

                    // przycisk zmiany statusu
                    $(h).find('.change-status-btn').click(function() {
                        var status = $(this).attr('status');

                        $.post('airts/' + id + '/active', {status: status}, function(newStatus) {
                            var statusRevert = newStatus == '1' ? '0' : '1';

                            $(h).find('.change-status-btn[status=' + newStatus + ']').addClass('hidden');
                            $(h).find('.change-status-btn[status=' + statusRevert + ']').removeClass('hidden');
                        });
                    });
                });
            })();
            {

            }
        });
    };
</script>