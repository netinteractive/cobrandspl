<h3 class="page-title">
    Dodawanie przekierowania
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach


<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-exchange"></i>Routing
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::open(array('url' => 'administrator/airts', 'class'=>"form-horizontal")) }}
        <div class="form-actions top fluid">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green">Dodaj</button>
                {{ HTML::link('administrator/airts', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">Z</label>
                <div class="col-md-4">
                    {{ Form::text('from', Input::old('from'), array('class'=>'form-control')) }}
                    <span class="help-block">Adres w przeglądarce </span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Do</label>
                <div class="col-md-4">
                    {{ Form::text('to', Input::old('to'), array('class'=>'form-control')) }}
                    <span class="help-block">Strona docelowa </span>
                </div>
            </div>
        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>