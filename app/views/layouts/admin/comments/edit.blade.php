<h3 class="page-title">
    Edycja komentarza
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Edytuj komentarz
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($comment, array('url' => array('administrator/comments', $comment->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="submit" class="btn green">Zapisz</button>
                {{ HTML::link('administrator/comments', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-2 control-label">Data dodania</label>
                <div class="col-md-10">
                    {{ $comment->getFormattedCreatedDate() }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Autor</label>
                <div class="col-md-10">
                    {{ $comment->user->companyname }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Akcja</label>
                <div class="col-md-10">
                    {{ $comment->action->getNameFormatted() }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Treść</label>
                <div class="col-md-10">
                    {{ Form::textarea('content', null, array('class'=>'form-control')) }}
                </div>
            </div>

        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>
