@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

@if (Session::has('error-message'))
<div class="alert alert-danger">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj komentarzami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-comments"></i>Komentarze
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="comments-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Autor</td>
                        <td>Akcja</td>
                        <td>Zawartość</td>
                        <td>Data dodania</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($comments as $comment)
                    <tr rel="{{ $comment->id }}">
                        <td>{{ $comment->id }}</td>
                        <td>{{ $comment->user->companyname }}</td>
                        <td>{{ $comment->action->getNameFormatted() }} <a href='/administrator/actions/{{$comment->action->id}}/edit'>Link</a> | <a href='/action/{{$comment->action->id}}'>Podgląd</a></td>
                        <td>{{ $comment->content }}</td>
                        <td>{{ $comment->getFormattedCreatedDate() }}</td>
                        <td>
                            <div class="">
                                <a href="comments/{{ $comment->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                                {{ Form::open(array('url' => 'administrator/comments/' . $comment->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
<style>.hidden {display: none;}</style>
<script>
    function initTable() {

        $('#comments-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć ten komentarz?');
        });
        initTable();
    };




</script>