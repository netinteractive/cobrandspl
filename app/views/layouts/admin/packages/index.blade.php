@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif
@if (Session::has('error-message'))
<div class="alert alert-danger">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif
<h3 class="page-title">
    Zarządzaj pakietami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-archive"></i>Pakiety
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="packages-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Nazwa</td>
                        <td>Cena</td>
                        <td>Rodzaj</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($packages as $package)
                    <tr rel="{{ $package->id }}">
                        <td>{{ $package->id }}</td>
                        <td>{{ $package->name }}</td>
                        <td>{{ number_format($package->price,2,',',' ') }} zł</td>
                        <td>{{ $package->packagetype->human_name }}</td>
                        <td>
                            <div class="">
                                <a href="packages/{{ $package->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                                {{ Form::open(array('url' => 'administrator/packages/' . $package->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>.hidden {display: none;}</style>
<script>
    function initTable() {

        $('#packages-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć ten pakiet?');
        });
        initTable();
    };




</script>