<h3 class="page-title">
    Edycja pakietu
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Edytuj pakiet
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($package, array('url' => array('administrator/packages', $package->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="submit" class="btn green">Zapisz</button>
                {{ HTML::link('administrator/packages', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-2 control-label">Nazwa</label>
                <div class="col-md-10">
                    {{ Form::text('name', $package->name, array('class'=>'form-control')) }}
                    <span class="help-block">{{ $errors->first('name')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Cena</label>
                <div class="col-md-9">
                    {{ Form::text('price', $package->price, array('class'=>'form-control','type'=>'number')) }}
                    <span class="help-block">{{ $errors->first('price')}}</span>
                </div>
                <div class="col-md-1">
                    zł.
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Rodzaj</label>
                <div class="col-md-10">
                    {{ $package->packagetype->human_name }}
                </div>
            </div>

        </div>
        {{ Form::close() }}
        <!-- END FORM-->
    </div>
</div>
