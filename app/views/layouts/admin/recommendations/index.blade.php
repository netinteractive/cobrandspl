@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif
@if (Session::has('erorr-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj rekomendacjami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-quote-right"></i>Rekomendacje
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="recommendations-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Autor</td>
                        <td>Przypisane do</td>
                        <td>Akcja</td>
                        <td>Treść</td>
                        <td>Data dodania</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($recommendations as $rcm)
                    <tr rel="{{ $rcm->id }}">
                        <td>{{ $rcm->id }}</td>
                        <td>{{ $rcm->addedUser->companyname }}</td>
                        <td>{{ $rcm->user->companyname }}</td>
                        <td>{{ $rcm->action->getNameFormatted() }}</td>
                        <td>{{ $rcm->content }}</td>
                        <td>{{ $rcm->getFormattedCreatedDate() }}</td>
                        <td>
                            <div class="">
                                <a href="recommendations/{{ $rcm->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                                {{ Form::open(array('url' => 'administrator/recommendations/' . $rcm->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>.hidden {display: none;}</style>
<script>
    function initTable() {

        $('#recommendations-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć tę rekomendację?');
        });
        initTable();
    };




</script>