@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif
@if (Session::has('error-message'))
<div class="alert alert-danger">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif

<h3 class="page-title">
    Edycja danych użytkownika
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Edytuj dane użytkownika
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($user, array('url' => array('administrator/users', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="submit" class="btn green">Zapisz</button>
                {{ HTML::link('administrator/users', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>

        <div class="form-body">
            <image src='{{ $user->avatar->getUrl() }}' style='margin-left:20px'/>
            <h2>Dane firmy</h2>
            <div class="form-group">
                <label class="col-md-2 control-label">Login</label>
                <div class="col-md-6">
                    {{ Form::text('login', Input::old('login')?Input::old('login'):$user->login, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('login')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nazwa firmy</label>
                <div class="col-md-6">
                    {{ Form::text('companyname', Input::old('companyname')?Input::old('companyname'):$user->companyname, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('companyname')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">NIP</label>
                <div class="col-md-6">
                    {{ Form::text('nip', Input::old('nip')?Input::old('nip'):$user->nip, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('nip')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">REGON</label>
                <div class="col-md-6">
                    {{ Form::text('regon', Input::old('regon')?Input::old('regon'):$user->regon, array('class'=>'form-control')) }}
                    <span class="help-block">{{ $errors->first('regon')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Branże:</label>
                <div class="col-md-6">
                    @foreach($user->branches as $branch)
                    {{ $branch->name }} | 
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Ulica</label>
                <div class="col-md-6">
                    {{ Form::text('street', Input::old('street')?Input::old('street'):$user->street, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('street')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Kod i miasto</label>
                <div class="col-md-10">
                    <div class="col-md-5" style="padding-left: 0">
                        {{ Form::text('postcode', Input::old('postcode')?Input::old('postcode'):$user->postcode, array('class'=>'form-control','required'=>'required')) }}
                        <span class="help-block">{{ $errors->first('postcode')}}</span>
                    </div>
                    <div class="col-md-5" style="padding-left: 0">
                        {{ Form::text('city', Input::old('city')?Input::old('city'):$user->city, array('class'=>'form-control','required'=>'required')) }}
                        <span class="help-block">{{ $errors->first('city')}}</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Województwo</label>
                <div class="col-md-6">
                    <select name='district' id='district' class="form-control">
                        <?php
                        $index = '';
                        if (Input::old('district')) {
                            $index = Input::old('district');
                        } else {
                            $index = $user->district_id;
                        }
                        ?>
                        @foreach($districts as $d)

                        @if($d->id == $index)
                        <option value='{{ $d->id }}' selected="selected">{{ $d->name }}</option>
                        @else
                        <option value='{{ $d->id }}'>{{ $d->name }}</option>
                        @endif
                        {{ $d->name }}
                        @endforeach
                    </select>
                    <span class="help-block">{{ $errors->first('district')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Wielkość firmy</label>
                <div class="col-md-6">
                    <select name='companysize' id='companysize' class="form-control">
                        <?php
                        $index = '';
                        if (Input::old('companysize')) {
                            $index = Input::old('companysize');
                        } else {
                            $index = $user->companysize_id;
                        }
                        ?>
                        @foreach($companysizes as $d)

                        @if($d->id == $index)
                        <option value='{{ $d->id }}' selected="selected">{{ $d->name }}</option>
                        @else
                        <option value='{{ $d->id }}'>{{ $d->name }}</option>
                        @endif
                        {{ $d->name }}
                        @endforeach
                    </select>
                    <span class="help-block">{{ $errors->first('district')}}</span>
                </div>
            </div>
            <h2>Dane kontaktowe</h2>
            <div class="form-group">
                <label class="col-md-2 control-label">Imię</label>
                <div class="col-md-6">
                    {{ Form::text('name', Input::old('name')?Input::old('name'):$user->name, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('name')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nazwisko</label>
                <div class="col-md-6">
                    {{ Form::text('surname', Input::old('surname')?Input::old('surname'):$user->surname, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('surname')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Telefon 1</label>
                <div class="col-md-6">
                    {{ Form::text('phone_main', Input::old('phone_main')?Input::old('phone_main'):$user->phone_main, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('phone_main')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Telefon 2</label>
                <div class="col-md-6">
                    {{ Form::text('phone_alternative', Input::old('phone_alternative')?Input::old('phone_alternative'):$user->phone_alternative, array('class'=>'form-control')) }}
                    <span class="help-block">{{ $errors->first('phone_alternative')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">E-mail</label>
                <div class="col-md-6">
                    {{ Form::text('email', Input::old('email')?Input::old('email'):$user->email, array('class'=>'form-control','required'=>'required')) }}
                    <span class="help-block">{{ $errors->first('email')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Opis działalności firmy</label>
                <div class="col-md-6">
                    <textarea name='description'>{{ Input::old('description')?Input::old('description'):$user->description }}</textarea>
                    <span class="help-block">{{ $errors->first('description')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Newsletter</label>
                <div class="col-md-6">
                    @if($user->subscribedToNewsletter())
                    <input type="checkbox" value="on" name="newsletter_accepted" checked="checked"/>
                    @else
                    <input type="checkbox" value="on" name="newsletter_accepted"/>
                    @endif
                    <span class="help-block">{{ $errors->first('newsletter_accepted')}}</span>
                </div>
            </div>
        </div>
        {{ Form::close() }}

    </div>
</div>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>Zmień hasło użytkownika
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        {{ Form::model($user, array('route' => array('changePasswordAdmin', $user->id), 'method' => 'POST', 'class' => 'form-horizontal')) }}
        <div class="form-actions top fluid ">
            <div class="col-md-9">
                <button type="submit" class="btn green">Zmień hasło</button>
                {{ HTML::link('administrator/users', 'Anuluj', array('class'=>'btn default')) }}
            </div>
        </div>

        <div class="form-body">
            <div class="form-group">
                <label class="col-md-2 control-label">Hasło</label>
                <div class="col-md-4">
                    <input type="password" name="password" min="2" max="45" id="password" value="" class='form-control' required/>
                    <span class="help-block">{{ $errors->first('password')}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Potwierdź hasło</label>
                <div class="col-md-4">
                    <input type="password" name="password_confirm" min="2" max="45" id="password_confirm" value="" class='form-control' required/>
                    <span class="help-block">{{ $errors->first('password_confirm')}}</span>
                </div>
            </div>
        </div>
        {{ Form::close() }}

    </div>
</div>
<script>


    window.onload = function() {

        tinymce.init({
            selector: '.tinymce',
            toolbar: "link | image",
            relative_urls: false,
            selector: "textarea",
                    language: "pl",
            menubar: false,
            statusbar: false,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality template textcolor paste textcolor"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
        });
    }
</script>
