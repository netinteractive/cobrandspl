@if (Session::has('success-message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {{ Session::get('success-message') }}
</div>
@endif

@if (Session::has('error-message'))
<div class="alert alert-danger">
    <span class="glyphicon glyphicon-remove"></span>
    {{ Session::get('error-message') }}
</div>
@endif

<h3 class="page-title">
    Zarządzaj użytkownikami
</h3>

<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Użytkownicy
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="users-list">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Login</td>
                        <td>Nazwa firmy</td>
                        <td>E-mail</td>
                        <td>Imię</td>
                        <td>Nazwisko</td>
                        <td>Miasto</td>
                        <td>Aktywny</td>
                        <td>Zaakceptowany</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr rel="{{ $user->id }}">
                        <td>{{ $user->id }}</td>
                        <td>{{{ $user->login }}}</td>
                        <td>{{{ $user->companyname }}}</td>
                        <td>{{{ $user->email }}}</td>
                        <td>{{{ $user->name }}}</td>
                        <td>{{{ $user->surname }}}</td>
                        <td>{{{ $user->city }}}</td>
                        <td>
                            @if($user->activated)
                            <span class="glyphicon glyphicon-ok"></span>
                            @else
                            <span class="glyphicon glyphicon-remove"></span>
                            @endif
                        </td>
                        <td>
                            @if($user->accepted)
                            <a class="change-status-btn btn btn-xs btn-danger hidden" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @else
                            <a class="change-status-btn btn btn-xs btn-danger" status="1" title="aktywuj"><span class="glyphicon glyphicon-remove"></span></a>
                            <a class="change-status-btn btn btn-xs btn-success hidden" status="0" title="wyłącz"><span class="glyphicon glyphicon-ok"></span></a>
                            @endif
                        </td>
                        <td>
                            <div class="">
                                <a href="users/{{ $user->id }}/edit" class="btn btn-xs blue inline">Edytuj</a>
                                {{ Form::open(array('url' => 'administrator/users/' . $user->id, 'class'=>'inline')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'delete-btn btn btn-xs red')) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
    function initTable() {

        $('#users-list').dataTable();

    }
    window.onload = function() {
        $('.delete-btn').click(function() {
            return confirm('Czy na pewno chcesz usunąć tego użytkownika? Wszystkie powiązane z tym użytkownikiem obiekty również zostaną usunięte!');
        });
        initTable();
        $(function() {
            $('#users-list tbody tr').each(function(i, h) {

                //id rekordu
                var id = $(this).attr('rel');

                // przycisk zmiany statusu
                $(h).find('.change-status-btn').click(function() {
                    var status = $(this).attr('status');

                    $.post('users/' + id + '/accept', {status: status}, function(newStatus) {
                        var statusRevert = newStatus == '1' ? '0' : '1';

                        $(h).find('.change-status-btn[status=' + newStatus + ']').addClass('hidden');
                        $(h).find('.change-status-btn[status=' + statusRevert + ']').removeClass('hidden');
                    });
                });
            });
        });
    };
</script>

<style>.hidden {display: none;}</style>