<title>{{{ $page->title }}}</title>

<h2>{{{ $page->title }}}</h2>

<div id="content">
    {{ $page->content }}
</div>