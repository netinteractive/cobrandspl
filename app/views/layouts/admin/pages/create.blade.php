<h3 class="page-title">
    Dodawanie strony
</h3>

@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{{ $error }}}
</div>
@endforeach

<div class="portlet box blue">
        <div class="portlet-title">
                <div class="caption">
                        <i class="fa fa-edit"></i>Strona statyczna
                </div>
        </div>
        <div class="portlet-body form">
                <!-- BEGIN FORM-->
                {{ Form::open(array('url' => 'administrator/pages', 'class'=>'form-horizontal')) }}
                <div class="form-actions top fluid ">
                        <div class="col-md-9">
                                <button type="submit" class="btn green">Dodaj</button>
                                {{ HTML::link('administrator/pages', 'Anuluj', array('class'=>'btn default')) }}
                        </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                            <label class="col-md-1 control-label">Tytuł</label>
                            <div class="col-md-11">
                                    {{ Form::text('title', Input::old('title'), array('class'=>'form-control')) }}
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-1 control-label">Treść</label>
                            <div class="col-md-11">
                                    {{ Form::textarea('content', Input::old('content'), array('class'=>'form-control tinymce')) }}
                            </div>
                    </div>
                </div>
                {{ Form::close() }}
                <!-- END FORM-->
        </div>
</div>

<script>

    
window.onload = function() {
    
    function elFinderBrowser (field_name, url, type, win) {
        tinymce.activeEditor.windowManager.open({
            file: '{{ asset('elfinder/tinymce') }}',// use an absolute path!
            title: 'elFinder 2.0',
            width: 900,
            height: 450,
            resizable: 'yes'
        }, {
            setUrl: function (url) {
                win.document.getElementById(field_name).value = url;
            }
        });
        return false;
    }
    
    tinymce.init({
        selector: '.tinymce', 
        toolbar: "link | image",
        relative_urls : false,
        file_browser_callback : elFinderBrowser,
        selector: "textarea",
        language : "pl",
        menubar: false,
        statusbar: false,
        plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality template textcolor paste textcolor"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
    });
}
</script>