<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
    <head>
        <meta name="csrf-token" content="<?= csrf_token() ?>"/>
        <title>@yield('title')</title>
        <link href="{{URL::asset('css/styles.css')}}" rel="stylesheet"/>
        <script type="text/javascript" src="{{URL::asset('js/jquery-2.1.1.min.js')}}"></script>
        @yield('scripts')
    </head>
    <body>
        <div id='page' style='width:800px;margin: 0 auto;'>
            @include('layouts.components.header')

            @include('layouts.components.subheader')

            @include('layouts.components.info_error_area')

            {{ $content }}

            @include('layouts.banners.horizontal')

            @include('layouts.components.footer')

        </div>
    </body>
</html>