<div id="buy_package_modal" style='border:solid 1px;margin-bottom: 5px;display: block;width:300px;display:none'>
    {{ Form::open(array('url'=>route('payPackage'))) }}
    @foreach(Package::all() as $package)
    <input class="package_radio" type="radio" name="selected_package_id" id="package{{ $package->id }}" value="{{ $package->id }}" onchange="onPackageSelect('{{ $package->price }} zł',{{ $package->id }})">{{ $package->name }}
    <br/>
    @endforeach
    <br/>
    Cena pakietu: <span id='package_price'></span>
    <br/>
    <input type='button' value='Anuluj' onclick="hideBuyModal()">
    <input id="buyButton" type='submit' value='Przejdź do płatności' disabled="disabled"/>
    
    {{ Form::close() }}
</div>

