<div style='border:solid 1px;margin-bottom: 5px;display: inline-block;width:100%'>
    {{ $action->getNameFormatted() }}
    <br/>
    Organizator {{ $action->user->companyname }}
    <br/>
    Czas do zakończenia {{ $action->getTimeLeft() }}
    <br/>
    Budżet {{$action->getBudget()}}
    <br/>
    @if($action->getRequiredPartnerCount() == 0)
    {{ $action->getCurrentPartnerCount() }} osób zgłoszonych
    @else
    {{ $action->getCurrentPartnerCount() }}/{{ $action->getRequiredPartnerCount() }} osób zgłoszonych
    @endif
    <br/>
    {{ link_to_route('actionView','Podgląd',array($action->getId())) }}
    @if(Sentry::check())
    @if($action->user_id == Sentry::getUser()->id)
    <input type='button' onclick="window.location.href='/action/edit/{{$action->id}}'" value="Edytuj"/>
    @endif
    @endif
</div>