@if($action->files->count() > 0)
<ol>
    @foreach($action->files as $af)
    <li>
        <img src='{{ $af->getThumbUrl() }}' />
        {{ $af->original_filename }}
        <a href='{{ $af->getUrl() }}'>Pobierz</a>
        @if(Sentry::check() && Sentry::getUser()->id == $action->user_id)
        <input type='button' value='Usuń' onclick='editActionRemoveFile({{$af->id}},{{$action->id}})'/>
        @endif
    </li>
    @endforeach
</ol>
@else
<i>brak</i>
@endif

