<div id="libraryModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <div id="library_content">

                </div>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="$('#libraryModal').modal('hide')">Anuluj</button>
                <button type="button" class="btn btn-primary" onclick="onAddProductFromLibrary()">Dodaj</button>
            </div>
        </div>
    </div>
</div>
