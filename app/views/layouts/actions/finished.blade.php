@extends('layouts.master')

@section('scripts')

@stop

@section('title')
{{Lang::get('msg.actions_finished.page_title')}}
@stop
@section('content')
<h2>Akcje przeprowadzone</h2>
@foreach($actions as $action)
@include('layouts.actions.action_box_small',$action)
@endforeach
{{ $actions->links() }}
@stop