@extends('layouts.master')

@section('scripts')
<script type="text/javascript" src="{{URL::asset('global/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_action.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/vendor/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.iframe-transport.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_brand_modal.js')}}"></script>
<script>
tinymce.init({
    selector: "#action_description",
    toolbar: "link | image",
    language: "pl",
    menubar: false,
    statusbar: false,
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality template textcolor paste textcolor"
    ],
    toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
            relative_urls: false,
});

tinymce.init({
    selector: "#action_description_more",
    toolbar: "link | image",
    language: "pl",
    menubar: false,
    statusbar: false,
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality template textcolor paste textcolor"
    ],
    toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
            relative_urls: false,
});
</script>
<script>

    $(function() {
        $('#avatarupload').fileupload({
            dataType: 'json',
            add: function(e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                    document.getElementById("avatar_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                    goUpload = false;
                }
                if (uploadFile.size > 2000000) { // 2mb
                    document.getElementById("avatar_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                    goUpload = false;
                }
                if (goUpload == true) {
                    data.submit();
                }
            },
            done: function(e, data) {
                if (data.result.error) {
                    document.getElementById("avatar_upload_error").innerHTML = data.result.error;
                } else if (data.result.success) {
                    document.getElementById("avatar_placeholder").innerHTML = '<img src=\'' + data.result.avatar_url + '\' />';
                    document.getElementById("avatar_id").value = data.result.avatar_id;
                } else {
                    document.getElementById("avatar_upload_error").innerHTML = 'Wystąpił nieznany błąd';
                }
            }
        });

        $('#action_file_upload').fileupload({
            dataType: 'json',
            add: function(e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                if (!(/\.(gif|jpg|jpeg|png|doc|docx|xls|xlsx|pdf)$/i).test(uploadFile.name)) {
                    document.getElementById("action_file_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg, png oraz dokumenty doc, docx, xls, xlsx oraz pdf.";
                    goUpload = false;
                }
                if (uploadFile.size > 10000000) { // 10mb
                    document.getElementById("action_file_upload_error").innerHTML = "Maksymalny rozmiar pliku to 10MB";
                    goUpload = false;
                }
                if (goUpload == true) {
                    data.submit();
                }
            },
            done: function(e, data) {
                if (data.result.error) {
                    document.getElementById("action_file_upload_error").innerHTML = data.result.error;
                } else if (data.result.success) {
                    document.getElementById("action_files_box").innerHTML = data.result.html;
                } else {
                    document.getElementById("action_file_upload_error").innerHTML = 'Wystąpił nieznany błąd';
                }
            }
        });
    });
</script>
@stop

@section('stylesheets')
<link href="{{URL::asset('css/dropdown.css')}}" rel="stylesheet"/>
@stop

@section('title')
{{Lang::get('msg.add_action.page_title')}}
@stop
@section('content')

<div id="avatar_placeholder" style="width:600px;height:300px;border:solid 1px;">
    @if(Session::has('avatar_url'))
    <img src='{{Session::get('avatar_url')}}' />
    @endif
</div>
<input id="avatarupload" type="file" name="avatarfile" data-url="{{ route('actionUploadAvatar') }}"/>
<span id="avatar_upload_error" class="register-error"></span>
{{ Form::open(array('url'=>route('actionAddAction'),'method'=>'PUT','id'=>'add_action_form')) }}
<input type="hidden" id="avatar_id" name="avatar_id" value="{{ Input::old('avatar_id') }}"/>
<label>Nazwa brandu</label>
<div id='action_brands_box'> 
    @if(Input::old('action_selected_brand'))
    {{App::make('ActionController')->renderActionBrandsRadioList(Sentry::getUser(),Input::old('action_selected_brand'))}}
    @else
    {{App::make('ActionController')->renderActionBrandsRadioList(Sentry::getUser(),0)}}
    @endif
</div>
<span class="register-error">{{ $errors->first('action_selected_brand')}}</span>
<br/>
<span>Dodaj nowy brand <input type="button" value="+" id="add_brand_button" name="add_brand_button" onclick="showAddBrandModal('signedin')"/></span>
@include('layouts.register.add_brand_modal')
<br/>

<label for="company_name">Firma</label>
<span id="company_name">{{Sentry::getUser()->companyname}}</span>
<br/>

<label for="action_city">Miasto*</label>
<input type="text" maxlength="200" id="action_city" name="action_city" value="{{ Input::old('action_city') }}" required/>
<span class="register-error">{{ $errors->first('action_city')}}</span>
<br/>
<label for="action_district">Województwo</label>
{{Form::select('action_district', $districts_list,Input::old('action_district'),array())}}
<div class="container">
    <div class="row">
        <label for="action_category">{{Lang::get('msg.add_action.action_category')}}</label>
        <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default btn-xs" style="width:500px">
                <span id="selected_category_name">{{ Session::has('category_name')?Session::get('category_name'):'Wybierz kategorię' }}</span> <span class="caret"></span>
            </a>
            {{ App::make('CategoryController')->renderList(); }}
        </div>
    </div>

    <input type="hidden" id="action_category_hidden" name="action_category" value="{{ Input::old('action_category') }}"/>
    <span class="register-error">{{ $errors->first('action_category')}}</span>
    <br/>

    <input type="button" value="Dodaj produkt z biblioteki produktów" style="width:400px" onclick="showLibrary()"/>
    <br/>
    <br/>
    <div id="library_selected_products">
        @include('layouts.actions.products_list')
    </div>

    <label for="action_brnaches">Branże akcji</label>
    <span id="action_branches"></span>
    <br/>

    <label for="action_description">{{Lang::get('msg.add_action.action_description')}}</label>
    <textarea id="action_description" name="action_description" min="10" max="3000">{{ Input::old('action_description') }}</textarea>
    <br/>
    <span class="register-error">{{ $errors->first('action_description')}}</span>

    <label for="action_description_more">Informacje dodatkowe</label>
    <textarea id="action_description_more" name="action_description_more" min="10" max="3000">{{ Input::old('action_description_more') }}</textarea>
    <br/>
    <span class="register-error">{{ $errors->first('action_description_more')}}</span>
    <br/>
    <label for="action_budget">Budżet Akcji</label>
    <input type="text" min="2" max="20" id="action_budget" name="action_budget" value="{{ Input::old('action_budget') }}" onkeyup="calculateShare()" required/>
    <br/>
    <span class="register-error">{{ $errors->first('action_budget')}}</span>
    <label for="action_partners_count">Ilość oczekiwanych brandów</label>
    <input type="text" min="2" max="3" id="action_partners_count" name="action_partners_count" value="{{ Input::old('action_partners_count')?Input::old('action_partners_count'):1 }}" onkeyup="calculateShare()"/>
    <br/>
    
    <label for="action_share">Udział w PLN</label>
    <span id="action_share"></span>
    <br/>
    
    <span class="register-error">{{ $errors->first('action_partners_count')}}</span>
    <label for="action_stage_1_date">Okres pozyskiwania brandów</label><br/>
    <label for="action_stage_1_date">{{Lang::get('msg.add_action.stage_1')}}</label>
    <input type="text" min="2" max="20" id="action_stage_1_date" name="action_stage_1_date" value="{{ Input::old('action_stage_1_date') }}" class="datepicker" required/>
    <br/>
    <span class="register-error">{{ $errors->first('action_stage_1_date')}}</span>
    <label for="action_stage_2_date">{{Lang::get('msg.add_action.stage_2')}}</label>
    <input type="text" min="2" max="20" id="action_stage_2_date" name="action_stage_2_date" value="{{ Input::old('action_stage_2_date') }}" class="datepicker" required/>
    <br/>
    <span class="register-error">{{ $errors->first('action_stage_2_date')}}</span>
    
    <label for="action_date_from">{{Lang::get('msg.add_action.action_date')}}</label>
    Od:<input type="text" min="2" max="20" id="action_date_from" name="action_date_from" class="datepicker" value="{{ Input::old('action_date_from') }}" required/>
    Do:<input type="text" min="2" max="20" id="action_date_to" name="action_date_to" class="datepicker" value="{{ Input::old('action_date_to') }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('action_date_from')}}</span>
    <span class="register-error">{{ $errors->first('action_date_to')}}</span>
    <input type="button" value="Anuluj?"><input type="submit" value="Dodaj akcję"/>
    {{ Form::close() }}
    Dokumenty i zdjęcia
    <input id="action_file_upload" type="file" name="action_file" data-url="{{ route('actionUploadFile') }}"/>
    <span id="action_file_upload_error" class="register-error"></span>
    <br/>
    <div id="action_files_box">
        @include('layouts.actions.add.action_files')
    </div>
    @include('layouts.actions.library_modal')
    @stop