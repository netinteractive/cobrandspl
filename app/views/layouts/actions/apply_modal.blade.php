<div id="applyModal" class="modal fade">
    <div class="modal-dialog">
        {{ Form::open(array('url'=>route('applyToAction',array($action->id,Sentry::getUser()->id)),'method'=>'POST','id'=>'apply_form')) }}
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <div id="content">
                    <h4>Określ widoczność Twojego zgłoszenia</h4>
                    <input type='radio' name='visibility_type' value='visible' checked/> Zgłoszenie widoczne dla wszystkich
                    <br/>
                    <input type='radio' name='visibility_type' value='invisible' /> Zgłoszenie ukryte
                    <h4>Wybierz brand</h4>
                    @foreach(Sentry::getUser()->brands as $brand)
                        <input type='radio' name='apply_brand' value='{{$brand->id}}' checked/> {{$brand->name}}
                        <br/>
                    @endforeach
                </div>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="$('#applyModal').modal('hide')">Anuluj</button>
                <button type="button" class="btn btn-primary" onclick="$('#apply_form').submit()">Dodaj</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>
