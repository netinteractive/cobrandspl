@extends('layouts.master')

@section('scripts')
<script type="text/javascript" src="{{URL::asset('js/script_action.js')}}"></script>
@if(Session::has('showModal'))
<script type="text/javascript">
        $(document).ready(function() {
showAddCommentModal({{ Session::get('showModal') }});
        });</script>
@else
<script type="text/javascript">
            $(document).ready(function() {
    if (window.location.hash) {
    if (window.location.hash == '#poinformuj_partnerow') {
    if (document.getElementById("inform_partners_modal")) {
    document.getElementById("inform_partners_modal").style.display = "block";
    }
    }
    }
    });</script>
@endif
@stop

@section('title')
{{Lang::get('msg.action.page_title')}}
@stop
@section('content')

{{ $action->getNameFormatted() }}
<br/>
{{$action->city}} {{$action->district->name}}
<br/>
<div id="avatar_placeholder" style="width:600px;height:300px;border:solid 1px;">
    @if($action->avatar != null)
    <img src='{{$action->avatar->getUrl()}}' />
    @endif
</div>
Termin akcji: {{ $action->getFormattedActionDateStart() }} - {{ $action->getFormattedActionDateEnd() }}
<br/>
Etap 1: {{ $action->getFormattedStageDeadlineOne() }}
<br/>
Etap 2: {{ $action->getFormattedStageDeadlineTwo() }}
<br/>
@if($action->getRequiredPartnerCount() == 0)
Poszukiwanych brandów {{ $action->getCurrentPartnerCount() }}
@else
Poszukiwanych brandów {{ $action->getCurrentPartnerCount() }}/{{ $action->getRequiredPartnerCount() }}
@endif
<br/>
Udział w PLN {{number_format($action->budget/$action->required_partner_count,2)}}
<br/>
Opis Akcji
<br/>
{{ \NetInteractive\Cobrands\Utils\ValidationUtils::decode($action->getDescription()) }}
<br/>
<br/>
@if($action->description_more != null)
<a href="#" onclick="$(this).hide();$('#description_more_box').show();return false;">Pokaż więcej &gt;&gt;</a>
<div id="description_more_box" style="display: none">
    {{\NetInteractive\Cobrands\Utils\ValidationUtils::decode($action->description_more)}}
</div>
@endif
<br/>

Produkty:<br/>
@foreach($action->products as $product)
    @include('layouts.products.product_box',$product)
@endforeach

<b>Budżet: {{ $action->getFormattedBudget() }}</b>
<br/>
@if(Sentry::check())
@if($action->user_id != Sentry::getUser()->id)
@include('layouts.actions.apply_link')
<br/>
@endif
@endif
Dokumenty i zdjęcia:<br/>
@include('layouts.actions.show.action_files')
<br/>
@if(Sentry::check())
<br/>
<a name='poinformuj_partnerow'></a>
<input type="button" value="Poinformuj zaprzyjaźnione brandy" onclick='showPartnersModal()'/>
@include('layouts.actions.inform_partners',$action)
<br/>
@endif
@if(ActionFacade::isAllowedForCommenting($action->id))
Komentarze:<br/>
@foreach($action->comments() as $comment)
@include('layouts.comments.box',$comment)
@endforeach
@if(ActionFacade::isAllowedForCommentingByLoggedUser($action->id))
<input type='button' value='Dodaj komentarz' onclick='showAddCommentModal({{ $action->id }})'/>
@include('layouts.comments.add')
@endif
@endif
<br/>
Przesłane zgłoszenia ({{ $action->getCurrentPartnerCount() }})
<br/>
@foreach($action->applications as $app)
@if($app->visible)
@include('layouts.actions.application_box',$app)
@endif
@endforeach
@if(Sentry::check())
@if($action->user_id != Sentry::getUser()->id)
@include('layouts.actions.apply_link')
@endif
@endif
@if(Sentry::check())
@if($action->user_id == Sentry::getUser()->id)
<input type='button' onclick="window.location.href='/action/edit/{{$action->id}}'" value="Edytuj"/>
@endif
@endif
@if(Sentry::check())
@if($action->user_id != Sentry::getUser()->id)
@include('layouts.actions.apply_modal')
@endif
@endif
@stop