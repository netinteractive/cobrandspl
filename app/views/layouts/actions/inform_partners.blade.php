<div id='inform_partners_modal' style='width:100%;border:solid 1px;display:none'>
    {{ Form::open(array('url'=>route('actionInformPartners',$action->id),'onsubmit'=>'return validatePartners()')) }}
    <div style='text-align: center'>
        {{ $action->getNameFormatted() }}<br/>
        Wybierz brandy, do których ma zostać wysłana wiadomość
    </div>
    <div style='width:90%;height: 300px;border:solid 1px;overflow-y: scroll;margin:0 auto'>
        @foreach(PartnerFacade::getCurrentUserAcceptedPartners() as $ap)
        <div style='border:solid 1px;margin-bottom: 5px;display: inline-block;width:200px'>
            <input type="checkbox" name="selectedPartners[]" value="{{ $ap->brand->id }}" />
            <img src='{{ $ap->brand->logo->getThumbUrl() }}' align='left'/>
            {{ $ap->brand->name }}
            <br/>
        </div>
        @endforeach
    </div>
    <div style='text-align: right;width:90%;margin:0 auto'>
        <div style='margin-top:10px;margin-bottom:10px'>
            <input type='button' value='Odznacz wszystkich'  onclick="deselectAllPartners()"/>
            <input type='button' value='Zaznacz wszystkich'  onclick="selectAllPartners()"/>
        </div>
        <textarea style='width:100%;height: 200px;resize:none' name='message' required></textarea>
        <div style='margin-top:10px;margin-bottom:10px'>
            <input type='button' value='Anuluj' onclick='closePartnersModal()'/>
            <input type='submit' value='Wyślij' />
        </div>
    </div>
    {{ Form::close() }}
</div>