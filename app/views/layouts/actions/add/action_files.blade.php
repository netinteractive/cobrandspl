@if(Session::has('action_files'))
<ol>
    @foreach(Session::get('action_files') as $af)
    <li>
        <img src='{{ $af->getThumbUrl() }}' />
        {{ $af->original_filename }}
        <a href='{{ $af->getUrl() }}'>Pobierz</a>
        <input type='button' value='Usuń' onclick='addActionRemoveFile({{$af->id}})'/>
    </li>
    @endforeach
</ol>
@endif
