<div style='border:solid 1px;margin-bottom: 5px;display: inline-block;width:100%'>
    {{ $action->getNameFormatted() }}
    <br/>
    Termin akcji: {{ $action->getFormattedActionDateStart() }} - {{ $action->getFormattedActionDateEnd() }}
    <br/>
    Etap 1: {{ $action->getFormattedStageDeadlineOne() }}
    <br/>
    Etap 2: {{ $action->getFormattedStageDeadlineTwo() }}
    <br/>
    @if($action->getRequiredPartnerCount() == 0)
    Poszukiwanych partnerów {{ $action->getCurrentPartnerCount() }}
    @else
    Poszukiwanych partnerów {{ $action->getCurrentPartnerCount() }}/{{ $action->getRequiredPartnerCount() }}
    @endif
    <br/>
    @if(ActionFacade::isActionEndingAllowed($action->id))
    <input type='button' value='Zakończ'/>
    @endif
    @if(ActionFacade::isAddRecommendationAllowed($action->id))
    <input type='button' value='Dodaj rekomendację' onclick="showAddRecommendationModal({{ $action->id }})"/>
    @endif
    @if($action->user_id == Sentry::getUser()->id)
    <input type='button' onclick="window.location.href='/action/edit/{{$action->id}}'" value="Edytuj"/>
    @endif
    {{ link_to_route('actionView','Podgląd',array($action->getId())) }}
</div>