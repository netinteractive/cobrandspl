@extends('layouts.master')

@section('title')
{{Lang::get('msg.home.page_title')}}
@stop

@section('content')
@include('layouts.components.actions_partners_links')
<br/>
<br/>
@foreach($partners as $partner)
@include('layouts.partners.partner_box',$partner)
@endforeach
{{ $partners->links() }}
@stop