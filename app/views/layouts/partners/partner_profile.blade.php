@extends('layouts.master')

@section('scripts')
<script type="text/javascript" src="{{URL::asset('js/script_private_message.js')}}"></script>
@if(Session::has('showMessageModal'))
<script type="text/javascript">
        $(document).ready(function() {
showSendMessageModal();
});</script>
@endif
@stop

@section('title')
{{Lang::get('msg.action.page_title')}}
@stop
@section('content')
<img src="{{ $user->avatar->getUrl() }}" />
<br/>
{{ link_to_route('showPartnerActions','Przejrzyj akcje ('.$user->actions->count().')',$user->id) }}
<br/>
@if(Sentry::check() && (Sentry::getUser()->id != $user->id))
<input type="button" value="Wyślij wiadomość" onclick="showSendMessageModal()" />
<br/>
@include('layouts.partners.send_message')
@endif
<h1>{{ $user->companyname }}</h1>
Ilość zaprzyjaźnionych partnerów ({{ $user->getAcceptedPartnersCount() }})
<br/>
Nazwa firmy: {{ $user->companyname }} <br/>
NIP: {{ $user->nip }}<br/>
REGON: {{ $user->regon }}<br/>
Branże 
@foreach($user->branches as $branch)
{{ $branch->name }} | 
@endforeach
<br/>
Ulica: {{ $user->street }} <br/>
Kod i miasto: {{ $user->postcode }} {{ $user->city }}<br/>
Województwo: {{ $user->district->name }}<br/>
Wielkość firmy: {{ $user->companySize->name }}<br/>
Osoba kontaktowa: {{ $user->name }} {{ $user->surname }}
<br/>
<h2>Opis działaności firmy:</h2>
{{ $user->description }}
<br/>
<br/>
<h2>Rekomendacje</h2>
@foreach($user->recommendations as $recom)
@include('layouts.recommendations.recommendation_box',$recom)
@endforeach
{{ link_to(URL::previous(), 'Wróć') }}
@stop