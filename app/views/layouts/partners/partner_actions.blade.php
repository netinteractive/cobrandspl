@extends('layouts.master')

@section('scripts')

@stop

@section('title')
{{Lang::get('msg.action.page_title')}}
@stop
@section('content')
<h2>Akcje użytkownika {{ $username }}</h2>
@foreach($activeActions as $action)
@include('layouts.actions.action_box_small',$action)
@endforeach
<h2>Zakończone akcje użytkownika {{ $username }}</h2>
@foreach($endedActions as $action)
@include('layouts.actions.action_box_small',$action)
@endforeach
{{ link_to(URL::previous(), 'Wróć') }}
@stop