<div id='send_private_message_modal' style="display: none">
    {{ Form::open(array('url'=>route('partnerSendMessage',$user->id))) }}
    <input type="hidden" id="user_id" name="user_id" value='{{ $user->id }}'/>
    <h3>Wyślij wiadomość</h3>
    <textarea style="width:500px;height: 150px;resize: none;" id='private_message_content' name="private_message_content" required>{{ Input::old('private_message_content') }}</textarea>
    <br/>
    <span class="register-error">{{ $errors->first('private_message_content')}}</span>
    <br/>
    <input type="button" value="Anuluj" onclick='hideSendMessageModal()'/><input type="submit" value="Wyślij"/>
    {{ Form::close() }}
</div>