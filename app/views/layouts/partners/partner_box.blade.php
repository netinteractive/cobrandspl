<div style='border:solid 1px;margin-bottom: 5px;display: inline-block'>
    <img src='{{ $partner->avatar->getThumbUrl() }}' align='left'/>
    {{ $partner->companyname }}
    <br/>
    Branże:
    @foreach($partner->branches as $partnerBranch)
    {{ $partnerBranch->name }} | 
    @endforeach
    <br/>
    NIP: {{ $partner->nip }}
    <br/>
    {{ $partner->description }}
    <br/>
    {{ $partner->partners()->count() }} kontaktów
    <br/>
    @if($partner->accepted)
    Zweryfikowany
    @else
    Niezweryfikowany
    @endif
    <br/>
    {{ link_to_route('showPartner','Podgląd',$partner->id) }}
    <br/>
    {{ link_to_route('showPartnerActions','Przeglądaj akcje',$partner->id) }}
</div>