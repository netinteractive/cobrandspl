<div id="selectBrandModal" class="modal fade">
    <div class="modal-dialog">
        {{ Form::open(array('url'=>route('partnerAddAction'),'method'=>'POST')) }}
        <input type="hidden" name="partner_brand_id" id="partner_brand_id" value="{{$brand->id}}"/>
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <h3>Wybierz jeden ze swoich brandów</h3>
                @foreach(PartnerFacade::brandsLeftToLikeThisBrand($brand->id) as $brand)
                <input type="radio" name="brand_id" value="{{$brand->id}}" checked/>{{$brand->name}}<br/>
                @endforeach
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="$('#selectBrandModal').modal('hide')">Anuluj</button>
                <button type="submit" class="btn btn-primary">Zapisz</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>