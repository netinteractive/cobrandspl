@extends('layouts.master')

@section('title')
{{Lang::get('msg.home.page_title')}}
@stop

@section('content')
@include('layouts.components.slider')
@include('layouts.components.info_banners')
@include('layouts.components.top_ten')
@include('layouts.components.actions_partners_links')
<br/>
<br/>
@include('layouts.components.action_under_links')
@foreach($actions as $action)
@include('layouts.actions.action_box',$action)
@endforeach
{{ link_to('/actions/list/category/all/district/all/sortby/latest/sorttype/asc/','Zobacz wszystkie >>') }}
@stop

