<div id='add_comment_modal' style="display: none">
    {{ Form::open(array('url'=>route('addComment'))) }}
    <input type="hidden" id="action_id" name="action_id" />
    <h3>Dodaj komentarz</h3>
    <textarea style="width:500px;height: 150px;resize: none;" id='comment_content' name="comment_content">{{ Input::old('comment_content') }}</textarea>
    <br/>
    <span class="register-error">{{ $errors->first('comment_content')}}</span>
    <br/>
    <input type="submit" value="Dodaj" min="10" max="3000"/>
    {{ Form::close() }}
</div>

