@if(Sentry::getUser()->brands->count() == 0)
    <i>brak</i>
@else
    @foreach(Sentry::getUser()->brands as $brand)
    <div id="brand_{{$brand->id}}">
        {{ link_to_route('showBrand',$brand->name,array($brand->id)) }}<input type="button" value="usuń" onclick='removeBrandConfirm({{$brand->id}})'/>
    </div>
    @endforeach
@endif