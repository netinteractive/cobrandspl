<ul>
    <li style='display: inline'>{{ link_to_route("myProfileView","Mój profil") }}</li>
    <li style='display: inline'>{{ link_to_route("myActionsView","Moje Akcje") }}</li>
    <li style='display: inline'>{{ link_to_route("myApplicationsView","Moje Zgłoszenia") }}</li>
    <li style='display: inline'>{{ link_to_route("myPartnersView","Moi Zaprzyjaźnione Brandy") }}</li>
    @if(Sentry::getUser()->is_producer)
        <li style='display: inline'>{{ link_to_route("myProductsView","Moje Produkty",array('date','desc')) }}</li>
    @endif
</ul>