<div id="personal_details_edit" style="display: none">
    {{ Form::open(array('url'=>route('myProfileSaveAction'))) }}
    <input type='hidden' name='user_id' id='user_id' value='{{ $user->id }}'/>
    <h2>{{Lang::get('msg.register.company_data_header')}}</h2>


    <label for="edit_companyname">{{Lang::get('msg.register.companyname_label')}}</label>
    <input type="text" min="2" max="250" id="edit_companyname" name="edit_companyname" value="{{ Input::old('edit_companyname')?Input::old('edit_companyname'):$user->companyname }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_companyname')}}</span>


    <label for="edit_nip">{{Lang::get('msg.register.nip_label')}}</label>
    <input type="text" min="10" max="10" id="edit_nip" name="edit_nip" value="{{ Input::old('edit_nip')?Input::old('edit_nip'):$user->nip }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_nip')}}</span>


    <label for="edit_regon">{{Lang::get('msg.register.regon_label')}}</label>
    <input type="text" min="5" max="15" id="edit_regon" name="edit_regon" value="{{ Input::old('edit_regon')?Input::old('edit_regon'):$user->regon }}"/>
    <br/>


    <label for="edit_branch">{{Lang::get('msg.register.branches_label')}}</label>
    <select onchange="onEditBranchSelect()" id="edit_branch_selector">
        @foreach(BranchFacade::getMainBranches() as $mainBranch)
        <optgroup label="{{ $mainBranch->name }}">
            @foreach($mainBranch->getChildren() as $child)
            <option value="{{ $child->id }}">{{ $child->name }}</option>
            @endforeach
        </optgroup>
        @endforeach
    </select>
    <input type="hidden" name="edit_branches" value="" id="edit_branches"/>
    <div id="selected_branches_box">
        @foreach($user->branches as $b)
        <span style='display:block' id='branch{{ $b->id }}'>{{ $b->name }}<input type='button' onclick="removeBranch('{{ $b->id }}')" value='usuń'></span>
        @endforeach

    </div>
    <br/>
    <span class="register-error">{{ $errors->first('edit_branches')}}</span>


    <label for="edit_street">{{Lang::get('msg.register.street_label')}}</label>
    <input type="text" name="edit_street" min="3" max="200" id="edit_street" value="{{ Input::old('edit_street')?Input::old('edit_street'):$user->street }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_street')}}</span>


    <label for="edit_postcode">{{Lang::get('msg.register.postcode_city_label')}}</label>
    <input type="text" name="edit_postcode" min="2" max="20" id="edit_postocde" value="{{ Input::old('edit_postcode')?Input::old('edit_postcode'):$user->postcode }}" required/>
    <input type="text" name="edit_city" min="2" max="200" id="edit_city" placeholder="Miasto" value="{{ Input::old('edit_city')?Input::old('edit_city'):$user->city }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_postcode')}}</span>
    <span class="register-error">{{ $errors->first('edit_city')}}</span>


    <label for="edit_district">{{Lang::get('msg.register.district_label')}}</label>
    <select name='edit_district' id='edit_district'>
        <?php
        $index = '';
        if (Input::old('edit_district')) {
            $index = Input::old('edit_district');
        } else {
            $index = $user->district_id;
        }
        ?>
        @foreach($districts as $d)

        @if($d->id == $index)
        <option value='{{ $d->id }}' selected="selected">{{ $d->name }}</option>
        @else
        <option value='{{ $d->id }}'>{{ $d->name }}</option>
        @endif
        {{ $d->name }}
        @endforeach
    </select>
    <br/>
    <span class="register-error">{{ $errors->first('edit_district')}}</span>


    <label for="edit_companysize">{{Lang::get('msg.register.companysize_label')}}</label>
    <select name='edit_companysize' id='edit_companysize'>
        <?php
        $index = '';
        if (Input::old('edit_companysize')) {
            $index = Input::old('edit_companysize');
        } else {
            $index = $user->companysize_id;
        }
        ?>
        @foreach($companysizes as $d)

        @if($d->id == $index)
        <option value='{{ $d->id }}' selected="selected">{{ $d->name }}</option>
        @else
        <option value='{{ $d->id }}'>{{ $d->name }}</option>
        @endif
        {{ $d->name }}
        @endforeach
    </select>
    <br/>
    <span class="register-error">{{ $errors->first('edit_companysize')}}</span>


    <h2>{{Lang::get('msg.register.contact_data_header')}}</h2>

    {{Lang::get('msg.register.contact_person_label')}}<br/>
    <label for="edit_name">Imię i nazwisko</label>
    <input type="text" name="edit_name" min="2" max="100" id="edit_name" value="{{ Input::old('edit_name')?Input::old('edit_name'):$user->name }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_name')}}</span>

    <label for="edit_phone_main">{{Lang::get('msg.register.phone_main_label')}}</label>
    <input type="text" name="edit_phone_main" min="2" max="45" id="edit_phone_main" value="{{ Input::old('edit_phone_main')?Input::old('edit_phone_main'):$user->phone_main }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_phone_main')}}</span>


    <label for="edit_phone_alternative">{{Lang::get('msg.register.phone_alternative_label')}}</label>
    <input type="text" name="edit_phone_alternative" min="2" max="45" id="edit_phone_alternative" value="{{ Input::old('edit_phone_alternative')?Input::old('edit_phone_alternative'):$user->phone_alternative }}" />
    <br/>
    <span class="register-error">{{ $errors->first('edit_phone_alternative')}}</span>


    <label for="edit_email">{{Lang::get('msg.register.email_label')}}</label>
    <input type="email" name="edit_email" min="2" max="45" id="edit_email" value="{{ Input::old('edit_email')?Input::old('edit_email'):$user->email }}" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_email')}}</span>

    <label for="edit_description">{{Lang::get('msg.register.description_label')}}</label>
    <textarea name="edit_description" id="edit_description" min="10" max="3000">{{ Input::old('edit_description')?Input::old('edit_description'):$user->description }}</textarea>
    <br/>
    <span class="register-error">{{ $errors->first('edit_description')}}</span>
    <br/>
    @if($user->subscribedToNewsletter())
    <input type="checkbox" value="on" name="edit_newsletter_accepted" checked="checked"/>
    @else
    <input type="checkbox" value="on" name="edit_newsletter_accepted"/>
    @endif
    <label for="edit_newsletter_accepted">{{Lang::get('msg.register.newsletter_label')}}</label>
    <br/>
    <input type="button" value="Anuluj" onclick='cancelEditProfile()'/><input type="submit" value="Zapisz zmiany" />
    {{ Form::close();}}
    {{ Form::open(array('url'=>route('myProfilePasswordChangeAction'))) }}
    <h2>Zmiana hasła</h2>
    <input type='hidden' name='user_id' id='user_id' value='{{ $user->id }}'/>
    <label for="edit_password">{{Lang::get('msg.register.password_label')}}</label>
    <input type="password" name="edit_password" min="2" max="45" id="edit_password" value="" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_password')}}</span>
    <label for="edit_password_confirm">{{Lang::get('msg.register.password_confirm_label')}}</label>
    <input type="password" name="edit_password_confirm" min="2" max="45" id="edit_password_confirm" value="" required/>
    <br/>
    <span class="register-error">{{ $errors->first('edit_password_confirm')}}</span>
    <input type="submit" value="Zmień hasło" />
    {{ Form::close();}}
</div>