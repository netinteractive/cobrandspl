@extends('layouts.master')

@section('title')
Płatność za pakiet
@stop


@section('content')
Kupujesz pakiet: {{ $package->name }}<br/>
Cena pakietu: {{ $package->price }}<br/>
{{ $button }}
@stop
