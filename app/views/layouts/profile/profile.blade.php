@extends('layouts.master')

@section('title')
{{Lang::get('msg.home.page_title')}}
@stop


@section('content')
@include('layouts.profile.components.profile_links')
@stop