@extends('layouts.profile.profile')

@section('content')
@parent
<h2>Oczekujące zaproszenia</h2>
@foreach($waitingPartners as $wp)
<div style='border:solid 1px;margin-bottom: 5px;display: inline-block'>
    <img src='{{ $wp->brand->logo->getThumbUrl() }}' align='left'/>
    {{ $wp->brand->name }}
    <br/>
    Branże:
    @foreach($wp->brand->branches as $partnerBranch)
    {{ $partnerBranch->name }} | 
    @endforeach
    <br/>
    {{ Form::open(array('url'=>route('partnerAcceptAction',array($wp->getUserId(),$wp->getBrandId())))) }}
    <input type='submit' value='Potwierdź' />
    {{ Form::close() }}
    <br/>
    {{ Form::open(array('url'=>route('partnerRejectAction',array($wp->getUserId(),$wp->getBrandId())))) }}
    <input type='submit' value='Odrzuć' />
    {{ Form::close() }}
</div>
<div style='clear:both' />
@endforeach
<h2>Zaprzyjaźnieni partnerzy</h2>
@foreach($acceptedPartners as $ap)
<div style='border:solid 1px;margin-bottom: 5px;display: inline-block'>
    {{ Form::open(array('url'=>route('partnerRemoveAction',array($ap->getUserId(),$ap->getBrandId())))) }}
    <input type='submit' value='Usuń' />
    {{ Form::close() }}
    <img src='{{ $ap->brand->logo->getThumbUrl() }}' align='left'/>
    {{ $ap->brand->name }}
    <br/>
    Branże:
    @foreach($ap->brand->branches as $partnerBranch)
    {{ $partnerBranch->name }} | 
    @endforeach
    <br/>
    {{ link_to_route('showBrand','Podgląd',$ap->brand->id) }}
</div>
<div style='clear:both' />
@endforeach
@stop