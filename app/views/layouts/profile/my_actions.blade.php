@extends('layouts.profile.profile')

@section('scripts')

@stop

@section('content')
@parent
{{link_to_route('actionAddView',Lang::get('msg.home.add_action'))}}
<br/>
Moje akcje<br/>
@foreach(UserFacade::getUserActiveActions(Sentry::getUser()->id) as $action)
@include('layouts.actions.action_box_small',$action)
@endforeach
Zakończone akcje<br/>
@foreach(UserFacade::getUserFinishedActions(Sentry::getUser()->id) as $action)
@include('layouts.actions.action_box_small',$action)
@endforeach
@stop
