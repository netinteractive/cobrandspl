@extends('layouts.profile.profile')

@section('title')
Cobrand.pl - lista produktów
@stop
@section('scripts')
@if(Sentry::getUser()->is_producer)
<script type="text/javascript" src="{{URL::asset('global/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_product_modal.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/vendor/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.iframe-transport.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/bootbox.min.js')}}"></script>
@endif
<script type="text/javascript" src="{{URL::asset('js/scripts_products_list.js')}}"></script>
@stop
@section('content')
@parent
@if(Sentry::getUser()->is_producer)
<input type="button" value="+ Dodaj nowy produkt" onclick="addProductClick()" />
@endif
Sortuj po: 
<select id="sort_option" onchange="myProductListSortChange()">
    <option value="date_desc" <?php if($sortby=='date' && $sorttype=='desc') echo "selected"; ?>>od najnowszych</option>
    <option value="date_asc" <?php if($sortby=='date' && $sorttype=='asc') echo "selected"; ?>>od najstarszych</option>
    <option value="price_desc" <?php if($sortby=='price' && $sorttype=='desc') echo "selected"; ?>>od najdroższych</option>
    <option value="price_asc" <?php if($sortby=='price' && $sorttype=='asc') echo "selected"; ?>>od najtańszych</option>
</select>
@foreach($products as $product)
@include('layouts.products.product_box',$product)
@endforeach
{{ $products->links() }}

@if(Sentry::getUser()->is_producer)
    @include('layouts.register.add_product_modal')
@endif
@stop