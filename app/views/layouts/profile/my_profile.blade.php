@extends('layouts.profile.profile')

@section('scripts')
<script type="text/javascript" src="{{URL::asset('global/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/script_buy.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/script_abuse.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/script_edit_profile.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/vendor/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.iframe-transport.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_brand_modal.js')}}"></script>
@if(Session::has('showModal'))
<script type="text/javascript">
        $(document).ready(function() {
showAddAbuseModal({{ Session::get('showModal') }});

});</script>
@endif

@if(Session::has('showEdit'))
<script type="text/javascript">
        $(document).ready(function() {
editProfile();
});</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
    if(window.location.hash) {
    if(window.location.hash == '#wykup_pakiet') {
        document.getElementById("buy_package_modal").style.display = "block";
    }
}

});

            @if($user->branches->count() == 0)
                var selectedBranches = new Array();
            @else
                var selectedBranches = [<?php $counter = 0;?>@foreach($user->branches as $b)"{{ $b->id }}"<?php if($counter+1 != $user->branches->count()) {echo ",";}$counter++; ?>@endforeach];
            @endif
            
</script>
<script>
tinymce.init({
    selector: "#edit_description",
    toolbar: "link | image",
    language: "pl",
    menubar: false,
    statusbar: false,
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality template textcolor paste textcolor"
    ],
    toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
            relative_urls: false,
});

</script>
<script>
    $(function() {
        $('#avatarupload').fileupload({
            dataType: 'json',
            add: function(e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
                    document.getElementById("avatar_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                    goUpload = false;
                }
                if (uploadFile.size > 2000000) { // 2mb
                    document.getElementById("avatar_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                    goUpload = false;
                }
                if (goUpload == true) {
                    data.submit();
                }
            },
            done: function(e, data) {
                if (data.result.error) {
                    document.getElementById("avatar_upload_error").innerHTML = data.result.error;
                } else if (data.result.success) {
                    document.getElementById("avatar_placeholder").innerHTML = '<img src=\'' + data.result.avatar_url + '\' />';
                    document.getElementById("avatar_thumb_placeholder").innerHTML = '<img src=\'' + data.result.avatar_thumb_url + '\' />';
                } else {
                    document.getElementById("avatar_upload_error").innerHTML = 'Wystąpił nieznany błąd';
                }
            }
        });
    });
</script>
@stop

@section('title')
{{Lang::get('msg.action.page_title')}}
@stop
@section('content')
@parent
<div>
    <div id="avatar_placeholder" style="width:150px;height:150px;border:solid 1px;">
    <img src="{{ $user->avatar->getUrl() }}" />
    </div>
    <br/>
    <input id="avatarupload" type="file" name="avatarfile" data-url="{{ route('uploadAvatar',$user->id) }}"/>
    <span id="avatar_upload_error" class="register-error"></span>
    <input type="hidden" id="avatar_id" name="avatar_id" value="{{$user->avatar->id}}"/>
    <br/>
    <div id="personal_details_show" style="display: block">
        <h2>{{Lang::get('msg.register.company_data_header')}}</h2>
        <hr/>
        Nazwa firmy: {{ $user->companyname }} <br/>
        NIP: {{ $user->nip }}<br/>
        REGON: {{ $user->regon }}<br/>
        Branże 
        @foreach($user->branches as $branch)
        {{ $branch->name }} | 
        @endforeach
        <br/>
        Ulica: {{ $user->street }} <br/>
        Kod i miasto: {{ $user->postcode }} {{ $user->city }}<br/>
        Województwo: {{ $user->district->name }}<br/>
        Wielkość firmy: {{ $user->companySize->name }}<br/>
        <h2>{{Lang::get('msg.register.contact_data_header')}}</h2>
        <hr/>
        Osoba kontaktowa: {{ $user->name }} {{ $user->surname }}<br/>
        Telefon 1: {{ $user->phone_main }}<br/>
        Telefon 2: @if($user->phone_alternative) {{ $user->phone_alternative }} @else brak @endif
        <br/>
        E-mail: {{ $user->email }}
        </br>
        Newsletter: @if($user->subscribedToNewsletter()) Tak @else Nie @endif
        <br/>
        
        <h2>Moje brandy</h2>
        <hr/>
        <div id="profile_brands_list">
            @include('layouts.profile.components.brands_list')
        </div>
        <span>Dodaj nowy brand <input type="button" value="+" id="add_brand_button" name="add_brand_button" onclick="showAddBrandModal('signedin')"/></span>
        @include('layouts.register.add_brand_modal')
        <h2>Opis działaności firmy:</h2>
        <hr/>
        {{ \NetInteractive\Cobrands\Utils\ValidationUtils::decode($user->description) }}
        <br/>
        <br/>
        <input type="button" value="Edytuj" onclick="editProfile()"/>
    </div>
    @include('layouts.profile.edit')
    <br/>
    <h3>Pakiet usług</h3>
    @foreach(UserFacade::getUserActiveMonthlyPackages(Sentry::getUser()->id) as $po)
    {{ $po->package->name }} pozostało {{ $po->getTimeLeft() }}<br/>
    @endforeach
    @foreach(UserFacade::getUserActiveSinglePackages(Sentry::getUser()->id) as $po)
    {{ $po->package->name }}<br/>
    @endforeach
    <a name='wykup_pakiet'></a>
    <input type="button" value="Wykup pakiet" onclick="showBuyModal()"/>
    @include('layouts.packages.buy')

    <br/>
    <h2>Rekomendacje</h2>
    @foreach($user->recommendations as $recom)
    @include('layouts.recommendations.recommendation_box',$recom)
    @endforeach
    @include('layouts.abuse.add')
    @stop