@extends('layouts.profile.profile')

@section('scripts')
<script type="text/javascript" src="{{URL::asset('js/script_recommendations.js')}}"></script>
@if(Session::has('showModal'))
<script type="text/javascript">
        $(document).ready(function() {
showAddRecommendationModal({{ Session::get('showModal') }});
        });

</script>
@endif
@stop

@section('content')
@parent
Moje zgłoszenia
@foreach(UserFacade::getUserAppliedActiveActions(Sentry::getUser()->id) as $action)
@include('layouts.actions.action_box_small',$action)
@endforeach
Zakończone akcje
@foreach(UserFacade::getUserAppliedFinishedActions(Sentry::getUser()->id) as $action)
@include('layouts.actions.action_box_small',$action)
@endforeach
@include('layouts.recommendations.add')
@stop
