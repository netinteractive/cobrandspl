<select id="product_subcategory_selector" onchange="onLibrarySubcategoryChange()" style='min-width: 300px'>
    <option class="first-option">Wybierz...</option>
    @foreach($subCategories as $subCategory)
    <option value="{{ $subCategory->id }}">{{ $subCategory->name }}</option>
    @endforeach
</select>