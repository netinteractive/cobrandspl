<label for='product_category_selector'>Wybierz kategorię produktu</label>
<div id="product_category_selector_box">
    <select id="product_category_selector" onchange='onLibraryCategoryChange()'>
        <option class="first-option">Wybierz...</option>
        @foreach($productCategories as $mainCategory)
        <option value="{{ $mainCategory->id }}">{{ $mainCategory->name }}</option>
        @endforeach
    </select>
</div>
<br/>
<label for='product_subcategory_selector'>Wybierz podkategorię produktu</label>
<div id="product_subcategory_selector_box">
    <select id="product_subcategory_selector" style='min-width: 300px' disabled="">
        <option value="">Wybierz kategorię</option>
    </select>
</div>
<br/>
<label for='product_selector'>Wybierz odpowiedni produkt</label>
<div id="product_selector_box">
    <select id="product_selector" size='10' style='min-width:300px' disabled="">
        <option value="">Brak</option>
    </select>
</div>