<select id="product_selector" size='10' style='min-width:300px'>
    @foreach($products as $product)
    <option value="{{ $product->id }}">{{ $product->name }}</option>
    @endforeach
</select>