@extends('layouts.master')

@section('title')
{{Lang::get('msg.home.page_title')}}
@stop

@section('content')
@include('layouts.components.actions_partners_links')
<br/>
<br/>
@include('layouts.components.action_under_links')
@foreach($actions as $action)
@include('layouts.actions.action_box',$action)
@endforeach
{{ $actions->links() }}
@stop