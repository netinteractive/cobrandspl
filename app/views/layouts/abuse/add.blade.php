<div id='add_abuse_modal' style="display: none">
    {{ Form::open(array('url'=>route('addAbuse'))) }}
    <input type="hidden" id="recommendation_id" name="recommendation_id" />
    <h3>Dodaj nadużycie</h3>
    <textarea style="width:500px;height: 150px;resize: none;" id='abuse_content' name="abuse_content">{{ Input::old('abuse_content') }}</textarea>
    <br/>
    <span class="register-error">{{ $errors->first('abuse_content')}}</span>
    <br/>
    <input type="submit" value="Wyślij" min="10" max="3000"/>
    {{ Form::close() }}
</div>

