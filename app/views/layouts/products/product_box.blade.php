<div style='border:solid 1px;margin-bottom: 5px;display: inline-block;width:100%'>
    <img src="{{$product->logo->getUrl()}}" />
    <br/>
    Data wystawienia produktu: {{$product->getFormattedCreatedAtDate()}}
    <br/>
    <a href="{{route('showProductView',$product->id)}}" >{{$product->name}}</a>
    <br/>
    {{$product->category->getParent()->name}}/{{$product->category->name}}
    <br/>
    @if($product->price_count >= 1)
    Cena: {{$product->getFormattedPrice_1()}} od {{$product->count_1_from}} szt. do {{$product->count_1_to}} szt.
    <br/>
    @endif
    @if($product->price_count >= 2)
    Cena: {{$product->getFormattedPrice_2()}} od {{$product->count_2_from}} szt. do {{$product->count_2_to}} szt.
    <br/>
    @endif
    @if($product->price_count == 3)
    Cena: {{$product->getFormattedPrice_3()}} od {{$product->count_3_from}} szt. do {{$product->count_3_to}} szt.
    <br/>
    @endif
    Na stanie: {{$product->product_count}} szt.
    <br/>
</div>