@extends('layouts.master')

@section('title')
Cobrand.pl - karta produktu
@stop

@if($product->user_id == Sentry::getUser()->id)
@section('scripts')
<script type="text/javascript" src="{{URL::asset('global/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_product_modal.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/vendor/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.iframe-transport.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/bootbox.min.js')}}"></script>
@stop
@endif

@section('content')
<div style='border:solid 1px;margin-bottom: 5px;display: inline-block;width:100%'>
    <img src="{{$product->logo->getUrl()}}" />
    <br/>
    <a href="{{route('showPartner',$product->user->id)}}">{{$product->user->companyname}}</a>
    <br/>
    {{$product->name}}
    <br/>
    {{$product->category->getParent()->name}}/{{$product->category->name}}
    <br/>
    Opis<br/>
    {{NetInteractive\Cobrands\Utils\ValidationUtils::decode($product->description)}}
    <br/>
    @if($product->price_count >= 1)
    Cena: {{$product->getFormattedPrice_1()}} od {{$product->count_1_from}} szt. do {{$product->count_1_to}} szt.
    <br/>
    @endif
    @if($product->price_count >= 2)
    Cena: {{$product->getFormattedPrice_2()}} od {{$product->count_2_from}} szt. do {{$product->count_2_to}} szt.
    <br/>
    @endif
    @if($product->price_count == 3)
    Cena: {{$product->getFormattedPrice_3()}} od {{$product->count_3_from}} szt. do {{$product->count_3_to}} szt.
    <br/>
    @endif
    Dostępność produktu: {{$product->product_count}} szt.
    <br/>
    <br/>
    <a href="{{ URL::previous() }}">&lt; Wróć</a>
    @if($product->user_id == Sentry::getUser()->id)
    <input type="button" value="Edytuj" onclick="showEditProductModal({{$product->id}})"/>
    @endif
</div>

@include('layouts.products.edit_product_modal')
@stop