<div id="editBrandModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <input type="hidden" id="brand_add_type" value="" />
                <div id="brand_image_placeholder" class="product-image-placeholder">
                </div>
                <input id="brand_image_upload" type="file" name="brand_image_file" data-url="{{ route('uploadBrandImage') }}"/>
                <span id="brand_image_upload_error" class="register-error"></span>
                <span class="register-error" id="brand_image_error"></span>
                <input type="hidden" name="brand_image_id" id="brand_image_id" value="0"/>
                <label for="brand_name">Nazwa brandu</label>
                <input type="text" name="brand_name" maxlength="200" id="brand_name" value=""/>
                <br/>
                <span class="register-error" id="brand_name_error"></span>
                <label for="register_branch">{{Lang::get('msg.register.branches_label')}}</label>
                <select onchange="onBrandBranchSelect()" id="registration_branch_selector">
                    @foreach(BranchFacade::getMainBranches() as $mainBranch)
                    <optgroup label="{{ $mainBranch->name }}">
                        @foreach($mainBranch->getChildren() as $child)
                        <option value="{{ $child->id }}">{{ $child->name }}</option>
                        @endforeach
                    </optgroup>
                    @endforeach
                </select>
                <input type="hidden" name="brand_branches" value="" id="brand_branches"/>
                <div id="selected_branches_box"></div>
                <br/>
                <label for="brand_description">Opis:</label>
                <textarea name="brand_description" id="brand_description" maxlength="3000"></textarea>
                <br/>
                <span class="register-error" id="brand_description_error"></span>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="$('#editBrandModal').modal('hide')">Anuluj</button>
                <button type="button" class="btn btn-primary" onclick="saveBrand({{$brand->id}})">Zapisz</button>
            </div>
        </div>
    </div>
</div>
