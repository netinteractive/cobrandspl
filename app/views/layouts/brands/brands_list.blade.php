@extends('layouts.master')

@section('title')
{{Lang::get('msg.home.page_title')}}
@stop

@section('content')
@include('layouts.components.actions_partners_links')
<br/>
<br/>
@foreach($brands as $brand)
@include('layouts.brands.brand_box',$brand)
<div style='clear:both'></div>
@endforeach
{{ $brands->links() }}
@stop