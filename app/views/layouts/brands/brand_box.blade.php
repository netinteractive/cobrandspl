<div style='border:solid 1px;margin-bottom: 5px;display: inline-block'>
    <img src='{{ $brand->logo->getThumbUrl() }}' align='left'/>
    {{ $brand->name }} / {{$brand->user->companyname}}
    <br/>
    Branże brandu:
    @foreach($brand->branches as $branch)
        {{$branch->name}}
    <br/>
    @endforeach
    <br/>
    Liczba kontaktów: {{PartnerFacade::getBrandContactsNumber($brand->id)}}
    <br/>
    {{NetInteractive\Cobrands\Utils\ValidationUtils::decode($brand->description)}}
    <br/>
    {{ link_to_route('showBrandActions','Przeglądaj akcje',array($brand->id)) }}
    <br/>
    {{ link_to_route('showBrand','Podgląd',array($brand->id)) }}
    <br/>
</div>