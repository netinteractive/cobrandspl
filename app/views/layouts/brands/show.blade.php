@extends('layouts.master')

@section('title')
Cobrand.pl - karta brandu
@stop

@if(Sentry::check())
@if($brand->user_id == Sentry::getUser()->id)
@section('scripts')
<script type="text/javascript" src="{{URL::asset('global/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_brand_modal.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/vendor/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.iframe-transport.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/bootbox.min.js')}}"></script>
@stop
@endif
@endif

@section('content')
<div style='border:solid 1px;margin-bottom: 5px;display: inline-block;width:100%'>
    <img src="{{$brand->logo->getUrl()}}" />
    <br/>
    {{ $brand->name }} / {{$brand->user->companyname}}
    <br/>
    Branże brandu:
    @foreach($brand->branches as $branch)
    {{$branch->name}}
    <br/>
    @endforeach
    <br/>
    {{$brand->user->companyname}}
    <br/>
    Ulica: {{$brand->user->street}}
    <br/>
    Kod i miasto: {{$brand->user->postcode}} {{$brand->user->city}}
    <br/>
    Województwo: {{$brand->user->district->name}}
    <br/>
    Opis<br/>
    {{NetInteractive\Cobrands\Utils\ValidationUtils::decode($brand->description)}}
    <br/>
    <br/>
    @if(PartnerFacade::isHaveAnyBrandsLeftToLikeThisBrand($brand->id))
        @if(Sentry::check() && Sentry::getUser()->id != $brand->user->id)
        <a href="#" onclick="$('#selectBrandModal').modal('show');return false;">+Zaprzyjaźniony brand</a>
        @endif
        <br/>
    @endif
    <a href="{{ URL::previous() }}">&lt; Wróć</a>
    @if(Sentry::check())
    @if($brand->user_id == Sentry::getUser()->id)
    <input type="button" value="Edytuj" onclick="showEditBrandModal({{$brand->id}})"/>
    @endif
    @endif
    <a href="#">Kontakty</a>
    {{ link_to_route('showBrandActions','Pokaż akcje brandu',array($brand->id)) }}
</div>

@include('layouts.brands.edit_brand_modal')
@include('layouts.partners.select_brand_modal')
@stop