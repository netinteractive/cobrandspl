@extends('layouts.master')

@section('title')
{{Lang::get('msg.login.page_title')}}
@stop

@section('content')
{{ Form::open(array('url'=>route('sendPasswordResetCodeAction'))) }}
<table>
    <caption><h3>Przypomnienie hasła</h3></caption>

    <tbody>
        <tr>
            <td>Twój adres e-mail:</td>
            <td>
                <input class="form-control" type="email" name="reminder_email" id="reminder_email" max="45" min="4" required>
            </td>
        </tr>

        <tr>
            <td></td>
            <td>{{ Form::submit('Wyślij przypomnienie',array())}}</td>
        </tr>


    </tbody>
</table>
{{ Form::close()}}
@stop