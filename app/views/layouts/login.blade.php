@extends('layouts.master')

@section('title')
{{Lang::get('msg.login.page_title')}}
@stop

@section('content')
{{ Form::open(array('url'=>route('loginAction'))) }}
E-mail<input type='email' id='login_email' name='login_email'/><br/>
Hasło<input type='password' id='login_password' name='login_password'/><br/>
<input type='checkbox' name='remember_me'/>{{Lang::get('msg.login.remember_me_checkbox')}}<br/>
<input type='checkbox' name='dont_logout'/>{{Lang::get('msg.login.dont_sign_out_checkbox')}}<br/>
<input type='submit' value='{{Lang::get('msg.login.login_button')}}'><br/>
{{link_to_route('passwordResetView','Przypomnij hasło')}}
{{ Form::close()}}
@stop

