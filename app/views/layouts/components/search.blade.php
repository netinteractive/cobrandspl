<div id='search_area' style='float:right'>
    {{ Form::open(array('url'=>route('search'))) }}
    <input type="hidden" name="is_advanced_search" id="is_advanced_search" value="0"/>
    <input type='text' width='200px;' placeholder='Wpisz czego szukasz' id="search_term" name="search_term"/>
    <select id="search_branch_selector" name="search_category_selector">
        <option value="0">Wszystkie kategorie</option>
        @foreach(CategoryFacade::getMainCategories() as $mainCategory)
        <optgroup label="{{ $mainCategory->name }}">
            @foreach($mainCategory->getChildren() as $child)
            <option value="{{ $child->id }}">{{ $child->name }}</option>
            @endforeach
        </optgroup>
        @endforeach
    </select>
    <input type='submit' value='Szukaj' onclick="$('#is_advanced_search').val(0)"/>
    <br/>
    <a href='#' onclick="onAdvancedSearchClick();
            return false;">Wyszukiwanie zaawansowane</a>
    <div id="advanced_search_box" style="display:none">
        <button onclick="onHideAdvancedSearchClick()">Zwiń^</button>
        <h3>Wyszukiwanie zaawansowane</h3>
        <label for="search_type">Szukam:</label>
        <select id="search_type" name="search_type" onchange="onSearchTypeChange()">
            <option value="1">Akcji</option>
            <option value="2">Brandów</option>
        </select>
        <br/>
        <div id="search_city_box">
            <label for="search_city">Miasto:</label>
            <input type="text" maxlength="150" id="search_city" name="search_city" />
            <br/>
        </div>
        <div id="search_district_box">
            <label for="search_city">Województwo:</label>
            <select id="search_district" name="search_district">
                <option value="0">Wszystkie</option>
                @foreach(District::all() as $district)
                    <option value="{{$district->id}}">{{$district->name}}</option>
                @endforeach
            </select>
            <br/>
        </div>
        <div id="search_category_box">
            @include('layouts.components.action_categories.category_selector')
            <br/>
        </div>
        <div id="search_branch_box">
            @include('layouts.components.brand_branches.category_selector')
            <br/>
        </div>
        <div id="search_budget_box">
            <label for="budget_from">Budżet</label>
            Od:<input type="text" id="budget_from" name="budget_from" />
            Do:<input type="text" id="budget_to" name="budget_to" />
            <br/>
        </div>
        <div id="search_date_box">
            <label for="date_from">Termin akcji</label>
            Od:<input type="text" id="date_from" name="date_from" class="datepicker"/>
            Do:<input type="text" id="date_to" name="date_to" class="datepicker"/>
            <br/>
        </div>
        <input type='submit' value='Pokaż' onclick="$('#is_advanced_search').val(1)"/>
    </div>
    {{ Form::close() }}
</div>