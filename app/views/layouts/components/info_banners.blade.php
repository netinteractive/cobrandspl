<div id='info_banner'>
    <div id='info1' style='width:23%;border-right: solid 1px;float:left'>
        {{ ActionFacade::getTotalActionsCount() }} Akcji zgłoszonych
    </div>
    <div id='info2' style='width:23%; float:left;border-right: solid 1px;'>
        {{ ActionFacade::getFinishedActionsCount() }} Akcji przeprowadzonych
    </div>
    <div id='info3' style='width:23%; float:left;border-right: solid 1px;'>
        {{ ActionFacade::getActiveActionsCount() }} Akcji aktywnych
    </div>
    <div id='info4' style='width:23%; float:left;border-right: solid 1px;'>
        {{ UserFacade::getRegisteredUsersCount() }} Firm zarejestrowanych
    </div>
</div>
<div style='clear:both'/>
<hr/>