<label for="subbranch_selector">Podbranża:</label>
<select id="subbranch_selector" name="subbranch_id">
    @if($mainBranchId == 0)
        <option value="">------</option>
    @else
        @if($subbranchId != null)
        @foreach(BranchFacade::getSubBranches($mainBranchId) as $cat)
            <option value="{{ $cat->id }}" <?php if($cat->id == $subbranchId) echo "selected"; ?>>{{ $cat->name }}</option>
        @endforeach
        @else
         <option value="0">Wszystkie</option>
        @foreach(BranchFacade::getSubBranches($mainBranchId) as $cat)
            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
        @endforeach
        @endif
    @endif
</select>
