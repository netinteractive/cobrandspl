<select onchange="reloadSubBranchesList()" id="branch_selector" name="branch_id">
    @if($branch_id != null)
    @foreach(BranchFacade::getMainBranches() as $mainBranch)
    <option value="{{ $mainBranch->id }}" <?php if($branch_id == $mainBranch->id) echo "selected"; ?>>{{ $mainBranch->name }}</option>
    @endforeach
    @else
    <option value="0">Wszystkie</option>
    @foreach(BranchFacade::getMainBranches() as $mainBranch)
    <option value="{{ $mainBranch->id }}">{{ $mainBranch->name }}</option>
    @endforeach
    @endif
</select>
