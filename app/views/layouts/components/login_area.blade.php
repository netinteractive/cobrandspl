@if(Sentry::check())
<div id='login' style='float:right'>
    <div id='avatar_thumb_placeholder' style='display:inline'>
        <img src="{{ Sentry::getUser()->avatar->getThumbUrl() }}">
    </div>
    {{link_to_route('myProfileView',Sentry::getUser()->companyname)}}
    {{link_to_route('logoutAction',Lang::get('msg.home.logout_link'))}}
    <br/>
    <input type="button" value="Wykup pakiet" onclick="location.href='profile#wykup_pakiet'"/>
</div>
@else
<div id='login' style='float:right'>
    {{link_to_route('registerView',Lang::get('msg.home.register_link'))}}
    {{link_to_route('loginView',Lang::get('msg.home.login_link'))}}
</div>

@endif