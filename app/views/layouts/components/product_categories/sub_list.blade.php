<label for="product_subcategory_selector">Podkategoria Produktu:</label>
<select id="product_subcategory_selector" name="product_category_id">
    @if($mainProductCategoryId == 0)
        <option value="">------</option>
    @else
        @if($subcategoryId != null)
        @foreach(ProductCategoryFacade::getSubCategories($mainProductCategoryId) as $cat)
            <option value="{{ $cat->id }}" <?php if($cat->id == $subcategoryId) echo "selected"; ?>>{{ $cat->name }}</option>
        @endforeach
        @else
         <option class="first-option">Wybierz...</option>
        @foreach(ProductCategoryFacade::getSubCategories($mainProductCategoryId) as $cat)
            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
        @endforeach
        @endif
       
    @endif
</select>
