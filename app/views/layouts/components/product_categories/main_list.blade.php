<select onchange="reloadProductSubCategoriesList()" id="product_category_selector">
    @if($category_id != null)
    @foreach(ProductCategoryFacade::getMainCategories() as $mainCategory)
    <option value="{{ $mainCategory->id }}" <?php if($category_id == $mainCategory->id) echo "selected"; ?>>{{ $mainCategory->name }}</option>
    @endforeach
    @else
    <option class="first-option">Wybierz...</option>
    @foreach(ProductCategoryFacade::getMainCategories() as $mainCategory)
    <option value="{{ $mainCategory->id }}">{{ $mainCategory->name }}</option>
    @endforeach
    @endif
    
</select>
