<label for="product_category">Kategoria Produktu:</label>
{{ App::make('ProductCategoryController')->renderMainProductCategoriesList($category_id); }}
<div id="product_subcategory_holder">
    {{ App::make('ProductCategoryController')->renderSubProductCategoriesList($category_id,$subcategory_id); }}
</div>