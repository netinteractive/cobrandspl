<div id='info_error'>
    @if(Session::has('message'))
    {{Session::get('message')}}
    @endif
    @if(Session::has('error'))
    {{Session::get('error')}}
    @endif
    @if($errors->has())

    @foreach ($errors->all() as $error)

    <div>{{ $error }}</div>

    @endforeach

    @endif
</div>
<div style='clear:both'/>
<hr/>