<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
    @foreach($categories as $cat)
    @if($cat->getChildren()->count() > 0)
    <li class="dropdown-submenu">
        <span style="cursor:pointer">{{ $cat->name }}</span>
        <ul class="dropdown-menu">
            @foreach($cat->getChildren() as $level_one)
            <li><span style="cursor:pointer" onclick="onCategorySelect({{$level_one->id}},'{{ $level_one->name }}')">{{ $level_one->name }}</span></li>
            @endforeach
        </ul>
    </li>
    @else
    <li><span style="cursor:pointer">{{ $cat->name }}</span></li>

    @endif
    @endforeach
</ul>