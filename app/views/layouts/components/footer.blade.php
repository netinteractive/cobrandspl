<div id='footer'>
    <div id='description_footer' style='width:33%;border-right: solid 1px;float:left'>
        Opis<br/>
        <a href='#' />Regulamin</a>
        <a href='#' />Kontakt</a>
    </div>
    <div id='newsletter_footer' style='width:33%; float:left;border-right: solid 1px;'>
        Newsletter<br/>
        {{ Form::open(array('url'=>route('newsletterSubscribeAction'))) }}
        <input type='email' placeholder='Wpisz e-mail' name="newsletter_email" id="newsletter_email" required/>
        <span class="register-error">{{ $errors->first('newsletter_email')}}</span>
        <input type='submit' value='Zapisz się' />
        {{ Form::close()}}
        {{link_to_route('registerProducerView','Rejestracja jako Producent')}}
    </div>
    <div id='facebook_footer' style='width:auto'>
        Facebook
    </div>
</div>
<div style='clear:both'/>
<hr/>