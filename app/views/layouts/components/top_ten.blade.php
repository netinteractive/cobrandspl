<div style='border:solid 1px;margin-bottom: 5px;display: inline-block' id="top_ten">
    <h3>AKTYWNI UŻYTKOWNICY (Top10)</h3>
    <ul>
        @foreach(UserFacade::retrieveTopTen() as $obj)
        <li><a href='partner/{{ $obj->user_id }}'>{{$obj->link}}</a></li>
        @endforeach
    </ul>
    {{ link_to_route('listPartners','Zobacz wszystkie firmy>') }}
</div>
<div style='clear:both' />

