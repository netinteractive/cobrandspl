<label for="action_subcategory_selector">Podkategoria:</label>
<select id="action_subcategory_selector" name="action_subcategory_id">
    @if($mainActionCategoryId == 0)
        <option value="">------</option>
    @else
        @if($subcategoryId != null)
        @foreach(CategoryFacade::getSubCategories($mainActionCategoryId) as $cat)
            <option value="{{ $cat->id }}" <?php if($cat->id == $subcategoryId) echo "selected"; ?>>{{ $cat->name }}</option>
        @endforeach
        @else
         <option value="0">Wszystkie</option>
        @foreach(CategoryFacade::getSubCategories($mainActionCategoryId) as $cat)
            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
        @endforeach
        @endif
    @endif
</select>
