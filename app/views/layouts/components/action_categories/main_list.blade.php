<select onchange="reloadActionSubCategoriesList()" id="action_category_selector" name="action_category_id">
    @if($category_id != null)
    @foreach(CategoryFacade::getMainCategories() as $mainCategory)
    <option value="{{ $mainCategory->id }}" <?php if($category_id == $mainCategory->id) echo "selected"; ?>>{{ $mainCategory->name }}</option>
    @endforeach
    @else
    <option value="0">Wszystkie</option>
    @foreach(CategoryFacade::getMainCategories() as $mainCategory)
    <option value="{{ $mainCategory->id }}">{{ $mainCategory->name }}</option>
    @endforeach
    @endif
</select>
