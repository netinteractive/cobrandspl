<div id='add_recommendation_modal' style="display: none">
    {{ Form::open(array('url'=>route('addRecommendation'))) }}
    <input type="hidden" id="action_id" name="action_id" />
    <h3>Dodaj rekomendację</h3>
    <textarea style="width:500px;height: 150px;resize: none;" id='recommendation_content' name="recommendation_content">{{ Input::old('recommendation_content') }}</textarea>
    <br/>
    <span class="register-error">{{ $errors->first('recommendation_content')}}</span>
    <br/>
    <input type="submit" value="Dodaj" min="10" max="3000"/>
    {{ Form::close() }}
</div>

