<div style='border:solid 1px;margin-bottom: 5px;display: block; width:600px'>
    {{ $recom->action->getNameFormatted() }}<br/>
    Dodano {{ $recom->getFormattedCreatedDate() }}<br/>
    {{ $recom->content }}<br/>
    Dodano przez: {{ $recom->addedUser->name }} {{ $recom->addedUser->surname }}<br/>
</div>
@if(AbuseFacade::isAbuseAllowedForRecommendation($recom->id))
<input type="button" value="Zgłoś nadużycie" onclick="showAddAbuseModal({{ $recom->id }})"/>
@endif