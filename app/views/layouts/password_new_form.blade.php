@extends('layouts.master')

@section('title')
{{Lang::get('msg.login.page_title')}}
@stop

@section('content')
{{ Form::open(array('url'=>route('resetPasswordAction'))) }}
<input type="hidden" name="password_reset_token" value="{{ $password_reset_token }}">
<span class="register-error">{{ $errors->first('password_reset_token')}}</span>
<table>
    <caption><h3>Zmiana hasła</h3></caption>

    <tbody>
        <tr>
            <td>Twój adres e-mail:</td>
            <td>
                <input value="" class="form-control" type="email" name="reset_email" id="reset_email" max="45" min="4" required>
            </td>
            <td class="register-error">{{ $errors->first('reset_email')}}</td>
        </tr>
        <tr>
            <td>Hasło:</td>
            <td>
                <input value="" class="form-control" type="password" name="reset_password" id="reset_password" max="45" min="4" required>
            </td>
            <td class="register-error">{{ $errors->first('reset_password')}}</td>
        </tr>
        <tr>
            <td>Potwierdzenie hasła:</td>
            <td>
                <input value="" class="form-control" type="password" name="reset_password_confirmation" id="reset_password_confirmation" max="45" min="4" required>
            </td>
            <td class="register-error">{{ $errors->first('reset_password_confirmation')}}</td>
        </tr>


        <tr>
            <td></td>
            <td>{{ Form::submit('Zatwierdź',array())}}</td>
        </tr>


    </tbody>
</table>
{{ Form::close()}}
@stop