<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
    <head>
        <meta name="_token" content="<?= csrf_token() ?>"/>
        <title>@yield('title')</title>
        <link href="{{URL::asset('global/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet"/>
        <link href="{{URL::asset('css/datepicker3.css')}}" rel="stylesheet"/>
        <link href="{{URL::asset('css/styles.css')}}" rel="stylesheet"/>
        @yield('stylesheets')
        <script type="text/javascript" src="{{URL::asset('js/jquery-2.1.1.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('global/plugins/bootstrap/js/bootstrap.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/bootstrap-datepicker.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/scripts.js')}}"></script>
        <script type="text/javascript">
            $(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    }
                });
                
                $('.datepicker').datepicker({
                    format: 'yyyy-mm-dd',
                })
            });
        </script>
        @yield('scripts')
    </head>
    <body>
        <div id='page' style='width:800px;margin: 0 auto;'>
            @include('layouts.components.header')

            @include('layouts.components.subheader')

            @include('layouts.components.info_error_area')

            @include('layouts.components.content')

            @include('layouts.banners.horizontal')

            @include('layouts.components.footer')

        </div>
    </body>
</html>