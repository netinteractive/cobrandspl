@extends('layouts.master')

@section('title')
{{Lang::get('msg.login.page_title')}}
@stop

@section('content')
Dziękujemy za rejestrację. Prosimy o potwierdzenie rejestracji poprzez kliknięcie w link aktywacji w Państwa mailu.
@stop