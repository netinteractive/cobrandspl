@extends('layouts.master')

@section('scripts')
<script type="text/javascript" src="{{URL::asset('global/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_product_modal.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_add_brand_modal.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/scripts_register.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/vendor/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.iframe-transport.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/bootbox.min.js')}}"></script>
<script>
$(function() {
    $('#avatarupload').fileupload({
        dataType: 'json',
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                document.getElementById("avatar_upload_error").innerHTML = "Dozwolone są tylko pliki graficzne gif, jpeg oraz png.";
                goUpload = false;
            }
            if (uploadFile.size > 2000000) { // 2mb
                document.getElementById("avatar_upload_error").innerHTML = "Maksymalny rozmiar pliku to 2MB";
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }
        },
        done: function(e, data) {
            if (data.result.error) {
                document.getElementById("avatar_upload_error").innerHTML = data.result.error;
            } else if (data.result.success) {
                document.getElementById("avatar_placeholder").innerHTML = '<img src=\'' + data.result.avatar_url + '\' />';
                document.getElementById("avatar_id").value = data.result.avatar_id;
            } else {
                document.getElementById("avatar_upload_error").innerHTML = 'Wystąpił nieznany błąd';
            }
        }
    });
    
    tinymce.init({
        selector: "#register_description",
        toolbar: "link | image",
        language: "pl",
        menubar: false,
        statusbar: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality template textcolor paste textcolor"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright | link unlink image | forecolor | fontsizeselect",
                relative_urls: false,
    });
});
</script>
@stop

@section('title')
{{Lang::get('msg.register.page_title')}}
@stop

@section('content')
<div id="avatar_placeholder" style="width:150px;height:150px;border:solid 1px;">
    @if(Session::has('avatar_url'))
    <img src='{{Session::get('avatar_url')}}' />
    @endif
</div>
<input id="avatarupload" type="file" name="avatarfile" data-url="{{ route('uploadAvatar') }}"/>
<span id="avatar_upload_error" class="register-error"></span>
@if($producer)
{{ Form::open(array('url'=>route('registerProducerAction'))) }}
@else
{{ Form::open(array('url'=>route('registerUserAction'))) }}
@endif

<input type="hidden" id="avatar_id" name="avatar_id" value="{{ Input::old('avatar_id') }}"/>
<h2>{{Lang::get('msg.register.company_data_header')}}</h2>
<label for="register_nip">Wpisz NIP aby zaciągnąc potrzebne dane(przykład 1234567890)</label>
<br/>
<label for="register_nip">{{Lang::get('msg.register.nip_label')}}</label>
<input type="text" min="10" max="10" id="register_nip" name="register_nip" value="{{ Input::old('register_nip') }}" required/>
<br/>
<img src="{{route('captchaNip')}}" id="captcha_image">Kod z obrazka: <input type="text" name="captcha_input" id="captcha_input"><input type="button" value="Pobierz" onclick="loadCompanyDataByNip()"><br />
<span class="register-error" id="nip_error">{{ $errors->first('register_nip')}}</span>
<label for="register_companyname">{{Lang::get('msg.register.companyname_label')}}</label>
<input type="text" min="2" max="250" id="register_companyname" name="register_companyname" value="{{ Input::old('register_companyname') }}" required/>
<br/>
<span class="register-error">{{ $errors->first('register_companyname')}}</span>
<label for="register_regon">{{Lang::get('msg.register.regon_label')}}</label>
<input type="text" min="5" max="15" id="register_regon" name="register_regon" value="{{ Input::old('register_regon') }}" required/>
<br/>

<label for="register_street">{{Lang::get('msg.register.street_label')}}</label>
<input type="text" name="register_street" min="3" max="200" id="register_street" value="{{ Input::old('register_street') }}" required/>
<br/>
<span class="register-error">{{ $errors->first('register_street')}}</span>
<label for="register_postcode">{{Lang::get('msg.register.postcode_city_label')}}</label>
<input type="text" name="register_postcode" min="2" max="20" id="register_postcode" value="{{ Input::old('register_postcode') }}" required/>
<input type="text" name="register_city" min="2" max="200" id="register_city" placeholder="Miasto" value="{{ Input::old('register_city') }}" required/>
<br/>
<span class="register-error">{{ $errors->first('register_postcode')}}</span>
<span class="register-error">{{ $errors->first('register_city')}}</span>
<label for="register_district">{{Lang::get('msg.register.district_label')}}</label>
{{Form::select('register_district', $districts_list,Input::old('register_district'),array())}}
<br/>
<span class="register-error">{{ $errors->first('register_district')}}</span>
<br/>
<label for="register_name">{{Lang::get('msg.register.name_label')}}</label>
<input type="text" name="register_name" min="2" max="100" id="register_name" value="{{ Input::old('register_name') }}" placeholder="Imię i nazwisko" required/>
<br/>
<label for="register_email">{{Lang::get('msg.register.email_label')}}</label>
<input type="email" name="register_email" min="2" max="45" id="register_email" value="{{ Input::old('register_email') }}" required/>
<br/>
<span class="register-error">{{ $errors->first('register_email')}}</span>
<label for="register_phone_main">{{Lang::get('msg.register.phone_main_label')}}</label>
<input type="text" name="register_phone_main" min="2" max="45" id="register_phone_main" value="{{ Input::old('register_phone_main') }}" required/>
<br/>
<span class="register-error">{{ $errors->first('register_phone_main')}}</span>
<label for="register_phone_alternative">{{Lang::get('msg.register.phone_alternative_label')}}</label>
<input type="text" name="register_phone_alternative" min="2" max="45" id="register_phone_alternative" value="{{ Input::old('register_phone_alternative') }}" />
<br/>
<span class="register-error">{{$errors->first('register_phone_alternative')}}</span>
<label for="register_password">{{Lang::get('msg.register.password_label')}}</label>
<input type="password" name="register_password" min="2" max="45" id="register_password" value="" required/>
<br/>
<span class="register-error">{{ $errors->first('register_password')}}</span>
<label for="register_password_confirm">{{Lang::get('msg.register.password_confirm_label')}}</label>
<input type="password" name="register_password_confirm" min="2" max="45" id="register_password_confirm" value="" required/>
<br/>
<span class="register-error">{{ $errors->first('register_password_confirm')}}</span>
<br/>
<label for="register_companysize">{{Lang::get('msg.register.companysize_label')}}</label>
{{Form::select('register_companysize', $companysizes_list,Input::old('register_companysize'),array())}}
<br/>
<span class="register-error">{{ $errors->first('register_companysize')}}</span>
<br/>

<label for="register_description">Opis działalności firmy</label>
<textarea name="register_description" id="register_description" maxlength="3000">{{ Input::old('register_description') }}</textarea>
<br/>
<span class="register-error">{{ $errors->first('register_description')}}</span>
<br/>

@if($producer)
@include('layouts.register.add_product_modal')
<div id="products_holder">
    @include('layouts.register.products_list')
</div>
<span>Dodaj nowy produkt <input type="button" value="+" id="add_product_button" name="add_product_button" onclick="showAddProductModal('register')"/></span>
<br/>
<br/>
@else
@include('layouts.register.add_brand_modal')
<div id="brands_holder">
    @include('layouts.register.brands_list')
</div>
<span>Dodaj nowy brand <input type="button" value="+" id="add_brand_button" name="add_brand_button" onclick="showAddBrandModal('register')"/></span>
<br/>
<br/>
@endif

{{Form::checkbox('register_terms_accepted', 'on', Input::old('register_terms_accepted', false),array('required'=>'required'))}}
<label for="register_terms_accepted">{{Lang::get('msg.register.terms_accepted_label')}}</label>
<br/>
{{Form::checkbox('register_newsletter_accepted', 'on', Input::old('register_newsletter_accepted', false))}}
<label for="register_newsletter_accepted">{{Lang::get('msg.register.newsletter_label')}}</label>
<br/>
<input type="submit" value="{{Lang::get('msg.register.register_button')}}" />
{{ Form::close();}}
@stop

