<div id="addProductModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <input type="hidden" id="product_add_type" value="" />
                <div id="product_image_placeholder" class="product-image-placeholder">
                    @if(Session::has('product_image_url'))
                    <img src='{{Session::get('product_image_url')}}' />
                    @endif
                </div>
                <input id="product_image_upload" type="file" name="product_image_file" data-url="{{ route('uploadProductImage') }}"/>
                <span id="product_image_upload_error" class="register-error"></span>
                <span class="register-error" id="product_image_error"></span>
                <input type="hidden" name="product_image_id" id="product_image_id" value="0"/>
                <label for="product_name">Nazwa produktu</label>
                <input type="text" name="product_name" maxlength="200" id="product_name" value=""/>
                <br/>
                <span class="register-error" id="product_name_error"></span>

                <div id="product_category_selectors">
                    @include('layouts.components.product_categories.category_selector')
                </div>
                <br/>
                <span class="register-error" id="product_category_error"></span>
                <label for="product_description">Opis produktu:</label>
                <textarea name="product_description" id="product_description" maxlength="3000"></textarea>
                <br/>
                <span class="register-error" id="product_description_error"></span>

                <div id="product_prices_holder">
                    <div id="product_price1_holder">
                        <label for="product_price1">Cena produktu</label>
                        <input style="width:70px" type="text" name="product_price1" maxlength="6" id="product_price1" value="" placeholder="PLN"/>
                        od:<input style="width:50px" type="text" name="product_price1_from" maxlength="4" id="product_price1_from" value="" placeholder="szt."/>
                        do:<input style="width:50px" type="text" name="product_price1_to" maxlength="4" id="product_price1_to" value="" placeholder="szt."/>
                        <br/>
                        <span class="register-error" id="product_price1_error"></span>
                    </div>
                    <div id="product_price2_holder" style="display:none">
                        <label for="product_price2">Cena produktu</label>
                        <input style="width:70px" type="text" name="product_price2" maxlength="6" id="product_price2" value="" placeholder="PLN"/>
                        od:<input style="width:50px" type="text" name="product_price2_from" maxlength="4" id="product_price2_from" value="" placeholder="szt."/>
                        do:<input style="width:50px" type="text" name="product_price2_to" maxlength="4" id="product_price2_to" value="" placeholder="szt."/>
                        <input type="button" value="usuń" onclick="removePrice(2)"/>
                        <br/>
                        <span class="register-error" id="product_price2_error"></span>
                    </div>
                    <div id="product_price3_holder" style="display:none">
                        <label for="product_price3">Cena produktu</label>
                        <input style="width:70px" type="text" name="product_price3" maxlength="6" id="product_price3" value="" placeholder="PLN"/>
                        od:<input style="width:50px" type="text" name="product_price3_from" maxlength="4" id="product_price3_from" value="" placeholder="szt."/>
                        do:<input style="width:50px" type="text" name="product_price3_to" maxlength="4" id="product_price3_to" value="" placeholder="szt."/>
                        <input type="button" value="usuń" onclick="removePrice(3)"/>
                        <br/>
                        <span class="register-error" id="product_price3_error"></span>
                    </div>
                    <div id="add_price_button_holder">
                        <input type="hidden" id="num_of_prices" name="num_of_prices" value="1" />
                        <input type="button" value="+" id="add_price_button" onclick="addNextPrice()"/><span id="addPriceLabel">Dodaj kolejną cenę</span>
                    </div>
                </div>

                <label for="product_count">Ilość produktów na stanie:</label>
                <input type="text" name="product_count" maxlength="9" id="product_count" value="" placeholder="szt."/>
                <br/>
                <span class="register-error" id="product_count_error"></span>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="$('#addProductModal').modal('hide')">Anuluj</button>
                <button type="button" class="btn btn-primary" onclick="addProduct()">Dodaj</button>
            </div>
        </div>
    </div>
</div>

